package misc.tracers;

import coloredtrails.client.ClientData;
import coloredtrails.client.ui.ChipSetRenderer;
import coloredtrails.client.ui.Icons;
import coloredtrails.client.ui.ListTable;
import coloredtrails.shared.types.ChipSet;
import coloredtrails.shared.types.HistoryEntry;
import coloredtrails.shared.types.HistoryLog;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by pbh on Aug 20, 2005 at 2:04:51 AM.
 */
public class ProposalListPanel extends JPanel implements Observer {

    private ChipsHistoryTable cht = null;

    private JTable chipstable = null;
    private int turn = 0;

    private final int UNFINISHED = 7;

    public ProposalListPanel() {
        super();

        cht = new ChipsHistoryTable();
        chipstable = new JTable(cht) {
            public TableCellRenderer getCellRenderer(int row, int column) {
                if (column == UNFINISHED) {
                    Class c = dataModel.getValueAt(row, column).getClass();
                    if (c.isAssignableFrom(String.class)) {
                        return new DefaultTableCellRenderer();
                    }
                }
                return super.getCellRenderer(row, column);
            }
        };
        chipstable.setDefaultRenderer(ChipSet.class,
                new ChipSetRenderer());

        JScrollPane jsp = getScrollPaneOfTable(chipstable,
                new Dimension(500, 300));

        JPanel chipmsgsp = new JPanel();
        chipmsgsp.setBackground(Color.WHITE);
        chipmsgsp.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createLineBorder(Color.black), "Chip Messages"));
        chipmsgsp.add(jsp);

        Box b = Box.createVerticalBox();

        b.add(chipmsgsp, BorderLayout.CENTER);

        add(b);
    }

    public int getSelectedMessageId() {
        return ((ProposalHistoryMessage) cht.data.get(
                chipstable.getSelectedRow())).getMessageId();
    }

    private JScrollPane getScrollPaneOfTable(JTable table, Dimension dm) {
        table.setPreferredScrollableViewportSize(dm);
        JScrollPane js = new JScrollPane(table);
        js.setViewportView(table);
        js.setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        return js;
    }

    public void update(Observable o, Object arg) {
        reset();
        setup();
        repaint();
    }

    public void reset() {
        cht.data.removeAllElements();
    }

    public void setup() {
        HistoryLog log = ClientData.getInstance().getGame().getHistoryLog();
        for (HistoryEntry he : log.getLoglist()) {
            if (he.getType().equals("proposal")) {
                System.err.println(
                        "fooBAR: " + he.getDataByValue("fromPerGameId"));
                System.err.println(
                        "BARfoo: " + he.getDataByValue("toPerGameId"));
                cht.data.add(
                        new ProposalHistoryMessage(he.getPhaseNum(),
                                (Integer) he.getDataByValue(
                                        "fromPerGameId"),
                                (Integer) he.getDataByValue("toPerGameId"),
                                new ChipSet((Hashtable) he.getDataByValue(
                                        "senderChips")),
                                new ChipSet((Hashtable) he.getDataByValue(
                                        "recipientChips")),
                                -1, "",
                                ((Integer) he.getDataByValue("messageId")).intValue()));
            }
        }
        cht.fireTableDataChanged();
    }

    class ChipsHistoryTable extends ListTable {

        public ChipsHistoryTable() {
            final String[] chipHistoryTitles = {"Phase #", "Sender",
                                                "Recipient",
                                                "Chips I Give",
                                                "Chips I Get",
                                                "Sends first",
                                                "Reply"};
            setColumnNames(chipHistoryTitles);
        }

        public boolean isCellEditable(int row, int col) {
            Object o = getValueAt(row, col);
            if (o == null) {
                return false;
            }
            Class c = o.getClass();

            return super.isCellEditable(row, col);
        }

        public Object getValueAt(int row, int col) {
            ProposalHistoryMessage phm =
                    (ProposalHistoryMessage) data.get(row);
            switch (col) {
                case 0:
                    return new Integer(phm.getPhaseNum());
                case 1:
                    return phm.getSenderIcon();
                case 2:
                    return phm.getRecipientIcon();
                case 3:
                    return phm.getChipsIGive();
                case 4:
                    return phm.getChipsIGet();
            }
            return "";
//
//            switch (col) {
//                case 0:
//                    return new Integer(chm.getTurn());
//                case 1:
//                    return chm.getTypeName();
//                case 2:
//                    return chm.getSenderIcon();
//                case 3:
//                    return chm.getRecipientIcon();
//                case 4:
//                    return chm.getChipsIGive();
//                case 5:
//                    return chm.getChipsIGet();
//                case 6:
//                    return chm.getFirstSender();
//                case 7:
//                    return chm.getAction();
//                default:
//                    return "";
//            }
        }
    }

    class ProposalHistoryMessage {

        private int phaseNum = -1;
        private int senderId = -1;
        private int recipientId = -1;
        private ChipSet chipsIGive = null;
        private ChipSet chipsIGet = null;
        private int sendsFirst = -1;
        private String reply = "";
        private int messageId = -1;

        public ProposalHistoryMessage(int phaseNum, int senderId, int recipientId,
                                      ChipSet chipsIGive, ChipSet chipsIGet, int sendsFirst,
                                      String reply, int messageId) {
            this.phaseNum = phaseNum;
            this.senderId = senderId;
            this.recipientId = recipientId;
            this.chipsIGive = chipsIGive;
            this.chipsIGet = chipsIGet;
            this.sendsFirst = sendsFirst;
            this.reply = reply;
            this.messageId = messageId;
        }

        public int getPhaseNum() {
            return phaseNum;
        }

        public int getSenderId() {
            return senderId;
        }

        public int getMessageId() {
            return messageId;
        }

        public ImageIcon getSenderIcon() {
            return Icons.getIconByPerGameId(senderId,
                    ClientData.getInstance().getPerGameId());
        }

        public int getRecipientId() {
            return recipientId;
        }

        public ImageIcon getRecipientIcon() {
            return Icons.getIconByPerGameId(recipientId,
                    ClientData.getInstance().getPerGameId());
        }

        public ChipSet getChipsIGive() {
            return chipsIGive;
        }

        public ChipSet getChipsIGet() {
            return chipsIGet;
        }

        public int getSendsFirst() {
            return sendsFirst;
        }

        public String getReply() {
            return reply;
        }

    }
}