//package edu.harvard.eecs.airg.coloredtrails.controller;

import edu.harvard.eecs.airg.coloredtrails.controller.ControlImpl;
import edu.harvard.eecs.airg.coloredtrails.controller.GameEndWatcher;
import edu.harvard.eecs.airg.coloredtrails.controller.Round;
import edu.harvard.eecs.airg.coloredtrails.controller.RoundAllocatorAltOffers;
import edu.harvard.eecs.airg.coloredtrails.shared.types.*;

/**
 * @author greg
 * @author KPozin, kobi
 * 
 */
public class AltOffersControl {

	/**
	 * NOTE: To get this to work the various experimental conditions, change the
	 * value passed in line 62 to the name of the config file you want to use.
	 */
	public static void main(String[] args) {

		System.out.println("Code Controller starting");
		int playersPerGame = 2;
		// The number of games being played at a time
		int numSimultGames;
		// The number of rounds
		int numRounds = 2;

		AltOffersConfigArguments configArgs = null;
		if (args.length > 0)
			configArgs = new AltOffersConfigArguments(args[0], args[1]);
		else {
			System.out.println("type arguments");
			System.exit(1);
		}

		// Initialize controller, IP and port must be specified
		ControlImpl controlImpl = new ControlImpl("tcp://127.0.0.1:8200");

		RoundAllocatorAltOffers ra = new RoundAllocatorAltOffers(controlImpl,
				"gameconfigs", "AlternativeOffersUltimatumConfig", false,
				playersPerGame, configArgs);

		// Run all the rounds, waiting for each one to fully complete
		for (int roundNum = 0; roundNum < numRounds; ++roundNum) {
			Round curRound = ra.getNewRound();
			System.out.println("Beginning Round " + roundNum);

			// Start the round's games
			numSimultGames = curRound.size();
			for (int g = 0; g < numSimultGames; ++g) {
				System.out.println("Trying to start game " + g);
				curRound.get(g).Start();
			}

			// Wait for this round to end before beginning the next
			GameEndWatcher gew = new GameEndWatcher(curRound);
			gew.waitForGamesToEnd();
			System.out.println("Ending Round " + roundNum + "!");
		}

	}

}
