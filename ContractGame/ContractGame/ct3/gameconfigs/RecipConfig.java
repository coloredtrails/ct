/*
	Colored Trails

	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

//import RecipExperiment.RecipConstants;
import RecipExperiment.RecipConstants;
import edu.harvard.eecs.airg.coloredtrails.server.ServerData;
import edu.harvard.eecs.airg.coloredtrails.server.ServerPhases;
import edu.harvard.eecs.airg.coloredtrails.shared.PlayerConnection;
import edu.harvard.eecs.airg.coloredtrails.shared.ScoringUtility;
import edu.harvard.eecs.airg.coloredtrails.shared.Constants;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.*;
import edu.harvard.eecs.airg.coloredtrails.shared.logger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.net.SocketAppender;

/**
 * This configuration implements much of the setup and functionality.
 * Including automatic exchange after accept an offer, and automatic movement.
 * 
 * More importantly, it showcases "rounds" and many of the new features of CT.
 * Rounds are not a native feature of CT, but are easy to implement in any sort of style.
 * This config file receives game data from the controller. As such, it is intimately tied to the its associated
 * controller and must be launched by the setuprecip controller.
 * 
 * This config also demonstrates getting input from the GUI (the mood selector) and the arbitrary messaging functionality.
 * 
 */
public class RecipConfig extends GameConfigDetailsRunnable implements PhaseChangeHandler
{
    static int messageId = 0;
    
    Logger logr;
    
    logger logbob;
    
    //used for switching roles.
    int	ProposerID	=	0;
    int	ResponderID	=	1;

    //Number of rounds to play, each round we switch roles
    int		NumRounds = RecipExperiment.RecipConstants.numRounds;
    int		Round	= 0;

    int numRows = 4;
    int numCols = 4;

    String CurrentPhaseName = null;
    boolean ended = false;

    ScoringUtility SU;
    
    /**
    * The scoring function used for players in the game.
    * 100 for a player reaching the goal,
    * -10 per unit distance if the player does not reach the goal,
    * 5 for each chip remaining after the player has reached the goal
    * or cannot move any farther towards teh goal.
    */
    //Scoring s = new Scoring(100, -10, 5);

    /** Local random generator for creating chipsets */
    static Random localrand = new Random();

    /** determines if there will be automatic movement */
    //boolean automaticMovement = true;
    /** determines if the chips will automatically transfer after a
     * proposal has been accepted */
    boolean automaticChipTransfer = true;
    /** determines if the phases will loop. */
    boolean phaseLoop = false;

    boolean offerMade = false;
    boolean offerResponded = false;
    boolean autoOffer = false;
    boolean autoResponse = false;
    BasicProposalDiscourseMessage offerMessage = null;

    /**
            Returns score of specified player, according to player's current state
    */
    public int getPlayerScore(PlayerStatus ps)
    {
        RowCol gpos = gs.getBoard().getGoalLocations().get(0);  // get first goal in list
        return (int)Math.floor(gs.getScoring().score(ps, gpos));  // should change to double at some point
    }


    /**
            Called by GameConfigDetailsRunnable methods when calculation and assignment
            of player scores is desired
    */
    protected void assignScores() {
        ServerData sd = ServerData.getInstance();
        PlayerConnection pc;
        double score;
        for( PlayerStatus ps : gs.getPlayers() ) {
            score = getPlayerScore(ps);
            ps.setScore((int) score);
            System.out.println("Player: " + ps.getPin() + "  Score: " + ps.getScore());
            pc = sd.getPlayerConnection(ps.getPin());
            pc.setScore(pc.getScore() + score);
        }
    }


    /**
            Called by server when a phase begins
    */
    public void beginPhase(String phasename) {
        logr.info("A New Phase Began: " + phasename);
        
                
        CurrentPhaseName = phasename;

        boolean ProposerCommunicationAllowed = false;
        boolean ProposerTransfersAllowed     = false;
        boolean ProposerMovesAllowed         = false;
        boolean ResponderCommunicationAllowed = false;
        boolean ResponderTransfersAllowed     = false;
        boolean ResponderMovesAllowed         = false;

        
        logr.info("Starting beginphase");
        
        if(phasename.equals("Offer Phase")) {
            
            logr.info("----------------------------------------- Round: " + Round + " -------------------------------------------------");
            ProposerCommunicationAllowed = true;
            logbob.log(String.valueOf(Round), "newround");
            logbob.log(phasename, "newphase");
            
        } else if( phasename.equals("Response Phase") ) {
            
//            ResponderCommunicationAllowed = true;
            
            logbob.log(String.valueOf(Round), "newround");
            logbob.log(phasename, "newphase");
            
        } else if( phasename.equals("Movement Phase") ) {
            
            logbob.log(String.valueOf(Round), "newround");
            logbob.log(phasename, "newphase");
            
            doAutomaticMovement( gs.getScoring() );

            // calculate scores after all players have moved
            // (e.g, in case a player's score depends on others' locations)
            assignScores();

            
        }else if(phasename.equals("Feedback Phase")) {
            
            logbob.log(phasename, "newphase");
            if(Round >= (NumRounds - 1)){
                System.out.println("Ending game");
                ((ServerPhases)gs.getPhases()).setLoop(false);
                System.out.println("Loop status: " + gs.getPhases().getIsLoop());

                //gs.setEnded();
                ended = true;
                return;
            } else {
                Round++;
            }
        } 

        PlayerStatus Responder = gs.getPlayerByPerGameId(ResponderID);
        PlayerStatus Proposer  = gs.getPlayerByPerGameId(ProposerID);

        Responder.setCommunicationAllowed(ResponderCommunicationAllowed);
        Responder.setTransfersAllowed(ResponderTransfersAllowed);
        Responder.setMovesAllowed(ResponderMovesAllowed);

        Proposer.setCommunicationAllowed(ProposerCommunicationAllowed);
        Proposer.setTransfersAllowed(ProposerTransfersAllowed);
        Proposer.setMovesAllowed(ProposerMovesAllowed);
        
        logr.info("ending beginphase");

    }


    /**
     *	Called by server when a phase ends
     */
    public void endPhase(String phasename) {
        if(ended){
            gs.sendArbitraryMessage("NEWROUND");
            gs.setEnded();
            logbob.close();
            logr.info("Game Over");
//            ServerData.getInstance().getClientCommands(gs.getGameId()).getClientAction("statusmessage", "Game Over!");
            return;
        }
        logr.info("A Phase Ended: " + phasename);

        if( phasename.equals( "Offer Phase") ){
            
            
            if(!offerMade){
                
                logr.info( "No offer was made, so making a null offer" );
                
                BasicProposalDiscourseMessage dm = new BasicProposalDiscourseMessage(ProposerID, ResponderID, 
                        messageId, new ChipSet(), new ChipSet() );
                messageId++;
                
                autoOffer = true;
                logbob.log( new Boolean(true) , "autooffer");
                gs.sendArbitraryMessage( Constants.AUTOOFFER + dm.getMessageId() + "-" + dm.getResponderID() );
                
                
                
                doDiscourse(dm);
            }
            
        } else if( phasename.equals( "Response Phase" ) ) {
            
            if(!offerResponded){
                
                logr.info( "No reponse to the offer, so rejecting the offer" );
                
                //Need to reject the offer
                                
                autoResponse = true;
                logbob.log( new Boolean(true) , "autorejectresponse");
                gs.sendArbitraryMessage( Constants.AUTORESPONSE );
                
                BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(offerMessage);
                response.rejectOffer();
                
                doDiscourse(response);
            
            }
            
        } else if( phasename.equals( "Movement Phase" ) ) {
            
        } else if( phasename.equals("Feedback Phase") ) {
        
            
            // if end of feedback phase, then end the game
            setupgame(Round);
            gs.sendArbitraryMessage("NEWROUND");
            gs.sendArbitraryMessage(Constants.NEWROUNDDIALOG);
//            ServerData.getInstance().getClientCommands(gs.getGameId()).getClientAction("statusmessage", "A new round is about to start!");
        }
    }

	// Override super method do discourse in order to make an automatic transfer after accept a proposal
    @Override
    public boolean doDiscourse(DiscourseMessage dm) {
        logr.info( "Received Discourse Message " );
        logr.info( "Class: " + dm.getClass() );
        logr.info( "Id: " + dm.getMessageId() );
        logr.info( "Type: " + dm.getMsgType() );
        logr.info( "From: " + dm.getFromPerGameId() );
        logr.info( "To: " + dm.getToPerGameId() );
        
        ChipSet offer = null;

        //Here we compare chips propsed with proposer's and recipient's chipset
        //first check type of message
        
        if( dm instanceof BasicProposalDiscussionDiscourseMessage ) {
            
                //We have received a response offer
            logbob.log(dm, "responsemessage" + Round);
            boolean result = gs.doDiscourse(dm);
            
            offerResponded = true;
            offer = ChipSet.subChipSets(((BasicProposalDiscourseMessage)dm).getChipsSentByResponder(), ((BasicProposalDiscourseMessage)dm).getChipsSentByProposer());

            System.out.println( "---- BasicProposalDiscussionDiscourseMessage ----" );

            BasicProposalDiscussionDiscourseMessage bpddm = (BasicProposalDiscussionDiscourseMessage) dm;
            
            logbob.log( bpddm.accepted() , "offerresponse" + Round);
            
            //Now to log the scores from the offer
            double offerP = SU.getOfferScore(offer, ProposerID);
            double offerR = SU.getOfferScore(offer, ResponderID);
            
            logbob.log(new Double(offerP), "offerP");
            logbob.log(new Double(offerR), "offerR");

            if( automaticChipTransfer && bpddm.accepted() )
            {
                System.out.println("&&&&&&&&&&&&& We have an accepted offer, chips before: &&&&&&&&&&&&&&&&&&");
                for( PlayerStatus player : gs.getPlayers() ) {
                    System.out.println("player pin: " +  player.getPin());
                    System.out.println("player chips: " + player.getChips());
                }
                
                doAutomaticChipTransfer( bpddm );
                
                System.out.println("&&&&&&&&&&&&&& Chips After &&&&&&&&&&&&&&&&&");
                for( PlayerStatus player : gs.getPlayers() ) {
                    System.out.println("player pin: " +  player.getPin());
                    System.out.println("player chips: " + player.getChips());
                }
                //System.out.println("Offer accepted");
            } else {
                System.out.println("Offer rejected");
                
            }
   
            //No matter what the response is to the offer, advance the phase
            if(CurrentPhaseName.equals("Response Phase") && !autoResponse ){
                
                ((ServerPhases)gs.getPhases()).advancePhase();
            } else if( autoResponse ) {
                //not so much a worry
            } else {
                logr.error("Received response outside of Response Phase!");
            }
            
            return(result);
            
        } else if ( (dm instanceof BasicProposalDiscourseMessage) ){
            //We have an offer
            System.out.println("We have an offer");
            logbob.log(dm, "offermessage" + Round);
            
            offerMessage = (BasicProposalDiscourseMessage) dm;
            offerMade = true;
            
            ChipSet ChipsSentByResponder = ((BasicProposalDiscourseMessage)dm).getChipsSentByResponder();
            ChipSet ChipsSentByProposer = ((BasicProposalDiscourseMessage)dm).getChipsSentByProposer();
            ChipSet ResponderChips = gs.getPlayerByPerGameId(ResponderID).getChips();
            ChipSet ProposerChips = gs.getPlayerByPerGameId(ProposerID).getChips();
            
            boolean validoffer = (ResponderChips.contains(ChipsSentByResponder)) && (ProposerChips.contains(ChipsSentByProposer));
            System.out.println("Result of chip comparison: " + validoffer);
            if(!validoffer)
                return(false);
            
            offer = ChipSet.subChipSets(((BasicProposalDiscourseMessage)dm).getChipsSentByResponder(), ((BasicProposalDiscourseMessage)dm).getChipsSentByProposer());
            logbob.log(offer, "offer");
            
            if( CurrentPhaseName.equals("Offer Phase") && !autoOffer )
                    ((ServerPhases)gs.getPhases()).advancePhase();
            else if(!autoOffer)
                logr.error("Received offer outside of Offer Phase!");
            
            boolean result = gs.doDiscourse(dm);
            
            return(result);
//            if(!result){
////                The offer is not valid, automatically reject the offer.
//                ((BasicProposalDiscussionDiscourseMessage)dm).rejectOffer();
//                if( dm instanceof BasicProposalDiscussionDiscourseMessage )
//                    System.out.println("Basic proposal discussion discourse message");
//                return(result);
//            }


        }
        
        return(true);
    }

    /**
            Start a new game and set up all of the game specifications.
    */
    ArrayList<GameStatus> game;
    public void run() {
        
        String filename = "CTRecipLog-";
        
        for(PlayerStatus ps : gs.getPlayers()){
            filename = filename.concat(ps.getPin() + "-");
        }
        filename = filename.substring(0, filename.length() - 1 );
        
        logbob = new logger(filename + ".log");
        
        System.out.println("Let the game begin...");

        System.out.println("Our game id= " + gs.getGameId());
        
        
        if( (new Random()).nextBoolean() ){
            ProposerID = 1;
            ResponderID = 0;
        } else {
            ProposerID = 0;
            ResponderID = 1;
        }
        
        
        /**
         * Here we give the client a commmand, and block until all clients have responded. The getClientCommands function
         * returns a hash of the pergameIds and client responses (if any)
         */
        HashMap returndata = ServerData.getInstance().getClientCommands(gs.getGameId()).getClientAction("showmoodpanel", null);
        for(Object k : returndata.keySet()){
            Object o = returndata.get(k);
            System.out.println(" returned data for " + k.toString() + "     " + o.toString() + "    " + (String)o);
            logbob.log((Serializable)o, "moodresponse" + k);
        }
        
        
        //This makes use of the data passed by the controller 
        game = (ArrayList<GameStatus>)gs.getDataFromController();
        
        for(PlayerStatus ps : gs.getPlayers() ){
            for(PlayerConnection pc : ServerData.getInstance().getPlayers()){
                if(ps.getPin() == pc.getPin())
                    ps.set("pc", pc);
            }
        }
        
        gs.setGamePalette(game.get(0).getGamePalette());
        gs.setScoring( game.get(0).getScoring() );


        // set up phase sequence
        ServerPhases ph = new ServerPhases(this);
        //indefinite, depend on deal being made to cause phase transition.
        //ph.addPhase("Communication Phase", 99);
        //ph.addPhase("Exchange Phase", 5);
        ph.addPhase("Offer Phase", 180);
        ph.addPhase("Response Phase", 90);
        ph.addPhase("Movement Phase", 5);
        ph.addPhase("Feedback Phase", 5);

        
        
        logbob.log((Serializable) ServerData.getInstance().getPlayers(), "playerconnections");
        logbob.log((Serializable) gs.getPlayers(), "players");
        //logbob.log("teststring", "test");
        //logbob.close();
        
        
        String players = " ";
        for(PlayerStatus ps : gs.getPlayers())
            players = players.concat(ps.getPin() + " ");
        
        logr = Logger.getLogger(this.getClass().getName() +  players );
        SocketAppender appender = new SocketAppender(RecipConstants.LOG_SERVER, 4445);
        logr.addAppender(appender);
        logr.setLevel(Level.INFO);
        logr.log(Level.INFO, "Startup");
        
        for(PlayerStatus ps : gs.getPlayers() ){
            logr.info("Player:  pin: " + ps.getPin() + " pergameId: " + ps.getPerGameId() + " type: " + ((PlayerConnection) ps.get("pc")).getClientClassType() );
        }
        

        gs.getPlayerByPerGameId(ResponderID).setRole("Responder");
        gs.getPlayerByPerGameId(ProposerID).setRole("Proposer"); 


        setupgame(0);
        gs.sendArbitraryMessage("NEWROUND");

                
        ph.setLoop(true);
        gs.setPhases(ph);
        
        
        gs.setInitialized();  // will generate GAME_INITIALIZED message
        
    }


    private void setupgame(int i){
        print("In setupgame for round: " + i);
        
        //swap roles
        if(ProposerID == 0){
            ProposerID	=	1;
            ResponderID	=	0;
            logr.info("player 1 is Proposer, player 0 is Responder");
        } else {
            ProposerID	=	0;
            ResponderID	=	1;
            logr.info("player 0 is Proposer, player 1 is Responder");
        }
        
        gs.getPlayerByPerGameId(ResponderID).setRole("Responder");
        gs.getPlayerByPerGameId(ProposerID).setRole("Proposer");

        gs.getPlayerByPerGameId(0).setChips(game.get(i).getPlayerByPerGameId(0).getChips());
        gs.getPlayerByPerGameId(1).setChips(game.get(i).getPlayerByPerGameId(1).getChips());
        
        gs.getPlayerByPerGameId(0).setPosition(game.get(i).getPlayerByPerGameId(0).getPosition());
        gs.getPlayerByPerGameId(1).setPosition(game.get(i).getPlayerByPerGameId(1).getPosition());
        
        
        gs.setBoard(game.get(i).getBoard());
        System.out.println("Board: " + gs.getBoard().toString() );
        SU = new ScoringUtility(gs, ProposerID, ResponderID);
        
        double nnP = SU.getDefaultScore(ProposerID);
        double nnR = SU.getDefaultScore(ResponderID);
        
        
        logbob.log(new GameStatus(gs), "round" + i);
//        logbob.log(new GameStatus(gs, true), "round" + i);
        logbob.log(new Board(gs.getBoard()), "board" );
        
        logbob.log(new Double(nnP), "nnP" + i);
        logbob.log(new Double(nnR), "nnR" + i);
        
        offerMade = false;
        offerResponded = false;
        autoOffer = false;
        autoResponse = false;
    }
    
    private void print(String s){
        System.out.println("(Game " + gs.getGameId() + ") " + s);
    }

        
    @Override
    public void requestGameEnd(){
        print("requested to end game nicely");
        Round = NumRounds;
    }
}


