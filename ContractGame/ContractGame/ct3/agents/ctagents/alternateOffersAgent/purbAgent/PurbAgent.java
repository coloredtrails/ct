/*
	Colored Trails

	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package ctagents.alternateOffersAgent.purbAgent;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.client.ClientGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;

import ctagents.FileLogger;
import ctagents.alternateOffersAgent.LearningHandler;
import ctagents.alternateOffersAgent.Negotiator;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.alternateOffersAgent.ProposerResponderPlayer;

/**
 * A class that extends the abstract class SimplePlayer for agents in the
 * responder role. In the Communication Phase, the responder agent waits for a
 * proposal, and when received, first checks if the proposal is feasible. If
 * not, rejects it. Then, checks if the proposal is beneficial, in this version
 * it means being able to reach the goal with the ChipSet that the agent has
 * after the exchange. If it's beneficial, accepts the offer. In the Exchange
 * Phase, if there's an offer accepted, sends the chips that are asked for In
 * the Movement Phase, if there's an offer accepted, does the movement
 * 
 * @author Yael Ejgenberg
 */
public class PurbAgent extends ProposerResponderPlayer {

	private Personality myPersonality;
	private Personality opponentPersonality;
	private PurbLearningHandler learnOpponent;
	protected Negotiator negotiator;

	// After 3 iterations we should be pretty sure of our appreciation of
	// the opponent's personality. To measure our security we divide the number
	// of iterations that passed by MIN_SECURITY_ITERATIONS, for example in the
	// first iteration, our security measurement of the opp. personality is only
	// 1/2, after the second is 2/3 and after the 3 is 1, the more iterations
	// have passed the more sure we are that we understand our opp.
	protected static final int MIN_SECURITY_ITERATIONS = 3;

	public PurbAgent(double coopLevel, double reliabLevel) {
		super();

		myPersonality = new Personality(coopLevel, reliabLevel);
		opponentPersonality = new Personality(0.5, 0.5);
		negotiator = new Negotiator(client, myPersonality, scoring);
		learnOpponent = new PurbLearningHandler(opponentPersonality);
	}

	protected void updatePersonality() {
		try {
			opponentPersonality.setReliabLevel(learnOpponent
					.getReliability(numOfIterations));
			opponentPersonality.setCoopLevel(learnOpponent.getCooperation(
					numOfIterations, numOfOffers));
		} catch (NullPointerException e) {
			FileLogger.getInstance(logName).writeln(
					"NullPointerException:  " + e.getMessage());
		} catch (RuntimeException r) {
			FileLogger.getInstance(logName).writeln(
					"RuntimeException: " + r.getMessage());
		}
		FileLogger.getInstance(logName).writeln(
				"In movement phase, op's reliab level is: "
						+ opponentPersonality.getReliabLevel());
		FileLogger.getInstance(logName).writeln(
				"In movement phase, op's coop level is: "
						+ opponentPersonality.getCoopLevel());
	}

	protected void doExchange() {
		if (!offerAccepted) {
			FileLogger
					.getInstance(logName)
					.writeln(
							"In exchange phase, offer not accepted --> not sending anything");
		} else {
			if (learnOpponent.isFirstAgreement()) {
				FileLogger
						.getInstance(logName)
						.writeln(
								"In exchange phase, offer accepted.This is the first agreement, sending the chips as agreed: "
										+ sending);
				client.communication.sendTransferRequest(getOpponent()
						.getPerGameId(), sending);
			} else if (myPersonality.isLowReliabPersonality())
				FileLogger
						.getInstance(logName)
						.writeln(
								"In exchange phase, offer accepted, but I am unreliable so I don't send anything");
			else if (myPersonality.isHighReliabPersonality()) {
				FileLogger.getInstance(logName).writeln(
						"1-In exchange phase, offer accepted. Sending the chips as agreed: "
								+ sending);
				client.communication.sendTransferRequest(getOpponent()
						.getPerGameId(), sending);
			} else {// My personality is medium reliable:
				if (opponentPersonality.isLowReliabPersonality())
					FileLogger
							.getInstance(logName)
							.writeln(
									"In exchange phase, offer accepted, but opponent is unreliable so I don't send anything");
				else if (opponentPersonality.isHighReliabPersonality()) {
					FileLogger.getInstance(logName).writeln(
							"2-In exchange phase, offer accepted. Sending the chips as agreed: "
									+ sending);
					client.communication.sendTransferRequest(getOpponent()
							.getPerGameId(), sending);
				} else { // opponent is also medium reliable:
					ClientGameStatus cgs = client.getGameStatus();
					PlayerStatus myPlayer = cgs.getMyPlayer();
					PlayerStatus opPlayer = getOpponent();
					// If opponent is TaskDependant (TD), that is, there isn't a
					// satisfiable path for it right now, and I'm either TD or
					// TI calculate chips for me after exchange and chips for
					// him after exchange:
					if (!negotiator.isThereSatisfiablePath(opPlayer)) {
						// chips for opponent after exchange:
						ChipSet opChips = opPlayer.getChips();
						ChipSet opPossibleFinalChipSet = ChipSet.subChipSets(
								opChips, opChipSetCommitted);
						opPossibleFinalChipSet = ChipSet.addChipSets(
								opPossibleFinalChipSet, sending);
						PlayerStatus newOpPlayer = (PlayerStatus) opPlayer
								.clone();
						newOpPlayer.setChips(opPossibleFinalChipSet);
						// chips for me after exchange:
						ChipSet myChips = myPlayer.getChips();
						ChipSet myPossibleFinalChipSet = ChipSet.subChipSets(
								myChips, sending);
						myPossibleFinalChipSet = ChipSet.addChipSets(
								myPossibleFinalChipSet, opChipSetCommitted);
						PlayerStatus newMyPlayer = (PlayerStatus) myPlayer
								.clone();
						newMyPlayer.setChips(myPossibleFinalChipSet);
						// After exchange I'm still TD but opponent becomes TI:
						// TODO At the moment we assume that all the chips in
						// Sending are needed to become TI, and not that there
						// are spare chips.
						if (!negotiator.isThereSatisfiablePath(newMyPlayer)
								&& negotiator
										.isThereSatisfiablePath(newOpPlayer)) {
							FileLogger
									.getInstance(logName)
									.writeln(
											"After exchange I'm still TD but opponent becomes TI");
							FileLogger.getInstance(logName).writeln(
									"sending.getNumChips() = "
											+ sending.getNumChips());
							if (sending.getNumChips() == 1) {
								int maxToSend = sending.getNumChips() - 1;
								Random rand = new Random();
								int iSend = rand.nextInt(maxToSend) + 1;
								FileLogger.getInstance(logName).writeln(
										"number of chips to send is: " + iSend);
								Set<ChipSet> powerSet = ChipSet
										.getPowerSet(sending);
								for (ChipSet cs : powerSet) {
									if (cs.getNumChips() == iSend) {
										sending = new ChipSet(cs);
										break;
									}
								}
								FileLogger.getInstance(logName).writeln(
										"In exchange phase, offer accepted. Sending partial amount of agreed chips: "
												+ sending);
								client.communication.sendTransferRequest(
										getOpponent().getPerGameId(), sending);
							} else if (isGameAboutToEnd() == EndingReasons.OPP_HAS_TO_MOVE
									|| isGameAboutToEnd() == EndingReasons.BOTH_HAVE_TO_MOVE) {
								String colorChipToSend = "";
								for (String color : sending.getColors()) {
									if (sending.getNumChips(color) > 0) {
										colorChipToSend = color;
										break;
									}
								}
								ChipSet newSending = new ChipSet();
								newSending.add(colorChipToSend, 1);
								FileLogger
										.getInstance(logName)
										.writeln(
												"In exchange phase, offer accepted. I'm sending one chip needed by opp. so that it can move and game won't end "
														+ newSending);
								client.communication.sendTransferRequest(
										getOpponent().getPerGameId(),
										newSending);
							} else {
								// I can't send the chip that will make the
								// opponent TI because I'm still TD Instead try
								// sending another chip, so that I won't be
								// consider completely uncooperative but on the
								// other hand won't kill my chances.
								ChipSet requiredChipsForPath = chosenPath
										.getRequiredChips(cgs.getBoard());
								ChipSet myExtra = myChips
										.getExtraChips(requiredChipsForPath);
								String colorChipNotToSend = sending.getColors()
										.iterator().next();
								ChipSet newSending = new ChipSet();
								for (String color : myExtra.getColors()) {
									// This color is different from the onr that
									// makes my opp. TI, so I can send it to
									// him.
									if (color.compareTo(colorChipNotToSend) != 0) {
										newSending.add(color, 1);
										break;
									}
								}
								if (!newSending.isEmpty())
									FileLogger
											.getInstance(logName)
											.writeln(
													"In exchange phase, offer accepted but the sending set has only one chip that makes opp. TI. Sending other chip: "
															+ newSending);
								client.communication.sendTransferRequest(
										getOpponent().getPerGameId(),
										newSending);
							}
						} else if (negotiator
								.isThereSatisfiablePath(newMyPlayer)
								&& !negotiator
										.isThereSatisfiablePath(newOpPlayer)) {
							// If I'm convinced that the opponent is MM then do
							// the following
							double measurement = (double) (numOfIterations / MIN_SECURITY_ITERATIONS);
							FileLogger.getInstance(logName).writeln(
									"My security measure is: " + measurement);

							if (measurement > 0.8) {
								FileLogger.getInstance(logName).writeln(
										"8-In exchange phase, offer accepted. Sending the chips as agreed: "
												+ sending);
								client.communication.sendTransferRequest(
										getOpponent().getPerGameId(), sending);
							} else
							// else, my security measurement about the
							// opponent's personality is not high enough,
							// that is I am not 100% sure that the opponent is
							// MM, then send only a partial amount of agreement.
							{
								// Send only part of the chips with probability
								// = 2/3 and send all the chips with probability
								// = 1/3
								Random rand = new Random();
								int r = rand.nextInt(3);
								if ((r == 0) || (r == 1)) {
									int iSend = rand.nextInt(sending
											.getNumChips());
									FileLogger.getInstance(logName).writeln(
											"In exchange phase, Sending partial amount = "
													+ iSend);
									Set<ChipSet> powerSet = ChipSet
											.getPowerSet(sending);
									for (ChipSet cs : powerSet) {
										if (cs.getNumChips() == iSend) {
											sending = new ChipSet(cs);
											break;
										}
									}
									FileLogger.getInstance(logName).writeln(
											"In exchange phase, offer accepted. Sending partial amount of agreed chips: "
													+ sending);
									client.communication.sendTransferRequest(
											getOpponent().getPerGameId(),
											sending);

								} else {
									FileLogger.getInstance(logName).writeln(
											"9-In exchange phase, offer accepted. Sending the chips as agreed: "
													+ sending);
									client.communication.sendTransferRequest(
											getOpponent().getPerGameId(),
											sending);
								}

							}
						}
						// Both players become TI after the exchange
						else if (negotiator.isThereSatisfiablePath(newMyPlayer)
								&& negotiator
										.isThereSatisfiablePath(newOpPlayer)) {
							if (sending.getNumChips() == 1) {
								FileLogger.getInstance(logName).writeln(
										"4-In exchange phase, offer accepted. Sending the chips as agreed: "
												+ sending);
								client.communication.sendTransferRequest(
										getOpponent().getPerGameId(), sending);
							} else {// Send only part of the chips with
									// probability
								// = 1/3 and send all the chips with probability
								// = 2/3
								Random rand = new Random();
								int r = rand.nextInt(3);
								if (r == 0) {
									int maxToSend = sending.getNumChips() - 1;
									int iSend = rand.nextInt(maxToSend) + 1;
									Set<ChipSet> powerSet = ChipSet
											.getPowerSet(sending);
									for (ChipSet cs : powerSet) {
										if (cs.getNumChips() == iSend) {
											sending = new ChipSet(cs);
											break;
										}
									}
									FileLogger.getInstance(logName).writeln(
											"In exchange phase, offer accepted. Sending partial amount of agreed chips: "
													+ sending);
									client.communication.sendTransferRequest(
											getOpponent().getPerGameId(),
											sending);

								} else {
									FileLogger.getInstance(logName).writeln(
											"5-In exchange phase, offer accepted. Sending the chips as agreed: "
													+ sending);
									client.communication.sendTransferRequest(
											getOpponent().getPerGameId(),
											sending);
								}
							}

						} else {
							// After exchange we both continue to be TD. Now
							// decide if needs to send or not. In this case and
							// may be in other cases we need to check if by not
							// sending at least one chip that the opponent needs
							// we cause the game to end. When we are still TD
							// after the exchange it is clearly not in our
							// interest to cause the game to end.
							Random rand = new Random();
							int r = rand.nextInt(3);
							// Send with probability 2/3 a partial amount of
							// what was agreed and with probability 1/3 all what
							// was agreed.
							if (((r == 0) || (r == 1))
									&& (sending.getNumChips() > 1)) {
								int maxToSend = sending.getNumChips() - 1;
								int iSend = rand.nextInt(maxToSend) + 1;
								Set<ChipSet> powerSet = ChipSet
										.getPowerSet(sending);
								for (ChipSet cs : powerSet) {
									if (cs.getNumChips() == iSend) {
										sending = new ChipSet(cs);
										break;
									}
								}
								FileLogger
										.getInstance(logName)
										.writeln(
												"In exchange phase, offer accepted and we both stay TD after exchange. Sending partial amount of agreed chips with prob 2/3: "
														+ sending);
								client.communication.sendTransferRequest(
										getOpponent().getPerGameId(), sending);

							} else {
								FileLogger.getInstance(logName).writeln(
										"6-In exchange phase, offer accepted. Sending the chips as agreed: "
												+ sending);
								client.communication.sendTransferRequest(
										getOpponent().getPerGameId(), sending);
							}

						}
					} else { // I am TD and opponent is TI
						FileLogger.getInstance(logName).writeln(
								"7-In exchange phase, offer accepted. Sending the chips as agreed: "
										+ sending);
						client.communication.sendTransferRequest(getOpponent()
								.getPerGameId(), sending);
					}
				}
			}
		}
	}

	protected LearningHandler getLearningHandler() {
		return learnOpponent;
	}

	protected Proposal generateBestProposal() {

		Proposal newProposal = negotiator.makeOffer(opponentPersonality,
				consecutiveNoMovements, isGameAboutToEnd(), numOfIterations,
				getLearningHandler().hasThereBeenAgreement());
		chosenPath = negotiator.getChosenPath();

		return newProposal;
	}

	protected Boolean shouldAcceptOffer(Proposal proposal) {

		ClientGameStatus cgs = client.getGameStatus();
		PlayerStatus myPlayer = cgs.getMyPlayer();
		PlayerStatus opPlayer = getOpponent();

		// Getting the opponents shortest path
		ArrayList<Path> oppShortestPaths = ShortestPaths.getShortestPaths(
				opPlayer.getPosition(), cgs.getBoard().getGoalLocations()
						.get(0), cgs.getBoard(), scoring,
				ShortestPaths.NUM_PATHS_RELEVANT, opPlayer.getChips(),
				myPlayer.getChips());

		int opConsecNoMovement = (Integer) cgs.getMyPlayer().get(
				"opConsecutiveNoMovement");
		FileLogger.getInstance(logName).writeln(
				"In shouldAcceptOffer, checking num of opConsecNoMove: "
						+ opConsecNoMovement);

		Boolean bAccept = negotiator.isProposalBeneficial(proposal,
				shortestPaths, consecutiveNoMovements, opConsecNoMovement,
				maxRoundMove, opponentPersonality, oppShortestPaths,
				isGameAboutToEnd(), numOfIterations, getLearningHandler()
						.hasThereBeenAgreement(), bIsCounterOffer);

		return bAccept;
	}
}