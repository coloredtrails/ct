package ctagents.alternateOffersAgent.purbAgent;

/**
<b>Description</b>
The Personality class defines all the different types of personalities for an agent.
An agent can be for example low cooperative and low reliable. Or it could be low cooperative but medium reliable and so on.   
  @author Yael Ejgenberg
 */
public class Personality {
	double coopLevel;
	double reliabLevel;
	
	//All the lower bounds are inclusive and the higher bounds exclusive.
	//For example if an agents personality is "Low cooperation" then:
	//  LOW_COOP_LOWER_BOUND <= cooperation level <  LOW_COOP_HIGHER_BOUND
	public static double LOW_COOP_LOWER_BOUND = 0.0;
	public static double LOW_COOP_HIGHER_BOUND = 0.3;
	
	public static double MED_COOP_LOWER_BOUND = 0.3;
	public static double MED_COOP_HIGHER_BOUND = 0.6;
	
	public static double HIGH_COOP_LOWER_BOUND = 0.6;
	public static double HIGH_COOP_HIGHER_BOUND = 1.0;
	
	
	public static double LOW_RELIAB_LOWER_BOUND = 0.0;
	public static double LOW_RELIAB_HIGHER_BOUND = 0.4;
	
	public static double MED_RELIAB_LOWER_BOUND = 0.4;
	public static double MED_RELIAB_HIGHER_BOUND = 0.8;
	
	public static double HIGH_RELIAB_LOWER_BOUND = 0.8;
	public static double HIGH_RELIAB_HIGHER_BOUND = 1.0;
	
	/**
	 * @param A Personality will be consisted of two letters, the first one indicates 
	 * the cooperation level and the second one the reliability level:
	 */
	public enum PersonalityType {LL, LM, LH, MM, MH, HM, HH, ML};
	
	public Personality(double coopLevel, double reliabLevel){
		this.coopLevel = coopLevel;
		this.reliabLevel = reliabLevel;
	}
	
	public void setCoopLevel(double coopLevel) {
		this.coopLevel = coopLevel;
	}
	
	public void setReliabLevel(double reliabLevel) {
		this.reliabLevel = reliabLevel;
	}
	
	public double getReliabLevel() {
		return this.reliabLevel;
	}
	
	public double getCoopLevel() {
		return this.coopLevel;
	}
	
	public boolean isLowReliabPersonality(){
		if(this.reliabLevel < LOW_RELIAB_HIGHER_BOUND)
			return true;
		return false;
	}

	public boolean isMediumReliabPersonality(){
		if((MED_RELIAB_LOWER_BOUND <= this.reliabLevel) && (this.reliabLevel < MED_RELIAB_HIGHER_BOUND))
			return true;
		return false;
	}

	public boolean isHighReliabPersonality(){
		if((HIGH_RELIAB_LOWER_BOUND <= this.reliabLevel) && (this.reliabLevel <= HIGH_RELIAB_HIGHER_BOUND))
			return true;
		return false;
	}

	
	public boolean isLowCoopPersonality() {
		if(this.coopLevel < LOW_COOP_HIGHER_BOUND)
			return true;
		return false;
	}
	

	
	public boolean isMediumCoopPersonality() {
		if((MED_COOP_LOWER_BOUND <= this.coopLevel) && ( this.coopLevel < MED_COOP_HIGHER_BOUND))
			return true;
		return false;
	}

	
	
	public boolean isHighCoopPersonality() {
		if((HIGH_COOP_LOWER_BOUND <= this.coopLevel) && (this.coopLevel <= HIGH_COOP_HIGHER_BOUND))
			return true;
		return false;
	}

	//{LL, LM, LH, MM, MH, HM, HH, ML};
	public PersonalityType whatIsMyType() throws NullPointerException{
		PersonalityType type = null;
		if( isLowCoopPersonality() && isLowReliabPersonality())
			type = PersonalityType.LL;
		else if(isLowCoopPersonality() && isMediumReliabPersonality())
			type = PersonalityType.LM;
		else if(isLowCoopPersonality() && isHighReliabPersonality())
				type = PersonalityType.LH;
		else if(isMediumCoopPersonality() && isMediumReliabPersonality())
			type = PersonalityType.MM;
		else if(isMediumCoopPersonality() && isHighReliabPersonality())
			type = PersonalityType.MH;
		else if(isHighCoopPersonality() && isMediumReliabPersonality())
			type = PersonalityType.HM;
		else if(isHighCoopPersonality() && isHighReliabPersonality())
			type = PersonalityType.HH;
		else if(isMediumCoopPersonality() && isLowReliabPersonality()){
			type = PersonalityType.ML;
		}

		if(type == null)
			throw(new NullPointerException("The personality type can't be null, cooperation= " + coopLevel + " reliability= " + reliabLevel));
		return type;
	}


	
}
