package ctagents.alternateOffersAgent.purbAgent;

import ctagents.FileLogger;
import ctagents.alternateOffersAgent.LearningHandler;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

/**
 * This class keeps track of the behavior of an opponent. Reliability is
 * measured by counting how many times the opponent committed to a change and
 * kept the commitment. Cooperation is measured by the exchange rate as defined
 * in Shavit's thesis: coop
 * 
 * @author Yael Ejgenberg
 */
public class PurbLearningHandler extends LearningHandler {

	private double currentReliability;

	public final static double delta = 0.9; // This is the time discount factor
	// to calculate cooperation and
	// reliability over time
	public final static double P = (double) 2 / 3; // P to calculate cooperation

	// and reliability level

	public PurbLearningHandler(Personality p) {
		if (p.isHighReliabPersonality())
			iKeptCommitmentAtPhase.add(2);
		else if (p.isMediumReliabPersonality())
			iKeptCommitmentAtPhase.add(1);
		else
			iKeptCommitmentAtPhase.add(-1);

		// We initialize the current reliability level to the level received at
		// the beginning.
		currentReliability = p.getReliabLevel();

		// Creating a dummy chipset that contains one chip just to be able to
		// set the first cooperative level to 1
		ChipSet cs = new ChipSet();
		cs.add("Black", 1);

		chipsOfferedByOp.add(cs);
		chipsOfferedByMe.add(cs);
		chipsSentByOp.add(new ChipSet());
		chipsSentByMe.add(new ChipSet());
	}

	public double getReliability(int phaseK) {
		if (phaseK <= 0)
			throw (new RuntimeException("Phase has to be > 0, instead is: "
					+ phaseK));

		FileLogger.getInstance("Agent").writeln(
				"In getReliability phaseK = " + phaseK);
		FileLogger.getInstance("Agent").writeln(
				"iKeptCommitmentAtPhase.get(phaseK) = "
						+ iKeptCommitmentAtPhase.get(phaseK));
		// If no agreement was made in this iteration then my appreciation of
		// opponent reliability stays unchanged:
		if (iKeptCommitmentAtPhase.get(phaseK) == NO_AGREEMENT) {
			FileLogger.getInstance("Agent").writeln(
					"getReliability = " + currentReliability);
			return currentReliability;
		}

		// If an agreement was made then calculate the weighted average of my
		// previous appreciation (90% of it) with my current appreciation (100%
		// of it)

		// If there was no agreement then iKeptCommitmentAtPhase(k) = 0
		// else, if commitment was not fulfilled iKeptCommitmentAtPhase(k) = -1
		// else if commitment was partially fulfilled iKeptCommitmentAtPhase(k)
		// = 1
		// else if commitment was partially fulfilled plus another extra chips
		// (that was not mentioned in agreement) is sent
		// iKeptCommitmentAtPhase(k) = 2
		// else if commitment was not fulfilled but some chip was sent
		// iKeptCommitmentAtPhase(k) = 3
		// else if commitment was fully accepted then iKeptCommitmentAtPhase(k)
		// = 4
		double reliabCommitK = 0.0;
		if (iKeptCommitmentAtPhase.get(phaseK) == NOT_FULFILLED)
			reliabCommitK = 0.0;
		else if (iKeptCommitmentAtPhase.get(phaseK) == PARTIALLY_FULFILLED)
			reliabCommitK = 0.5;
		else if (iKeptCommitmentAtPhase.get(phaseK) == PARTIALLY_FULFILLED_PLUS_OTHER)
			reliabCommitK = 0.75;
		else if (iKeptCommitmentAtPhase.get(phaseK) == NOT_FULFILLED_PLUS_OTHER)
			reliabCommitK = 0.3;
		else if (iKeptCommitmentAtPhase.get(phaseK) == FULFILLED)
			reliabCommitK = 1.0;

		FileLogger.getInstance("Agent").writeln(
				"reliabCommitK = " + reliabCommitK);
		double tempReliab = (delta * currentReliability + reliabCommitK) / 2;

		currentReliability = tempReliab;
		FileLogger.getInstance("Agent").writeln(
				"getReliability = " + currentReliability);
		return currentReliability;

	}

	// We calculate the cooperation level as follows:
	// coop_level(k) = P * 1/k * Sum from 1 to k of( delta ^ (k-l) *
	// cooperation_exchanges(k)) + (1-P) * 1/k * Sum from 1 to k of(delta ^(k-l)
	// * cooperation_offers)
	public double getCooperation(int phaseK, int numOfOffers) {
		if (phaseK <= 0)
			throw (new RuntimeException("Phase has to be > 0, instead is: "
					+ phaseK));
		FileLogger.getInstance("Agent").writeln(
				"In getCooperation phaseK = " + phaseK + ", numOfOffers = "
						+ numOfOffers);
		FileLogger.getInstance("Agent").writeln(
				"numOfChipsSent size = " + chipsSentByOp.size());
		FileLogger.getInstance("Agent").writeln(
				"numOfChipsReceived size = " + chipsSentByMe.size());
		double sum1 = 0;
		for (int l = 0; l <= phaseK; l++) {
			int coopExchangeK = 0;
			FileLogger.getInstance("Agent").writeln("l = " + l);
			FileLogger.getInstance("Agent").writeln(
					"numOfChipsSent.get(l) = " + chipsSentByOp.get(l));
			FileLogger.getInstance("Agent").writeln(
					"numOfChipsReceived.get(l) = " + chipsSentByMe.get(l));
			if ((chipsSentByOp.get(l).getNumChips() > 0)
					&& (chipsSentByOp.get(l).getNumChips() >= chipsSentByMe
							.get(l).getNumChips()))
				coopExchangeK = 1;
			else
				coopExchangeK = 0;
			sum1 += Math.pow(delta, phaseK - l) * coopExchangeK;
			// FileLogger.getInstance("Agent").writeln("coopExchange = " +
			// coopExchangeK + ", sum1 = " + sum1);

		}

		double sum2 = 0;
		for (int l = 0; l <= numOfOffers; l++) {
			int coopOfferK = 0;
			FileLogger.getInstance("Agent").writeln("l = " + l);
			FileLogger.getInstance("Agent").writeln(
					"numOfChipsOffered.get(l) = " + chipsOfferedByOp.get(l));
			FileLogger.getInstance("Agent").writeln(
					"numOfChipsRequested.get(l) = " + chipsOfferedByMe.get(l));
			if ((chipsOfferedByOp.get(l).getNumChips() > 0)
					&& (chipsOfferedByOp.get(l).getNumChips() >= chipsOfferedByMe
							.get(l).getNumChips()))
				coopOfferK = 1;
			else
				coopOfferK = 0;
			sum2 += Math.pow(delta, numOfOffers - l) * coopOfferK;
			// FileLogger.getInstance("Agent").writeln("coopOfferK = " +
			// coopOfferK + ", sum2 = " + sum2);

		}
		// double val = P * (double)1/phaseK * sum1 + (1-P) *
		// (double)1/numOfOffers * sum2;
		double val = P * (double) 1 / (phaseK + 1) * sum1 + (1 - P)
				* (double) 1 / (numOfOffers + 1) * sum2;
		FileLogger.getInstance("Agent").writeln("coop level = " + val);
		return val;
	}
}