package ctagents.alternateOffersAgent;

import java.util.Vector;

import ctagents.FileLogger;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

/**
 * This class keeps track of the behavior of an opponent.
 * Reliability is measured by counting how many times 
 * the opponent committed to a change and kept the commitment.
 * Cooperation is measured by the exchange rate as defined in Shavit's thesis:
 * coop  
 * @author Yael
 */
public abstract class LearningHandler {
	 
	protected int numOfCommitmentsMade = 0;
	protected int numOfCommitmentsKept = 0;
	protected Vector<Integer> iKeptCommitmentAtPhase = new Vector<Integer>(); //If there was no agreement then iKeptCommitmentAtPhase(k) = 0
																			//else, if commitment was not fulfilled iKeptCommitmentAtPhase(k) = -1 
																			// 		else if commitment was partially fulfilled iKeptCommitmentAtPhase(k) = 1
																			//else if commitment was partially fulfilled plus another extra chips (that was not mentioned in agreement) is sent iKeptCommitmentAtPhase(k) = 2
																			//		else if commitment was not fulfilled but some chip was sent iKeptCommitmentAtPhase(k) = 3	
																			//			else if commitment was fully accepted then iKeptCommitmentAtPhase(k) = 4
	protected Vector<ChipSet> chipsOfferedByOp = new Vector<ChipSet>() ;
	protected Vector<ChipSet>  chipsOfferedByMe = new Vector<ChipSet>();
	protected Vector<ChipSet> chipsSentByOp = new Vector<ChipSet>() ;
	protected Vector<ChipSet>  chipsSentByMe = new Vector<ChipSet>();
	
	public final static int NO_AGREEMENT = 0;
	public final static int NOT_FULFILLED = -1;
	public final static int PARTIALLY_FULFILLED = 1;
	public final static int PARTIALLY_FULFILLED_PLUS_OTHER = 2;
	public final static int NOT_FULFILLED_PLUS_OTHER = 3;
	public final static int FULFILLED = 4;
	
	public LearningHandler() {
	}
	
	public void addCommitmentMade() { numOfCommitmentsMade++;}
	public void addCommitmentKept() { numOfCommitmentsKept++;}
	
	//Since the vectors numOfChipsOffered and numOfChipsRequested are zero indexed we set the number of chips at the iteration number - 1. 
	//That is, at the first iteration the number of chips is set at index zero and so on.
	public void setChipsOfferedByOp(ChipSet chips, int iterNum) { 
		chipsOfferedByOp.add(new ChipSet(chips)) ;
	} 

	public void setChipsOfferedByMe(ChipSet chips, int iterNum) {
		chipsOfferedByMe.add(new ChipSet(chips));
	}
	
	public void setChipsSentByOp(ChipSet chips, int iterNum) { 
		chipsSentByOp.add(new ChipSet(chips)) ;
	} 
	public void setChipsSentByMe(ChipSet chips, int iterNum) {
		chipsSentByMe.add(new ChipSet(chips));
	}

	public void setWasCommitmentKeptAtPhase(int iKept, int iterNum){
		iKeptCommitmentAtPhase.add(iKept);		
	}

	public int getCommitmentInfoAtPhase(int phase){
		FileLogger.getInstance("Agent").writeln("In getCommitmentInfoAtPhase, phase is: " + phase);
		int val = iKeptCommitmentAtPhase.get(phase);
		
		String info = "unknown";
		switch(val){
		case NO_AGREEMENT: info = "NO_AGREEMENT";
		break;
		case NOT_FULFILLED: info = "NOT_FULFILLED";
		break;
		case PARTIALLY_FULFILLED: info = "PARTIALLY_FULFILLED";
		break;
		case PARTIALLY_FULFILLED_PLUS_OTHER: info = "PARTIALLY_FULFILLED_PLUS_OTHER";
		break;
		case NOT_FULFILLED_PLUS_OTHER: info = "NOT_FULFILLED_PLUS_OTHER";
		break;
		case FULFILLED: info = "FULFILLED";
		break;
		}
		FileLogger.getInstance("Agent").writeln("In getCommitmentInfoAtPhase, phase is: " + phase + " info is: " + info);
		return val;
	}
	
	public boolean hasThereBeenAgreement(){
		FileLogger.getInstance("Agent").writeln("In hasThereBeenAgreement");
		boolean val = false;
		
		//We start looking from index 1, that is from the second element, since the first element 
		//is a dummy one.
		for(int i = 1; i < iKeptCommitmentAtPhase.size(); i++){
			if(iKeptCommitmentAtPhase.get(i) !=  NO_AGREEMENT){
				val = true;
				break;
			}
		}
		if(val)
			FileLogger.getInstance("Agent").writeln("Yes, there has been agreement");
		else
			FileLogger.getInstance("Agent").writeln("There haven't been agreements yet");
		return val;
	}
	
	public int getNumOfCommitmentsMade() {
		return numOfCommitmentsMade;
	}
	
	public boolean isFirstAgreement(){
		FileLogger.getInstance("Agent").writeln("In isFirstAgreement");
		boolean val = false;
	
		if(numOfCommitmentsMade == 1){
			val = true;
			FileLogger.getInstance("Agent").writeln("There has been " + numOfCommitmentsMade + " agreement");
		}
		else{
			val = false;
			FileLogger.getInstance("Agent").writeln("There have been " + numOfCommitmentsMade + " agreement/s");
		}
			
		return val;
	}

}
