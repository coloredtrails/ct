package ctagents.alternateOffersAgent.palAgent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;

import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;

/**
 * This class was created to prevent using the ScoringUtility class which uses
 * the BestUse, cause this function is very expensive and can't be used for the
 * agent to calculate the repercussions of a chosen move. This class finds only
 * once the available path for each player and calculates the score according to
 * it, assuming there is only one path thus any movement on the board will not
 * change it. It is implemented according to the singleton pattern so all the
 * system will use the same hash table to prevent redundant calculations.
 * 
 * @author Yael Blumberg
 */
public class FixedPathScoreUtil {

	private static FixedPathScoreUtil INSTANCE;

	/*
	 * Represents a square on the board that has a position and a color
	 */
	private class Square {

		public RowCol pos;
		public String color;

		public Square(RowCol rc, String chipColor) {
			pos = rc;
			color = chipColor;
		}
	}

	private class State {

		public RowCol pos;
		public ChipSet chips;

		public State(RowCol rc, ChipSet curChips) {
			pos = rc;
			chips = curChips;
		}

		public State(State copy) {
			pos = new RowCol(copy.pos);
			chips = new ChipSet(copy.chips);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((chips == null) ? 0 : chips.hashCode());
			result = prime * result + ((pos == null) ? 0 : pos.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			State other = (State) obj;
			if (pos == null) {
				if (other.pos != null)
					return false;
			} else if (!pos.equals(other.pos))
				return false;
			if (chips == null) {
				if (other.chips != null)
					return false;
			} else if (!chips.equals(other.chips))
				return false;
			return true;
		}
	}

	private GameStatus gs;
	private ArrayList<ArrayList<Square>> paths;
	private Hashtable<State, Double> computedStates;

	// This array holds a list of strings of the colors in the path of the
	// player (each color appears once)
	private ArrayList<HashSet<String>> neededColors;

	public static FixedPathScoreUtil getInstance(GameStatus gs) {
		if (INSTANCE == null) {
			INSTANCE = new FixedPathScoreUtil(gs);
		}

		return INSTANCE;
	}

	private FixedPathScoreUtil(GameStatus gs) {
		this.gs = gs;
		paths = new ArrayList<ArrayList<Square>>();
		computedStates = new Hashtable<State, Double>();
		neededColors = new ArrayList<HashSet<String>>();

		RowCol goal = gs.getBoard().getGoalLocations().get(0);
		ChipSet chip0 = gs.getPlayerByPerGameId(0).getChips();
		ChipSet chip1 = gs.getPlayerByPerGameId(1).getChips();

		Path path0 = ShortestPaths
				.getShortestPaths(gs.getPlayerByPerGameId(0).getPosition(),
						goal, gs.getBoard(), gs.getScoring(),
						ShortestPaths.NUM_PATHS_RELEVANT, chip0, chip1).get(0);

		Path path1 = ShortestPaths
				.getShortestPaths(gs.getPlayerByPerGameId(1).getPosition(),
						goal, gs.getBoard(), gs.getScoring(),
						ShortestPaths.NUM_PATHS_RELEVANT, chip1, chip0).get(0);

		// First players path
		ArrayList<Square> Id0Path = new ArrayList<Square>();
		ArrayList<String> path0Colors = path0.getRequiredChipsInArray(gs
				.getBoard());
		HashSet<String> Id0Colors = new HashSet<String>();

		// Adding the start position (color doesn't matter)
		Id0Path.add(new Square(path0.getStartPoint(), ""));

		for (int i = 0; i < path0Colors.size(); ++i) {
			Id0Path.add(new Square(path0.getPoint(i + 1), path0Colors.get(i)));
			// Adds the color only if it wasn't added before
			Id0Colors.add(path0Colors.get(i));
		}

		// Second players path
		ArrayList<Square> Id1Path = new ArrayList<Square>();
		ArrayList<String> path1Colors = path1.getRequiredChipsInArray(gs
				.getBoard());
		HashSet<String> Id1Colors = new HashSet<String>();

		// Adding the start position (color doesn't matter)
		Id1Path.add(new Square(path1.getStartPoint(), ""));

		for (int i = 0; i < path1Colors.size(); ++i) {
			Id1Path.add(new Square(path1.getPoint(i + 1), path1Colors.get(i)));
			// Adds the color only if it wasn't added before
			Id1Colors.add(path1Colors.get(i));
		}

		paths.add(0, Id0Path);
		paths.add(1, Id1Path);
		neededColors.add(0, Id0Colors);
		neededColors.add(1, Id1Colors);
	}

	public HashSet<String> getNeededColors(int perGameId) {
		return neededColors.get(perGameId);
	}

	public double getPlayerScore(ChipSet cs, int perGameId) {

		Double score;
		RowCol pos = gs.getPlayerByPerGameId(perGameId).getPosition();

		State curState = new State(pos, cs);

		// Checking if the current location and current position were already
		// calculated before
		score = computedStates.get(curState);
		if (score != null) {
			return score;
		}

		// Copying the chipSet so we won't change the original
		ChipSet chips = new ChipSet(cs);

		// Searching the position on the path
		int curPos = -1;
		for (int i = 0; i < paths.get(perGameId).size(); ++i) {
			if (paths.get(perGameId).get(i).pos.equals(pos)) {
				curPos = i;
				break;
			}
		}

		if (curPos == -1) {
			System.err.println("Error! can't find current position: "
					+ pos.toString());
			return -1;
		}

		// Going on the path from the current position towards the goal and
		// looking for a missing chip
		int distance = 0;
		for (int i = curPos + 1; i < paths.get(perGameId).size(); ++i) {

			// If we have the needed chips - use it, otherwise calculating
			// distance to goal. IMPORTANT - we use only get and set and not
			// the contains and sub method since they use a loop thus
			// very expensive.
			int chipsNo = chips.getNumChips(paths.get(perGameId).get(i).color);
			if (chipsNo != 0) {
				chips.setNumChips(paths.get(perGameId).get(i).color,
						chipsNo - 1);
			} else {
				distance = paths.get(perGameId).size() - i;
				break;
			}
		}

		// Basic score.
		if (distance == 0) {
			score = (double) gs.getScoring().goalweight;
		} else {
			score = (double) (distance * gs.getScoring().distweight);
		}

		score += chips.getNumChips() * gs.getScoring().chipweight;

		// Adding this score to the hash table (creating a copy so
		// the key will not be changed from another place)
		computedStates.put(new State(curState), score);

		return score;
	}

	public String getPlayerRole(ChipSet cs, int perGameId) {

		String role = "TD";
		RowCol pos = gs.getPlayerByPerGameId(perGameId).getPosition();

		// Copying the chipSet so we won't change the original
		ChipSet chips = new ChipSet(cs);

		// Searching the position on the path
		int curPos = -1;
		for (int i = 0; i < paths.get(perGameId).size(); ++i) {
			if (paths.get(perGameId).get(i).pos.equals(pos)) {
				curPos = i;
				break;
			}
		}

		if (curPos == -1) {
			System.err.println("Error! can't find current position: "
					+ pos.toString());
			return "ERR";
		}

		// Going on the path from the current position towards the goal and
		// looking for a missing chip
		int distance = 0;
		for (int i = curPos + 1; i < paths.get(perGameId).size(); ++i) {

			// If we have the needed chips - use it, otherwise calculating
			// distance to goal. IMPORTANT - we use only get and set and not
			// the contains and sub method since they use a loop thus
			// very expensive.
			int chipsNo = chips.getNumChips(paths.get(perGameId).get(i).color);
			if (chipsNo != 0) {
				chips.setNumChips(paths.get(perGameId).get(i).color,
						chipsNo - 1);
			} else {
				distance = paths.get(perGameId).size() - i;
				break;
			}
		}
		if (distance == 0) {
			role = "TI";
		}
		return role;
	}

}