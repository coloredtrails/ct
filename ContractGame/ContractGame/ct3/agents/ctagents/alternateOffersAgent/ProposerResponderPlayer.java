/*
	Colored Trails

	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package ctagents.alternateOffersAgent;

import java.io.IOException;
import java.util.Random;

import edu.harvard.eecs.airg.coloredtrails.agent.events.RoleChangedEventListener;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.client.ClientGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Phases;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import ctagents.FileLogger;

/**
 * A class that extends the abstract class SimplePlayer for agents in the
 * responder role. In the Communication Phase, the responder agent waits for a
 * proposal, and when received, first checks if the proposal is feasible. If
 * not, rejects it. Then, checks if the proposal is beneficial. If it's
 * beneficial, accepts the offer. In the Exchange Phase, if there's an offer
 * accepted, sends the chips that are asked for. In the Movement Phase, if
 * there's an offer accepted, does the movement
 * 
 * @author Yael Ejgenberg
 */
public abstract class ProposerResponderPlayer extends SimplePlayer implements
		RoleChangedEventListener {

	/** role id of the proposer */
	protected static final int PROPOSER = 0;
	/** role id of the responder */
	protected static final int RESPONDER = 1;
	int myRole = PROPOSER;

	protected int consecutiveNoMovements = 0;
	protected int numOfIterations = 0;
	protected int numOfOffers = 0;

	boolean bMovedInRound = false;

	protected ChipSet oppCsBeforeExchange = new ChipSet();
	protected ChipSet myCsBeforeExchange = new ChipSet();
	protected ChipSet myCsAfterExchange = new ChipSet();
	protected ChipSet opChipSetCommitted = new ChipSet();

	protected int maxRoundMove;

	protected boolean bIsCounterOffer = false;
	protected String logName = "Agent";

	public ProposerResponderPlayer() {
		super();
		client.addRoleChangedEventListener(this);

		// We are overriding the scoring variable here with a new Scoring with
		// the values the AlternativeOffersUltimatum protocol uses. We should be
		// getting it from the GameStatus but the order of creation of objects
		// doesn't allow it. This should be reviewed later and fixed
		scoring = new Scoring(100, -10, 5);
		FileLogger.getInstance(logName).writeln(
				"Created agent, scoring is " + scoring.toString());
	}

	/**
	 * This method is meant to enforce the existence of a LearningHandler object
	 * without restricting only the base type properties
	 * 
	 * @return LearningHandler
	 */
	protected abstract LearningHandler getLearningHandler();

	/**
	 * This method is used to update the knowledge of the learning handler after
	 * an agreement is made
	 */
	protected abstract void updatePersonality();

	protected abstract Boolean shouldAcceptOffer(Proposal proposal);

	protected abstract Proposal generateBestProposal();

	protected abstract void doExchange();

	/**
	 * Implements the abstract method in SimplePlayer class If the receipt comes
	 * from a proposer, then takes proper action according to it
	 * 
	 * @param dm
	 *            Received discourse message
	 */
	private boolean hasRequestedNeededChipsIHadAndCantGive(Path pathToCheck,
			ChipSet myChipsBefExch, ChipSet chipsToSend) {
		boolean bHas = false;
		
		ClientGameStatus cgs = client.getGameStatus();
		ChipSet requiredChipsForPath = pathToCheck.getRequiredChips(cgs
				.getBoard());
		ChipSet myMissingBefExch = myChipsBefExch
				.getMissingChips(requiredChipsForPath);
		ChipSet myChipsAfterExch = ChipSet.subChipSets(myChipsBefExch,
				chipsToSend);
		ChipSet myMissingAfterExch = myChipsAfterExch
				.getMissingChips(requiredChipsForPath);

		ChipSet diffCS = ChipSet.subChipSets(myMissingBefExch,
				myMissingAfterExch);
		for (String color : diffCS.getColors()) {
			if (diffCS.getNumChips(color) < 0) {
				bHas = true;
				break;
			}
		}
		return bHas;
	}

	public void onReceipt(DiscourseMessage dm) {

		ClientGameStatus cgs = client.getGameStatus();
		int counterOffersCount = (Integer) cgs.getMyPlayer().get(
				"counterOffersCount");
		FileLogger.getInstance(logName).writeln(
				"In PropRespPlayer.onReceipt, counterOffersCount = "
						+ counterOffersCount);

		if (dm instanceof BasicProposalDiscussionDiscourseMessage) {

			// Message received is a response to my proposal
			FileLogger.getInstance(logName).writeln(
					"Agent received a BasicProposalDiscussionDiscourseMessage");
			BasicProposalDiscussionDiscourseMessage response = (BasicProposalDiscussionDiscourseMessage) dm;
			FileLogger.getInstance(logName).writeln(
					"The message is: " + response.toString());

			if (response.accepted()) {
				FileLogger.getInstance(logName).writeln(
						"Opp. accepted offer and learnOpponent updated");
				getLearningHandler().addCommitmentMade();

				// We want to know how many chips the opponent is willing to
				// give us and how many chips the opponent wants to receive. So
				// since at this point we were the ones that made the original
				// offer and the opponent only accepted it, we update the
				// learning object where the number of chips offered really
				// means the number of chips the opponent agreed to send to us,
				// and the number of chips requested means the number of chips
				// we offer to send to the opponent.
				FileLogger.getInstance(logName).writeln(
						"response.getChipsSentByResponder().getNumChips() is "
								+ response.getChipsSentByResponder()
										.getNumChips());
				FileLogger.getInstance(logName).writeln(
						"numOfIterations is " + numOfIterations);
				FileLogger.getInstance(logName).writeln(
						"numOfOffers is " + numOfOffers);
				getLearningHandler().setChipsOfferedByOp(
						response.getChipsSentByResponder(), numOfOffers);

				FileLogger
						.getInstance(logName)
						.writeln(
								"3) The message was accepted and learnOpponent updated");
				getLearningHandler().setChipsOfferedByMe(
						response.getChipsSentByProposer(), numOfOffers);
				FileLogger
						.getInstance(logName)
						.writeln(
								"4) The message was accepted and learnOpponent updated");
				offerAccepted = true;
				sending = new ChipSet(response.getChipsSentByProposer());
				FileLogger
						.getInstance(logName)
						.writeln(
								"5) The message was accepted and learnOpponent updated");

				opChipSetCommitted = (ChipSet) response
						.getChipsSentByResponder().clone();
				FileLogger.getInstance(logName).writeln(
						"opChipSetCommitted: " + opChipSetCommitted);
				bIsCounterOffer = false;
				FileLogger.getInstance(logName).writeln(
						"bIsCounterOffer = " + bIsCounterOffer);
			} else {

				FileLogger.getInstance(logName).writeln(
						"Response not accepted and learnOpponent updated");
				FileLogger.getInstance(logName).writeln(
						"numOfIterations is " + numOfIterations);
				getLearningHandler().setChipsOfferedByOp(new ChipSet(),
						numOfOffers);
				getLearningHandler().setChipsOfferedByMe(new ChipSet(),
						numOfOffers);

				offerAccepted = false;
				// reset opChipSetCommitted
				opChipSetCommitted = new ChipSet();
				bIsCounterOffer = !bIsCounterOffer;
				FileLogger.getInstance(logName).writeln(
						"bIsCounterOffer = " + bIsCounterOffer);
			}
			numOfOffers++;
		} else if (dm instanceof BasicProposalDiscourseMessage) {

			// Message received is a proposal message
			BasicProposalDiscourseMessage proposal = (BasicProposalDiscourseMessage) dm;
			FileLogger.getInstance(logName)
					.writeln("Got proposal: " + proposal);
			cgs = client.getGameStatus();

			// ChipSet after the possible exchange
			ChipSet chipsAfterExchange = new ChipSet();
			// Chips that will be received
			ChipSet mightBeReceived = proposal.getChipsSentByProposer();
			// Chips that will be sent
			ChipSet mightBeSent = proposal.getChipsSentByResponder();

			ChipSet myChips = cgs.getMyPlayer().getChips();
			FileLogger.getInstance(logName).writeln("myChips: " + myChips);

			// Update the learning object with the proposal received:
			getLearningHandler().setChipsOfferedByOp(mightBeReceived,
					numOfOffers);
			getLearningHandler().setChipsOfferedByMe(mightBeSent, numOfOffers);

			FileLogger.getInstance(logName).writeln(
					"num of offers is: " + numOfOffers);
			numOfOffers++;

			FileLogger.getInstance(logName).writeln(
					"Might be sent " + mightBeSent);
			// ChipSet after the exchange is myChips - sentChips + receivedChips
			chipsAfterExchange = ChipSet.subChipSets(myChips, mightBeSent);
			chipsAfterExchange = ChipSet.addChipSets(chipsAfterExchange,
					mightBeReceived);
			FileLogger.getInstance(logName).writeln(
					"chipsAfterExchange = " + chipsAfterExchange);

			// Create the message for response
			BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(
					proposal);

			if (response == null) {
				FileLogger
						.getInstance(logName)
						.writeln(
								"In ProposerResponderPlayer::onReceipt, response object is null!");
			} else {
				FileLogger.getInstance(logName).writeln(
						"In ProposerResponderPlayer::onReceipt, response is: "
								+ response);
			}

			// Check if the proposal is feasible or not, reject if not
			// Calculating how many chips I'm missing to reach
			// the goal (assuming only one available path)
			// Computing my distance from the goal
			RowCol myPos = client.getGameStatus().getMyPlayer().getPosition();
			Path myPath = ShortestPaths.getShortestPaths(myPos,
					client.getGameStatus().getBoard().getGoalLocations()
					.get(0), client.getGameStatus().getBoard(),
					scoring, ShortestPaths.NUM_PATHS_RELEVANT, myChips,
					getOpponent().getChips()).get(0);

			if ((!isFeasible(chipsAfterExchange)) || (hasRequestedNeededChipsIHadAndCantGive(myPath, 
					cgs.getMyPlayer().getChips(), mightBeSent))){
				response.rejectOffer();
				if (client.communication.sendDiscourseRequest(response))
					FileLogger.getInstance(logName).writeln(
							"Sent response succesfully.");
				FileLogger.getInstance(logName).writeln(
						"AGENT " + clientName
								+ ": Sending response: OFFER REJECTED");
				FileLogger.getInstance(logName).writeln(
						"---- Rejecting (Not Feasible) ---- \n");
				FileLogger.getInstance(logName).writeln(response.toString());
				return;
			}

			PlayerStatus myPlayer = cgs.getMyPlayer();
			PlayerStatus opPlayer = getOpponent();

			shortestPaths = ShortestPaths.getShortestPaths(myPlayer
					.getPosition(), cgs.getBoard().getGoalLocations().get(0),
					cgs.getBoard(), scoring, ShortestPaths.NUM_PATHS_RELEVANT,
					myPlayer.getChips(), opPlayer.getChips());

			Proposal newProposal = new Proposal();
			newProposal.chipsToReceive = mightBeReceived;
			newProposal.chipsToSend = mightBeSent;

			if (shouldAcceptOffer(newProposal)) {
				// accept offer
				response.acceptOffer();
				offerAccepted = true;
				FileLogger.getInstance(logName).writeln(
						"I accepted offer and learnOpponent updated");
				getLearningHandler().addCommitmentMade();
				sending = new ChipSet(mightBeSent);
				try {
					Random rand = new Random();
					int sleepTime = rand.nextInt(9); // randomly choose a number
					// between 0 (inclusive)
					// and 9 (exclusive).
					sleepTime *= 1000;
					sleepTime += 6000; // Always wait at least 6 seconds before
					// responding.
					// On top of that, randomly add to waiting time between 0
					// and 8 seconds
					System.out
							.println("Sleeptime in response is: " + sleepTime);
					Thread.sleep(sleepTime);
				} catch (InterruptedException ex) {
					FileLogger.getInstance(logName).writeln(ex.getMessage());
				}
				client.communication.sendDiscourseRequest(response);
				opChipSetCommitted = mightBeReceived;

				bIsCounterOffer = false;
				FileLogger.getInstance(logName).writeln(
						"bIsCounterOffer = " + bIsCounterOffer);

				FileLogger.getInstance(logName).writeln(
						"AGENT " + clientName
								+ ": Sending response: OFFER ACCEPTED.");

				FileLogger.getInstance(logName).writeln(
						"---- Accepting ---- \n");
				return;
			}

			response.rejectOffer();
			bIsCounterOffer = !bIsCounterOffer;
			FileLogger.getInstance(logName).writeln(
					"bIsCounterOffer = " + bIsCounterOffer);
			try {
				Random rand = new Random();
				int sleepTime = rand.nextInt(9); // randomly choose a number
				// between 0 (inclusive) and
				// 9 (exclusive).
				sleepTime *= 1000;
				sleepTime += 6000; // Always wait at least 6 seconds before
				// responding.
				// On top of that, randomly add to waiting time between 0 and 8
				// seconds
				System.out.println("Sleeptime in response is: " + sleepTime);
				Thread.sleep(sleepTime);
			} catch (InterruptedException ex) {
				FileLogger.getInstance(logName).writeln(ex.getMessage());

			}
			client.communication.sendDiscourseRequest(response);
			FileLogger.getInstance(logName).writeln(
					"AGENT " + clientName
							+ ": Sending response: OFFER REJECTED");
			FileLogger.getInstance(logName).writeln("---- Rejecting ---- \n");

		} else {
			FileLogger.getInstance(logName).writeln(
					"---- Received a discourse message ---- \n" + dm);
			FileLogger.getInstance(logName).writeln(
					"Received a discourse message: " + dm);
		}
	}

	/**
	 * Implements the abstract method in SimplePlayer class Every time a phase
	 * advances, the agent checks to see if the new phase is either
	 * Communication, Movement or Exchange phase. If it's one of these, takes
	 * the proper action.
	 * 
	 * @param ph
	 *            New phases object sent by the server
	 */
	public void phaseAdvanced(Phases ph) {

		ClientGameStatus cgs = client.getGameStatus();
		String phaseName = cgs.getPhases().getCurrentPhaseName();
		FileLogger.getInstance(logName).writeln(
				"AGENT " + clientName + ": A new phase began: " + phaseName);

		// Communication Phase: Wait for incoming proposals
		if (phaseName.equals("Communication Phase")) {
			cgs = client.getGameStatus();
			maxRoundMove = (Integer) cgs.getMyPlayer()
					.get("maxRoundNoMovement");
			FileLogger.getInstance(logName).writeln(
					"In Comm. ph., maxRoundNoMovement= " + maxRoundMove);

			numOfIterations++;
			bMovedInRound = false;

			String roleStr = cgs.getMyPlayer().getRole();
			FileLogger.getInstance(logName).writeln(
					"My role is now: " + roleStr);
			if (roleStr.equals("Proposer")) {
				myRole = PROPOSER;
				FileLogger.getInstance(logName).writeln(
						"AGENT " + clientName + ": About to make offer...");
			} else if (roleStr.equals("Responder")) {
				myRole = RESPONDER;
				FileLogger.getInstance(logName).writeln(
						"AGENT " + clientName
								+ ": Waiting for incoming messages...");
			}

			myCsBeforeExchange = cgs.getMyPlayer().getChips();
			oppCsBeforeExchange = getOpponent().getChips();
			makeOffer();
		}
		// Movement Phase: If there's an accepted offer, take your path to the
		// goal. Since movement phase occurs after communication and exchange
		// phase we can update here what we have learned about the opponent's
		// behavior regarding its personality
		else if (phaseName.equals("Movement Phase")) {

			FileLogger.getInstance(logName).writeln(
					"In movement phase, updating personality");

			myCsAfterExchange = cgs.getMyPlayer().getChips();
			FileLogger.getInstance(logName).writeln(
					"chipSetBeforeExchange: " + myCsBeforeExchange);
			FileLogger.getInstance(logName).writeln(
					"chipSetAfterExchange: " + myCsAfterExchange);

			ChipSet myCsLeftAfterSending = ChipSet.subChipSets(
					myCsBeforeExchange, sending);
			ChipSet actualChipSetSentByOP = ChipSet.subChipSets(
					myCsAfterExchange, myCsLeftAfterSending);

			FileLogger.getInstance(logName).writeln(
					"chipSetSentByMe: " + sending);

			FileLogger.getInstance(logName).writeln(
					"actualChipSetSentByOP: " + actualChipSetSentByOP);

			getLearningHandler().setChipsSentByOp(actualChipSetSentByOP,
					numOfOffers);
			getLearningHandler().setChipsSentByMe(sending, numOfOffers);

			// If there was no agreement then iKeptCommitmentAtPhase(k) = 0
			// else if commitment was not fulfilled
			// iKeptCommitmentAtPhase(k) = -1
			// else if commitment was partially fulfilled
			// iKeptCommitmentAtPhase(k) = 1
			// else if commitment was partially fulfilled plus another extra
			// chips (that was not mentioned in agreement) is sent
			// iKeptCommitmentAtPhase(k) = 2
			// else if commitment was not fulfilled but some chip was sent
			// iKeptCommitmentAtPhase(k) = 3
			// else if commitment was fully accepted then
			// iKeptCommitmentAtPhase(k) = 4

			if (!offerAccepted) {
				FileLogger
						.getInstance(logName)
						.writeln(
								"offer was not accepted, setWasCommitmentKeptAtPhase with 0");
				getLearningHandler().setWasCommitmentKeptAtPhase(
						LearningHandler.NO_AGREEMENT, numOfIterations);
			}// else offer was accepted:
			else if (actualChipSetSentByOP.equals(opChipSetCommitted)) {
				FileLogger.getInstance(logName).writeln(
						"actualChipSetSentByOP.equals(opChipSetCommitted)["
								+ opChipSetCommitted + "]");
				getLearningHandler().setWasCommitmentKeptAtPhase(
						LearningHandler.FULFILLED, numOfIterations);
			} else if (!actualChipSetSentByOP.isEmpty()
					&& opChipSetCommitted.contains(actualChipSetSentByOP)) {
				FileLogger.getInstance(logName).writeln(
						"opChipSetCommitted contains actualChipSetSentByOP "
								+ opChipSetCommitted);
				getLearningHandler().setWasCommitmentKeptAtPhase(
						LearningHandler.PARTIALLY_FULFILLED, numOfIterations);
			}
			// The actual chip set sent is not fully contained in the commitment
			// one, but the committed one has some
			// of the chips in the actual sent. This means that the actual sent
			// also has other chips not contained in the committed set.
			else if (!actualChipSetSentByOP.isEmpty()
					&& opChipSetCommitted.hasAnyOf(actualChipSetSentByOP)) {
				FileLogger.getInstance(logName).writeln(
						"opChipSetCommitted has any of actualChipSetSentByOP "
								+ opChipSetCommitted);
				getLearningHandler().setWasCommitmentKeptAtPhase(
						LearningHandler.PARTIALLY_FULFILLED_PLUS_OTHER,
						numOfIterations);
			} else if (!actualChipSetSentByOP.isEmpty()) {
				FileLogger.getInstance(logName).writeln(
						"actualChipSetSentByOP differs from opChipSetCommitted: "
								+ opChipSetCommitted);
				getLearningHandler().setWasCommitmentKeptAtPhase(
						LearningHandler.NOT_FULFILLED_PLUS_OTHER,
						numOfIterations);
			} else {
				FileLogger.getInstance(logName).writeln(
						"actualChipSetSentByOP->NOT equal opChipSetCommitted)["
								+ opChipSetCommitted + "]");
				getLearningHandler().setWasCommitmentKeptAtPhase(
						LearningHandler.NOT_FULFILLED, numOfIterations);
			}

			// After updating the learning handler with our last appreciation of
			// the opponent, update the opponents personality with what the
			// learning handler has learned.
			updatePersonality();

			// in the movement phase the agent position may change, so set
			// shortestPaths to null
			shortestPaths = null;
			// set offerAccepted back to false, so that in the next
			// communication phase will get set to the relevant value then.
			offerAccepted = false;

			// reset opChipSetCommitted
			opChipSetCommitted = new ChipSet();

			RowCol ppos = cgs.getMyPlayer().getPosition();
			RowCol gpos = cgs.getBoard().getGoalLocations().get(0);
			int delta_r = Math.abs(ppos.row - gpos.row);
			int delta_c = Math.abs(ppos.col - gpos.col);
			int manhattan = delta_r + delta_c;

			shortestPaths = ShortestPaths.getShortestPaths(ppos, gpos, cgs
					.getBoard(), scoring, ShortestPaths.NUM_PATHS_RELEVANT, cgs
					.getMyPlayer().getChips(), getOpponent().getChips());
			bMovedInRound = false;
			FileLogger.getInstance(logName).writeln(
					"About to decide movement. There are "
							+ shortestPaths.size()
							+ " shortestPaths, manhattan is: " + manhattan);
			FileLogger.getInstance(logName).writeln(
					"chipSetAfterExchange is: " + myCsAfterExchange);
			for (Path p : shortestPaths) {
				FileLogger.getInstance(logName).writeln("Checking path: " + p);
				FileLogger.getInstance(logName).writeln(
						"Required chips for p: "
								+ p.getRequiredChips(cgs.getBoard()));
				FileLogger.getInstance(logName).writeln(
						"p.getPoint(0): " + p.getPoint(0));
				// The first point in path is the actual position. We don't want
				// to count it.
				if ((p.getNumPoints() - 1) == manhattan
						&& myCsAfterExchange.contains(p.getRequiredChips(cgs
								.getBoard()))) {
					client.communication.sendMoveRequest(p.getPoint(1));
					bMovedInRound = true;
					consecutiveNoMovements = 0;
					break;
				}
			}
			FileLogger.getInstance(logName).writeln(
					"bMovedInRound= " + bMovedInRound);
			// TODO find a better way of obtaining max num of consecutive rounds
			// from the protocol. For now do it hard coded.
			int maxNumConsecNoMovRounds = 3;
			if (!bMovedInRound) {
				FileLogger.getInstance(logName).writeln(
						"consecutiveNoMovements = " + consecutiveNoMovements);
				// if the game finishes if I don't move, try to move:
				if (consecutiveNoMovements == (maxNumConsecNoMovRounds - 1)) {
					FileLogger
							.getInstance(logName)
							.writeln(
									"consecutiveNoMovements == (maxNumConsecNoMovRounds - 1)");
					double maxUtil = Double.NEGATIVE_INFINITY;
					Path thePath = null;
					for (Path p : shortestPaths) {
						FileLogger.getInstance(logName).writeln(
								"Check UF of path: " + p);
						ChipSet requiredChipsForPath = p.getRequiredChips(cgs
								.getBoard());
						ChipSet missing = myCsAfterExchange
								.getMissingChips(requiredChipsForPath);
						int numOfChipsLacking = missing.getNumChips();
						double val = calcMoveUtilityFunction(cgs.getMyPlayer(),
								gpos, numOfChipsLacking);
						FileLogger.getInstance(logName).writeln("UF = " + val);
						if (val > maxUtil) {
							maxUtil = val;
							thePath = p;
						}
					}
					FileLogger.getInstance(logName).writeln(
							"MaxUtil = " + maxUtil + " chosen path is: "
									+ thePath);
					if (thePath != null) {
						client.communication.sendMoveRequest(thePath
								.getPoint(1));
						bMovedInRound = true;
						consecutiveNoMovements = 0;
						FileLogger.getInstance(logName).writeln(
								"sent move request for point: "
										+ thePath.getPoint(1));
					}
				}
			}

		}
		// Exchange Phase: If there's an accepted offer, send the chips that are
		// asked for
		// Eventually refine this logic according to my personality the
		// opponent's personality
		else if (phaseName.equals("Exchange Phase")) {
			doExchange();
		} else if (phaseName.equals("Feedback Phase")) {
			FileLogger.getInstance(logName).flush();
			if (!bMovedInRound)
				consecutiveNoMovements++;
		} else if (phaseName.equals("Strategy Prep Phase")) {
			FileLogger
					.getInstance(logName)
					.writeln(
							"In ProposerResponderPlayer:phaseAdvanced, Strategy Preparation Phase");
		}
	}

	public enum EndingReasons {
		NOT_ENDING, I_HAVE_TO_MOVE, OPP_HAS_TO_MOVE, OPP_ONE_STEP_FROM_GOAL, BOTH_HAVE_TO_MOVE
	};

	// isGameAboutToend checks the three possible cases the game is about to
	// end.
	// If the game is not about to end return 0
	// If the game is about to end in the following cases :
	// 1) My player hasn't moved all the times it was allowed to not move.
	// That is if it doesn't move in the next movement phase the game ends.
	// Return 1
	// 2) Other player hasn't moved all the times it was allowed to not move.
	// That is if it doesn't move in the next movement phase the game ends.
	// Return 2
	// 3) If opponent is one square away of the goal and it has the chip to get
	// there the game is about to end.
	// Return 3
	// 4) If both me and opponent haven't moved all the times it was allowed to
	// not move.
	// That is if we both don't move in the next movement phase the game ends.
	// Return 4
	protected EndingReasons isGameAboutToEnd() {
		// If I don't move this round the game is about to end.
		boolean bIHaveToMove = false;
		boolean bOppHasToMove = false;
		if (consecutiveNoMovements == maxRoundMove - 1)
			// return 1;
			bIHaveToMove = true;

		// If opponent doesn't move this round the game is about to end.
		ClientGameStatus cgs = client.getGameStatus();
		PlayerStatus opPlayer = getOpponent();
		int opConsecNoMovement = (Integer) cgs.getMyPlayer().get(
				"opConsecutiveNoMovement");
		if (opConsecNoMovement == maxRoundMove - 1)
			// return 2;
			bOppHasToMove = true;
		if (bIHaveToMove && bOppHasToMove)
			return EndingReasons.BOTH_HAVE_TO_MOVE;

		if (bIHaveToMove)
			return EndingReasons.I_HAVE_TO_MOVE;
		if (bOppHasToMove)
			return EndingReasons.OPP_HAS_TO_MOVE;

		// if opponent is one square away of the goal and it has the chip to get
		// there the game is about to end:
		RowCol gpos = cgs.getBoard().getGoalLocations().get(0);
		RowCol ppos = opPlayer.getPosition();
		String goalColor = cgs.getBoard().getSquare(gpos).getColor();
		if (ppos.dist(gpos) == 1) {
			ChipSet opChips = opPlayer.getChips();
			boolean bHasColor = false;
			for (String color : opChips.getColors()) {
				if (color.equals(goalColor) && opChips.getNumChips(color) > 0) {
					bHasColor = true;
					break;
				}

			}
			if (bHasColor)
				return EndingReasons.OPP_ONE_STEP_FROM_GOAL;
			else
				return EndingReasons.NOT_ENDING;

		}
		return EndingReasons.NOT_ENDING;
	}

	private double calcMoveUtilityFunction(PlayerStatus ps, RowCol gpos,
			int numOfChipsLacking) {
		return scoring.inProcessScore(ps, gpos, numOfChipsLacking,
				isGameAboutToEnd());
	}

	/**
	 * Checks if the exchange is feasible or not
	 * 
	 * @param chipsAfterExchange
	 *            The ChipSet after the possible exchange
	 * @return true if all color numbers are greater or equal to 0, false
	 *         otherwise
	 */
	private boolean isFeasible(ChipSet chipsAfterExchange) {
		for (String color : chipsAfterExchange.getColors()) {
			if (chipsAfterExchange.getNumChips(color) < 0) {
				return false;
			}
		}
		return true;
	}

	protected int getMyID() {
		ClientGameStatus cgs = client.getGameStatus();
		return cgs.getMyPlayer().getPerGameId();
	}

	// this function is good only if there are 2 players. For a game with more
	// players
	// we need to think of a better function.
	protected PlayerStatus getOpponent() {
		int myID = getMyID();
		ClientGameStatus cgs = client.getGameStatus();
		for (PlayerStatus player : cgs.getPlayers()) {
			int playerID = player.getPerGameId();
			if (playerID != myID)
				return cgs.getPlayerByPerGameId(playerID);
		}
		throw new RuntimeException("Responder ID not found.");
	}

	protected void makeOffer() {

		// Check if at the moment I'm supposed at all to make an offer or not.
		// If my role now is being a PROPOSER then go ahead, propose. Otherwise,
		// if I am a RESPONDER then I have nothing to do in this function.
		if (myRole == RESPONDER)
			return;

		FileLogger.getInstance(logName).writeln(
				"In Make offer. scoring is: " + scoring.toString());
		ClientGameStatus cgs = client.getGameStatus();
		Board board = cgs.getBoard();
		shortestPaths = ShortestPaths.getShortestPaths(cgs.getMyPlayer()
				.getPosition(), board.getGoalLocations().get(0), board,
				scoring, ShortestPaths.NUM_PATHS_RELEVANT, client.getChips(),
				getOpponent().getChips());

		FileLogger.getInstance(logName).writeln(
				"In Make offer.My shortestPaths to look at is:  "
						+ shortestPaths);

		Proposal newProposal = generateBestProposal();

		FileLogger.getInstance(logName).writeln(
				"AGENT " + clientName + ": Best action Missing Chips: "
						+ newProposal.chipsToReceive);

		FileLogger.getInstance(logName).writeln(
				"AGENT " + clientName + ": Best action Extra Chips: "
						+ newProposal.chipsToSend);

		// Adding all the missing colors as zeros so the agent message and human
		// message will be the same
		for (String c : cgs.getGamePalette().getColors()) {
			newProposal.chipsToReceive.add(c, 0);
			newProposal.chipsToSend.add(c, 0);
		}

		// Create a proposal discourse message from the proposer to the sender
		// with default id
		int proposerID = cgs.getMyPlayer().getPerGameId();
		int responderID = getOpponent().getPerGameId();
		BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(
				proposerID, responderID, -1, newProposal.chipsToSend,
				newProposal.chipsToReceive);

		FileLogger.getInstance(logName).writeln(
				"---- sending proposal: proposal ");

		try {
			Random rand = new Random();
			int sleepTime = rand.nextInt(9); // randomly choose a number between
			// 0 (inclusive) and 9
			// (exclusive).
			sleepTime *= 1000;
			sleepTime += 12000; // Always wait at least 12 seconds before
			// proposing.
			// On top of that, randomly add to waiting time between 0 and 8
			// seconds
			System.out.println("Sleeptime in propose is: " + sleepTime);
			Thread.sleep(sleepTime);
		} catch (InterruptedException ex) {
			FileLogger.getInstance(logName).writeln(ex.getMessage());

		}

		client.communication.sendDiscourseRequest(proposal);
	}

	public void roleChanged() {
		ClientGameStatus cgs = client.getGameStatus();
		String phaseName = cgs.getPhases().getCurrentPhaseName();
		FileLogger.getInstance(logName).writeln(
				"AGENT " + clientName + "role changed event in phase: "
						+ phaseName);

		// Communication Phase: Wait for incoming proposals
		if (phaseName.equals("Communication Phase")) {
			cgs = client.getGameStatus();
			String roleStr = cgs.getMyPlayer().getRole();
			FileLogger.getInstance(logName).writeln(
					"My role is now: " + roleStr);
			if (roleStr.equals("Proposer")) {
				myRole = PROPOSER;
				FileLogger.getInstance(logName).writeln(
						"AGENT " + clientName + ": About to make offer...");

				myCsBeforeExchange = cgs.getMyPlayer().getChips();
				oppCsBeforeExchange = getOpponent().getChips();
				makeOffer();
			} else if (roleStr.equals("Responder")) {
				myRole = RESPONDER;
				FileLogger.getInstance(logName).writeln(
						"AGENT " + clientName
								+ ": Waiting for incoming messages...");
			}

		}
	}
	
	public void gameEnded() {
		try {
			FileLogger.getInstance(logName).close();
		} catch (IOException e) {
			System.out.println("Problem closing agents log file");
			e.printStackTrace();
		}
		super.gameEnded();
	}
}