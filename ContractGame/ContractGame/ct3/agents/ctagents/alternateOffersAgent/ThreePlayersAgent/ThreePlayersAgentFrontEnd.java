package ctagents.alternateOffersAgent.ThreePlayersAgent;

import ctagents.alternateOffersAgent.ThreePlayersAgent.ThreePlayersAgentPlayer.Culture;
import java.util.*;
import ctagents.FileLogger;


/**
 * Main class of the ThreePlayersAgent (Personality Adaptor Learning) agent.
 * 
 * @author Galit Haim
 * 
 */
public final class ThreePlayersAgentFrontEnd {
	
	public static void main(String[] args) {
		
		String ThreePlayersAgentlogName;
		if ((args.length != 1 && args[0] != null && args[1] != null && args[2] != null)) {
			if(args[0].equals("0"))
			{
			ThreePlayersAgentPlayer agent = new ThreePlayersAgentPlayer(
												Integer.parseInt(args[0]),
												Integer.parseInt(args[1]),
												Integer.parseInt(args[2]));
			agent.setClientName(args[1]);
					
			agent.start();
			}
			if(args[0].equals("1"))
			{
			ThreePlayersAgentPlayerSP agent = new ThreePlayersAgentPlayerSP(
												Integer.parseInt(args[0]),
												Integer.parseInt(args[1]),
												Integer.parseInt(args[2]));
			agent.setClientName(args[1]);
		
			agent.start();
			}
			if(args[0].equals("2"))
			{
				ThreePlayersAgentPlayerNastySP agent = new ThreePlayersAgentPlayerNastySP(
												Integer.parseInt(args[0]),
												Integer.parseInt(args[1]),
												Integer.parseInt(args[2]));
			agent.setClientName(args[1]);
		
			agent.start();
			}
			if(args[0].equals("3"))
			{
				ThreePlayersAgentPlayerLearningSP agent = new ThreePlayersAgentPlayerLearningSP(
												Integer.parseInt(args[0]),
												Integer.parseInt(args[1]),
												Integer.parseInt(args[2]),
												args[3]);
			agent.setClientName(args[1]);
		
			agent.start();
			}
			if(args[0].equals("4"))
			{
				ThreePlayersAgentPlayerSPg agent = new ThreePlayersAgentPlayerSPg(
												Integer.parseInt(args[0]),
												Integer.parseInt(args[1]),
												Integer.parseInt(args[2]),
												args[3]);
			agent.setClientName(args[1]);
		
			agent.start();
			}
			if(args[0].equals("5"))
			{
				ThreePlayersAgentPlayerRiskAverseSP agent = new ThreePlayersAgentPlayerRiskAverseSP(
												Integer.parseInt(args[0]),
												Integer.parseInt(args[1]),
												Integer.parseInt(args[2]));
			agent.setClientName(args[1]);
		
			agent.start();
			}
			if(args[0].equals("6"))
			{
				ThreePlayersAgentPlayerNeutralSP agent = new ThreePlayersAgentPlayerNeutralSP(
												Integer.parseInt(args[0]),
												Integer.parseInt(args[1]),
												Integer.parseInt(args[2]));
			agent.setClientName(args[1]);
		
			agent.start();
			}
		}
		else {
			System.out
					.println("You need to specify the agent role (CS or SP), an id for the agent and the culture of the opponent: "
							+ Culture.ISRAEL.ordinal()
							+ "-Israel, "
							+ Culture.USA.ordinal()
							+ "-USA, "
							+ Culture.LEBANON.ordinal() + "-Lebanon");
		}
	}
}