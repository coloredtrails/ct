//////////////////////////////////////////////////////////////////////////
// File:		ThreePlayersAgentPlayerNastySP 					   				//
// Purpose:		Implements the core of the ThreePlayersAgent agent.				//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.ThreePlayersAgent;

//Imports from Java common framework
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.lang.Math;



//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Phases;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.Constants;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.agent.events.RoleChangedEventListener;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.alternateOffersAgent.SimplePlayer;
import ctagents.alternateOffersAgent.ThreePlayersAgent.ThreePlayersAgentPlayer.Culture;
import ctagents.FileLogger;

import java.lang.Boolean;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.io.FileWriter;
import java.io.PrintWriter;


//////////////////////////////////////////////////////////////////////////
// Class:		ThreePlayersAgent						    				//
// Purpose:		Implements the ThreePlayersAgent player.							//
//////////////////////////////////////////////////////////////////////////
public class ThreePlayersAgentPlayerNastySP extends SimplePlayer implements RoleChangedEventListener
{
	private enum BoardType {
		DD, TD, TI
	};

	private enum AgentRoleType {
		CS, SP // customer or Service Provider
	};

	private static final int NUMBER_OF_SIMULTANEUSLY_PROPOSALS = 2;	
	private static final int NUMBER_OF_SECONDS_FOR_EARLY_RESPONSE = 20;
	private static final int MAX_TRANSFER_OF_CHIPS=4;
	private boolean lastWasDormant=false;

	private boolean respondToSendWasActivated=false;
	private int choosenOpponentID;
	private static final ScheduledExecutorService respondTimerThread = 
			Executors.newSingleThreadScheduledExecutor();

	//	private double weightProvider=0.1;
	//	//weights for flexibility part
	//	private double weightFlexibility=0.5;
	//
	//	private double weightBridgeReachable=20;
	//	private double weightCanCommit=20;
	//	//1-weightBridgeReachable-weightCanCommit
	//	private double weightNultipleSteps=20;
	Proposal proposer0innerProposal;
	Proposal proposer1innerProposal;

	private AgentRoleType agentRole;
	private Culture oppCulture;
	private BoardType oppBoardType;
	private String cultureFolder = "agents/ctagents/alternateOffersAgent/ThreePlayersAgent/";

	// Saves whether ThreePlayersAgent is the first proposer
	private boolean m_IsFirstProposer;

	// Saves the last transfer
	private ChipSet m_LastTransfer;

	// Saves whether this is the first communications round
	private boolean m_FirstCommunicationsRound;

	// Saves the last accepted proposal
	private Proposal m_LastAcceptedProposal;


	// Saves the scoring
	private Scoring m_Scoring;

	// Debugging related members
	private static long currTS=(new Date()).getTime(); 
	private static final FileLogger s_Logger = FileLogger.getInstance("ThreePlayersAgentPlayer"+currTS);
	private static final boolean s_IsDebug = true;

	// Maximum number of dormant rounds
	private static final int s_MaxDormantRounds = 2;

	public static final int NUMBER_OF_PLAYERS = 3;

	// Number of rounds to play, each round we switch roles
	int numOfRounds = 2;
	int roundInTheGame = 0;
	// int DISCOUNT_FACTOR_FOR_EACH_ROUND = 5;
	String CurrentPhaseName = null;
	boolean ended = false;
	int initialDormant = 0;
	int consecutiveNoMovementCounterPlayer1 = initialDormant;
	int numberofMoves = 0;

	String replyString0 = "UNKNOWN";
	String replyString1 = "UNKNOWN";

	// Number of counter offers allowed in each round
	static final int maxNumOfCounterOffers = 1;
	int counterOffersCount = 0;
	RowCol CSPlayerPosition = null;
	boolean CSPlayerReachedGoal = false;


	boolean[] communicationAllowed = new boolean[NUMBER_OF_PLAYERS];

	boolean[] movementAllowed = new boolean[NUMBER_OF_PLAYERS];


	private static PrintWriter csvLog;
	private String csvLogName = "CT_DbgLog_Agent_"+ (new Date()).getTime();
	private String proposal0String4Log = null;
	private String proposal1String4Log = null;
	private String reply0String4Log = null;
	private String reply1String4Log = null;
	private String gameStatusString4Log = null;

	DecimalFormat df;
	RowCol gpos0;
	RowCol gpos1;


	// gs is a member variable of GameConfigDetailsRunnable,
	// defined as: protected ServerGameStatus gs;
	/**
	 * The scoring function used for players in the game. 150 for a player (instead of 100)
	 * reaching the goal, -10 per unit distance if the player does not reach the
	 * goal, 5 for each chip remaining after the player has reached the goal or
	 * cannot move any farther towards the goal.
	 */
	Scoring s = new Scoring(150, 0, 5);

	/** Local random generator for creating chipsets */
	static Random localrand = new Random();

	/** determines if there will be automatic movement */
	boolean automaticMovement = false; 
	/**
	 * determines if the chips will automatically transfer after a proposal has
	 * been accepted
	 */
	boolean automaticChipTransfer = true; 
	/** determines if the phases will loop. */
	boolean phaseLoop = true;// false;

	PlayerStatus[] proposersInGame = new PlayerStatus[2];
	PlayerStatus[] respondersInGame = new PlayerStatus[2];
	PlayerStatus myPlayer;
	PlayerStatus CSplayer;
	int acceptReplyCounter = 0;
	int rejectReplyCounter = 0;
	int CSPlayerRcvdProposalsCounter = 0;


	/* Store the Proposal Messages sent by the two proposers.
	 * null if no proposal received.
	 */
	BasicProposalDiscourseMessage proposer0Proposal = null;
	BasicProposalDiscourseMessage proposer1Proposal = null;

	/* Store whether or not the proposals have been accepted.
	 * null means no proposal has been accepted.
	 */
	Boolean acceptedProposer0;
	Boolean acceptedProposer1;

	/*will hold the output of the scores*/
	FileWriter out;


	public void resetPermissionFlags() {
		for (int i=0; i < NUMBER_OF_PLAYERS; i++)
		{
			communicationAllowed[i] = false;
			movementAllowed[i] = false;
		}
	}

	private void endLog()
	{
		if ((myPlayer.getPosition().equals(gpos0)) || (myPlayer.getPosition().equals(gpos1)))
		{
			CSPlayerReachedGoal = true;
		}

		if (csvLog !=null){
			csvLog.println(csvLogName+ ",,,,,,,,,,,,,,,,,,,,,,,,"+CSPlayerReachedGoal);

			csvLog.flush();
			csvLog.close();
		}
		csvLog = null;
		csvLogName = null;

	}		


	public void setPermissions() {

		System.out.println( "entering setPermissions");

		PlayerStatus pStat;

		for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
			pStat = client.getGameStatus().getPlayerByPerGameId(i);
			pStat.setCommunicationAllowed(communicationAllowed[i]);
			pStat.setMovesAllowed(movementAllowed[i]);
		}
	}

	/* When the SP are the proposers the CS will be in the two entries of the responder;
	 * When the SP are the responders, the CS will be in the two entries of the proposer
	 */
	public void swapRoles() {
		PlayerStatus temp1, temp2;

		System.out.println( "entering swapRoles");


		temp1 = proposersInGame[0];
		temp2 = proposersInGame[1];
		proposersInGame[0] = respondersInGame[0];
		proposersInGame[1] = respondersInGame[1];
		respondersInGame[0] = temp1;
		respondersInGame[1] = temp2;
		// Now, update the role in the players' status
		for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
		{
			if (client.getGameStatus().getPlayerByPerGameId(i).getRole() == "Proposer"){
				client.getGameStatus().getPlayerByPerGameId(i).setRole("Responder");
				communicationAllowed[i] = false;
			}
			else { // was the responder
				client.getGameStatus().getPlayerByPerGameId(i).setRole("Proposer");
				communicationAllowed[i] = true;
			}
		}
		setPermissions();
	}


	/**
	 * Returns score of specified player, according to player's current state
	 */
	public int getPlayerScore(PlayerStatus ps) {

		int sc0 = (int) Math.floor(s.score(ps, gpos0)); // should change to double

		int sc1 = (int) Math.floor(s.score(ps, gpos1)); // should change to double

		if (!(ps.getPosition().equals(myPlayer.getPosition()))) // i.e the CSPlayer didn't arrive to the goal
		{
			sc0 -= s.goalweight;
			sc1 -= s.goalweight;
		}

		if (sc0 >= sc1)												
			return sc0;
		else
			return sc1;
	}

	/**
	 * Called by GameConfigDetailsRunnable methods when calculation and
	 * assignment of player scores is desired
	 */
	protected void assignScores() {
		for (PlayerStatus ps : client.getGameStatus().getPlayers()) {
			ps.setScore(getPlayerScore(ps));
		}
	}

	public int getMaxNonConsecRounds() {
		return numOfRounds;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		logMsg													//
	// Purpose:		Logs a new message to the log file.						//
	// Parameters:	@ message - The message to log.							//
	// Remarks:		* Logs only when debug mode is activated.				//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static void logMsg(String message)
	{
		// Write a new line with the message if we're in debug mode
		if (s_IsDebug)
		{
			s_Logger.writeln(message);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		ThreePlayersAgentPlayer constructor								//
	// Purpose:		Initializes the ThreePlayersAgentPlayer agent.							//
	//////////////////////////////////////////////////////////////////////////
	@SuppressWarnings("unchecked")
	public ThreePlayersAgentPlayerNastySP(int AgentRoleIn, int boardType, int culture)
	{
		super();
		logMsg("ThreePlayersAgentPlayerNastySP(): entering.");

		// We are a listener for the role changing event
		client.addRoleChangedEventListener(this);

		if (AgentRoleIn == AgentRoleType.CS.ordinal() )
			agentRole = AgentRoleType.CS;
		else{
			agentRole = AgentRoleType.SP;
		}

		// Getting the opponents culture
		if (culture == Culture.ISRAEL.ordinal()) {
			oppCulture = Culture.ISRAEL;
		} else if (culture == Culture.USA.ordinal()) {
			oppCulture = Culture.USA;
		} else if (culture == Culture.LEBANON.ordinal()) {
			oppCulture = Culture.LEBANON;
		}

		// Getting the board type: each condition has 2 numbers for friends and stranger
		if ((boardType == 110) || (boardType == 310)){
			oppBoardType = BoardType.DD;
		} else if ((boardType == 530)  || (boardType == 630)){
			oppBoardType = BoardType.TD;
		} else if ((boardType == 210) || (boardType == 410)) {
			oppBoardType = BoardType.TI;
		}

		// Updating the culture folder name
		cultureFolder += oppCulture.toString();
		// Updating the boardType folder name

//		cultureFolder += "/"+oppCulture.toString()+"_"+oppBoardType.toString();
//
//		WekaWrapper.setCultureFolder(cultureFolder);
//		WekaWrapper.setCulture(oppCulture);


		Long currTS = (new Date()).getTime();

		// The first round of communications
		this.m_FirstCommunicationsRound = true;
		// Initialize the scoring
		this.m_Scoring = null;

		// Initialize the last transfer
		this.m_LastTransfer = null;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getMe													//
	// Purpose:		Returns my player status.								//
	// Returns:		My player status.										//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private PlayerStatus getMe()
	{

		// Done easily using the client game status
		return client.getGameStatus().getMyPlayer();
	}

	private PlayerStatus getCSPlayer()
	{
		CSplayer=client.getGameStatus().getPlayerByPerGameId(CSplayer.getPerGameId());
		return this.CSplayer;
	}

	private PlayerStatus getPlayersByGameID(int gameID){
		return client.getGameStatus().getPlayerByPerGameId(gameID);
	}

	private PlayerStatus getSPPlayer(int goalID){
		RowCol goalPos = client.getGameStatus().getBoard().getGoalLocations().get(goalID);
		for (PlayerStatus playerStatus : client.getGameStatus().getPlayers()){
			if(playerStatus.getPosition().equals(goalPos)) return playerStatus;
		}
		return null;
	}
	//////////////////////////////////////////////////////////////////////////
	// Method:		createProposal											//
	// Purpose:		Generates a proposal.									//
	// Returns:		The best simulated proposal.							//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private Proposal createProposal()
	{
		// Initialize
		this.m_LastAcceptedProposal = null;
		System.out.println( "entering createProposal");
	
		PlayerStatus proposeTo=getCSPlayer();

		Proposal proposal=getBestCommitmentOffer();//csPosition, proposeTo);
		if(proposal==null){
			proposal=getEmptyProposal();
		}
		else{
			System.out.println("CCCCCCCCCCCCCC "+proposal.chipsToReceive+" "+proposal.chipsToSend);
		}
		choosenOpponentID = proposeTo.getPerGameId();
		return proposal;
	}


	private Proposal getEmptyProposal() {
		ChipSet reply=new ChipSet();
		RowCol rowCol=getMe().getPosition();
		RowCol OtherrowCol=new RowCol(rowCol.row,8-rowCol.col);
		String whatColorHeWants=client.getGameStatus().getBoard().getSquare(OtherrowCol).getColor();
		ChipSet offer=new ChipSet();
		offer.add(whatColorHeWants,0);
		reply.add("CTRed", 0);
		reply.add("CTPurple", 0);
		return new Proposal(reply,offer);
	}


	private Proposal getBestCommitmentOffer(){
		PlayerStatus csPlayer=getCSPlayer();
		ChipSet csChips=csPlayer.getChips();
		RowCol csPosition=csPlayer.getPosition();
		PlayerStatus proposeTo=getMe();
		ChipSet myChips=proposeTo.getChips();
		Proposal bestProposal=null;
		RowCol rowCol=proposeTo.getPosition();
		
		String whatColorIm=client.getGameStatus().getBoard().getSquare(rowCol).getColor();
		int myColorChipsNum=myChips.getNumChips(whatColorIm);
		
		RowCol OtherrowCol=new RowCol(rowCol.row,8-rowCol.col);
		String whatColorHeWants=client.getGameStatus().getBoard().getSquare(OtherrowCol).getColor();
		int myWantedChips=csChips.getNumChips(whatColorHeWants);
		int redChips=myChips.getNumChips("CTRed");
		int purpChips=myChips.getNumChips("CTPurple");
		int bestScore=0;
		for (int p = purpChips; p >=0; p--) {
			for (int m = 0; m <=myWantedChips; m++) {
				ChipSet offer=new ChipSet();
				offer.add("CTPurple", p);
				offer.add("CTRed", redChips);
				ChipSet reply= new ChipSet();
				reply.add(whatColorHeWants, m);
				ChipSet newChipset=ChipSet.addChipSets(csChips, offer);
				newChipset=ChipSet.subChipSets(newChipset, reply);
				int tempScore=getScore(newChipset);
				if(isCommitment(csPosition, proposeTo, newChipset)){
					if (isReachble(csPosition, proposeTo, newChipset)){
						if(tempScore>bestScore){
							if(offer.getNumChips()-reply.getNumChips()<30){
								bestScore=tempScore;
								offer.add(whatColorIm, myColorChipsNum);
								bestProposal=new Proposal(reply, offer);
							}
						}
					}
				}
			}
		}
		if(bestProposal==null){
			return bestProposal;
		}
		int diff=bestProposal.chipsToSend.getNumChips()-bestProposal.chipsToReceive.getNumChips();
		if(diff<1){
			ChipSet offer=new ChipSet();
			offer.add("CTPurple", 0);
			offer.add("CTRed", 0);
			offer.add(whatColorIm, myColorChipsNum);
			ChipSet reply=new ChipSet();
			reply.add(whatColorHeWants, 0);
			bestProposal=new Proposal(reply, offer);
		}
		/* The following code is a Nasty proposal the SP create. The SP will propose HALF of the "best proposal" */
	    for (String color : bestProposal.chipsToReceive.getColors()) {
	    	bestProposal.chipsToReceive.setNumChips(color, bestProposal.chipsToReceive.getNumChips(color)/3);
	    }
	    for (String color : bestProposal.chipsToSend.getColors()) {
	    	bestProposal.chipsToSend.setNumChips(color, bestProposal.chipsToSend.getNumChips(color)/3);
	    }
		
		return bestProposal;

	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		respondToSend										//
	// Purpose:		Responds to a proposal.									//
	// Parameters:	@ proposal - the proposal to respond to.				//
	// Returns:		A boolean which specifies whether to accept or reject.	//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public boolean responseToSend()
	{
		logMsg("responseToSend(): entering.");
		System.out.println( "entering responseToSend");

		if(!respondToSendWasActivated){
			respondToSendWasActivated=true;
			System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
			System.out.println(m_LastAcceptedProposal.chipsToReceive+" "+m_LastAcceptedProposal.chipsToSend);
			
			boolean goodProposal=checkProposal(this.m_LastAcceptedProposal);
			
			if(goodProposal){
				logMsg("responseToSend(): accepting proposal ");//(" + proposer0innerProposal.chipsToReceive + ", " + proposer0innerProposal.chipsToSend + ").");
				System.out.println("responseToSend(): accepting proposal ");//(" + proposer0innerProposal.chipsToReceive + ", " + proposer0innerProposal.chipsToSend + ").");
				System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
				BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(proposer0Proposal);
				this.m_LastAcceptedProposal = proposer0innerProposal;
				this.m_FirstCommunicationsRound = true;
				response.acceptOffer();
				client.communication.sendDiscourseRequest(response);
					
			}
			else{// No proposal is accepted. reject both
				logMsg("responseToSend(): rejecting proposal");
				System.out.println("responseToSend(): rejecting proposal");
				System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
				BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(proposer0Proposal);
				response.rejectOffer();
				client.communication.sendDiscourseRequest(response);
			}
			proposersInGame[0].setCommunicationAllowed(false); // Enable the CS player to answer only to one proposal
		}
		return true;

	}
	//////////////////////////////////////////////////////////////////////////
	// Method:		respondToProposal										//
	// Purpose:		Responds to a proposal.									//
	// Parameters:	@ proposal - the proposal to respond to.				//
	// Returns:		A boolean which specifies whether to accept or reject.	//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private boolean respondToProposal(Proposal proposal)
	{
		logMsg("respondToProposal(): entering.");
		System.out.println( "entering respondToProposal");


		// Initialize
		this.m_LastAcceptedProposal = null;

		// Get all the colors
		Set<String> allColors = client.getGameStatus().getBoard().getColors();

		// Create a simulation of the current game status
		logMsg("respondToProposal(): my chips: (" + getMe().getChips() + ").");

		if (getMe()==myPlayer){
			//don't care about return value. Simply save proposal
		}

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		onReceipt												//
	// Purpose:		Receives a discourse message.							//
	// Parameters:	@ discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void onReceipt(DiscourseMessage dm)
	{
		logMsg("onReceipt(): entering.");
		System.out.println( "entering onReceipt");
		//If we have a response message.
		if( dm instanceof BasicProposalDiscussionDiscourseMessage) {
			BasicProposalDiscussionDiscourseMessage bpddm = (BasicProposalDiscussionDiscourseMessage) dm;
			if (bpddm.accepted())
			{
				this.m_FirstCommunicationsRound = true;
				this.m_LastAcceptedProposal = new Proposal(bpddm.getChipsSentByResponder(), bpddm.getChipsSentByProposer());
			}
			else
			{
				this.m_FirstCommunicationsRound = !this.m_FirstCommunicationsRound;
				this.m_LastAcceptedProposal = null;
			}

			if (bpddm.getProposerID() != myPlayer.getPerGameId()) //i.e the proposer is the SP player 
			{
				if (bpddm.getResponderID() != myPlayer.getPerGameId()) // to be sure that for the SP proposing messages 
				{
					client.getGameStatus().getPlayerByPerGameId(bpddm.getProposerID()).setCommunicationAllowed(true);
					return; // result;
				}
			}

			boolean reply = bpddm.accepted();
			boolean endDiscussion = false;

			if (reply == true) {
				acceptReplyCounter++;
			}
			else {
				rejectReplyCounter++;
			}

			if (proposersInGame[0] == proposersInGame[1]) //i.e the proposer is the CS player 
			{
				if (rejectReplyCounter == 1)
					endDiscussion = true;
				if( bpddm.getResponderID() == respondersInGame[0].getPerGameId() ) {
					acceptedProposer0 = reply;
					proposer0Proposal=bpddm;
					if (acceptedProposer0 == true) replyString0 = "TRUE";
					else replyString0 = "FALSE";
				}
				if( bpddm.getResponderID() == respondersInGame[1].getPerGameId() ) {
					acceptedProposer1 = reply;
					proposer1Proposal=bpddm;
					if (acceptedProposer1 == true) replyString1 = "TRUE";
					else replyString1 = "FALSE";
				}
			}
			else // Not the same proposers
			{
				if (rejectReplyCounter == 2)
					endDiscussion = true;

				if( bpddm.getProposerID() == proposersInGame[0].getPerGameId() ) {
					acceptedProposer0 = reply;
					proposer0Proposal=bpddm;
					if (acceptedProposer0 == true) replyString0 = "TRUE";
					else replyString0 = "FALSE";
				}
				if( bpddm.getProposerID() == proposersInGame[1].getPerGameId() ) {
					acceptedProposer1 = reply;
					proposer1Proposal=bpddm;
					if (acceptedProposer1 == true) replyString1 = "TRUE";
					else replyString1 = "FALSE";
				}
			}

			//if there wasn't a proposal then it cannot be accepted.
			if( acceptedProposer0 == null )
			{
				acceptedProposer0 = false;
				replyString0 = "IGNORED";
			}
			if( acceptedProposer1 == null )
			{
				acceptedProposer1 = false;
				replyString1 = "IGNORED";
			}

			if ((proposer0Proposal == null) && (proposer1Proposal == null)){
				throw new RuntimeException( "Response message sent to unknown proposal.\n" );
			}
			if ((proposer0Proposal != null) &&( acceptedProposer0 == true)) {
				int propCS = getPlayerScore(proposersInGame[0]);
				int respCS = getPlayerScore(respondersInGame[0]);
				ChipSet proposer0toSend = proposer0Proposal.getChipsSentByProposer();
				ChipSet responderToSend = proposer0Proposal.getChipsSentByResponder();


				exchange( proposersInGame[0], respondersInGame[0], proposer0toSend, responderToSend );
				if (csvLog != null) {
					reply0String4Log = replyString0 + "," +propCS+ "," +respCS + "," +
							proposer0toSend + "," +responderToSend + "," + getPlayerScore(proposersInGame[0])+","+
							getPlayerScore(respondersInGame[0]);
				}
				proposer0Proposal = null;
				proposer0innerProposal=null;
				acceptedProposer0 = null;

			}
			//else, if only the proposal from Proposer 1 was accepted.
			else if ((proposer1Proposal != null) && ( acceptedProposer1 == true)) {
				int propCS = getPlayerScore(proposersInGame[1]);
				int respCS = getPlayerScore(respondersInGame[1]);
				ChipSet proposer1toSend = proposer1Proposal.getChipsSentByProposer();
				ChipSet responderToSend = proposer1Proposal.getChipsSentByResponder();

				exchange( proposersInGame[1], respondersInGame[1], proposer1toSend, responderToSend );

				if (csvLog != null) {
					reply1String4Log = replyString1 + "," +propCS+ "," +respCS + "," +
							proposer1toSend + "," +responderToSend + "," + getPlayerScore(proposersInGame[1])+","+
							getPlayerScore(respondersInGame[1]);
				}
				proposer1Proposal = null;
				proposer1innerProposal=null;
				acceptedProposer1 = null;

			}
			else { //case that we had one reject, and one accept
				//	getLog()
				//	.writeln( "case that we had one reject, and one accept" );            	
			}
			if ((acceptReplyCounter == 1) ||// ==1: to enable only ONE accept. == 2:only when there are 2 proposals and 2 answers 
					//(or when phase time expired, move on to the next phase 
					(endDiscussion == true))	
			{ 
				if (csvLog != null) {    
					if (proposer0Proposal != null) {
						reply0String4Log = replyString0 + "," +getPlayerScore(proposersInGame[0])+ "," 
								+getPlayerScore(respondersInGame[0])+",,,,";
					}
					if (proposer1Proposal != null) {
						reply1String4Log = replyString1 + "," +getPlayerScore(proposersInGame[1])+ "," 
								+getPlayerScore(respondersInGame[1])+",,,,";
					}
				}
				//client.getGameStatus().sendArbitraryMessage(Constants.ENDDISCUSSION);
				proposer0Proposal = null;
				proposer0innerProposal=null;
				proposer1Proposal = null;
				proposer1innerProposal=null;
				client.getGameStatus().getPhases().advancePhase();
			}   
		}
		///////////////////////////////////////////////////////////////////////////////////////////////
		//else, if we have a proposal message
		///////////////////////////////////////////////////////////////////////////////////////////////
		else if( dm instanceof BasicProposalDiscourseMessage) {
			BasicProposalDiscourseMessage bpdm = (BasicProposalDiscourseMessage) dm;
			if (proposersInGame[0] == proposersInGame[1]) // i.e, the same proposer in case of CS player, I'll check multi-cases using the responders
			{
				if( bpdm.getResponderID() == respondersInGame[0].getPerGameId()||bpdm.getResponderID() == respondersInGame[1].getPerGameId() ) {
					System.out.println(">>>>>>>>>>>>>>>>>>>> 0");
					//Make sure the responder doesn't rcv multiple proposals.
					if( proposer0Proposal != null ) {
						throw new RuntimeException( "Responder 0 has already rcv a proposal message this communication phase." );
					}
					proposer0Proposal = bpdm;
					if (csvLog != null) {
						proposal0String4Log = csvLogName+ "," + roundInTheGame + "," + client.getGameStatus().getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
								+client.getGameStatus().getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
								+(dm.getFromPerGameId() == myPlayer.getPerGameId()) + "," 
								+ bpdm.getChipsSentByProposer().removeZeros() + "," 
								+ bpdm.getChipsSentByResponder().removeZeros();
					}
					// The message is a proposal
					BasicProposalDiscourseMessage proposal = (BasicProposalDiscourseMessage)dm;
					Proposal proposalToConsider = new Proposal(proposer0Proposal.getChipsSentByProposer(), proposer0Proposal.getChipsSentByResponder()); 
					logMsg("onReceipt(): opponent\'s proposal: (" + proposalToConsider.chipsToReceive + ", " + proposalToConsider.chipsToSend + ").");

					// Build a response to the proposal
					BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(proposal);

					// If the proposal should be accepted by us
					respondToProposal(proposalToConsider);
					this.m_LastAcceptedProposal = proposalToConsider;
					this.m_FirstCommunicationsRound = true;
					responseToSend();

				}
				else{
					
				}
				proposersInGame[0].setCommunicationAllowed(false); /* Enable the CS player to send only one proposal */

			}
			else {
				/////////////////////////////////////////////////////////////
				// this means that the proposers are the service provider	
				// the proposers are different, check according to the proposers
				//If the proposal is from Proposer 0
				/////////////////////////////////////////////////////////////
				System.out.println("CSPlayer got a proposal");
				if( bpdm.getProposerID() == proposersInGame[0].getPerGameId() ) {
					//Make sure the proposer isn't sending multiple proposals.
					if( proposer0Proposal != null ) {
						//throw new RuntimeException( "Proposer 0 has already sent a proposal message this communication phase." );
					}
					else {
						CSPlayerRcvdProposalsCounter++;
						proposer0Proposal = bpdm;
						if (csvLog != null) {
							proposal0String4Log = csvLogName+  "," + roundInTheGame + "," + client.getGameStatus().getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
									+client.getGameStatus().getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
									+(dm.getFromPerGameId() == myPlayer.getPerGameId()) + "," 

            					+ bpdm.getChipsSentByProposer().removeZeros() + "," 
            					+ bpdm.getChipsSentByResponder().removeZeros();
						}
						// The message is a proposal
						BasicProposalDiscourseMessage proposal = (BasicProposalDiscourseMessage)dm;
						Proposal proposalToConsider = new Proposal(proposer0Proposal.getChipsSentByProposer(), proposer0Proposal.getChipsSentByResponder()); 
						logMsg("onReceipt(): opponent\'s proposal: (" + proposalToConsider.chipsToReceive + ", " + proposalToConsider.chipsToSend + ").");
						proposer0innerProposal = proposalToConsider;


					}

				}
				//If the proposal is from Proposer 1
				else if( bpdm.getProposerID() == proposersInGame[1].getPerGameId() ) {
					if( proposer1Proposal != null ) {
						//Make sure the proposer isn't sending multiple proposals.
						//throw new RuntimeException( "Proposer 1 has already sent a proposal message this communication phase." );
					}
					else {
						CSPlayerRcvdProposalsCounter++;
						proposer1Proposal = bpdm;
						if (csvLog != null) {
							proposal1String4Log = csvLogName+  "," + roundInTheGame + "," + client.getGameStatus().getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
									+client.getGameStatus().getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
									+(dm.getFromPerGameId() == myPlayer.getPerGameId()) + "," 

            					+ bpdm.getChipsSentByProposer().removeZeros() + "," 
            					+ bpdm.getChipsSentByResponder().removeZeros();
						}

						// The message is a proposal
						BasicProposalDiscourseMessage proposal = (BasicProposalDiscourseMessage)dm;
						Proposal proposalToConsider = new Proposal(proposer1Proposal.getChipsSentByProposer(), proposer1Proposal.getChipsSentByResponder()); 
						logMsg("onReceipt(): opponent\'s proposal: (" + proposalToConsider.chipsToReceive + ", " + proposalToConsider.chipsToSend + ").");
						proposer1innerProposal = proposalToConsider;

					}

				}
				

			}

			//If the proposal is from the responder then we have a problem.
//			if(( bpdm.getProposerID() == respondersInGame[0].getPerGameId() ) ||
//					( bpdm.getProposerID() == respondersInGame[1].getPerGameId() ))
//			{
//
//				throw new RuntimeException( "Responder should not be sending proposal messages." );
//			}
		}

		// return result;
	}
	ScheduledFuture<?> scheduledFuture;

	/**
	 * Exchanges chips between agents.
	 * @param a A player that is to exchange chips with b.
	 * @param b A player that is to exchange chips with a.
	 * @param a_to_b The chips a wishes to send to b.
	 * @param b_to_a The chips b wishes to send to a.
	 */
	private static void exchange( PlayerStatus a, PlayerStatus b, ChipSet a_to_b, ChipSet b_to_a ) {
		//determine of the chips can be transfered
		boolean aCanSend = a.getChips().contains( a_to_b );
		boolean bCanSend = b.getChips().contains( b_to_a );

		System.out.println("enter exchange");
		//If the chips can be transfered, then transfer them.
		if( aCanSend && bCanSend ) {
			ChipSet aCS = ChipSet.subChipSets( a.getChips(), a_to_b );
			aCS = ChipSet.addChipSets( aCS, b_to_a );

			ChipSet bCS = ChipSet.subChipSets( b.getChips(), b_to_a );
			bCS = ChipSet.addChipSets( bCS, a_to_b );

			a.setChips( aCS );
			b.setChips( bCS );

			System.out.println("made the exchange");

		}
	}


	//////////////////////////////////////////////////////////////////////////
	// Method:		roleChanged												//
	// Purpose:		Called whenever a role has changed.						//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void roleChanged()
	{
		logMsg("roleChanged(): entering.");
		System.out.println( "entering roleChanged");


		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}

		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		logMsg("roleChanged(): current phase is \'" + phaseName + "\'.");

		// Communication phase 
		if (phaseName.equals("Communication Phase"))
		{
			String roleStr = getMe().getRole();
			logMsg("roleChanged(): new role is \'" + roleStr + "\'.");
			System.out.println("roleChanged(): new role is \'" + roleStr + "\'.");
			// If we are a proposer we should make a proposal
			if (roleStr.equals("Proposer"))
			{
				// Get my proposal
				Proposal myProposal = createProposal();

				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = choosenOpponentID;

				// Create the proposal message
				if(myProposal!=null){
					BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myProposal.chipsToSend, myProposal.chipsToReceive);
					client.communication.sendDiscourseRequest(proposal);

				}
			}
		}
	}

	private int closetsGoal(RowCol position,ChipSet chipsCS){
		int steps0= leastStepsTowardGoal(position,0,chipsCS);
		int steps1= leastStepsTowardGoal(position,1,chipsCS);
		if(steps1>steps0) return 0;
		if(steps0>steps1) return 1;
		return 2;
	}

	Path p=null;
	private int leastStepsTowardGoal(RowCol position,int goal,ChipSet chipsCS){
		RowCol dest = client.getGameStatus().getBoard().getGoalLocations().get(goal);
		ArrayList<Path> paths = ShortestPaths.getShortestPaths(position, dest, client.getGameStatus().getBoard(), this.m_Scoring, ShortestPaths.NUM_PATHS_RELEVANT, chipsCS, getSPPlayer(goal).getChips());
		int minMove=Integer.MAX_VALUE;
		// Iterate the paths
		for (Path path : paths)
		{
			ChipSet temp= ChipSet.addChipSets(chipsCS, new ChipSet());
			//System.out.println("current path is "+path);
			//System.out.println("current chips are "+temp);
			int i;
			boolean goodPath=true;
			for(i=1;i<path.getNumPoints();i++){
				RowCol pointToMove = path.getPoint(i);
				String neededColor = client.getGameStatus().getBoard().getSquare(pointToMove).getColor();
				if (temp.getNumChips(neededColor) > 0){
					temp.add(neededColor, -1);

				}
				else {
					goodPath=false;
					break;
				}
			}
			if(i<minMove&& goodPath){
				minMove=i;
				p=path;
			}
		}
		return minMove;
	}

	int commitedTo=2;

	private boolean isReachble(RowCol position,PlayerStatus SP,ChipSet remainingChips){
		Path myP=findAvailPath(remainingChips, position, SP);
		return myP!=null;
	}
	/**
	 * check if can reach the opponent location
	 * @param position current position
	 * @param sP0 my id
	 * @param remainingChips current chipset
	 * @return
	 */
	private boolean isCommitment(RowCol position,PlayerStatus SP,ChipSet remainingChips){
		int curPlayePerGameID=SP.getPerGameId();
		int oppId=0;
		switch (curPlayePerGameID) {
		case 2:
			oppId=1;
			break;
		case 1:
			oppId=2;
			break;
		}

		ChipSet oppCS=getPlayersByGameID(oppId).getChips();
		ChipSet myTempCS=ChipSet.addChipSets(remainingChips, oppCS);
		Path p=findAvailPath(myTempCS, position, getPlayersByGameID(oppId));
		if(p==null){
			return true;
		}

		return false; 
	}


	private int getScore(ChipSet chipsCS){
		return s.getChipSetWeight(chipsCS);
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		compareProposals										//
	// Purpose:		chooses one of two proposals							//
	// Returns:		id of the accepted proposal, 2 if reject both			//
	// Remarks:		* Should be called from the movement phase.				//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////

	private boolean checkProposal(Proposal proposal0){
		CSplayer=getCSPlayer();
		ChipSet origChipsCS=CSplayer.getChips();
		
		ChipSet origChipsMy=CSplayer.getChips();
		RowCol position = CSplayer.getPosition();
		double currentScore=getScore(origChipsMy);
		int score0=0;

		if(proposal0!=null){
			ChipSet newChipsCS0= ChipSet.addChipSets(origChipsCS, proposal0.chipsToSend);
			newChipsCS0=ChipSet.subChipSets(newChipsCS0, proposal0.chipsToReceive);
			
			ChipSet myNewCS= ChipSet.addChipSets(origChipsMy, proposal0.chipsToReceive);
			myNewCS=ChipSet.subChipSets(myNewCS, proposal0.chipsToSend);
			
			score0=getScore(myNewCS);
			myPlayer=getMe();
			if(isCommitment(position,myPlayer,newChipsCS0)){
				// Nasty agent. always return true
//				if(score0+150<currentScore){
//				//	if(weCanGetBetterCommitment(position,myPlayer,newChipsCS0)){
//					return false;
//				}
//				else{
//					return true;
//				}
				return true;
			}
			System.out.println("current score "+currentScore+" score "+score0);
			if(score0>=currentScore){
				return true;
			}
		}
		// A change for the Nasty agent/ Always accept the CS proposals
		return true;
		//return false;
	}



	private boolean weCanGetBetterCommitment(RowCol position, PlayerStatus SP,
			ChipSet remainingChips) {
		double bestScore=getBestCommitmentScore(position,SP);
		double givenScore=getScore(remainingChips);
		return bestScore>givenScore;
	}

	private double getBestCommitmentScore(RowCol position, PlayerStatus SP) {
		ChipSet csChips=SP.getChips();
		ChipSet myChips=getMe().getChips();
		RowCol rowCol=SP.getPosition();
		RowCol OtherrowCol=new RowCol(rowCol.row,8-rowCol.col);
		String whatColorHeWants=client.getGameStatus().getBoard().getSquare(OtherrowCol).getColor();
		int myWantedChips=myChips.getNumChips(whatColorHeWants);
		int redChips=csChips.getNumChips("CTRed");
		int purpChips=csChips.getNumChips("CTPurple");
		int bestScore=0;
		for (int p = purpChips; p >=0; p--) {
			for (int m = 0; m <=myWantedChips; m++) {
				ChipSet offer=new ChipSet();
				offer.add("CTPurple", p);
				offer.add("CTRed", redChips);
				ChipSet reply= new ChipSet();
				reply.add(whatColorHeWants, m);
				ChipSet newChipset=ChipSet.addChipSets(myChips, offer);
				newChipset=ChipSet.subChipSets(newChipset, reply);
				int tempScore=getScore(newChipset);
				if (isReachble(position, SP, newChipset))
					if(isCommitment(position,SP, newChipset)){
						if(tempScore>bestScore)
							bestScore=tempScore;
					}
			}
		}
		return bestScore;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		phaseAdvanced											//
	// Purpose:		A callback for phase advances.							//
	// Parameters:	@ discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void phaseAdvanced(Phases phases)
	{
		logMsg("phaseAdvanced(): entering.");
		System.out.println( "entering phaseAdvanced");

		PlayerStatus pStat;

		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}

		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		logMsg("phaseAdvanced(): new phase is \'" + phaseName + "\'.");

		if(phaseName.equals("Communication Phase")) {
			//	getLog().writeln(" numberofMoves in round "+roundInTheGame+" is "+numberofMoves);

			if (csvLog != null) {    

				if (proposer0Proposal != null) {
					if (reply0String4Log == null)
						reply0String4Log = "IGNORED" + "," +getPlayerScore(proposersInGame[0])+ "," 
								+getPlayerScore(respondersInGame[0])+ ",,,,";
					//	    			csvLog.println(proposal0String4Log+"," + "IGNORED" + "," + getPlayerScore(proposersInGame[0]) + ","
					//	    					+ getPlayerScore(respondersInGame[0]) + ",");
					//	    			csvLog.flush();
				}
				if (proposer1Proposal != null) {
					if (reply1String4Log == null)
						reply1String4Log = "IGNORED" + "," +getPlayerScore(proposersInGame[1])+ "," 
								+getPlayerScore(respondersInGame[1])+ ",,,,";
					//	     			csvLog.println(proposal1String4Log+"," + "IGNORED" + "," + getPlayerScore(proposersInGame[1]) + ","
					//	     					+ getPlayerScore(respondersInGame[1]) + ",");
					//	     			csvLog.flush();
				}
				gameStatusString4Log = numberofMoves + "," + myPlayer.getPosition().toString()+ ","  
						+ client.getGameStatus().getPlayerByPerGameId(0).getChips() + "," + getPlayerScore(client.getGameStatus().getPlayerByPerGameId(0))+ ","  
						+ client.getGameStatus().getPlayerByPerGameId(1).getChips() + "," + getPlayerScore(client.getGameStatus().getPlayerByPerGameId(1))+ ","  
						+ client.getGameStatus().getPlayerByPerGameId(2).getChips() + "," + getPlayerScore(client.getGameStatus().getPlayerByPerGameId(2));
				if (proposal0String4Log != null){
					csvLog.println(proposal0String4Log+"," + reply0String4Log+"," + gameStatusString4Log);
					csvLog.flush();
				}
				if (proposal1String4Log != null) {
					csvLog.println(proposal1String4Log+"," + reply1String4Log+"," + gameStatusString4Log);
					csvLog.flush();
				}
			}
			acceptReplyCounter = 0; // initialize the counter for the next negotiation phase
			rejectReplyCounter = 0;
			CSPlayerRcvdProposalsCounter = 0;
			respondToSendWasActivated=false;
			proposer0Proposal = null;
			proposer1Proposal = null;
			acceptedProposer0 = null;
			acceptedProposer1 = null;
			replyString0 = "UNKNOWN";
			replyString1 = "UNKNOWN";

			proposal0String4Log = null;
			proposal1String4Log = null;
			reply0String4Log = null;
			reply1String4Log = null;
			gameStatusString4Log = null;

			numberofMoves = 0;
			String roleStr = getMe().getRole();
			if (roleStr.equals("Proposer"))
			{
				// Get my proposal
				Proposal myProposal = createProposal();

				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = choosenOpponentID;

				// Create the proposal message
				if(myProposal!=null){
					BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myProposal.chipsToSend, myProposal.chipsToReceive);
					/*if(choosenOpponentID==0)
						proposer0Proposal=proposal;
					if(choosenOpponentID==1)
						proposer1Proposal=proposal;
					 */

					client.communication.sendDiscourseRequest(proposal);

				}
			}
			//roleChanged();

			//client.getGameStatus().sendArbitraryMessage(Constants.NEWPHASE);


			if (ended == false)
				++roundInTheGame; //only update the round for communicaton phase.
			for (PlayerStatus p : client.getGameStatus().getPlayers()) {
				p.setGameRound(roundInTheGame);
			}
		}
		//	getLog()
		//	.writeln( "---------- Beginning " + phaseName + " at round " + roundInTheGame + " ----------" );

		//Initialize communication, transfers and movement to false.
		//boolean communicationAllowed = false;

		if(phaseName.equals("Feedback Phase")){
			if(scheduledFuture!=null){
				scheduledFuture.cancel(true);
			}
			if (myPlayer.getPosition() == CSPlayerPosition) /* The CSPlayer remain in the same position and did not move */
				consecutiveNoMovementCounterPlayer1++;
			else{
				consecutiveNoMovementCounterPlayer1 = initialDormant;
				CSPlayerPosition = myPlayer.getPosition();
			}
			// update game status about dormant moves
			myPlayer.setMyNumDormantRounds(
					consecutiveNoMovementCounterPlayer1);
			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				// Update anyway... 
				// if (pStat != CSPlayer)
				pStat.setHisNumDormantRounds(consecutiveNoMovementCounterPlayer1);
			}

			if (consecutiveNoMovementCounterPlayer1 >= numOfRounds) {
					System.out.println("!!!!!!!!!bad if");
				ended = true;
			}

			else if ((myPlayer.getPosition().equals(gpos0))||
					(myPlayer.getPosition().equals(gpos1)))
			{
				ended = true;
			}
			else {
				// This way we can let each player know how many consecutive no
				// movements the OTHER has
				myPlayer.set("myConsecutiveNoMovement",
						consecutiveNoMovementCounterPlayer1);
			}
			if (ended == false) {
				swapRoles();

				setPermissions();
			}

		}


		// FYI - for the first phase it won't work from here
		else if(phaseName.equals("Communication Phase")) { //REQUESTS_PH

			//set communication, transfers, and moves of the agents.

			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				pStat.setCommunicationAllowed(communicationAllowed[i]);
			}
			myPlayer.setMovesAllowed(false);
		} 
		else if (phaseName.equals("Movement Phase")) {
			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				pStat.setCommunicationAllowed(false);
			}
			myPlayer.setMovesAllowed(true);
			System.out.println( "my player is: " + getMe() + "  CSPlayer is: " + myPlayer);
			if (getMe().getPin()==myPlayer.getPin())
			{	
				/// TO BE CHANGED!!!!!!!!!!
				myPlayer = getMe(); /// Patch
				myPlayer.setMovesAllowed(true);/// Patch
				getMe().setMovesAllowed(true);
				System.out.println( "Movement phase: consec... ="+ consecutiveNoMovementCounterPlayer1);
				System.out.println( "my player is: " + getMe() + "  CSPlayer is: " + myPlayer);

			}


			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				pStat.setCommunicationAllowed(false);
			}
			myPlayer.setMovesAllowed(true); // Allow to move only the initial CS player and NOT the SP players
		}


		// Strategy prepreration phase
		if (phaseName.equals("Strategy Prep Phase"))
		{
			int entry = 0;
			gpos0 = client.getGameStatus().getBoard().getGoalLocations().get(0); // get first goal
			// in list
			gpos1 = client.getGameStatus().getBoard().getGoalLocations().get(1); // get second goal
			// in list


			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				pStat.setGameRound(0);
				//gs.sendGamePlayersChangedMessage(gs.getPlayers(), pStat.getPerGameId());
				if ((pStat.getPosition().equals(gpos0)) || 
						(pStat.getPosition().equals(gpos1)))
				{
					communicationAllowed[i] = true;
					movementAllowed[i] = false; // The SP players should never move
					proposersInGame[entry] = pStat;
					entry++;
					client.getGameStatus().getPlayerByPerGameId(i).setRole("Proposer");
					pStat.setHisNumDormantRounds(
							consecutiveNoMovementCounterPlayer1);
					pStat.setMyNumDormantRounds(
							consecutiveNoMovementCounterPlayer1);
				}
				else {
					communicationAllowed[i] = false;
					movementAllowed[i] = true; // The CS can move whenever he has enough chips to move with...
					respondersInGame[0] = pStat;
					respondersInGame[1] = pStat;
					client.getGameStatus().getPlayerByPerGameId(i).setRole("Responder");
					CSplayer=client.getGameStatus().getPlayerByPerGameId(i);
					myPlayer = pStat;
					myPlayer.setMyNumDormantRounds(
							consecutiveNoMovementCounterPlayer1);
					myPlayer.setHisNumDormantRounds(
							consecutiveNoMovementCounterPlayer1);
				}
			}	
		}

	}

	private int chooseGoalRandonmly(){
		int num=(int)(Math.random()*2);
		return num;

	}

	private RowCol getNextPos() {
		RowCol curPos=getMe().getPosition();
		String currentColor=client.getGameStatus().getBoard().
				getSquare(getMe().getPosition()).getColor();
		if(currentColor.equals("CTGreen")){
			//	return new RowCol(curPos.row, curPos.col-1);
			Random rand=new Random();
			int choice=rand.nextInt(2);
			switch (choice) {
			case 0:
				return new RowCol(curPos.row, curPos.col+1);
			case 1:
				return new RowCol(curPos.row, curPos.col-1);

			default:
				break;
			}
		}
		int nextCol=curPos.col;
		int nextRow=curPos.row-1;
		String nextColor=client.getGameStatus().getBoard().
				getSquare(nextRow,nextCol).getColor();
		if(client.getGameStatus().getBoard().
				getSquare(nextRow,nextCol).getColor().equals("CTPurple")){
			if(hasChipWithColor("CTPurple")){
				return new RowCol(nextRow, nextCol);
			}
		}

		if(currentColor.equals("CTPurple")){
			int closestGoal=closetsGoal(getMe().getPosition(),getMe().getChips());
			switch (closestGoal) {
			case 0:
				return new RowCol(curPos.row, curPos.col-1);
			case 1:
				return new RowCol(curPos.row, curPos.col+1);
			default:
				//break tie
				int choice=(int)(Math.random()*2);
				switch (choice) {
				case 0:
					return new RowCol(curPos.row, curPos.col+1);
				case 1:
					return new RowCol(curPos.row, curPos.col-1);

				default:
					break;
				}
			}
		}
		if(currentColor.equals("CTYellow")){
			if(nextColor.equals("CTYellow")){
				if(hasChipWithColor("CTYellow")){
					return new RowCol(nextRow, nextCol);
				}
			}
			nextRow++;
			nextCol++;
			nextColor=client.getGameStatus().getBoard().
					getSquare(nextRow,nextCol).getColor();
			if(nextColor.equals("CTYellow")){
				if(hasChipWithColor("CTYellow")){
					return new RowCol(nextRow, nextCol);
				}
			}
		}

		if(currentColor.equals("grey78")){
			if(nextColor.equals("grey78")){
				if(hasChipWithColor("grey78")){
					return new RowCol(nextRow, nextCol);
				}
			}
			nextRow++;
			nextCol--;
			nextColor=client.getGameStatus().getBoard().
					getSquare(nextRow,nextCol).getColor();
			if(nextColor.equals("grey78")){
				if(hasChipWithColor("grey78")){
					return new RowCol(nextRow, nextCol);
				}
			}
		}
		return null;
	}

	private boolean hasChipWithColor(String nextCOlor){
		if (getMe().getChips().getNumChips(nextCOlor)>0)
			return true;
		return false;
	}

	private Path getCheapestPath(ArrayList<Path> paths) {
		Path bestPath=null;
		int bestWeight=Integer.MAX_VALUE;
		for (Path path : paths)
		{
			int weight=path.getWeight();
			if(weight<bestWeight){
				bestPath=path;
				bestWeight=weight;
			}
		}
		return bestPath;
	}

	private Path findAvailPath(ChipSet chipsCS,RowCol position,PlayerStatus SP){
		RowCol dest = SP.getPosition();
		ArrayList<Path> paths = ShortestPaths.getShortestPaths(position, dest, client.getGameStatus().getBoard(), this.m_Scoring, ShortestPaths.NUM_PATHS_RELEVANT, chipsCS,SP.getChips());
		for (Path path : paths) {
			ChipSet reqierd=path.getRequiredChips(client.getGameStatus().getBoard());
			if(chipsCS.getMissingChips(reqierd).isEmpty())
				return path;
		}
		return null;
	}


	private ArrayList<Path> findAllAvailPath(ChipSet chipsCS,RowCol position,PlayerStatus SP){
		RowCol dest = SP.getPosition();
		ArrayList<Path> ans=new ArrayList<Path>();
		ChipSet css=new ChipSet(getMe().getChips());
		css.addChipSet(SP.getChips());
		css=chipsCS;
		ArrayList<Path> paths = //ShortestPaths.getShortestPaths(position, dest, client.getGameStatus().getBoard(), this.m_Scoring, ShortestPaths.NUM_PATHS_RELEVANT, chipsCS,SP.getChips());
				ShortestPaths.getShortestPathsForMyChips(position, dest, client.getGameStatus().getBoard(),this.m_Scoring, ShortestPaths.NUM_PATHS_RELEVANT, css);
		for (Path path : paths) {
			ChipSet reqierd=path.getRequiredChips(client.getGameStatus().getBoard());
			if(chipsCS.getMissingChips(reqierd).isEmpty())
				ans.add(path);
		}
		return paths;
	}
	private int getDistance(ChipSet chipsCS,RowCol position,PlayerStatus SP){
		Path p=getCheapestPath(findAllAvailPath(chipsCS, position, SP));
		if(p==null)
			return Integer.MAX_VALUE;
		return p.getPoints().size();
	}
}
