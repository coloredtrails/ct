//////////////////////////////////////////////////////////////////////////
// File:		ThreePlayersAgentPlayerLearningSP 					   				//
// Purpose:		Implements the core of the ThreePlayersAgent agent.				//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.ThreePlayersAgent;

//Imports from Java common framework
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.lang.Math;



//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Phases;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.Constants;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.agent.events.RoleChangedEventListener;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.alternateOffersAgent.SimplePlayer;
import ctagents.alternateOffersAgent.ThreePlayersAgent.ThreePlayersAgentPlayer.Culture;
import ctagents.FileLogger;

import java.lang.Boolean;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;


//////////////////////////////////////////////////////////////////////////
// Class:		ThreePlayersAgent						    				//
// Purpose:		Implements the ThreePlayersAgent player.							//
//////////////////////////////////////////////////////////////////////////
public class ThreePlayersAgentPlayerLearningSP extends SimplePlayer implements RoleChangedEventListener
{
	private enum PathType {
		MY_PATH, MIDDLE, OTHER_PATH, MAX_PATH
	};
	static int NO_GENEROSITY = 555; // default value that indicates that there is no possible generosity
	static int ACCEPTED_GENEROSITY = 550; // value that indicates that the generosity exist. Therefore, the incoming proposal should be accepted
	static int REJECTED_GENEROSITY = 560; // value that indicates that the generosity does not exist. Therefore, the incoming proposal should be rejected
	static int MAX_NUM_OF_GENEROSITIES_IN_ARRAY = 50;
	static int NUMBER_OF_MINIMAL_CHIPS = 10; // We need at least 10 chips (10*5=50 points) to leave in our chip set 
		//to ensure that if the CS will reach our goal, the final score will be at  least 200 as was in the initial score (50+150 bonus of RG)
	static int MAX_ROUNDS = 100; // Actually the number of rounds in the game is much lower....	
	static int[ ][ ][] GenerosityVsPathMatrx=new int[MAX_ROUNDS][PathType.MAX_PATH.ordinal()][MAX_NUM_OF_GENEROSITIES_IN_ARRAY];

	private boolean respondToSendWasActivated=false;
	private int choosenOpponentID;

	Proposal proposer0innerProposal;
	Proposal proposer1innerProposal;

	private Culture oppCulture;
	private String cultureFolder = "agents/ctagents/alternateOffersAgent/ThreePlayersAgent/";

	// Saves whether this is the first communications round
	private boolean m_FirstCommunicationsRound;

	// Saves the last accepted proposal
	private Proposal m_LastAcceptedProposal;


	// Saves the scoring
	private Scoring m_Scoring;

	// Debugging related members
	private static long currTS=(new Date()).getTime(); 
	private static final FileLogger s_Logger = FileLogger.getInstance("ThreePlayersAgentPlayer"+currTS);
	private static final boolean s_IsDebug = true;

	public static final int NUMBER_OF_PLAYERS = 3;

	// Number of rounds to play, each round we switch roles
	int numOfRounds = 2;
	int roundInTheGame = 0,  lineNum = 0, chosenLine = 0;
	// int DISCOUNT_FACTOR_FOR_EACH_ROUND = 5;
	String CurrentPhaseName = null;
	boolean ended = false;
	int initialDormant = 0;
	int consecutiveNoMovementCounterPlayer1 = initialDormant;
	int numberofMoves = 0;

	String replyString0 = "UNKNOWN";
	String replyString1 = "UNKNOWN";

	// Number of counter offers allowed in each round
	static final int maxNumOfCounterOffers = 1;
	int counterOffersCount = 0;
	RowCol CSPlayerPosition = null;
	boolean CSPlayerReachedGoal = false;


	boolean[] communicationAllowed = new boolean[NUMBER_OF_PLAYERS];

	boolean[] movementAllowed = new boolean[NUMBER_OF_PLAYERS];


	private static PrintWriter csvLog;
	private String csvLogName = "CT_DbgLog_Agent_"+ (new Date()).getTime();
	private String proposal0String4Log = null;
	private String proposal1String4Log = null;
	private String reply0String4Log = null;
	private String reply1String4Log = null;
	private String gameStatusString4Log = null;

	DecimalFormat df;
	RowCol gpos0;
	RowCol gpos1;


	// gs is a member variable of GameConfigDetailsRunnable,
	// defined as: protected ServerGameStatus gs;
	/**
	 * The scoring function used for players in the game. 150 for a player (instead of 100)
	 * reaching the goal, -10 per unit distance if the player does not reach the
	 * goal, 5 for each chip remaining after the player has reached the goal or
	 * cannot move any farther towards the goal.
	 */
	Scoring s = new Scoring(150, 0, 5);

	/** Local random generator for creating chipsets */
	static Random localrand = new Random();

	/** determines if there will be automatic movement */
	boolean automaticMovement = false; 
	/**
	 * determines if the chips will automatically transfer after a proposal has
	 * been accepted
	 */
	boolean automaticChipTransfer = true; 
	/** determines if the phases will loop. */
	boolean phaseLoop = true;// false;

	PlayerStatus[] proposersInGame = new PlayerStatus[2];
	PlayerStatus[] respondersInGame = new PlayerStatus[2];
	PlayerStatus myPlayer;
	PlayerStatus CSplayer;
	int acceptReplyCounter = 0;
	int rejectReplyCounter = 0;
	int CSPlayerRcvdProposalsCounter = 0;


	/* Store the Proposal Messages sent by the two proposers.
	 * null if no proposal received.
	 */
	BasicProposalDiscourseMessage proposer0Proposal = null;
	BasicProposalDiscourseMessage proposer1Proposal = null;

	/* Store whether or not the proposals have been accepted.
	 * null means no proposal has been accepted.
	 */
	Boolean acceptedProposer0;
	Boolean acceptedProposer1;

	/*will hold the output of the scores*/
	FileWriter out;


	public void resetPermissionFlags() {
		for (int i=0; i < NUMBER_OF_PLAYERS; i++)
		{
			communicationAllowed[i] = false;
			movementAllowed[i] = false;
		}
	}


	public void setPermissions() {

		System.out.println( "entering setPermissions");

		PlayerStatus pStat;

		for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
			pStat = client.getGameStatus().getPlayerByPerGameId(i);
			pStat.setCommunicationAllowed(communicationAllowed[i]);
			pStat.setMovesAllowed(movementAllowed[i]);
		}
	}

	/* When the SP are the proposers the CS will be in the two entries of the responder;
	 * When the SP are the responders, the CS will be in the two entries of the proposer
	 */
	public void swapRoles() {
		PlayerStatus temp1, temp2;

		System.out.println( "entering swapRoles");


		temp1 = proposersInGame[0];
		temp2 = proposersInGame[1];
		proposersInGame[0] = respondersInGame[0];
		proposersInGame[1] = respondersInGame[1];
		respondersInGame[0] = temp1;
		respondersInGame[1] = temp2;
		// Now, update the role in the players' status
		for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
		{
			if (client.getGameStatus().getPlayerByPerGameId(i).getRole() == "Proposer"){
				client.getGameStatus().getPlayerByPerGameId(i).setRole("Responder");
				communicationAllowed[i] = false;
			}
			else { // was the responder
				client.getGameStatus().getPlayerByPerGameId(i).setRole("Proposer");
				communicationAllowed[i] = true;
			}
		}
		setPermissions();
	}


	/**
	 * Returns score of specified player, according to player's current state
	 */
	public int getPlayerScore(PlayerStatus ps) {

		int sc0 = (int) Math.floor(s.score(ps, gpos0)); // should change to double

		int sc1 = (int) Math.floor(s.score(ps, gpos1)); // should change to double

		if (!(ps.getPosition().equals(myPlayer.getPosition()))) // i.e the CSPlayer didn't arrive to the goal
		{
			sc0 -= s.goalweight;
			sc1 -= s.goalweight;
		}

		if (sc0 >= sc1)												
			return sc0;
		else
			return sc1;
	}

	/**
	 * Called by GameConfigDetailsRunnable methods when calculation and
	 * assignment of player scores is desired
	 */
	protected void assignScores() {
		for (PlayerStatus ps : client.getGameStatus().getPlayers()) {
			ps.setScore(getPlayerScore(ps));
		}
	}

	public int getMaxNonConsecRounds() {
		return numOfRounds;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		logMsg													//
	// Purpose:		Logs a new message to the log file.						//
	// Parameters:	@ message - The message to log.							//
	// Remarks:		* Logs only when debug mode is activated.				//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static void logMsg(String message)
	{
		// Write a new line with the message if we're in debug mode
		if (s_IsDebug)
		{
			s_Logger.writeln(message);
		}
	}
	static int MatrixNumOfLines = 0;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		readFileIntoMatrix										//
	// Purpose:		read the csvfile, and update the matrix accordingly.	//
	//////////////////////////////////////////////////////////////////////////
	private static void readFileIntoMatrix(String csvfile)
	{
		System.out.println("Processing file: " + csvfile);
		for (int i = 0; i < MAX_ROUNDS; i++)
			for (int j = 0; j < PathType.MAX_PATH.ordinal();j++)
				for (int l = 0; l < MAX_NUM_OF_GENEROSITIES_IN_ARRAY; l++)
					GenerosityVsPathMatrx[i][j][l] = NO_GENEROSITY;
		
		
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(csvfile));
			
			// Processing the log lines
			String []tabs;
			String line;
			while ((line = br.readLine()) != null) {
				tabs=line.split(",");
				int idx = 0;
				int numOfValues = 0;
				for (int j=0; j < PathType.MAX_PATH.ordinal();j++){
					numOfValues = Integer.parseInt(tabs[idx]);
					for (int l=0; l< numOfValues; l++) {
						idx++;
						GenerosityVsPathMatrx[MatrixNumOfLines][j][l] = Integer.parseInt(tabs[idx]);
					}
					idx++;
				}
				MatrixNumOfLines++;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		for (int i = 0; i < MatrixNumOfLines; i++)
			for (int j = 0; j < PathType.MAX_PATH.ordinal();j++) {
				System.out.println(j);
				for (int l = 0; GenerosityVsPathMatrx[i][j][l] != NO_GENEROSITY; l++)
					System.out.print(","+GenerosityVsPathMatrx[i][j][l]);
			System.out.println();
			}
		
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		ThreePlayersAgentPlayer constructor								//
	// Purpose:		Initializes the ThreePlayersAgentPlayer agent.							//
	//////////////////////////////////////////////////////////////////////////
	public ThreePlayersAgentPlayerLearningSP(int AgentRoleIn, int boardType, int culture, String csvfilename)
	{
		super();
		logMsg("ThreePlayersAgentPlayerLearningSP(): entering.");

		// We are a listener for the role changing event
		client.addRoleChangedEventListener(this);

		// Getting the opponents culture
		if (culture == Culture.ISRAEL.ordinal()) {
			oppCulture = Culture.ISRAEL;
		} else if (culture == Culture.USA.ordinal()) {
			oppCulture = Culture.USA;
		} else if (culture == Culture.LEBANON.ordinal()) {
			oppCulture = Culture.LEBANON;
		}

		// Updating the culture folder name
		cultureFolder += oppCulture.toString();
		// The first round of communications
		this.m_FirstCommunicationsRound = true;
		// Initialize the scoring
		this.m_Scoring = null;

		// Initialize the last transfer
		
		readFileIntoMatrix(csvfilename);
		
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getMe													//
	// Purpose:		Returns my player status.								//
	// Returns:		My player status.										//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private PlayerStatus getMe()
	{

		// Done easily using the client game status
		return client.getGameStatus().getMyPlayer();
	}

	private PlayerStatus getCSPlayer()
	{
		CSplayer=client.getGameStatus().getPlayerByPerGameId(CSplayer.getPerGameId());
		return this.CSplayer;
	}

	private PlayerStatus getSPPlayerUsingGoalPos(int row, int col){
		RowCol goalPos = new RowCol(row,col);
		
		for (PlayerStatus playerStatus : client.getGameStatus().getPlayers()){
			if(playerStatus.getPosition().equals(goalPos)) return playerStatus;
		}
		return null;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		createProposal											//
	// Purpose:		Generates a proposal.									//
	// Returns:		The best simulated proposal.							//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private Proposal createProposal()
	{
		// Initialize
		this.m_LastAcceptedProposal = null;
		System.out.println( "entering createProposal");
	
		PlayerStatus proposeTo=getCSPlayer();

		Proposal proposal=getBestOffer();
		if(proposal==null){
			proposal=getEmptyProposal();
		}
		else{
			System.out.println("CCCCCCCCCCCCCC "+proposal.chipsToReceive+" "+proposal.chipsToSend);
		}
		choosenOpponentID = proposeTo.getPerGameId();
		return proposal;
	}


	private Proposal getEmptyProposal() {
		ChipSet reply=new ChipSet();
		RowCol rowCol=getMe().getPosition();
		RowCol OtherrowCol=new RowCol(rowCol.row,8-rowCol.col);
		String whatColorHeWants=client.getGameStatus().getBoard().getSquare(OtherrowCol).getColor();
		ChipSet offer=new ChipSet();
		offer.add(whatColorHeWants,0);
		reply.add("CTRed", 0);
		reply.add("CTPurple", 0);
		return new Proposal(reply,offer);
	}

	boolean isGameInCommitState()
	{
		ChipSet bothChipSetsSPg=ChipSet.addChipSets(getSPPlayerUsingGoalPos(0,0).getChips(), getCSPlayer().getChips());

		ChipSet bothChipSetsSPy=ChipSet.addChipSets(getSPPlayerUsingGoalPos(0,8).getChips(), getCSPlayer().getChips());
		System.out.println("bothChipSetsSPg = "+bothChipSetsSPg+" bothChipSetsSPy +"+bothChipSetsSPy);
		if (isReachble(getCSPlayer().getPosition(), getSPPlayerUsingGoalPos(0,0), bothChipSetsSPg) == false)
			return (true); // nothing for nothing in case the game is in a commitment state
		if (isReachble(getCSPlayer().getPosition(), getSPPlayerUsingGoalPos(0,8), bothChipSetsSPy) == false)
			return (true); // nothing for nothing in case the game is in a commitment state
		return (false);
	}
	
	
	private PathType getPath(RowCol rowCol)
	{
		PathType whichPath = PathType.MY_PATH;
		if (rowCol.col == 4)
			whichPath = PathType.MIDDLE;
		else if (rowCol.col < 4)
			whichPath = PathType.OTHER_PATH;
		
		System.out.println("rowCol = "+rowCol+" whichPath ="+whichPath);
		return (whichPath);
	}
	
	// This function checks the matrix with generosity per path per round. 
	private int getGenerosity(int procType, int incomingGen, PathType whichPath, ChipSet myChips, ChipSet csChips)
	{
		int chosen_generosity = NO_GENEROSITY;
		System.out.println("getGenerosity: incomingGen ="+incomingGen+"whichPath = "+whichPath+" myChips = "+myChips+ " csChips = "+csChips);
		
		System.out.println("lineNum is "+lineNum);
		chosenLine = lineNum;
		if (procType ==1) {
			if (incomingGen >= 0) // Always accept positive generosity, i.e., that the CS request for less or equal chips than what he offers to send
				return (ACCEPTED_GENEROSITY);
			if ((myChips.getNumChips()+incomingGen) < NUMBER_OF_MINIMAL_CHIPS) // Always reject in case of leasen the SPy chips than the minumum
				return (REJECTED_GENEROSITY);
		}
		else // If in any case, only minimal chips were left, there is nothing to propose/accept.
			if (myChips.getNumChips() < NUMBER_OF_MINIMAL_CHIPS)
				return (NO_GENEROSITY);
		// If there is NO possible generosity, look for possible generosities in the second round, within the same path
		if (GenerosityVsPathMatrx[chosenLine][whichPath.ordinal()][0] == NO_GENEROSITY)
		{
			System.out.println("in the cur round it's None. chosenLine =" +
					chosenLine+" MatrixNumOfLines = "+MatrixNumOfLines);
			
			if ((lineNum+2) >= MatrixNumOfLines)// i.e., there are no "next lines/rounds in the matrix
				// we take "+2" 'cause odd rounds are for the SP proposals.
				return (NO_GENEROSITY);
			else
			{
			// If ALSO in the next round there is NO possible Generosities, return NO_GENEROSITY.
			if (GenerosityVsPathMatrx[lineNum+2][whichPath.ordinal()][0] == NO_GENEROSITY)
			{
				System.out.println("Also in the next round, it's NONE");
				return (NO_GENEROSITY);
			}
			else
			{
				chosenLine = lineNum+2; // We'll look on the next SP line in the file 
			}
			} // end else
		}

		int i=0; int j = 0; int genVal = 0;
		// i.e., the SP has the generosity chip set within his chip set
		if (procType == 0) { // i.e. handle proposing a proposal
		int [] actualPossibleGenValues = new int[MAX_NUM_OF_GENEROSITIES_IN_ARRAY]; // This variable will hold the generosities that actually are possible to offer, 
		
		while (( genVal = GenerosityVsPathMatrx[chosenLine][whichPath.ordinal()][i]) != NO_GENEROSITY)
		{
			if ((myChips.getNumChips()-genVal) >= NUMBER_OF_MINIMAL_CHIPS) // Take only those genVal (if any) that will leave you at least the minimal chips
			{
				actualPossibleGenValues[j] = genVal;
				System.out.print("actualPossibleGenValues = "+actualPossibleGenValues[j]);
				j++;
			}
			i++;
		}
		if (j==0) // There are no possible generosity values either because there are not in the file, 
			// or because the SP doesn't have the chips 
		{
			return (NO_GENEROSITY);
		}
		actualPossibleGenValues[j] = NO_GENEROSITY; // Indicates the last entry of this array 
		// Choosing randomly from the array of actual possible generosities
		int chosenGenerosityIdx = localrand.nextInt(j);		
		chosen_generosity = actualPossibleGenValues[chosenGenerosityIdx];
		
		System.out.println("j="+(j)+" chosen idx = "+chosenGenerosityIdx+" chosen_generosity = "+chosen_generosity);
		return chosen_generosity;
		}
		else { //the procedure is to respond to an incoming proposal 
			System.out.println("i="+(i)+" incomingGen = "+incomingGen);
			if ((myChips.getNumChips()-incomingGen) < NUMBER_OF_MINIMAL_CHIPS)
				return (REJECTED_GENEROSITY); // Reject a proposal that asked number of chips that will cause the SP to leave with less than the minimum
			// In this array, the 1st index will indicate how many values of generosities within the "Acceptance";
			// Then will appear all the possible generosities to accept;
			// Then will appear the number of Value that indicate how many values of generosities within the "rejection";
			// Then will appear all the possible generosities to reject, i.e., these generosities exist in the data, but they are rejected since their final avg score is very low.
			int numOfGen2Accept = GenerosityVsPathMatrx[chosenLine][whichPath.ordinal()][0];
			int numOfGen2Reject = GenerosityVsPathMatrx[chosenLine][whichPath.ordinal()][numOfGen2Accept+1];
			System.out.println("numOfGen2Accept = "+numOfGen2Accept+"numOfGen2Reject ="+numOfGen2Reject);
			boolean possible2Accept = false;
			if (numOfGen2Accept != NO_GENEROSITY)
			{
				i = 1;
				for (int idx = 0; idx < numOfGen2Accept; idx++, i++) // the i starts at 1 'cause idx 0 already used.
				{
					genVal = GenerosityVsPathMatrx[chosenLine][whichPath.ordinal()][i];
					System.out.println("i="+(i)+" genVal = "+genVal);
					if (incomingGen == genVal)
					{
						return (ACCEPTED_GENEROSITY);
					}
					else if (incomingGen == (genVal+1)) // smoothing the data. Handle these cases that don't exist in the data
					{
						System.out.println("incomingGen == (genVal-1)");
						possible2Accept = true;
					}
				}
				if (numOfGen2Reject != NO_GENEROSITY) // Check there are values in the data for rejection
				{
				i = numOfGen2Accept+2; 
				for (int idx=0; idx < numOfGen2Reject; idx++,i++)
				{
					genVal = GenerosityVsPathMatrx[chosenLine][whichPath.ordinal()][i];
					System.out.println("i="+(i)+" genVal = "+genVal);
					if (incomingGen == genVal) // Check if the incomingGen exist in the "Rejection" data 
					{
						return (REJECTED_GENEROSITY);
					}
				}
				}
				if (possible2Accept == true) // If the incomingGen was in epsilon of the "Accepted" data
					return (ACCEPTED_GENEROSITY);
				else
					return (REJECTED_GENEROSITY); // The incomingGen is not even in the epsilon. Reject.
			}
			else
				return (REJECTED_GENEROSITY); // There is no value for acceptance
		}
		
	}
	
	private Proposal getBestOffer(){
		PlayerStatus csPlayer=getCSPlayer();
		ChipSet csChips=csPlayer.getChips();
		PlayerStatus proposeTo=getMe();
		ChipSet myChips=proposeTo.getChips();
		Proposal bestProposal=null;
		RowCol rowCol=csPlayer.getPosition();
		
		// First of all, it's needed to check if the game is in a state of commitment.
		// If so, propose "nothing for nothing offer"
		
		if (isGameInCommitState() == true)
		{
			System.out.println("the Game is in a commitment state");
			return (null); 
		}
		
		PathType whichPath = getPath(rowCol);
		
		int generosity = getGenerosity(0, NO_GENEROSITY, whichPath, myChips, csChips);
		if (generosity == NO_GENEROSITY)
		{
			System.out.println ("generosity = "+generosity);
			return (null); //therefore, the offer to propose will be nothing for nothing offer
		}
		int redChips=myChips.getNumChips("CTRed");
		int myPurpleChips = myChips.getNumChips("CTPurple");
		int csPurpleChips = csChips.getNumChips("CTPurple");
		int csGreyChips = csChips.getNumChips("grey78");
		int csRedChips = csChips.getNumChips("CTRed");
		
		// check how many purples the CS has. 
		// and send him purples number in case of PATH!= MY_PATH: 7[the longest bridge]-the purples the CS already has
		// check how many purples the CS has. 
		// and send him purples number: 7[the longest bridge]-the purples the CS already has
		int requestedPurples = 7 - csPurpleChips;

		if (generosity <= 0) // Check if the chosen generosity is negative
		{
			generosity = Math.abs(generosity); // Change it to positive number 
			ChipSet chips2Send =new ChipSet();		
			chips2Send.add("CTRed", 0);
			ChipSet chips2Rcv =new ChipSet();
			if (csGreyChips >= generosity)
				chips2Rcv.add("grey78",generosity); /// ???? Decide on this case
			else // there is not enough grey chips
			{
				if (csRedChips >= generosity)
					chips2Rcv.add("CTRed",generosity); /// ???? Decide on this case)
				else // Maybe send part of this and part of that color???? 
					return null;
			}
			bestProposal = new Proposal(chips2Rcv, chips2Send);
		}
		else if ((whichPath == PathType.MY_PATH) || ((whichPath == PathType.MIDDLE) && (lineNum == 0)))
		{
			if (generosity <= redChips)
			{
			ChipSet chips2Send =new ChipSet();
			chips2Send.add("CTRed", generosity);
			ChipSet chips2Rcv =new ChipSet();
			chips2Rcv.add("grey78",0);
			bestProposal = new Proposal(chips2Rcv, chips2Send);
			}
			else
			{
				// If I don't have enough RED chips to send, try to send a proposal of purple & red chips
				if (redChips+myPurpleChips > generosity)
				{
					ChipSet chips2Send =new ChipSet();
					chips2Send.add("CTRed", redChips);
					chips2Send.add("CTPurple", generosity-redChips);
					ChipSet chips2Rcv =new ChipSet();
					chips2Rcv.add("grey78",0);
					bestProposal = new Proposal(chips2Rcv, chips2Send);
				}
				else // There is not enough purple+red chips to send (it can happen that myWhole chips is greater than
					// generosity if I have also yellow or grey chips,. but we don't handle these cases. 
					// We will send only RED and or PURPLE Chips  
					return (null);
			}
		}
		else // the path is MIDDLE or OTHER_PATH
		{
			if (isReachble(getCSPlayer().getPosition(), getSPPlayerUsingGoalPos(0,8), csChips) == true)  // i,e,the CS already has the chips to reach my goal, therfore I'll propose ONLY red chips
			{
				if (generosity <= redChips)
				{
				ChipSet chips2Send =new ChipSet();
				chips2Send.add("CTRed", generosity);
				ChipSet chips2Rcv =new ChipSet();
				chips2Rcv.add("grey78",0);
				bestProposal = new Proposal(chips2Rcv, chips2Send);
				}
				else
				{
					// If I don't have enough RED chips to send, try to send a proposal of purple & red chips
					if (redChips+myPurpleChips > generosity)
					{
						ChipSet chips2Send =new ChipSet();
						chips2Send.add("CTRed", redChips);
						chips2Send.add("CTPurple", generosity-redChips);
						ChipSet chips2Rcv =new ChipSet();
						chips2Rcv.add("grey78",0);
						bestProposal = new Proposal(chips2Rcv, chips2Send);
					}
					else // There is not enough purple+red chips to send (it can happen that myWhole chips is greater than
						// generosity if I have also yellow or grey chips,. but we don't handle these cases. 
						// We will send only RED and or PURPLE Chips  
						return (null);
				}
			} // The CS can't reach my goal with the chips he has	
			else 
			{
				if (whichPath == PathType.MIDDLE)
				{
					if (rowCol.row == 1)
						requestedPurples = 3; // In the longest bridge, if the CS is in the middle, only 3 purples are needed to send him to reach MY_PATH
					else if (rowCol.row == 3)
						requestedPurples = 1; // In the shortest bridge, if the CS in the middle, only one purple chip is needed to send him to reach MY_PATH
				}
				else // the path is other
				{
					if ((rowCol.row == 1) || (rowCol.row == 2))
						requestedPurples = 7; // In the longest bridge, if the CS is in the middle, only 3 purples are needed to send him to reach MY_PATH
					else if (rowCol.row >= 3)
						requestedPurples = 3; // In the shortest bridge, if the CS in the middle, only one purple chip is needed to send him to reach MY_PATH
				}
				requestedPurples = requestedPurples - csPurpleChips; 
				if (myPurpleChips >= requestedPurples)
				{
					if (myPurpleChips >= generosity)
					{
						ChipSet chips2Send =new ChipSet();
						chips2Send.add("CTPurple", generosity);
							
						ChipSet chips2Rcv =new ChipSet();
						chips2Rcv.add("grey78",0);
						bestProposal = new Proposal(chips2Rcv, chips2Send);
					}
					else
					{
					ChipSet chips2Send =new ChipSet();
					chips2Send.add("CTPurple", requestedPurples);
					chips2Send.add("CTRed", generosity-requestedPurples);
						
					ChipSet chips2Rcv =new ChipSet();
					chips2Rcv.add("grey78",0);
					bestProposal = new Proposal(chips2Rcv, chips2Send);
					}
				}
				else 
					return (null); // Nothing to propose.  I don't have enough purple chips to propose, so the CS won't be able to reach my goal
			}
		}
		
	return bestProposal;

	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		respondToSend										//
	// Purpose:		Responds to a proposal.									//
	// Parameters:	@ proposal - the proposal to respond to.				//
	// Returns:		A boolean which specifies whether to accept or reject.	//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public boolean responseToSend()
	{
		logMsg("responseToSend(): entering.");
		System.out.println( "entering responseToSend");

		if(!respondToSendWasActivated){
			respondToSendWasActivated=true;
			System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
			System.out.println(m_LastAcceptedProposal.chipsToReceive+" "+m_LastAcceptedProposal.chipsToSend);
			
			boolean goodProposal=checkProposal(this.m_LastAcceptedProposal);
			
			if(goodProposal){
				logMsg("responseToSend(): accepting proposal ");//(" + proposer0innerProposal.chipsToReceive + ", " + proposer0innerProposal.chipsToSend + ").");
				System.out.println("responseToSend(): accepting proposal ");//(" + proposer0innerProposal.chipsToReceive + ", " + proposer0innerProposal.chipsToSend + ").");
				System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
				BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(proposer0Proposal);
				this.m_LastAcceptedProposal = proposer0innerProposal;
				this.m_FirstCommunicationsRound = true;
				response.acceptOffer();
				client.communication.sendDiscourseRequest(response);
					
			}
			else{// No proposal is accepted. reject both
				logMsg("responseToSend(): rejecting proposal");
				System.out.println("responseToSend(): rejecting proposal");
				System.out.println("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
				BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(proposer0Proposal);
				response.rejectOffer();
				client.communication.sendDiscourseRequest(response);
			}
			proposersInGame[0].setCommunicationAllowed(false); // Enable the CS player to answer only to one proposal
		}
		return true;

	}
	//////////////////////////////////////////////////////////////////////////
	// Method:		respondToProposal										//
	// Purpose:		Responds to a proposal.									//
	// Parameters:	@ proposal - the proposal to respond to.				//
	// Returns:		A boolean which specifies whether to accept or reject.	//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private boolean respondToProposal(Proposal proposal)
	{
		logMsg("respondToProposal(): entering.");
		System.out.println( "entering respondToProposal");


		// Initialize
		this.m_LastAcceptedProposal = null;

		// Create a simulation of the current game status
		logMsg("respondToProposal(): my chips: (" + getMe().getChips() + ").");

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		onReceipt												//
	// Purpose:		Receives a discourse message.							//
	// Parameters:	@ discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void onReceipt(DiscourseMessage dm)
	{
		logMsg("onReceipt(): entering.");
		System.out.println( "entering onReceipt");
		//If we have a response message.
		if( dm instanceof BasicProposalDiscussionDiscourseMessage) {
			BasicProposalDiscussionDiscourseMessage bpddm = (BasicProposalDiscussionDiscourseMessage) dm;
			if (bpddm.accepted())
			{
				this.m_FirstCommunicationsRound = true;
				this.m_LastAcceptedProposal = new Proposal(bpddm.getChipsSentByResponder(), bpddm.getChipsSentByProposer());
			}
			else
			{
				this.m_FirstCommunicationsRound = !this.m_FirstCommunicationsRound;
				this.m_LastAcceptedProposal = null;
			}

			if (bpddm.getProposerID() != myPlayer.getPerGameId()) //i.e the proposer is the SP player 
			{
				if (bpddm.getResponderID() != myPlayer.getPerGameId()) // to be sure that for the SP proposing messages 
				{
					client.getGameStatus().getPlayerByPerGameId(bpddm.getProposerID()).setCommunicationAllowed(true);
					return; // result;
				}
			}

			boolean reply = bpddm.accepted();
			boolean endDiscussion = false;

			if (reply == true) {
				acceptReplyCounter++;
			}
			else {
				rejectReplyCounter++;
			}

			if (proposersInGame[0] == proposersInGame[1]) //i.e the proposer is the CS player 
			{
				if (rejectReplyCounter == 1)
					endDiscussion = true;
				if( bpddm.getResponderID() == respondersInGame[0].getPerGameId() ) {
					acceptedProposer0 = reply;
					proposer0Proposal=bpddm;
					if (acceptedProposer0 == true) replyString0 = "TRUE";
					else replyString0 = "FALSE";
				}
				if( bpddm.getResponderID() == respondersInGame[1].getPerGameId() ) {
					acceptedProposer1 = reply;
					proposer1Proposal=bpddm;
					if (acceptedProposer1 == true) replyString1 = "TRUE";
					else replyString1 = "FALSE";
				}
			}
			else // Not the same proposers
			{
				if (rejectReplyCounter == 2)
					endDiscussion = true;

				if( bpddm.getProposerID() == proposersInGame[0].getPerGameId() ) {
					acceptedProposer0 = reply;
					proposer0Proposal=bpddm;
					if (acceptedProposer0 == true) replyString0 = "TRUE";
					else replyString0 = "FALSE";
				}
				if( bpddm.getProposerID() == proposersInGame[1].getPerGameId() ) {
					acceptedProposer1 = reply;
					proposer1Proposal=bpddm;
					if (acceptedProposer1 == true) replyString1 = "TRUE";
					else replyString1 = "FALSE";
				}
			}

			//if there wasn't a proposal then it cannot be accepted.
			if( acceptedProposer0 == null )
			{
				acceptedProposer0 = false;
				replyString0 = "IGNORED";
			}
			if( acceptedProposer1 == null )
			{
				acceptedProposer1 = false;
				replyString1 = "IGNORED";
			}

			if ((proposer0Proposal == null) && (proposer1Proposal == null)){
				throw new RuntimeException( "Response message sent to unknown proposal.\n" );
			}
			if ((proposer0Proposal != null) &&( acceptedProposer0 == true)) {
				int propCS = getPlayerScore(proposersInGame[0]);
				int respCS = getPlayerScore(respondersInGame[0]);
				ChipSet proposer0toSend = proposer0Proposal.getChipsSentByProposer();
				ChipSet responderToSend = proposer0Proposal.getChipsSentByResponder();


				exchange( proposersInGame[0], respondersInGame[0], proposer0toSend, responderToSend );
				if (csvLog != null) {
					reply0String4Log = replyString0 + "," +propCS+ "," +respCS + "," +
							proposer0toSend + "," +responderToSend + "," + getPlayerScore(proposersInGame[0])+","+
							getPlayerScore(respondersInGame[0]);
				}
				proposer0Proposal = null;
				proposer0innerProposal=null;
				acceptedProposer0 = null;

			}
			//else, if only the proposal from Proposer 1 was accepted.
			else if ((proposer1Proposal != null) && ( acceptedProposer1 == true)) {
				int propCS = getPlayerScore(proposersInGame[1]);
				int respCS = getPlayerScore(respondersInGame[1]);
				ChipSet proposer1toSend = proposer1Proposal.getChipsSentByProposer();
				ChipSet responderToSend = proposer1Proposal.getChipsSentByResponder();

				exchange( proposersInGame[1], respondersInGame[1], proposer1toSend, responderToSend );

				if (csvLog != null) {
					reply1String4Log = replyString1 + "," +propCS+ "," +respCS + "," +
							proposer1toSend + "," +responderToSend + "," + getPlayerScore(proposersInGame[1])+","+
							getPlayerScore(respondersInGame[1]);
				}
				proposer1Proposal = null;
				proposer1innerProposal=null;
				acceptedProposer1 = null;

			}
			else { //case that we had one reject, and one accept
				//	getLog()
				//	.writeln( "case that we had one reject, and one accept" );            	
			}
			if ((acceptReplyCounter == 1) ||// ==1: to enable only ONE accept. == 2:only when there are 2 proposals and 2 answers 
					//(or when phase time expired, move on to the next phase 
					(endDiscussion == true))	
			{ 
				if (csvLog != null) {    
					if (proposer0Proposal != null) {
						reply0String4Log = replyString0 + "," +getPlayerScore(proposersInGame[0])+ "," 
								+getPlayerScore(respondersInGame[0])+",,,,";
					}
					if (proposer1Proposal != null) {
						reply1String4Log = replyString1 + "," +getPlayerScore(proposersInGame[1])+ "," 
								+getPlayerScore(respondersInGame[1])+",,,,";
					}
				}
				//client.getGameStatus().sendArbitraryMessage(Constants.ENDDISCUSSION);
				proposer0Proposal = null;
				proposer0innerProposal=null;
				proposer1Proposal = null;
				proposer1innerProposal=null;
				client.getGameStatus().getPhases().advancePhase();
			}   
		}
		///////////////////////////////////////////////////////////////////////////////////////////////
		//else, if we have a proposal message
		///////////////////////////////////////////////////////////////////////////////////////////////
		else if( dm instanceof BasicProposalDiscourseMessage) {
			BasicProposalDiscourseMessage bpdm = (BasicProposalDiscourseMessage) dm;
			if (proposersInGame[0] == proposersInGame[1]) // i.e, the same proposer in case of CS player, I'll check multi-cases using the responders
			{
				if( bpdm.getResponderID() == respondersInGame[0].getPerGameId()||bpdm.getResponderID() == respondersInGame[1].getPerGameId() ) {
					System.out.println(">>>>>>>>>>>>>>>>>>>> 0");
					//Make sure the responder doesn't rcv multiple proposals.
					if( proposer0Proposal != null ) {
						throw new RuntimeException( "Responder 0 has already rcv a proposal message this communication phase." );
					}
					proposer0Proposal = bpdm;
					if (csvLog != null) {
						proposal0String4Log = csvLogName+ "," + roundInTheGame + "," + client.getGameStatus().getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
								+client.getGameStatus().getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
								+(dm.getFromPerGameId() == myPlayer.getPerGameId()) + "," 
								+ bpdm.getChipsSentByProposer().removeZeros() + "," 
								+ bpdm.getChipsSentByResponder().removeZeros();
					}
					// The message is a proposal
					Proposal proposalToConsider = new Proposal(proposer0Proposal.getChipsSentByProposer(), proposer0Proposal.getChipsSentByResponder()); 
					logMsg("onReceipt(): opponent\'s proposal: (" + proposalToConsider.chipsToReceive + ", " + proposalToConsider.chipsToSend + ").");

					// Build a response to the proposal

					// If the proposal should be accepted by us
					respondToProposal(proposalToConsider);
					this.m_LastAcceptedProposal = proposalToConsider;
					this.m_FirstCommunicationsRound = true;
					responseToSend();

				}
				else{
					
				}
				proposersInGame[0].setCommunicationAllowed(false); /* Enable the CS player to send only one proposal */

			}
			else {
				/////////////////////////////////////////////////////////////
				// this means that the proposers are the service provider	
				// the proposers are different, check according to the proposers
				//If the proposal is from Proposer 0
				/////////////////////////////////////////////////////////////
				System.out.println("CSPlayer got a proposal");
				if( bpdm.getProposerID() == proposersInGame[0].getPerGameId() ) {
					//Make sure the proposer isn't sending multiple proposals.
					if( proposer0Proposal != null ) {
						//throw new RuntimeException( "Proposer 0 has already sent a proposal message this communication phase." );
					}
					else {
						CSPlayerRcvdProposalsCounter++;
						proposer0Proposal = bpdm;
						if (csvLog != null) {
							proposal0String4Log = csvLogName+  "," + roundInTheGame + "," + client.getGameStatus().getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
									+client.getGameStatus().getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
									+(dm.getFromPerGameId() == myPlayer.getPerGameId()) + "," 

            					+ bpdm.getChipsSentByProposer().removeZeros() + "," 
            					+ bpdm.getChipsSentByResponder().removeZeros();
						}
						// The message is a proposal
						Proposal proposalToConsider = new Proposal(proposer0Proposal.getChipsSentByProposer(), proposer0Proposal.getChipsSentByResponder()); 
						logMsg("onReceipt(): opponent\'s proposal: (" + proposalToConsider.chipsToReceive + ", " + proposalToConsider.chipsToSend + ").");
						proposer0innerProposal = proposalToConsider;


					}

				}
				//If the proposal is from Proposer 1
				else if( bpdm.getProposerID() == proposersInGame[1].getPerGameId() ) {
					if( proposer1Proposal != null ) {
						//Make sure the proposer isn't sending multiple proposals.
						//throw new RuntimeException( "Proposer 1 has already sent a proposal message this communication phase." );
					}
					else {
						CSPlayerRcvdProposalsCounter++;
						proposer1Proposal = bpdm;
						if (csvLog != null) {
							proposal1String4Log = csvLogName+  "," + roundInTheGame + "," + client.getGameStatus().getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
									+client.getGameStatus().getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
									+(dm.getFromPerGameId() == myPlayer.getPerGameId()) + "," 

            					+ bpdm.getChipsSentByProposer().removeZeros() + "," 
            					+ bpdm.getChipsSentByResponder().removeZeros();
						}

						// The message is a proposal
						Proposal proposalToConsider = new Proposal(proposer1Proposal.getChipsSentByProposer(), proposer1Proposal.getChipsSentByResponder()); 
						logMsg("onReceipt(): opponent\'s proposal: (" + proposalToConsider.chipsToReceive + ", " + proposalToConsider.chipsToSend + ").");
						proposer1innerProposal = proposalToConsider;

					}

				}
				

			}
		}
	}
	ScheduledFuture<?> scheduledFuture;

	/**
	 * Exchanges chips between agents.
	 * @param a A player that is to exchange chips with b.
	 * @param b A player that is to exchange chips with a.
	 * @param a_to_b The chips a wishes to send to b.
	 * @param b_to_a The chips b wishes to send to a.
	 */
	private static void exchange( PlayerStatus a, PlayerStatus b, ChipSet a_to_b, ChipSet b_to_a ) {
		//determine of the chips can be transfered
		boolean aCanSend = a.getChips().contains( a_to_b );
		boolean bCanSend = b.getChips().contains( b_to_a );

		System.out.println("enter exchange");
		//If the chips can be transfered, then transfer them.
		if( aCanSend && bCanSend ) {
			ChipSet aCS = ChipSet.subChipSets( a.getChips(), a_to_b );
			aCS = ChipSet.addChipSets( aCS, b_to_a );

			ChipSet bCS = ChipSet.subChipSets( b.getChips(), b_to_a );
			bCS = ChipSet.addChipSets( bCS, a_to_b );

			a.setChips( aCS );
			b.setChips( bCS );

			System.out.println("made the exchange");

		}
	}


	//////////////////////////////////////////////////////////////////////////
	// Method:		roleChanged												//
	// Purpose:		Called whenever a role has changed.						//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void roleChanged()
	{
		logMsg("roleChanged(): entering.");
		System.out.println( "entering roleChanged");


		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}

		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		logMsg("roleChanged(): current phase is \'" + phaseName + "\'.");

		// Communication phase 
		if (phaseName.equals("Communication Phase"))
		{
			String roleStr = getMe().getRole();
			logMsg("roleChanged(): new role is \'" + roleStr + "\'.");
			System.out.println("roleChanged(): new role is \'" + roleStr + "\'.");
			// If we are a proposer we should make a proposal
			if (roleStr.equals("Proposer"))
			{
				// Get my proposal
				Proposal myProposal = createProposal();

				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = choosenOpponentID;

				// Create the proposal message
				if(myProposal!=null){
					BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myProposal.chipsToSend, myProposal.chipsToReceive);
					client.communication.sendDiscourseRequest(proposal);

				}
			}
		}
	}

	Path p=null;
	
	private boolean checkProposal(Proposal proposal0){
		CSplayer=getCSPlayer();
		if(proposal0!=null){
			if (isGameInCommitState() == true)
			{
				System.out.println("Incoming message: the Game is in a commitment state");
				return (false); 
			}
			
			PathType whichPath = getPath(CSplayer.getPosition());
			// Since the proposer must ask for chips that we have 
			// (otherwise, he won't be able to press on the chips in the window proposal), 
			// it's NOT needed to check if the SP has these chips.
			System.out.println ("chipsToSend = "+proposal0.chipsToSend+"chipsToReceive"+proposal0.chipsToReceive.getNumChips());
			int generosity = getGenerosity(1, proposal0.chipsToReceive.getNumChips()-proposal0.chipsToSend.getNumChips(),
											whichPath, getMe().getChips(), CSplayer.getChips());
			System.out.println ("generosity = "+generosity);
			if (generosity == REJECTED_GENEROSITY)
			{
				return (false); //therefore, reject the incoming offer
			}
			else if (generosity == ACCEPTED_GENEROSITY)
			{
				return (true); //therefore, accept the incoming offer
			}
			else { // Unknown case
				System.out.println ("Unknown return value");
				return (false); //therefore, reject the incoming offer
			}
		}
		else {
			System.out.println("Incoming null proposal. ??????");
			return (false);
		}
}




	//////////////////////////////////////////////////////////////////////////
	// Method:		phaseAdvanced											//
	// Purpose:		A callback for phase advances.							//
	// Parameters:	@ discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void phaseAdvanced(Phases phases)
	{
		logMsg("phaseAdvanced(): entering.");
		System.out.println( "entering phaseAdvanced");

		PlayerStatus pStat;

		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}

		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		logMsg("phaseAdvanced(): new phase is \'" + phaseName + "\'.");

		if(phaseName.equals("Communication Phase")) {
			//	getLog().writeln(" numberofMoves in round "+roundInTheGame+" is "+numberofMoves);

			if (csvLog != null) {    

				if (proposer0Proposal != null) {
					if (reply0String4Log == null)
						reply0String4Log = "IGNORED" + "," +getPlayerScore(proposersInGame[0])+ "," 
								+getPlayerScore(respondersInGame[0])+ ",,,,";
					//	    			csvLog.println(proposal0String4Log+"," + "IGNORED" + "," + getPlayerScore(proposersInGame[0]) + ","
					//	    					+ getPlayerScore(respondersInGame[0]) + ",");
					//	    			csvLog.flush();
				}
				if (proposer1Proposal != null) {
					if (reply1String4Log == null)
						reply1String4Log = "IGNORED" + "," +getPlayerScore(proposersInGame[1])+ "," 
								+getPlayerScore(respondersInGame[1])+ ",,,,";
					//	     			csvLog.println(proposal1String4Log+"," + "IGNORED" + "," + getPlayerScore(proposersInGame[1]) + ","
					//	     					+ getPlayerScore(respondersInGame[1]) + ",");
					//	     			csvLog.flush();
				}
				gameStatusString4Log = numberofMoves + "," + myPlayer.getPosition().toString()+ ","  
						+ client.getGameStatus().getPlayerByPerGameId(0).getChips() + "," + getPlayerScore(client.getGameStatus().getPlayerByPerGameId(0))+ ","  
						+ client.getGameStatus().getPlayerByPerGameId(1).getChips() + "," + getPlayerScore(client.getGameStatus().getPlayerByPerGameId(1))+ ","  
						+ client.getGameStatus().getPlayerByPerGameId(2).getChips() + "," + getPlayerScore(client.getGameStatus().getPlayerByPerGameId(2));
				if (proposal0String4Log != null){
					csvLog.println(proposal0String4Log+"," + reply0String4Log+"," + gameStatusString4Log);
					csvLog.flush();
				}
				if (proposal1String4Log != null) {
					csvLog.println(proposal1String4Log+"," + reply1String4Log+"," + gameStatusString4Log);
					csvLog.flush();
				}
			}
			acceptReplyCounter = 0; // initialize the counter for the next negotiation phase
			rejectReplyCounter = 0;
			CSPlayerRcvdProposalsCounter = 0;
			respondToSendWasActivated=false;
			proposer0Proposal = null;
			proposer1Proposal = null;
			acceptedProposer0 = null;
			acceptedProposer1 = null;
			replyString0 = "UNKNOWN";
			replyString1 = "UNKNOWN";

			proposal0String4Log = null;
			proposal1String4Log = null;
			reply0String4Log = null;
			reply1String4Log = null;
			gameStatusString4Log = null;

			numberofMoves = 0;
			String roleStr = getMe().getRole();
			if (roleStr.equals("Proposer"))
			{
				// Get my proposal
				Proposal myProposal = createProposal();

				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = choosenOpponentID;

				// Create the proposal message
				if(myProposal!=null){
					BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myProposal.chipsToSend, myProposal.chipsToReceive);
					/*if(choosenOpponentID==0)
						proposer0Proposal=proposal;
					if(choosenOpponentID==1)
						proposer1Proposal=proposal;
					 */

					client.communication.sendDiscourseRequest(proposal);

				}
			}
			//roleChanged();

			//client.getGameStatus().sendArbitraryMessage(Constants.NEWPHASE);


			if (ended == false)
				++roundInTheGame; //only update the round for communicaton phase.
			for (PlayerStatus p : client.getGameStatus().getPlayers()) {
				p.setGameRound(roundInTheGame);
			}
		}
		//	getLog()
		//	.writeln( "---------- Beginning " + phaseName + " at round " + roundInTheGame + " ----------" );

		//Initialize communication, transfers and movement to false.
		//boolean communicationAllowed = false;

		if(phaseName.equals("Feedback Phase")){
			if(scheduledFuture!=null){
				scheduledFuture.cancel(true);
			}
			if (myPlayer.getPosition() == CSPlayerPosition) /* The CSPlayer remain in the same position and did not move */
				consecutiveNoMovementCounterPlayer1++;
			else{
				consecutiveNoMovementCounterPlayer1 = initialDormant;
				CSPlayerPosition = myPlayer.getPosition();
			}
			// update game status about dormant moves
			myPlayer.setMyNumDormantRounds(
					consecutiveNoMovementCounterPlayer1);
			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				// Update anyway... 
				// if (pStat != CSPlayer)
				pStat.setHisNumDormantRounds(consecutiveNoMovementCounterPlayer1);
			}

//			if (consecutiveNoMovementCounterPlayer1 >= numOfRounds) {
//					System.out.println("!!!!!!!!!bad if");
//				ended = true;
//			}

//			else 
			if ((myPlayer.getPosition().equals(gpos0))||
					(myPlayer.getPosition().equals(gpos1)))
			{
				ended = true;
			}
			else {
				// This way we can let each player know how many consecutive no
				// movements the OTHER has
				myPlayer.set("myConsecutiveNoMovement",
						consecutiveNoMovementCounterPlayer1);
			}
			if (ended == false) {
				swapRoles();

				setPermissions();
			}

		}


		// FYI - for the first phase it won't work from here
		else if(phaseName.equals("Communication Phase")) { //REQUESTS_PH

			//set communication, transfers, and moves of the agents.

			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				pStat.setCommunicationAllowed(communicationAllowed[i]);
			}
			myPlayer.setMovesAllowed(false);
		} 
		else if (phaseName.equals("Movement Phase")) {
			lineNum++;
			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				pStat.setCommunicationAllowed(false);
			}
			myPlayer.setMovesAllowed(true);
			System.out.println( "my player is: " + getMe() + "  CSPlayer is: " + myPlayer);
			if (getMe().getPin()==myPlayer.getPin())
			{	
				/// TO BE CHANGED!!!!!!!!!!
				myPlayer = getMe(); /// Patch
				myPlayer.setMovesAllowed(true);/// Patch
				getMe().setMovesAllowed(true);
				System.out.println( "Movement phase: consec... ="+ consecutiveNoMovementCounterPlayer1);
				System.out.println( "my player is: " + getMe() + "  CSPlayer is: " + myPlayer);

			}


			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				pStat.setCommunicationAllowed(false);
			}
			myPlayer.setMovesAllowed(true); // Allow to move only the initial CS player and NOT the SP players
		}


		// Strategy prepreration phase
		if (phaseName.equals("Strategy Prep Phase"))
		{
			int entry = 0;
			gpos0 = client.getGameStatus().getBoard().getGoalLocations().get(0); // get first goal
			// in list
			gpos1 = client.getGameStatus().getBoard().getGoalLocations().get(1); // get second goal
			// in list


			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = client.getGameStatus().getPlayerByPerGameId(i);
				pStat.setGameRound(0);
				//gs.sendGamePlayersChangedMessage(gs.getPlayers(), pStat.getPerGameId());
				if ((pStat.getPosition().equals(gpos0)) || 
						(pStat.getPosition().equals(gpos1)))
				{
					communicationAllowed[i] = true;
					movementAllowed[i] = false; // The SP players should never move
					proposersInGame[entry] = pStat;
					entry++;
					client.getGameStatus().getPlayerByPerGameId(i).setRole("Proposer");
					pStat.setHisNumDormantRounds(
							consecutiveNoMovementCounterPlayer1);
					pStat.setMyNumDormantRounds(
							consecutiveNoMovementCounterPlayer1);
				}
				else {
					communicationAllowed[i] = false;
					movementAllowed[i] = true; // The CS can move whenever he has enough chips to move with...
					respondersInGame[0] = pStat;
					respondersInGame[1] = pStat;
					client.getGameStatus().getPlayerByPerGameId(i).setRole("Responder");
					CSplayer=client.getGameStatus().getPlayerByPerGameId(i);
					myPlayer = pStat;
					myPlayer.setMyNumDormantRounds(
							consecutiveNoMovementCounterPlayer1);
					myPlayer.setHisNumDormantRounds(
							consecutiveNoMovementCounterPlayer1);
				}
			}	
		}

	}



	private Path findAvailPath(ChipSet chipsCS,RowCol position,PlayerStatus SP){
		RowCol dest = SP.getPosition();
		ArrayList<Path> paths = ShortestPaths.getShortestPaths(position, dest, client.getGameStatus().getBoard(), this.m_Scoring, ShortestPaths.NUM_PATHS_RELEVANT, chipsCS,SP.getChips());
		for (Path path : paths) {
			ChipSet reqierd=path.getRequiredChips(client.getGameStatus().getBoard());
			if(chipsCS.getMissingChips(reqierd).isEmpty())
				return path;
		}
		return null;
	}

	private boolean isReachble(RowCol position,PlayerStatus SP,ChipSet remainingChips){
		Path myP=findAvailPath(remainingChips, position, SP);
		return myP!=null;
	}
	


}
