:: LaunchAll.bat
:: 
:: Starts the server, human GUI, and agent
:: 

@echo off
SET /a agentpin=%1 + 10
echo %agentpin% 
echo %1 

:: SERVER
START /B java -jar dist/ct3.jar -s
CALL wait 3


:: HUMAN
START /B java -jar dist/ct3.jar -c ctgui.original.GUI --pin %1 --client_hostip localhost
CALL wait 5


:: AGENT
START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.palAgent.PalAgentFrontEnd %agentpin% 1
CALL wait 4


:: CONTROLLER OPTIONS

:: Friends, Balanced
IF %1==100 goto :100
IF %1==200 goto :200
IF %1==300 goto :300
IF %1==400 goto :400
IF %1==500 goto :500
IF %1==600 goto :600

:run
START /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl %dep% %rel%
goto :eof

:: Friends, Balanced
:100
   SET dep=DD
   SET rel=FRIEND
   goto :run

:: Friends, Human Ind
:200
   SET dep=ID
   SET rel=FRIEND
   goto :run

:: Strangers, Balanced
:300
   SET dep=DD
   SET rel=STRANGER
   goto :run

:: Strangers, Human Ind
:400
   SET dep=ID
   SET rel=STRANGER
      goto :run


:: Friends, Comp Ind
:500
   SET dep=DI
   SET rel=FRIEND
   goto :run

:: Strangers, Comp Ind
:600
   SET dep=DI
   SET rel=STRANGER
   goto :run

