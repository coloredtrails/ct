The controller provides an API for directly interacting with the Colored Trails server. Using the controller, one can query the server for the state of currently running games and status of connected players, as well as dynamically assign players to configuration files, then launch their respective games. For example, it is possible to set up a round-robin-style tournament where each player is automatically paired exactly once with every other player in the pool. To do this, a controller would be written which queries the server for connected players, matches the players to each other and loads the appropriate configuration files to launch the game, then waits until the games are finished and repeats the whole process. In this way, the controller completes the idea of a Colored Trails experiment, and for many experiments it is as important as the configuration file.

Documentation for the controller API can be found in the Javadoc that comes with Colored Trails.

RobustGroupControl.java (which can be found in the gameconfigs directory) and CodeController.java (in the controller directory) are sample controller implementations.

CodeController.java provides functionality similar to the "ant runadmin -Dconfigfile=lib/adminconfig/TheAutomat.txt" command, and is run using "ant runctrl".

To use the RobustGroupControl example program, build CT, run the server, and connect the clients as described in README_BUILD.txt. However, instead of using "ant runadmin" or "ant runctrl" to run the controller, use the command (run in the ct3 root directory) :

$ java -classpath dist/ct3.jar:gameconfigs/ RobustGroupControl

Utility classes for controller implementation can be found in src/edu/harvard/eecs/airg/coloredtrails/controller. For example, Game.java is a utility class to help launch and manage Colored Trails games, GameEndWatcher.java blocks until a given list of Games are finished, and Matching.java pairs players (using some basic assumptions) that have been divided into two groups.
