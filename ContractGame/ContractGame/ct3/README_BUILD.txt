BUILDING COLORED TRAILS
=======================

######################
### IMPORTANT NOTE ###: The path to the CT system must not contain any spaces; otherwise, the CT administrator will not operate properly. This limitation will be fixed in a future release.
######################


What you need to build Colored Trails:

1) You'll need 'ant' to run our build.xml file. This file contains 'ant' targets for building the CT system and for running it.
The 'ant' system is easy to install and can be found here:
http://ant.apache.org/

2) We recommend using the Eclipse IDE for development with the CT system. Of course, you are fee to use whatever IDE you prefer.
The Eclips IDE can be found here:
http://www.eclipse.org/ 
 

=============================================


To build the CT system, type:

$ ant

This will build the system; the ant build file will also compile all *.java files in folder 'gameconfigs'

To build the Javadoc, type:

$ ant javadoc

To clean the workspace, type:

$ ant clean


Running the CT system (see README_AGENTS.txt in agents/ctagents for more information):

Playing a CT game requires running a  server, clients and a game administrator class. 
The build.xml file has some dedicated targets to run these classes.

1) start up the server:

$ ant runserver

2) wait until CT server is up, then start up separate xterms and run the clients, for example:

$ ant runclient1

(runs a GUI client with a pin of 10)

$ ant runclient2

(runs a GUI client with a pin of 20)

GUI clients can also be started such that they connect to the server immediately:

$ java -jar dist/ct3.jar -c ctgui.original.GUI --pin 20 --client_hostip ct.localhost.com


CT computer agents use JAVA classes to play, rather than
GUIs. Examples of  simple computer agents for playing CT can be found
in agents/ctagents. (More details can be found in agents/README_AGENTS.txt).

 Each agent needs a FrontEnd class to
fire it up. For example, the FrontEnd class for one of the  simple agent is
called ctagents.example.SimpleAgentFrontEnd. There is a dedicated ant
target for running this class.  that uses the client ID supplied at
the command line:

$ ant runagentadaptor -Dclientid=10


3) once the clients have registered with the server, start up the game administrator, for example:

$ ant runadmin -Dconfigfile=lib/adminconfig/TheAutomat.txt

will start the administrator and pass the contents of TheAutomat.txt
to it; this text file contains instructions to the administrator that
tell it to start a game between the agents using IDs 10 and 20. To use
a different game configuration class, simply specify a different text
file argument in the ant command.

If more control over game start is needed, the controller functionality can be used.
The command

$ ant runctrl

provides functionality equivalent to the above "runadmin" command, but queries the server
for connected players and uses the first two to start the game. Many possible options exist
for customizing the controller. The example given is CodeController.java in the controller/
directory. For example, if customized, the controller could connect to the server, query
connected players, start up an equal number of computer agents and wait for them to initialize,
then run a tournament.