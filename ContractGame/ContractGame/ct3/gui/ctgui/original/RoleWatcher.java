/*
	Colored Trails
	
	Copyright (C) 2006, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctgui.original;

import edu.harvard.eecs.airg.coloredtrails.client.ClientGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.Constants;

import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

public class RoleWatcher implements Observer {
	JLabel	RoleLabel;
	RoleWatcher(ClientGameStatus gs, JLabel RoleLabel)
	{
		this.RoleLabel = RoleLabel;
		//System.out.println("**************In RoleWatcher**************");
		gs.addObserver(this);
	}
	public void update(Observable gs, Object state)
	{
		if (state instanceof String) {
			String s = (String) state;
			if (s.equals(Constants.NEWROUND)) {
				RoleLabel.setText("");
				RoleLabel.setFont(new Font("", Font.BOLD, 12));
			}
		}
		
		String Role = ((ClientGameStatus)gs).getMyPlayer().getRole();
        int myNumDorm =   ((ClientGameStatus)gs).getMyPlayer().getMyDormantRounds();
        int hisNumDorm =   ((ClientGameStatus)gs).getMyPlayer().getHisDormantRounds();
        int quid = ((ClientGameStatus)gs).getMyPlayer().getTimeStampID();
        String rltp = ((ClientGameStatus)gs).getMyPlayer().getRelationship();
        if(Role.equals("GameEnded")){
			RoleLabel.setText("Game over!!! Your score is:" + " " + ((ClientGameStatus)gs).getMyPlayer().getScore());
			RoleLabel.setFont(new Font("", Font.BOLD, 18));
			RoleLabel.setVisible(true);
		} else if((Role.equals("Responder")) || (Role.equals("Proposer")))
		{
			
			//System.out.println("-----------------" + Role + "------------------");
//			if (rltp.length()!=0)
//			RoleLabel.setText("<html>Your ID: "+quid + ". Other participant is a " + rltp+"." +"<BR>You are a " + Role+". Dormant: Me " +myNumDorm + ", Other "+hisNumDorm );
//			else
//				
//			RoleLabel.setText("<html>Your ID: "+quid +"<BR>You are a " + Role+". Dormant: Me " +myNumDorm + ", Other "+hisNumDorm  +
			if (rltp.length()!=0)
				RoleLabel.setText("<html>Your ID: "+quid + ". Other participant is a " + rltp+"." +"<BR>You are a " + Role+". ");
			else
							
				RoleLabel.setText("<html>Your ID: "+quid +". You are a " + Role+ ".  Round " + ((ClientGameStatus)gs).getMyPlayer().getGameRound()
						
						+ ".  no movements: "+ ((ClientGameStatus)gs).getMyPlayer().getHisDormantRounds()
//					+ "<BR>NOTE: The customer must always move, otherwise, the game is over!!!!" 
						);
			
			RoleLabel.setVisible(true);
		}
	}
}