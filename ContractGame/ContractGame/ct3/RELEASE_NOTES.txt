
New features for BETA RELEASE 103  (7/22/2008)
================-----============== 
- Functionality for partial visibility
- API for Controller system (enables to run administrator clients and games)
- Javadoc specification for agent and sever classes
- Enhanced documentation for configuration files, with new and improved examples

 BETA RELEASE 102  (05/2008)
============================== 

- New feature. The administrator client can be run via a web-based application (in addition to  the command line).
For details and instructions, see webctrl/README.

- New feature: Several games can be run in parallel using the same server. 
Every time the administrator is run with new clients, a new game will commence. 

- Greatly improved efficiency and reduced lag time when running many
game rounds.  History log is registered by server but no longer sent
to clients.

- Bug fixes. "Skipping phase" problem solved; Game Status message made more efficient.


 BETA RELEASE 101  (02/05/2008)
================================ 
Release Notes


Compatible OS platforms:

CT3 now runs on Mac OS, UNIX, and Windows XP


New Features:

1) Game phases with indefinite length. You can now create game phases
that do not have timers associated with them, and thus can last indefinitely.
Transitions out of such game phases are accomplished with phase advances
invoked in the game configuration class. For  examples,  see the config HOWTO document. 

2) New "GAME_INITIALIZED" message. This new message is issued after the
"GAME_STARTED" message and after the game configuration class has initialized
all game-state objects. In contrast, "GAME_STARTED" is issued before game
state is initialized.

3) Newly designed administrator functionality (does not rely on XML-RPC)

4) Improvements in efficiency of GamePalette class.  (ArrayList rather than
two HashTables)

5) Expanded example 2 in agents directory. This example now supports
both GUI and computational agents.

6) Improvements in design and functionality of  the general CT configuration class
TheAutomatConfig.  This class Now includes flags to control automatic movement
and binding chip transfers.


Known issues with CT3 Beta release:

1) The pathname to the CT3 directory must not contain white space; 
if it does, the administrator will not operate.

2) When running a GUI-based client, a "Colored Trails Taskbar" window 
opens. Click the "Connect..." button to connect to the CT server. After
clicking this button, a new window will open with a text box in which
you can type the IP address of the server, or leave the default value
"localhost" if the server is running on the same machine as the client.
After clicking the "Connect" button on this second window, the client
will connect to the CT server. Note that the "Connect..." button on
the "Colored Trails Taskbar" window is still enabled, though it should
be disabled. Do not click the "Connect..." button on the taskbar window
a second time.


To report bugs and limitations, or request new features, go to
the CT3 web site (www.eecs.harvard.edu/ai/ct) and click on the 
"Submit a Bug/Request" link.



 BETA RELEASE 100 (11/2007)
=====================================



Compatible OS platforms:

CT3 currently runs on Mac OS and should run on other UNIX systems.
CT3 does not operate on Windows; we are working on 
adding this functionality.


Known issues with CT3 Beta release:

1) The pathname to the CT3 directory must not contain white space; 
if it does, the administrator will not operate.

2) When running a GUI-based client, a "Colored Trails Taskbar" window 
opens. Click the "Connect..." button to connect to the CT server. After
clicking this button, a new window will open with a text box in which
you can type the IP address of the server, or leave the default value
"localhost" if the server is running on the same machine as the client.
After clicking the "Connect" button on this second window, the client
will connect to the CT server. Note that the "Connect..." button on
the "Colored Trails Taskbar" window is still enabled, though it should
be disabled. Do not click the "Connect..." button on the taskbar window
a second time.


To report bugs and limitations, or request new features, go to
the CT3 web site (www.eecs.harvard.edu/ai/ct) and click on the 
"Submit a Bug/Request" link.
