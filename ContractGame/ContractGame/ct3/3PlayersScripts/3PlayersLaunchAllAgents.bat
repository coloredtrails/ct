:: LaunchAll.bat
:: 
:: Starts the server, human GUI, and agent
:: 

@echo off
SET /a agentpin=110
echo %agentpin%

:: SERVER
START /B java -jar dist/ct3.jar -s
CALL wait 3

:: AGENT
START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.ThreePlayersAgent.ThreePlayersAgentFrontEnd 0 510 0
CALL wait 5

:: HUMAN
START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.ThreePlayersAgent.ThreePlayersAgentFrontEnd 1 530 0
CALL wait 5

:: AGENT
START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.ThreePlayersAgent.ThreePlayersAgentFrontEnd 1 630 0
CALL wait 5



:: CONTROLLER OPTIONS
:: Friends, Balanced
:100
   SET dep=DD
   SET rel=FRIEND
   goto :run

:run
START /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl3Players DI
goto :eof



:: Friends, Human Ind
:200
   SET dep=ID
   SET rel=FRIEND
   goto :run

:: Strangers, Balanced
:300
   SET dep=DD
   SET rel=STRANGER
   goto :run

:: Strangers, Human Ind
:400
   SET dep=ID
   SET rel=STRANGER
      goto :run


:: Friends, Comp Ind
:500
   SET dep=DI
   SET rel=FRIEND
   goto :run

:: Strangers, Comp Ind
:600
   SET dep=DI
   SET rel=STRANGER
   goto :run

