package edu.harvard.eecs.airg.coloredtrails.controller;

import java.util.ArrayList;

public class Round extends ArrayList<Game>
{
	protected int roundNum = -1;
	
	public Round()
	{
		
	}
	
	public Round(int roundNum)
	{
		this.roundNum = roundNum;
	}
	
	public int getRoundNum()
	{
		return roundNum;
	}
}
