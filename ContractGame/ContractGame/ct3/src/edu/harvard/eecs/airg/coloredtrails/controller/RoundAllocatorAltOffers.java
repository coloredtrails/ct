package edu.harvard.eecs.airg.coloredtrails.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import edu.harvard.eecs.airg.coloredtrails.controller.Game;
import edu.harvard.eecs.airg.coloredtrails.shared.PlayerConnection;

/**
 * @author KPozin
 * 
 */
public class RoundAllocatorAltOffers extends RoundAllocator {
	/**
	 * List of players who are active in the current round. This will be
	 * equivalent to allPlayers if no one is sitting out.
	 */
	private List<PlayerConnection> activePlayers;

	private List<PlayerConnection> sidelinedPlayers;

	/**
	 * 
	 * @param controlImpl
	 * @param configDir Path to config file
	 * @param className Name of config file
	 * @param refreshOnEachRound
	 *            Whether the server should be polled on each round (to check
	 *            for disconnected or new players)
	 * @param playersPerGame
	 */
	public RoundAllocatorAltOffers(ControlImpl controlImpl, String configDir,
			String className, boolean refreshOnEachRound, int playersPerGame,
			Serializable data) {
		super(controlImpl, configDir, className, refreshOnEachRound,
				playersPerGame, data);

		playersPerGame = 2;
	}

	public Round getNewRound() {

		optionallyRefreshPlayers();

		Round tempRound = new Round();

		if (activePlayers == null) {
			
			activePlayers = allPlayers.subList(0, numActivePlayers);
			sidelinedPlayers = allPlayers.subList(numActivePlayers, allPlayers
					.size());
		} else {
			activePlayers = new ArrayList<PlayerConnection>(sidelinedPlayers);

			if (refreshOnEachRound) {
				// Ensure that any players who have disconnected are removed
				activePlayers.retainAll(allPlayers);
			}

			List<PlayerConnection> tempRemaining = new ArrayList<PlayerConnection>(
					allPlayers);
			tempRemaining.removeAll(activePlayers);
			
			// no shuffling for contract game
			//Collections.shuffle(tempRemaining);

			int numFromRemaining = numActivePlayers - activePlayers.size();

			activePlayers.addAll(tempRemaining.subList(0, numFromRemaining));
			sidelinedPlayers = new ArrayList<PlayerConnection>(tempRemaining
					.subList(numFromRemaining, tempRemaining.size()));

		}

		for (int g = 0; g < numSimultGames; ++g) {
			int[] playerIDs = new int[playersPerGame];
			for (int i = 0; i < playersPerGame; ++i) {
				playerIDs[i] = activePlayers.get(i + g * playersPerGame)
						.getPin();
			}

			Game game = new Game(ci, playerIDs, configDir, className, data);
			tempRound.add(game);
		}

		rounds.add(tempRound);
		return tempRound;
	}
}
