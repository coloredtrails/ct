/*
	Colored Trails
	
	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;
//A class used to write data to the log files. Notice that everything we write
//or do not write is affected by the parameter  RunExperiments.settings.LogDetailLevel

import java.io.*;

public class FileWriter
{
	// Main method
	static FileOutputStream fout;	
	PrintStream ps;
	
	public FileWriter (String name)
	{
		try
		{
		    // Open an output stream
		    fout = new FileOutputStream (name);
		    ps=new PrintStream(fout);
		    writeln("StartingFile");
		}
		// Catches any error conditions
		catch (IOException e)
		{
			System.err.println ("Unable to write to file");
			System.exit(-1);
		}
	}
	
	public void write(String text)
	{
		if(Settings.LogDetailLevel==1) ps.print (text);
	}
	
	public void writeln(String text)
	{
		if(Settings.LogDetailLevel==1) ps.println (text);
	}
	
	public static void finish()
	{
		try
		{
		    fout.close();		
		}
		// Catches any error conditions
		catch (IOException e)
		{
			System.err.println ("Unable to write to file");
			System.exit(-1);
		}
	}
	
	//This one will write to log file even if it is turned off
	public void writeForced(String text)
	{
		ps.print (text);

	}
	
	//This one will write to log file even if it is turned off
	public void writelnForced(String text)
	{
		ps.println (text);

	}
	
}


