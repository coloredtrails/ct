/*
	Colored Trails
	
	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;

import java.util.ArrayList;

import edu.harvard.eecs.airg.coloredtrails.shared.types.Goal;

public class GoalsList extends ArrayList<Goal>{
	
	public Goal getByID(int id)
	{
		for(int i=0;i<this.size();i++)
		{
			if(Integer.parseInt(this.get(i).getID())==id) return this.get(i);
		}
		return null;
	}
	
	public ArrayList<Goal> toArrayList()
	{
		ArrayList<Goal> goals=new ArrayList<Goal>();
		for(int i=0;i<this.size();i++) goals.add(this.get(i));
		return goals;
	}

}
