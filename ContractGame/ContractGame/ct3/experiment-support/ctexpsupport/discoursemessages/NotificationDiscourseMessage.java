/*
Colored Trails

Copyright (C) 2007, President and Fellows of Harvard College.  All Rights Reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

//package edu.harvard.eecs.airg.coloredtrails.shared.discourse;
package ctexpsupport.discoursemessages;

import java.util.Hashtable;

import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.HistoryEntry;
import ctexpsupport.dudi.Notification;

/**
Discourse message sent from server to clients to provide a briefing.

@author Sevan G. Ficici
*/
public class NotificationDiscourseMessage extends DiscourseMessage
{
public static final String mname = "notification";
private static Notification notification;

// STANDARD METHODS FIRST:
/**
  Constructor
*/
public NotificationDiscourseMessage() {}


public NotificationDiscourseMessage(int fromPerGameId, int toPerGameId, int messageId)
{
    super(fromPerGameId, toPerGameId, mname, messageId);
}

public NotificationDiscourseMessage(DiscourseMessage other)
{
    super(other);
}

public String getMsgType()
{
    return mname;
}

public NotificationDiscourseMessage clone()
{
    return new NotificationDiscourseMessage(this);
}

public HistoryEntry toHistoryEntry(String phaseName, int phaseNum, int secondsIntoPhase)
{
    Hashtable<String, Object> entry = new Hashtable<String, Object>();
    
    // STANDARD FIELDS
    entry.put("type", mname);
    entry.put("toPerGameId", new Integer(getToPerGameId()));
    entry.put("fromPerGameId", new Integer(getFromPerGameId()));
    entry.put("messageId", new Integer(getMessageId()));
    
    // FIELDS SPECIFIC TO THIS MESSAGE TYPE
    entry.put("notification", getNotification());

    return new HistoryEntry(phaseName, phaseNum, secondsIntoPhase, entry);
}

    public static Notification getNotification() {
        return notification;
    }

    public static void setNotification(Notification notification) {
        NotificationDiscourseMessage.notification = notification;
    }
}

