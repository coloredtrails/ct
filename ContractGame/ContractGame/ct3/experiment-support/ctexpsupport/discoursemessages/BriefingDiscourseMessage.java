/*
Colored Trails

Copyright (C) 2007, President and Fellows of Harvard College.  All Rights Reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

//package edu.harvard.eecs.airg.coloredtrails.shared.discourse;
package ctexpsupport.discoursemessages;

import java.util.Hashtable;

import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.HistoryEntry;
import ctexpsupport.dudi.Briefing;

/**
 * Discourse message sent from server to clients to provide a briefing.
 *
 * @author Sevan G. Ficici
 */
public class BriefingDiscourseMessage extends DiscourseMessage {
    public static final String mname = "briefing";

    public String getBriefing() {
        return briefing;
    }

    public void setBriefing(String briefing) {
        this.briefing = briefing;
    }

    private String briefing;

// STANDARD METHODS FIRST:

    /**
     * Constructor
     */
    public BriefingDiscourseMessage() {
    }

    public BriefingDiscourseMessage(int fromPerGameId, int toPerGameId, int messageId) {
        super(fromPerGameId, toPerGameId, mname, messageId);
    }

    public BriefingDiscourseMessage(DiscourseMessage other) {
        super(other);
    }

    public String getMsgType() {
        return mname;
    }

    public BriefingDiscourseMessage clone() {
        return new BriefingDiscourseMessage(this);
    }

    public HistoryEntry toHistoryEntry(String phaseName, int phaseNum, int secondsIntoPhase) {
        Hashtable<String, Object> entry = new Hashtable<String, Object>();

        // STANDARD FIELDS
        entry.put("type", mname);
        entry.put("toPerGameId", new Integer(getToPerGameId()));
        entry.put("fromPerGameId", new Integer(getFromPerGameId()));
        entry.put("messageId", new Integer(getMessageId()));

        // FIELDS SPECIFIC TO THIS MESSAGE TYPE
        entry.put("briefing", getBriefing());

        return new HistoryEntry(phaseName, phaseNum, secondsIntoPhase, entry);
    }


}
