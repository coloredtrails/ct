:: LaunchAll.bat
:: 
:: Starts the server, human GUI, and agent
:: 

@echo off
SET /a agentpin=510
echo %agentpin%

:: SERVER
START /B java -jar dist/ct3.jar -s
CALL wait 3


:: HUMAN
START /B java -jar dist/ct3.jar -c ctgui.original.GUI --pin 500 --client_hostip localhost
CALL wait 5


:: AGENT
START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.palAgent.PalAgentFrontEnd %agentpin% 0 1
CALL wait 4


:: CONTROLLER OPTIONS
:: Friends, Comp Ind
:500
   SET dep=DI
   SET rel=FRIEND
   goto :run



:run
START /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl %dep% %rel%
goto :eof

:: Friends, Balanced
:100
   SET dep=DD
   SET rel=FRIEND
   goto :run


:: Friends, Human Ind
:200
   SET dep=ID
   SET rel=FRIEND
   goto :run

:: Strangers, Balanced
:300
   SET dep=DD
   SET rel=STRANGER
   goto :run

:: Strangers, Human Ind
:400
   SET dep=ID
   SET rel=STRANGER
      goto :run



:: Strangers, Comp Ind
:600
   SET dep=DI
   SET rel=STRANGER
   goto :run

