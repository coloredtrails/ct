
public class ReliabilityData {
	public double reliability; 
	public double promisedScore;
	public double actualScore;
	public double promisedBenefit;
	public double actualBenefit;
	
	public ReliabilityData() {
		reliability = 0.5;
		promisedScore = 0.0;
		actualScore = 0.0;
		actualBenefit = 0.0;
		promisedBenefit = 0.0;
	}
}
