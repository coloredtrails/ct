import java.util.Vector;

/**
 * This class holds a vector of PlayerData and allows certain searches on the
 * players.
 * 
 * @author Yael Blumberg
 * 
 */
public class Players {

	private Vector<PlayerData> players;

	public Players(int size) {
		players = new Vector<PlayerData>();
		players.setSize(size);
	}

	public PlayerData addPlayer(int pin, int index) {
		PlayerData newPlayer = new PlayerData(pin);
		players.set(index, newPlayer);

		return (newPlayer);
	}

	/*
	 * Returns a player object by a index
	 */
	public PlayerData getPlayerByIndex(int index) {

		return players.get(index);
	}

	/*
	 * Returns a player object by a given pin. If it doesn't exists, return
	 * null.
	 */
	public PlayerData getPlayerByPin(int pin) {

		for (PlayerData p : players) {
			if (p.getPin() == pin) {
				return p;
			}
		}

		return null;
	}

	/*
	 * Returns the opponent player object by a given pin. This is relevant only
	 * to two players game
	 */
	public PlayerData getOpponentByPin(int pin) {

		for (PlayerData p : players) {
			if (p.getPin() != pin) {
				return p;
			}
		}

		return null;
	}
}