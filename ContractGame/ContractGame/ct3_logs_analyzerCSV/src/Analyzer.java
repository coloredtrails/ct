import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import ctagents.alternateOffersAgent.ProposerResponderPlayer.EndingReasons;

import edu.harvard.eecs.airg.coloredtrails.server.ServerGameStatus;
import edu.harvard.eecs.airg.coloredtrails.server.ServerPhases;
import edu.harvard.eecs.airg.coloredtrails.shared.GameBoardCreator;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;

public class Analyzer {

	// Creating a scoring object with the relevant weights to be used in score
	// calculations
	public static Scoring scoreCalculator = new Scoring(100, -10, 5);

	private MyDateFormat formatter = new MyDateFormat();
	private Date startTime;
	private Date endTime;
	private int rounds = 0;
	private Players players;
	private ExperimentData exp;
	private ServerGameStatus gs = null;

	private Vector<PlayerData> firstPlayersVec = null;
	private Vector<PlayerData> secondPlayersVec = null;
	private OffersVec offers;

	private PrintWriter out;
	private PrintWriter offerOut;
	private String fileName = "";
	private String offersFile;

	private static String HEADLINES = "file,uni\',type of board,relationship,rounds,game time,reason ending,type of player,"
			+ "Is Player TI,pin,player ID,score,reached Goal,numOfAcceptedOffers,numOfRejectedOffers,"
			+ "numOfFullyKeptAgreements,numOfUnkeptAgreements,numOfPartiallyKeptAgreements,"
			+ "numOfPartiallyPlusKeptAgreements,Diff chips agreed vs actual,beyondAgreedChips,"
			+ "numOfProposals,isProposerFirst,type of offers,actual offers,actual exchange,total promised,total gave,total accepted"
			+ ",generousity,selfishness,accepted generousity,accepted selfishness";

	private static String OFFERS_HEADLINES = "file,uni\',relationship,type of player,prop reached goal,resp reached goal,pin,type of offer,send,recieve,current score,resulting score,"
			+ "hypothetical score,utility value,resp current,resp resulting,resp hypothetical,"
			+ "resp utility,accepted,actual sent,actual recieved,"
			+ "prop promised,prop gave,prop promised benefit,prop actual benefit,reliabilty prop,resp promised,"
			+ "resp gave,resp promised benefit,resp actual benefit,reliability resp,"
			+ "prev prop promised,prev prop gave,prev prop promised benefit,prev prop actual benefit,prev reliabilty prop,prop WPR,"
			+ "prev resp promised,prev resp gave,prev resp promised benefit,prev resp actual benefit,prev reliability resp,resp WPR,"
			+ "prop dormant,resp dormant,prop missing chips,prop amount missing,resp missing chips,resp amount missing";

	public void doAnalyzeDir(String dir, String firstPlayer, String boardPrefix)
			throws Exception {

		File dirF = new File(dir);
		exp = new ExperimentData();

		// If the first player is fixed, saving it
		if (!firstPlayer.equals("notFixed")) {
			exp.firstPlayer = firstPlayer;
		}

		exp.playerType = ExperimentData.PlayerType.NONE;
		File[] children = dirF.listFiles();
		String boardName;

		for (int i = 0; i < children.length; i++) {

			fileName = "";
			boardName = boardPrefix;

			if (children != null) {
				if (children[i].isDirectory()) {
					if (children[i].getName().contains("Strangers")) {
						exp.playerType = ExperimentData.PlayerType.STRANGERS;
					} else if (children[i].getName().contains("Friends")) {
						exp.playerType = ExperimentData.PlayerType.FRIENDS;
					}

					if (children[i].getName().contains("Harvard")) {
						exp.university = "Harvard";
					} else if (children[i].getName().contains("Beirut")) {
						exp.university = "Beirut";
					}

					if (children[i].getName().contains("Board_D-D")) {
						exp.boardType = ExperimentData.BoardType.BOTH_DD;
						fileName = dir + "\\LogsAnalysys_BOTH_DD.csv";
						boardName += "boardXml_both_need_three.txt";
					} else if (children[i].getName().contains("Agent_TI")) {
						exp.boardType = ExperimentData.BoardType.AGENT_TI;
						fileName = dir + "\\LogsAnalysys_AGENT_TI.csv";
						boardName += "boardXml_Agent_TI.txt";
					} else if (children[i].getName().contains("Human_TI")) {
						exp.boardType = ExperimentData.BoardType.GUI_TI;
						fileName = dir + "\\LogsAnalysys_GUI_TI.csv";
						boardName += "boardXml_GUI_TI.txt";
					} else if (children[i].getName().contains("1ST_PLAYER_TI")) {
						exp.boardType = ExperimentData.BoardType.FIRST_PLAYER_TI;
						fileName = dir + "\\LogsAnalysys_FirstPlayerTI.csv";
						boardName += "boardXml_GUI_TI.txt";
					} else if (children[i].getName().contains("2ND_PLAYER_TI")) {
						exp.boardType = ExperimentData.BoardType.SECOND_PLAYER_TI;
						fileName = dir + "\\LogsAnalysys_SecondPlayerTI.csv";
						boardName += "boardXml_Agent_TI.txt";
					} else if (children[i].getName().contains(
							"BOTH_HUMAN_TI_TD")) {
						exp.boardType = ExperimentData.BoardType.BOTH_HUMAN_TI_TD;
						fileName = dir + "\\LogsAnalysys_BothHuman_"
								+ children[i].getName() + ".csv";
						boardName += "boardXml_Agent_TI.txt";
					}

					// The current folder was not relevant to the analysis
					if (fileName.equals("")) {
						continue;
					}

					// Getting the first player in this folder if it's not fixed
					// for all the analysis
					if (firstPlayer.equals("notFixed")) {
						if (children[i].getName().contains("Human1")) {
							exp.firstPlayer = "HUMAN";

						} else if (children[i].getName().contains("Human2")) {
							exp.firstPlayer = "AGENT";
						}
					}

					offersFile = fileName.substring(0, fileName.indexOf("."));
					offersFile = offersFile.concat("_offers.csv");

					// If there is already a file with the same name in this
					// directory don't override it,
					// but append the new data to the existing one.
					// Otherwise, create new file and write to it.
					try {
						File checkFile = new File(fileName);
						if (checkFile.exists()) {
							FileOutputStream fop = new FileOutputStream(
									checkFile, true);
							out = new PrintWriter(fop);

							fop = new FileOutputStream(new File(offersFile),
									true);
							offerOut = new PrintWriter(fop);
						} else {
							out = new PrintWriter(fileName);
							out.println(HEADLINES);
							offerOut = new PrintWriter(offersFile);
							offerOut.println(OFFERS_HEADLINES);
						}

					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

					// Fill these vector with data corresponding to the type of
					// board.
					firstPlayersVec = new Vector<PlayerData>();
					secondPlayersVec = new Vector<PlayerData>();

					if (firstPlayersVec == null || secondPlayersVec == null)
						throw (new RuntimeException(
								"Couldn't store data of players. Vector is null"));

					File[] gchildren = children[i].listFiles();
					for (int g = 0; g < gchildren.length; g++) {

						if (gchildren[g].isDirectory()) {
							String[] files = gchildren[g].list();
							if (files != null) {
								for (int f = 0; f < files.length; f++) {
									// Get name of file or directory
									String fileName = files[f];
									if (fileName.startsWith(
											"AlternativeOffersConfig", 0)) {
										String myDir = gchildren[g].toString();
										String myFile = myDir + "\\" + fileName;

										// Analyzing the game
										doAnalyzeFile(myFile, boardName);
									}
								}
							}
						} else if (gchildren[g].isFile()) {
							if (gchildren[g].getName().startsWith(
									"AlternativeOffersConfig", 0)) {
								String myFile = gchildren[g].toString();
								doAnalyzeFile(myFile, boardName);
							}
						}
					}
					// the children of this directory are files:
				} else {
					String fileName = children[i].getName();
					if (fileName.startsWith("AlternativeOffersConfig", 0)) {
						String myFile = children[i].toString();
						try {
							out = new PrintWriter(dir + "\\LogsAnalysys.csv");
							out.println(HEADLINES);
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}

						doAnalyzeFile(myFile, boardName);
					}
				}
			}

			// Calculating and printing the averages
			String fstPlayer = exp.firstPlayer;
			String secondPlayer = null;
			if (exp.firstPlayer.compareTo("AGENT") == 0)
				secondPlayer = "HUMAN";
			else if (exp.firstPlayer.compareTo("HUMAN") == 0)
				secondPlayer = "AGENT";
			else if (exp.firstPlayer.compareTo("BOTH_HUMAN") == 0) {
				if (players.getPlayerByIndex(0).isTI) {
					fstPlayer = "TI";
					secondPlayer = "TD";
				} else {
					fstPlayer = "TD";
					if (players.getPlayerByIndex(1).isTI) {
						secondPlayer = "TI";
					} else {
						secondPlayer = "TD";
					}
				}
			}

			// If there is a different between the players, calcing a separate
			// average
			if (!fstPlayer.equals(secondPlayer)) {
				String avg1 = fstPlayer + " average score is: "
						+ calcAverageScore(firstPlayersVec);
				String avg2 = secondPlayer + " average score is: "
						+ calcAverageScore(secondPlayersVec);
				String avgSum = "Average of sum of scores is: "
						+ calcAverageOfSumOfScores(firstPlayersVec,
								secondPlayersVec);
				out.println();
				out.println(avg1);
				out.println(avg2);
				out.println(avgSum);
			} else {
				Vector<PlayerData> allData = new Vector<PlayerData>(
						firstPlayersVec);
				allData.addAll(secondPlayersVec);

				String avg1 = fstPlayer + " average score is: "
						+ calcAverageScore(allData);

				out.println();
				out.println(avg1);
			}
			out.close();
		}
	}

	public void doAnalyzeFile(String fileName, String boardName)
			throws Exception {

		players = new Players(2);
		offers = new OffersVec();
		rounds = 0;
		exp.reason = ExperimentData.ReasonEndGame.UNDEFINED;

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			System.out.println("Processing file: " + fileName);

			String line;

			PlayerStatus ps1 = null;
			PlayerStatus ps2 = null;
			ChipsVec chipsForFirst = null;
			ChipsVec chipsForSec = null;
			RowCol firstPos = null;
			RowCol secPos = null;
			Boolean isFirstScore = true;
			int humanId = Integer.MIN_VALUE;

			startTime = new Date();
			endTime = new Date();

			// Processing the log lines
			while ((line = br.readLine()) != null) {
				String[] res;
				if (line.contains("This is the CT debug logger")) {
					try {
						startTime = formatter.parse(line);
						System.out.println(startTime.toString());
					} catch (NullPointerException p) {
						System.err.println(p.getMessage());
					}
				} else if (line.contains("ID Number:")) {
					String id = line.split(":")[3];
					humanId = Integer.parseInt(id.trim());

					// getting the number of consecutive movements
				} else if (line
						.contains("Number of consecutive no movements for player")) {

					line = line.trim();
					int noMove = Integer.parseInt(line
							.substring(line.length() - 1));

					int pin = Integer.parseInt(line.substring(
							line.indexOf("player") + 6, line.indexOf("is"))
							.trim());

					players.getPlayerByPin(pin).setNoConcecMovment(noMove);

					// Getting the players Pin number and chips
				} else if (line.contains("Setting chips for player:")) {
					res = line.split(": ");
					// removing any extra characters after the players pin
					res = res[res.length - 1].split(" ");
					int pin = new Integer(res[0]);

					// The new version of the configuration also writes the
					// perGameId. If it exists, lets use it
					int id = -1;
					if (res.length > 1) {
						res = res[res.length - 1].split(":");
						if (res.length > 1) {
							id = new Integer(res[res.length - 1]);
						}
					}

					// reading the next line containing the chips this player
					// got
					line = br.readLine();
					ChipsVec playersChips = new ChipsVec(line.substring(line
							.indexOf("'")));

					boolean isTI = false;

					// If the board is not symmetrical, checking which player is
					// TI
					if (exp.boardType != ExperimentData.BoardType.BOTH_DD) {

						// TODO: change the way of determining the isTI to be
						// more general.
						// At least count zeros and not expect certain color..

						if (!line.contains("'grey78':0")) {
							isTI = true;
						}
					}

					// Getting the players start position
					// from a string formatted: "Player position: (R:6,C:0)"
					line = br.readLine();
					int rowStart = line.indexOf("R:") + 2;
					int colStart = line.indexOf("C:") + 2;
					int row = Integer.parseInt(line.substring(rowStart,
							rowStart + 1));
					int col = Integer.parseInt(line.substring(colStart,
							colStart + 1));

					// Evaluating if this is the first or second player
					// according for one of the following conditions:
					// 1. We have the perGameId and it equals to 0. (This is the
					// most powerful argument)
					// 2. This is an Agent vs human game thus the first player
					// is the human, it's pin divides by 100 and the agent
					// doesn't (agent = human +10).
					// 3. This is a human vs human game were the pins are 10-20
					int reminder = pin % 100;
					if ((id != 1)
							&& ((id == 0) || (pin == 10) || (reminder == 0))) {
						players.addPlayer(pin, 0);
						players.getPlayerByIndex(0).isTI = isTI;

						// Checking if the player has an ID
						if (humanId != Integer.MIN_VALUE) {
							players.getPlayerByIndex(0).setID(humanId);
						}

						// Checking if the first player or second
						if (ps1 == null) {
							ps1 = new PlayerStatus(pin);
							ps1.setPerGameId(pin);
							ps1.setMovesAllowed(true);
							chipsForFirst = new ChipsVec(playersChips);
							firstPos = new RowCol(row, col);
						} else {
							ps2 = new PlayerStatus(pin);
							ps2.setPerGameId(pin);
							ps2.setMovesAllowed(true);
							chipsForSec = new ChipsVec(playersChips);
							secPos = new RowCol(row, col);
						}
					} else if ((id != 0)
							&& ((id == 1) || (pin == 20) || (reminder == 10))) {
						players.addPlayer(pin, 1);
						players.getPlayerByIndex(1).isTI = isTI;
						
						// Checking if the player has an ID
						if (humanId != Integer.MIN_VALUE) {
							players.getPlayerByIndex(1).setID(humanId + 1);
						}


						// Checking if the first player or second
						if (ps1 == null) {
							ps1 = new PlayerStatus(pin);
							ps1.setPerGameId(pin);
							ps1.setMovesAllowed(true);
							chipsForFirst = new ChipsVec(playersChips);
							firstPos = new RowCol(row, col);

						} else {
							ps2 = new PlayerStatus(pin);
							ps2.setPerGameId(pin);
							ps2.setMovesAllowed(true);
							chipsForSec = new ChipsVec(playersChips);
							secPos = new RowCol(row, col);
						}
					}

					// Now that we know the players, creating the game board for
					// the game
					if (ps2 != null && ps1 != null) {

						Set<PlayerStatus> players = new HashSet<PlayerStatus>();
						players.add(ps1);
						players.add(ps2);

						gs = new ServerGameStatus();
						gs.setPlayers(players);

						ServerPhases ph = new ServerPhases(
								new DummyPhaseHandler());
						ph.addPhase("Strategy Prep Phase");
						ph.addPhase("Communication Phase");
						ph.addPhase("Exchange Phase");
						ph.addPhase("Movement Phase");
						ph.addPhase("Feedback Phase");
						gs.setPhases(ph);

						GameBoardCreator gbc = new GameBoardCreator(gs);
						gbc.createGameBoard(boardName);

						// Enforcing the ChipsCet and position we read from the
						// file for the players,
						// to make sure we start in the same situation the
						// original game started
						gs.getPlayerByPerGameId(ps1.getPerGameId()).setChips(
								chipsForFirst.toChipSet());
						gs.getPlayerByPerGameId(ps1.getPerGameId())
								.setPosition(firstPos);
						gs.getPlayerByPerGameId(ps2.getPerGameId()).setChips(
								chipsForSec.toChipSet());
						gs.getPlayerByPerGameId(ps2.getPerGameId())
								.setPosition(secPos);
					}

					// Getting the final scores
				} else if (line.contains("score is:")) {
					res = line.split(":");

					Double score = new Double(res[res.length - 1])
							.doubleValue();
					int pin = Integer.parseInt(line
							.substring(line.indexOf("Player: ") + 8,
									line.indexOf(",role")).trim());
					PlayerData curPlayer = players.getPlayerByPin(pin);

					curPlayer.score = score;

					if (curPlayer.score >= 100)
						curPlayer.bReachedGoal = true;

					// If this is the first score we read, we need to keep
					// reading till reaching the next score.
					// Otherwise, the reading of the file is over.
					if (isFirstScore) {
						isFirstScore = false;
					} else {
						try {
							endTime = formatter.parse(line);
							System.out.println(endTime.toString());
						} catch (NullPointerException p) {
							System.err.println(p.getMessage());
						}

						// even if there are more lines after this
						// one they are irrelevant, don't process
						break;
					}
				} else if (line.contains("Feedback phase, Round:")) {
					rounds++;
				} else if (line.contains("Received communication message")) {

					Vector<String> lines = new Vector<String>();
					lines.add(line);
					do {
						line = br.readLine();
						if (line == null) {
							System.out
									.println("In middle of paragraph starting with"
											+ "[A New Phase Began: Communication Phase], line read is null");
							break;
						}
						lines.add(line);

						// ouch!! ugly, but in games that both players got all
						// the chips needed it gets checked in the beginning of
						// comm. phase therefore the last comm. phase paragraph
						// is irrelevant and instead we need to process end of
						// game data.
						if (line
								.contains("All players have all needed chips to get to goal,"
										+ " end the game")) {
							exp.reason = ExperimentData.ReasonEndGame.BOTH_HAVE_CHIPS;
						} else if (line.contains("score is:")) {
							res = line.split(":");

							Double score = new Double(res[res.length - 1])
									.doubleValue();
							int pin = Integer.parseInt(line.substring(
									line.indexOf("Player: ") + 8,
									line.indexOf(",role")).trim());
							PlayerData curPlayer = players.getPlayerByPin(pin);

							curPlayer.score = score;

							if (curPlayer.score >= 100)
								curPlayer.bReachedGoal = true;

							// If this is the first score we read, we need to
							// keep
							// reading till reaching the next score.
							// Otherwise, the reading of the file is over.
							if (isFirstScore) {
								isFirstScore = false;
							} else {
								try {
									endTime = formatter.parse(line);
									System.out.println(endTime.toString());
								} catch (NullPointerException p) {
									System.err.println(p.getMessage());
								}

								// even if there are more lines after this
								// one they are irrelevant, don't process
								break;
							}
						}
					} while ((line != null)
							&& !line
									.contains("A Phase Ended: Communication Phase"));

					processCommPhase(lines);

				} else if (line.contains(" sent:")) {

					processExchange(line);

					// Finding movements and performing them on the board
					// The lines format is: Player 10 moved to (R:4,C:3)
				} else if (line.contains("moved to")) {

					processMove(line);

					// Finding the ending reason
				} else if (line
						.contains("Ending game, all players move as much as they can towards goal")) {
					exp.reason = ExperimentData.ReasonEndGame.NO_CONSEC_MOVEMENTS;
				} else if (line
						.contains("got to goal, advance the other and end game")) {
					exp.reason = ExperimentData.ReasonEndGame.ONE_REACHED_GOAL;
				} else if (line
						.contains("All players have all needed chips to get to goal, end the game")) {
					exp.reason = ExperimentData.ReasonEndGame.BOTH_HAVE_CHIPS;
					players.getPlayerByIndex(0).bReachedGoal = true;
					players.getPlayerByIndex(1).bReachedGoal = true;
				}

			} // end while
		} // end try
		catch (IOException e) {
			System.err.println("Error: " + e);
		}

		// Making sure that if both players reached goal, the reason is
		// mentioned
		if (players.getPlayerByIndex(0).bReachedGoal
				&& players.getPlayerByIndex(1).bReachedGoal) {
			exp.reason = ExperimentData.ReasonEndGame.BOTH_HAVE_CHIPS;
		}

		players.getPlayerByIndex(0).processAgreementsData(
				offers
						.getOffersByProposer(players.getPlayerByIndex(0)
								.getPin()));
		players.getPlayerByIndex(1).processAgreementsData(
				offers
						.getOffersByProposer(players.getPlayerByIndex(1)
								.getPin()));

		long diff = endTime.getTime() - startTime.getTime();
		long diffSec = diff / 1000;
		long diffMin = diffSec / 60;
		long diffSecOfMin = diffSec % 60;

		exp.overallGameTime = diff;
		exp.numOfRoundsPlayed = rounds;

		System.out.println("lentgh of game is " + diffMin + " minutes and "
				+ diffSecOfMin + " seconds");

		System.out.println("Exp. data: " + exp.toString());

		// Writing the players data to the games file
		OffersVec player1offers = offers.getOffersByProposer(players
				.getPlayerByIndex(0).getPin());
		OffersVec player2offers = offers.getOffersByProposer(players
				.getPlayerByIndex(1).getPin());

		String player1Details = players.getPlayerByIndex(0).toString()
				+ ","
				+ player1offers.typeOfOffersToString()
				+ ","
				+ player1offers.offersDetailsToString()
				+ ","
				+ player1offers.exchangesDetailsToString()
				+ ","
				+ offers.getTotalPromised(players.getPlayerByIndex(0).getPin())
				+ ","
				+ offers.getTotalGave(players.getPlayerByIndex(0).getPin())
				+ ","
				+ offers.getAcceptedOffers().getOffers().size()
				+ ","
				+ offers.calcGenerousity(players.getPlayerByIndex(0).getPin())
				+ ","
				+ offers.calcSelfishness(players.getPlayerByIndex(0).getPin())
				+ ","
				+ offers.calcAcceptedGenerousity(players.getPlayerByIndex(0)
						.getPin())
				+ ","
				+ offers.calcAcceptedSelfishness(players.getPlayerByIndex(0)
						.getPin());

		String player2Details = players.getPlayerByIndex(1).toString()
				+ ","
				+ player2offers.typeOfOffersToString()
				+ ","
				+ player2offers.offersDetailsToString()
				+ ","
				+ player2offers.exchangesDetailsToString()
				+ ","
				+ offers.getTotalPromised(players.getPlayerByIndex(1).getPin())
				+ ","
				+ offers.getTotalGave(players.getPlayerByIndex(1).getPin())
				+ ","
				+ offers.getAcceptedOffers().getOffers().size()
				+ ","
				+ offers.calcGenerousity(players.getPlayerByIndex(1).getPin())
				+ ","
				+ offers.calcSelfishness(players.getPlayerByIndex(1).getPin())
				+ ","
				+ offers.calcAcceptedGenerousity(players.getPlayerByIndex(1)
						.getPin())
				+ ","
				+ offers.calcAcceptedSelfishness(players.getPlayerByIndex(1)
						.getPin());

		out.println(fileName + "," + exp.toString() + "," + player1Details);
		out.println(fileName + "," + exp.toString() + "," + player2Details);
		out.flush();

		// Writing the offers data to the offers file
		for (Offer of : offers.getOffers()) {
			offerOut.println(fileName + "," + exp.university + ","
					+ exp.playerType + ","
					+ players.getPlayerByPin(of.getProposer()).typeOfPlayer
					+ ","
					+ players.getPlayerByPin(of.getProposer()).bReachedGoal
					+ ","
					+ players.getOpponentByPin(of.getProposer()).bReachedGoal
					+ "," + of.toString());
		}
		offerOut.flush();

		if (players.getPlayerByIndex(0).isProposerFirst) {
			firstPlayersVec.add(players.getPlayerByIndex(0));
			secondPlayersVec.add(players.getPlayerByIndex(1));			
		} else {
			firstPlayersVec.add(players.getPlayerByIndex(1));
			secondPlayersVec.add(players.getPlayerByIndex(0));
		}
	}

	private Offer addOffer(ChipsVec chipsToSend, ChipsVec chipsToReceive,
			ServerGameStatus gs, PlayerData proposer, PlayerData responder,
			int roundNo) {

		Offer currOffer;

		// Creating the offer from the proposer perspective
		EndingReasons isGameAboutToEnd = UtilityCalculator.isGameAboutToEnd(
				proposer.getNoConcecMovment(), responder.getNoConcecMovment(),
				gs.getPlayerByPerGameId(responder.getPin()), gs.getBoard());
		currOffer = proposer.addOffer(chipsToSend, chipsToReceive, gs,
				isGameAboutToEnd);
		currOffer.setRound(rounds);

		// CAlculating the opponents lacking chips
		currOffer.setResponderLackingChips(responder.getLackingChips(gs));
		// Saving the number of no consecutive movements
		currOffer.setProposerNoConsec(proposer.getNoConcecMovment());
		currOffer.setResponderNoConsec(responder.getNoConcecMovment());

		// Calculating the reliability as if nothing was sent in
		// case no exchange will be made
		currOffer.setPropRelaib(responder.calcOppReliabilty(gs, currOffer));
		currOffer.setRespRelaib(proposer.calcOppReliabilty(gs, currOffer));

		// Creating the offer from the responder perspective to get his
		// estimation
		isGameAboutToEnd = UtilityCalculator.isGameAboutToEnd(responder
				.getNoConcecMovment(), proposer.getNoConcecMovment(), gs
				.getPlayerByPerGameId(proposer.getPin()), gs.getBoard());
		Offer responderOffer = responder.addOffer(chipsToReceive, chipsToSend,
				gs, isGameAboutToEnd);
		currOffer.setEstimation(responderOffer.getEstimation(Offer.PROPOSER),
				Offer.RESPONDER);

		// Getting all the accepted offers
		OffersVec acceptedOffers = offers.getAcceptedOffers();

		if (acceptedOffers.getOffers().isEmpty()) {

			// This is the first offer in the game, reliability is unknown
			currOffer.setPrevPropRelaib(new ReliabilityData());
			currOffer.setPrevRespRelaib(new ReliabilityData());

			// Calculating a heuristic WPR, according to Galit's calculations
			if (players.getPlayerByPin(currOffer.getProposer()).typeOfPlayer
					.equals("AGENT")) {
				currOffer.setPropWeightedPrevRelaibility(1, 0.68);
				currOffer.setRespWeightedPrevRelaibility(1, 0.43);
			} else {
				currOffer.setPropWeightedPrevRelaibility(1, 0.43);
				currOffer.setRespWeightedPrevRelaibility(1, 0.68);
			}
		} else {

			// Getting the reliability data from the last accepted offer
			Offer lastOffer = acceptedOffers.getOffers().lastElement();

			if (currOffer.getProposer() == lastOffer.getProposer()) {
				currOffer.setPrevPropRelaib(lastOffer.getPropRelaib());
				currOffer.setPrevRespRelaib(lastOffer.getRespRelaib());

				// Calculating the new WPR for each player
				currOffer.setPropWeightedPrevRelaibility(lastOffer
						.getPropWeightedPrevRelaibility(), currOffer
						.getPrevPropRelaib().reliability);
				currOffer.setRespWeightedPrevRelaibility(lastOffer
						.getRespWeightedPrevRelaibility(), currOffer
						.getPrevRespRelaib().reliability);
			} else {
				currOffer.setPrevPropRelaib(lastOffer.getRespRelaib());
				currOffer.setPrevRespRelaib(lastOffer.getPropRelaib());

				// Calculating the new WPR for each player
				currOffer.setRespWeightedPrevRelaibility(lastOffer
						.getPropWeightedPrevRelaibility(), currOffer
						.getPrevRespRelaib().reliability);
				currOffer.setPropWeightedPrevRelaibility(lastOffer
						.getRespWeightedPrevRelaibility(), currOffer
						.getPrevPropRelaib().reliability);
			}
		}

		offers.addOffer(currOffer);
		proposer.numOfProposals++;

		return currOffer;
	}

	private void processMove(String line) {

		// Getting the wanted player and moving it on the board
		for (PlayerStatus p : gs.getPlayers()) {
			if (line.contains("Player " + p.getPin())) {

				// Extracting the new location from the line
				int row = Integer.parseInt(String.valueOf(line.charAt(line
						.lastIndexOf(",") - 1)));
				int col = Integer.parseInt(String.valueOf(line.charAt(line
						.lastIndexOf(":") + 1)));
			
				RowCol curPos = p.getPosition();
				RowCol newpos = new RowCol(row, col);

				System.out.println("before move:" + p.getChips().toString());
				gs.doMove(p.getPerGameId(), newpos);

				players.getPlayerByPin(p.getPin()).removeFirstPointOnPath(
						curPos, newpos);

				System.out.println("after move:"
						+ gs.getPlayerByPerGameId(p.getPerGameId()).getChips()
								.toString());
				break;
			}
		}
	}

	private void processCommPhase(Vector<String> lines) {
		// Illegal paragraph, don't process it.
		if (lines.size() < 12)
			return;
		// do something with the lines
		System.out.println("Start processCommPhase");

		int lineCurser = 0;
		String[] strSplit = null;

		for (int lineNo = 1; lineNo < lines.size(); ++lineNo) {
			if (lines.get(lineNo).contains("From player:")) {
				strSplit = lines.get(lineNo).split(":");
				lineCurser = lineNo;
				break;
			}
		}

		int proposerPin = Integer
				.parseInt(strSplit[strSplit.length - 1].trim());
		PlayerData proposer = players.getPlayerByPin(proposerPin);
		PlayerData responder = players.getOpponentByPin(proposerPin);

		// If the type of players where not determined yet, this is the first
		// offer
		if (proposer.typeOfPlayer == null) {
			setTypeOfPlayers(proposerPin);
		}

		// Getting the chips offered
		String chips;
		ChipsVec chipsToSend;
		ChipsVec chipsToReceive;
		int index = -1;

		for (int lineNo = lineCurser; lineNo < lines.size(); ++lineNo) {
			if (lines.get(lineNo).contains("Chips to send by proposer")) {
				index = lines.get(lineNo).indexOf("'");
				lineCurser = lineNo;
				break;
			}
		}

		if (index != -1) {
			chips = lines.get(lineCurser).substring(index);
			chipsToSend = new ChipsVec(chips);
			System.out.println("chipsToSend: " + chipsToSend);
		} else {
			chipsToSend = new ChipsVec();
		}

		for (int lineNo = lineCurser; lineNo < lines.size(); ++lineNo) {
			if (lines.get(lineNo).contains("Chips to send by responder")) {
				index = lines.get(lineNo).indexOf("'");
				lineCurser = lineNo;
				break;
			}
		}

		if (index != -1) {
			chips = lines.get(lineCurser).substring(index);
			chipsToReceive = new ChipsVec(chips);
			System.out.println("chipsToReceive: " + chipsToReceive);
		} else
			chipsToReceive = new ChipsVec();

		Offer currOffer;

		// Adding the offer to the proposer
		currOffer = addOffer(chipsToSend, chipsToReceive, gs, proposer,
				responder, rounds);

		// Checking the reaction of the responder
		for (int lineNo = lineCurser; lineNo < lines.size(); ++lineNo) {
			if (lines.get(lineNo).contains("accepted offer")) {
				responder.numOfAcceptedOffers++;
				currOffer.setAccepted(true);
				lineCurser = lineNo;
				break;
			} else if (lines.get(lineNo).contains("rejected offer")) {
				responder.numOfRejectedOffers++;
				currOffer.setAccepted(false);
				lineCurser = lineNo;
				break;
			}
		}

		// If responder rejected offer then there is a swap in the roles and
		// the communication phase has more lines to process:
		if (!currOffer.isAccepted()) {

			boolean didSwap = false;

			for (int lineNo = lineCurser; lineNo < lines.size(); ++lineNo) {
				if (lines.get(lineNo).contains("Swaping roles:")) {
					lineCurser = lineNo;
					didSwap = true;
					break;
				}
			}

			if (didSwap) {
				if ((lines.get(lineCurser + 1).contains("Proposer is: "
						+ proposer.getPin())))
					System.err
							.println("Error! Should have swapped roles and didn't");
				else {

					PlayerData temp = responder;
					responder = proposer;
					proposer = temp;
					proposerPin = proposer.getPin();

					// Checking where does the counter offer begins
					int startCounterOffer = 0;
					for (int lineNo = lineCurser; lineNo < lines.size(); lineNo++) {

						if (lines.get(lineNo).contains("Chips to send by ")) {
							startCounterOffer = lineNo;
							break;
						}
					}

					if (startCounterOffer != 0) {

						index = lines.get(startCounterOffer).indexOf("'");
						if (index != -1) {
							chips = lines.get(startCounterOffer).substring(
									index);
							chipsToSend = new ChipsVec(chips);
						} else
							chipsToSend = new ChipsVec();

						index = lines.get(startCounterOffer + 1).indexOf("'");
						if (index != -1) {
							chips = lines.get(startCounterOffer + 1).substring(
									index);
							chipsToReceive = new ChipsVec(chips);
						} else
							chipsToReceive = new ChipsVec();

						currOffer = addOffer(chipsToSend, chipsToReceive, gs,
								proposer, responder, rounds);

						// Searching for the response for this proposal
						for (int lineNo = startCounterOffer + 1; lineNo < lines
								.size(); ++lineNo) {
							if (lines.get(lineNo).contains("accepted offer")) {
								responder.numOfAcceptedOffers++;
								currOffer.setAccepted(true);
								break;
							} else if (lines.get(lineNo).contains(
									"rejected offer")) {
								responder.numOfRejectedOffers++;
								currOffer.setAccepted(false);
								break;
							}
						}
					}
				}
			}
		}

		for (String st : lines) {
			System.out.println(st);
		}

		System.out.println("End processCommPhase");
	}

	/**
	 * This method goes over the exchange and updates all the relevant objects
	 * according to its data
	 * 
	 * @param line
	 * @throws Exception
	 */
	private void processExchange(String line) throws Exception {

		ChipsVec actualSent = null;
		int index = line.indexOf("'");
		if (index == -1) {
			actualSent = new ChipsVec();
		} else {
			actualSent = new ChipsVec(line.substring(index));
		}

		PlayerData sender = null;
		PlayerData reciever = null;

		// Checking who is the sender in this line
		for (int ind = 0; ind < 2; ++ind) {
			if (line
					.contains(players.getPlayerByIndex(ind).getPin() + " sent:")) {
				sender = players.getPlayerByIndex(ind);
				reciever = players.getOpponentByPin(sender.getPin());
				break;
			}
		}

		try {
			offers.setActualSent(rounds, actualSent, sender.getPin());
		} catch (Exception e) {
			System.err.println(e.getMessage());

			// Assuming the error was caused by a communication phase with no
			// offers but the player decided to send something anyway
			// Creating an offer that the proposer is the opponent of the
			// previous proposer or the first player if this is the first round
			int responder = players.getPlayerByIndex(1).getPin();
			if (offers.getOffers().size() > 0) {
				responder = offers.getOffers().lastElement().getProposer();
			} else {
				// If this was supposed to be the first offer of the game and it
				// wasn't sent, we have to set the players types at this point
				setTypeOfPlayers(players.getOpponentByPin(responder).getPin());
			}

			addOffer(new ChipsVec(), new ChipsVec(), gs, players
					.getOpponentByPin(responder), players
					.getPlayerByPin(responder), rounds);

			offers.setActualSent(rounds, actualSent, sender.getPin());
		}

		Offer curOffer = offers.getOffers().lastElement();

		// Calculating the reliability of the player who sent the chips
		ReliabilityData senderReliability = reciever.calcOppReliabilty(gs,
				curOffer);

		if (sender.getPin() == curOffer.getProposer()) {
			curOffer.setPropRelaib(senderReliability);
		} else {
			curOffer.setRespRelaib(senderReliability);
		}

		// Performing the exchange on the board
		System.out.println("chips to send by " + sender.getPin() + ":"
				+ actualSent.toString());
		gs.doTransfer(sender.getPin(), reciever.getPin(), actualSent
				.toChipSet());
		System.out.println("chips after send:"
				+ gs.getPlayerByPerGameId(sender.getPin()).getChips()
						.toString());
	}

	private double calcAverageScore(Vector<PlayerData> vec) {
		if (vec.isEmpty())
			return 0;
		double avg = 0;
		for (PlayerData pD : vec) {
			avg += pD.score;
		}
		avg /= (double) vec.size();
		return avg;
	}

	private double calcAverageOfSumOfScores(Vector<PlayerData> firstPlayersVec,
			Vector<PlayerData> secondPlayersVec) {
		if (firstPlayersVec.isEmpty() || secondPlayersVec.isEmpty())
			return 0;
		if (firstPlayersVec.size() != secondPlayersVec.size())
			return 0;

		double avg = 0;
		for (int i = 0; i < firstPlayersVec.size(); i++) {
			avg += firstPlayersVec.get(i).score + secondPlayersVec.get(i).score;
		}
		avg /= (double) firstPlayersVec.size();
		return avg;
	}

	public static void main(String[] args) {

		// Checking if the arguments were entered
		if (args.length == 0) {
			System.out
					.println("Please enter log's directory, first player and board name prefix if needed");
		} else {

			// Getting the parameters and analyzing the data
			String dir = args[0];
			String firstPlayer = args[1];

			// If a third parameter was entered, use it as a prefix for the
			// board name
			String boradFilenamePrefix = "";
			if (args.length == 3) {
				boradFilenamePrefix = args[2];
			}

			Analyzer a = new Analyzer();

			try {
				a.doAnalyzeDir(dir, firstPlayer, boradFilenamePrefix);
			} catch (Exception e) {
				e.printStackTrace();
			}

			// For debug..
			System.out.println("Exp. data: " + a.exp.toString());
			System.out.println("Player1 data: "
					+ a.players.getPlayerByIndex(0).toString());
			System.out.println("Player2 data: "
					+ a.players.getPlayerByIndex(1).toString());
		}
	}

	private void setTypeOfPlayers(int proposerPin) {

		PlayerData proposer = players.getPlayerByPin(proposerPin);
		PlayerData responder = players.getOpponentByPin(proposerPin);

		// Starting from the assumption: BOTH_HUMAN (this is true when
		// exp.firstPlayer is neither AGENT nor HUMAN)
		proposer.typeOfPlayer = "HUMAN";
		responder.typeOfPlayer = "HUMAN";

		// If this it an even round ,this is truly the first proposer in
		// the game
		if (rounds % 2 == 0) {
			proposer.isProposerFirst = true;
			responder.isProposerFirst = false;

			if (exp.firstPlayer.compareTo("AGENT") == 0) {
				proposer.typeOfPlayer = "AGENT";
				// An agent has no ID
				proposer.setID(0);
			} else if (exp.firstPlayer.compareTo("HUMAN") == 0) {
				responder.typeOfPlayer = "AGENT";
				// An agent has no ID
				responder.setID(0);
			}
		} else {
			proposer.isProposerFirst = false;
			responder.isProposerFirst = true;

			if (exp.firstPlayer.compareTo("AGENT") == 0) {
				responder.typeOfPlayer = "AGENT";
				// An agent has no ID
				responder.setID(0);
			} else if (exp.firstPlayer.compareTo("HUMAN") == 0) {
				proposer.typeOfPlayer = "AGENT";
				// An agent has no ID
				proposer.setID(0);
			}
		}
	}
}