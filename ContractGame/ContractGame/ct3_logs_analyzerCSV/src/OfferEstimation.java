
public class OfferEstimation {

	private Double currentScore;
	private Double resultingScore;
	private Double hypotheticalScore;
	private Double utilityVal;
	
	public Double getCurrentScore() {
		return currentScore;
	}
	public void setCurrentScore(Double currentScore) {
		this.currentScore = currentScore;
	}
	public Double getResultingScore() {
		return resultingScore;
	}
	public void setResultingScore(Double resultingScore) {
		this.resultingScore = resultingScore;
	}
	public Double getHypotheticalScore() {
		return hypotheticalScore;
	}
	public void setHypotheticalScore(Double hypotheticalScore) {
		this.hypotheticalScore = hypotheticalScore;
	}
	public Double getUtilityVal() {
		return utilityVal;
	}
	public void setUtilityVal(Double utilityVal) {
		this.utilityVal = utilityVal;
	}
}