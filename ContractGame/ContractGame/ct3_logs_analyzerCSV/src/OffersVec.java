import java.util.Vector;

/**
 * This class holds a vector of Offers. It enables functions that evolves a list
 * of offers such as getting all the offers of a specific proposer.
 * 
 * @author Yael Blumberg
 * 
 */
public class OffersVec {

	private Vector<Offer> offers;

	public OffersVec() {
		offers = new Vector<Offer>();
	}

	public void addOffer(Offer newOffer) {
		offers.add(newOffer);
	}

	public Vector<Offer> getOffers() {
		return offers;
	}

	/*
	 * Returns all the offers a certain player proposed
	 */
	public OffersVec getOffersByProposer(int pin) {
		OffersVec playersOffers = new OffersVec();

		for (Offer of : offers) {

			if (of.getProposer() == pin) {
				playersOffers.addOffer(of);
			}
		}

		return playersOffers;
	}

	/*
	 * Returns all the offers that were accepted
	 */
	public OffersVec getAcceptedOffers() {
		OffersVec acceptedOffers = new OffersVec();

		for (Offer of : offers) {

			if (of.isAccepted()) {
				acceptedOffers.addOffer(of);
			}
		}

		return acceptedOffers;
	}
	
	public void setActualSent(int round, ChipsVec v, int sender)
			throws Exception {
		Offer of = offers.lastElement();

		if ((of.getRound() != round)) {
			throw new Exception(
					"An exchange accured not in the same round as the offer");

		} else {
			// Checking if this is the proposer or responder
			if (of.getProposer() == sender) {
				of.setActualSent(v);
			} else {
				of.setActualRecieved(v);
			}
			
		}
	}

	public int getTotalPromised(int pin) {
		
		int promised = 0;
		
		for (Offer of : offers) {
			if (of.isAccepted()) {
				if (of.getProposer() == pin) {
					promised += of.getPropRelaib().promisedBenefit;
				} else {
					promised += of.getRespRelaib().promisedBenefit;
				}
			}
		}
		
		return promised;
	}
	
	public int getTotalGave(int pin) {
		
		int gave = 0;
		
		for (Offer of : offers) {
			if (of.isAccepted()) {			
				if (of.getProposer() == pin) {
					gave += of.getPropRelaib().actualBenefit;
				} else {
					gave += of.getRespRelaib().actualBenefit;
				}
			}
		}
		
		return gave;
	}
	
	public double calcGenerousity (int pin) {
		
		int totalSugsested = 0;
		int offersNo = 0;
		double average = 0.0;
		
		for (Offer of : offers) {
			if (of.getProposer() == pin) {
				totalSugsested += of.getPropRelaib().promisedBenefit;
				++ offersNo;
			}
		}
		
		if (offersNo != 0) {
			average = (double)totalSugsested / offersNo;
		}
		
		return average;
	}
	
	public double calcSelfishness (int pin) {
		
		int totalAsked = 0;
		int offersNo = 0;
		double average = 0.0;
		
		for (Offer of : offers) {
			if (of.getProposer() == pin) {
				totalAsked += of.getRespRelaib().promisedBenefit;
				++ offersNo;
			}
		}
		
		if (offersNo != 0) {
			average = (double)totalAsked / offersNo;
		}
		
		return average;
	}
	
public double calcAcceptedGenerousity (int pin) {
		
		int totalSugsested = 0;
		int offersNo = 0;
		double average = 0.0;
		
		for (Offer of : offers) {
			if ((of.getProposer() == pin) && (of.isAccepted())) {
				totalSugsested += of.getPropRelaib().promisedBenefit;
				++ offersNo;
			}
		}
		
		if (offersNo != 0) {
			average = (double)totalSugsested / offersNo;
		}
		
		return average;
	}
	
	public double calcAcceptedSelfishness (int pin) {
		
		int totalAsked = 0;
		int offersNo = 0;
		double average = 0.0;
		
		for (Offer of : offers) {
			if ((of.getProposer() == pin) && (of.isAccepted())) {
				totalAsked += of.getRespRelaib().promisedBenefit;
				++ offersNo;
			}
		}
		
		if (offersNo != 0) {
			average = (double)totalAsked / offersNo;
		}
		
		return average;
	}
	
	public String typeOfOffersToString() {
		String str = "";
		for (Offer o : offers) {
			str += o.getType();
			str += "; ";
		}
		return str;
	}

	public String offersDetailsToString() {
		String offersStr = "[";
		for (Offer of : offers) {
			offersStr += of.offerDetailsToString() + "]   [";
		}
		return offersStr;
	}

	public String exchangesDetailsToString() {

		String sent = "[";
		for (Offer of : offers) {
			if (of.isAccepted()) {
				sent += of.actualExchangeToString().toString() + "]   [";
			}
		}
		return sent;
	}
}