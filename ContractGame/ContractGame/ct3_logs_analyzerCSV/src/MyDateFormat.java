import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MyDateFormat {

	Date date;
	
	public MyDateFormat(){
		date = new Date();
	}
	
	public Date parse(String s){
		String[] res1 = s.split("\t");
		String[]res2 = res1[0].split(" ");
	
		String dateStr =  res2[3]  ;  
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		try
		{ 
		date = df.parse(dateStr);             
		System.out.println("date = " + df.format(date)); 
		} catch (ParseException e) 
		{ 
		e.printStackTrace(); 
		System.err.println("offset " + e.getErrorOffset());
		} 
		
		return date;
	}
}
