<colors> CTRed CTPurple CTGreen grey78 </colors>
<rows> 7 </rows>
<columns> 5 </columns>
<squaresColors>	
1 0 0 2 1
1 3 1 3 1
1 0 1 2 1
1 3 1 3 1
1 0 1 2 1
1 3 1 3 1
1 0 1 2 1
</squareColors>
<goalPosition> 0 2 </goalPosition>
<numberOfPlayers> 2 </numberOfPlayers>
<firstPlayerChips> 1 0 13 10 </firstPlayerChips>
<firstPlayerStartingPos> 6 4 </firstPlayerStartingPos>
<secondPlayerChips> 5 0 10 0 </secondPlayerChips>
<secondPlayerStartingPos> 6 0 </secondPlayerStartingPos>
