import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Vector;

public class AnalyzerLebanon {

	MyDateFormat formatter = new MyDateFormat();
	Date startTime;
	Date endTime;
	int rounds = 0;
	PlayerData player1;
	PlayerData player2;
	ExperimentData exp;
	
	Vector<PlayerData> firstPlayersVec = null;
	Vector<PlayerData> secondPlayersVec = null;
	boolean bAddDataToPlayersVec = false; 
	PrintWriter out;
	String name;
	private static String HEADLINES = "file,type of board,type of player,rounds,game time,reason ending,type of player,pin,score,reached Goal,numOfAcceptedOffers,numOfRejectedOffers,numOfFullyKeptAgreements,numOfUnkeptAgreements,numOfPartiallyKeptAgreements,numOfPartiallyPlusKeptAgreements,Diff chips agreed vs actual,beyondAgreedChips,numOfProposals,isProposerFirst,type of offers,actual offers,actual sent";
	
	public void doAnalyzeDir(String dir, String firstPlayer){
		File dirF = new File(dir); 
		exp = new ExperimentData();
		exp.firstPlayer = firstPlayer;
		File[] children = dirF.listFiles();
		for (int i=0; i<children.length; i++) {
			if(children != null){
				if(children[i].isDirectory()){
					if(children[i].getName().contains("Strangers")){
						exp.playerType = ExperimentData.PlayerType.STRANGERS;
					}
					else if(children[i].getName().contains("Friends")){
						exp.playerType = ExperimentData.PlayerType.FRIENDS;
					}
					
					if(children[i].getName().contains("Board_D-D")){
						exp.boardType = ExperimentData.BoardType.BOTH_DD;
						name = dir + "\\LogsAnalysys_BOTH_DD_FirstPlayer_" + firstPlayer + ".csv";
					}
					else if(children[i].getName().contains("Agent_TI")){
						exp.boardType = ExperimentData.BoardType.AGENT_TI;
						name = dir + "\\LogsAnalysys_AGENT_TI_FirstPlayer_" + firstPlayer + ".csv";
					}
					else if (children[i].getName().contains("Human_TI")){
						exp.boardType = ExperimentData.BoardType.GUI_TI;
						name = dir + "\\LogsAnalysys_GUI_TI_FirstPlayer_" + firstPlayer + ".csv";
					}
					else if (children[i].getName().contains("_1ST_PLAYER_TI")){
						exp.boardType = ExperimentData.BoardType.FIRST_PLAYER_TI;
						name = dir + "\\LogsAnalysys_FirstPlayerTI_" + firstPlayer + ".csv";
					}
					else if (children[i].getName().contains("_2ND_PLAYER_TI")){
						exp.boardType = ExperimentData.BoardType.SECOND_PLAYER_TI;
						name = dir + "\\LogsAnalysys_SecondPlayerTI_" + firstPlayer + ".csv";
					}
					
					//If there is already a file with the same name in this directory don't override it, 
					// but append the new data to the existing one.
					//Otherwise, create new file and write to it.	
					try {
						File checkFile = new File(name);
						if(checkFile.exists()){
							FileOutputStream fop=new FileOutputStream(checkFile, true);
							out = new PrintWriter(fop);
							bAddDataToPlayersVec = true;
						}
						else {
							out = new PrintWriter(name);
							out.println(HEADLINES);
							bAddDataToPlayersVec = false;
						}
						
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}

					//Fill these vector with data corresponding to the type of board. Every time we start a new board we create
					//new vectors.
					if(!bAddDataToPlayersVec){
						firstPlayersVec = new Vector<PlayerData>();
						secondPlayersVec = new Vector<PlayerData>();
					}
					if(firstPlayersVec == null || secondPlayersVec == null)
						throw(new RuntimeException("Couldn't store data of players. Vector is null"));
						
					File[] gchildren = children[i].listFiles();
					for(int g = 0 ; g < gchildren.length; g++){
						if(gchildren[g].isDirectory()){
							String[] files = gchildren[g].list();
							if (files != null) {
								for (int f=0; f < files.length; f++) {
									// Get name of file or directory
									String fileName = files[f];
									if(fileName.startsWith("AlternativeOffersConfig", 0)){
										String myDir = gchildren[g].toString();
										String myFile = myDir + "\\" + fileName;
										doAnalyzeFile(myFile);
									}
								}
							}
						}
					}
				}
				else //the children of this directory are files:
				{
					String fileName = children[i].getName();
					if(fileName.startsWith("AlternativeOffersConfig", 0)){
						String myFile= children[i].toString();
						try {
							out = new PrintWriter(dir + "\\LogsAnalysys.csv");
							out.println(HEADLINES);
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						doAnalyzeFile(myFile);
					} 
				}
				String avg1 = firstPlayer + " average score is: " + calcAverageScore(firstPlayersVec);
				String secondPlayer;
				if(firstPlayer.compareTo("AGENT") == 0)
					secondPlayer = "HUMAN";
				else if(firstPlayer.compareTo("HUMAN") == 0) 
					secondPlayer = "AGENT";
				else secondPlayer = "HUMAN";
				String avg2 = secondPlayer  + " average score is: " + calcAverageScore(secondPlayersVec);
				String avgSum ="Average of sum of scores is: " +  calcAverageOfSumOfScores(firstPlayersVec, secondPlayersVec);
				out.println();
				out.println(avg1);
				out.println(avg2);
				out.println(avgSum);
				out.close();
			}
		}
	}
	public void doAnalyzeFile(String fileName){
		player1 = new PlayerData();
		player2 = new PlayerData();
		rounds = 0;

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			System.out.println("Processing file: " + fileName);
			String line;
			startTime = new Date();
			endTime = new Date();
			while ((line = br.readLine()) != null) { // while loop begins here
				String[] res;
				if(line.contains("This is the CT debug logger")){
					try{
						startTime = formatter.parse(line);
						System.out.println(startTime.toString());
					}catch(NullPointerException p){
						System.err.println(p.getMessage());
					}
				}
				else if (line.contains("Setting chips for player:")){
					res = line.split(": ");
					int pin = new Integer(res[res.length - 1]);
					int reminder = pin % 100;
					if(reminder == 0)
						player1.setPin(pin);
					else if (reminder == 10)
						player2.setPin(pin);
				}
				else if ( line.contains("00 ,role:") && line.contains("score is:")){
					res = line.split(":");
					player1.score = new Double(res[res.length - 1]).doubleValue(); 
					if(player1.score >= 100)
						player1.bReachedGoal = true; //default is false!
				}
				else if(line.contains("10 ,role:") && line.contains("score is:")){
					try{
						endTime = formatter.parse(line);
						System.out.println(startTime.toString());
					}catch(NullPointerException p){
						System.err.println(p.getMessage());
					}
					res = line.split(":");
					player2.score = new Double(res[res.length-1]).doubleValue();
					if(player2.score >= 100)
						player2.bReachedGoal = true; //default is false!
					//break; //even if there are more lines after this one they are irrelevant, don't process
				}else if (line.contains("Feedback phase, Round:"))
					rounds++;
				else if(line.contains("A New Phase Began: Communication Phase")){
					Vector<String> lines = new Vector<String>();
					lines.add(line);
					do{
						line = br.readLine();
						if(line == null){
							System.out.println("In middle of paragraph starting with [A New Phase Began: Communication Phase], line read is null");
							break;
						}
						lines.add(line);
						//ouch!! ugly, but in games that both players got all the chips needed it gets checked in the beginning of
						//comm. phase therefore the last comm. phase paragraph is irrelevant and instead we need to process end of game data.
						if(line.contains("All players have all needed chips to get to goal, end the game")){
							exp.reason = ExperimentData.ReasonEndGame.BOTH_HAVE_CHIPS;
							break;
						}
					}while((line != null ) && !line.contains("A Phase Ended: Communication Phase"));
					processCommPhase(lines);
				}
				else if(line.contains("A Phase Ended: Exchange Phase")){
					Vector<String> lines = new Vector<String>();
					lines.add(line);
					do{
						line = br.readLine();
						lines.add(line);
					}while((line != null ) && !line.contains("Protocol: A New Phase Began: Movement Phase"));
					processExchangePhase(lines);
				} 
				else if(line.contains("Ending game, all players move as much as they can towards goal")){
					exp.reason = ExperimentData.ReasonEndGame.NO_CONSEC_MOVEMENTS;
				}
				else if(line.contains("got to goal, advance the other and end game")){
					exp.reason = ExperimentData.ReasonEndGame.ONE_REACHED_GOAL;
				}
				else if(line.contains("All players have all needed chips to get to goal, end the game")){
					exp.reason = ExperimentData.ReasonEndGame.BOTH_HAVE_CHIPS;
					player1.bReachedGoal = true;
					player2.bReachedGoal = true;
				}

			} // end while 
		} // end try
		catch (IOException e) {
			System.err.println("Error: " + e);
		}

		player1.processAgreementsData();
		player2.processAgreementsData();

		long diff = endTime.getTime() - startTime.getTime();
		long diffSec = diff / 1000;
		long diffMin = diffSec / 60;
		long diffSecOfMin = diffSec % 60;

		exp.overallGameTime = diff;
		exp.numOfRoundsPlayed = rounds;

		System.out.println("lentgh of game is " + diffMin + " minutes and " + diffSecOfMin + " seconds");

		System.out.println("Exp. data: " + exp.toString());
		System.out.println("Player1 data: " + player1.toString());
		System.out.println("Player2 data: " + player2.toString());

		out.println(fileName + "," + exp.toString() + "," + player1.toString());
		out.println(fileName + "," + exp.toString() + "," + player2.toString());
		out.flush();

		if(firstPlayersVec == null)
			firstPlayersVec = new Vector<PlayerData>();
		if(secondPlayersVec == null)
			secondPlayersVec = new Vector<PlayerData>();
		firstPlayersVec.add(player1);
		secondPlayersVec.add(player2);
	}

	private void processCommPhase(Vector<String> lines){
		//Illegal paragraph, don't process it.
		if(lines.size() < 14)
			return;
		//do something with the lines
		System.out.println("Start processCommPhase");
		boolean bAccepted = false;

		//if(lines.size() == 14){
		bAccepted = true;
		//PlayerData proposer;
		int proposerPin = -1;
		//if(lines.get(3).contains("From player: 10")){
		if(lines.get(3).contains("From player: ")){
			String[] tokens = lines.get(3).split(": ");
			int pin = new Integer(tokens[tokens.length - 1]);
			int reminder = pin % 100;
			if(reminder == 0){	
				if(rounds == 0){
					player1.isProposerFirst = true;
					player2.isProposerFirst = false;
					if(exp.firstPlayer.compareTo("AGENT")== 0){
						player1.typeOfPlayer = "AGENT";
						player2.typeOfPlayer = "HUMAN";
					}
					else if(exp.firstPlayer.compareTo("HUMAN")== 0){
						player1.typeOfPlayer = "HUMAN";
						player2.typeOfPlayer = "AGENT";
					}
					else{ //BOTH_HUMAN
						player1.typeOfPlayer = "HUMAN";
						player2.typeOfPlayer = "HUMAN";
					}
				}
				proposerPin = player1.pin;
			} else{
				if(rounds == 0){
					player2.isProposerFirst = true;
					player1.isProposerFirst = false;
					if(exp.firstPlayer.compareTo("AGENT")== 0){
						player2.typeOfPlayer = "AGENT";
						player1.typeOfPlayer = "HUMAN";
					}
					else if(exp.firstPlayer.compareTo("HUMAN")== 0){
						player2.typeOfPlayer = "HUMAN";
						player1.typeOfPlayer = "AGENT";
					}
					else{ //BOTH_HUMAN
						player1.typeOfPlayer = "HUMAN";
						player2.typeOfPlayer = "HUMAN";
					}
				}
				proposerPin = player2.pin; 
			}
			
		}
		int index = lines.get(6).indexOf("'");
		String chips;
		Vector<Chips> chipsToSend;
		Vector<Chips> chipsToReceive;
		if(index != -1){
			chips = lines.get(6).substring(index);
			chipsToSend =  splitChipsString(chips);
			System.out.println("chipsToSend: " + chipsToSend);
		}
		else { 
			chipsToSend = new Vector<Chips>();
		}
		index = lines.get(7).indexOf("'");
		if(index != -1){
			chips = lines.get(7).substring(index);
			chipsToReceive =  splitChipsString(chips);
			System.out.println("chipsToReceive: " + chipsToReceive);
		}
		else chipsToReceive = new Vector<Chips>();

		/*
		int countToSend = 0;
		for(Chips cs : chipsToSend)
			countToSend += cs.number;
		int countToReceive = 0;
		for(Chips cs: chipsToReceive)
			countToReceive += cs.number;
		*/
		
		AgreementData c1 = new AgreementData();
		AgreementData c2 = new AgreementData();
		if(proposerPin == player1.pin){
			//player1.addOffer(countToSend, countToReceive);
			player1.addOffer(chipsToSend, chipsToReceive);
			player1.numOfProposals++;
			c1.toSend = chipsToSend;
			c1.round = rounds;

			c2.toSend = chipsToReceive;
			c2.round = rounds;
		}
		else {
			//player2.addOffer(countToSend, countToReceive);
			player2.addOffer(chipsToSend, chipsToReceive);
			player2.numOfProposals++;
			c2.toSend = chipsToSend;
			c2.round = rounds;

			c1.toSend = chipsToReceive;
			c1.round = rounds;
		}

		if (lines.get(12).contains("accepted offer") ){
			if(lines.get(12).contains("00 accepted") ){
				player1.numOfAcceptedOffers++;
				if((proposerPin % 100) == 10)
					System.err.println("Error! The proposer accepted the offer!");
			}
			else
				player2.numOfAcceptedOffers++;
			c1.bAccepted = true;
			c2.bAccepted = true;
		}
		else if (lines.get(12).contains("rejected offer")){
			if(lines.get(12).contains("00 rejected") )
				player1.numOfRejectedOffers++;
			else
				player2.numOfRejectedOffers++;
			c1.bAccepted = false;
			c2.bAccepted = false;
		}
		player1.addAgreementDataVec(c1);
		player2.addAgreementDataVec(c2);

		//If responder rejected offer then there is a swap in the roles and the communication phase has more lines to process:
		if(lines.size() > 14 && lines.get(13).contains("Swaping roles:")){
			String[] tokens = lines.get(14).split(": ");
			int linePropPin = new Integer(tokens[tokens.length -1]);
			if(linePropPin == proposerPin)
			//if( (lines.get(14).contains("Proposer is: 10") && proposerPin == 10) || (lines.get(14).contains("Proposer is: 20") && proposerPin == 20)) 
				System.err.println("Error! Should have swapped roles and didn't");
			else{
				if(proposerPin == player1.pin)
					proposerPin = player2.pin;
				else proposerPin = player1.pin;


				String firstLine, secondLine;
				if(lines.size() > 25){
					for(int i = 26; i < lines.size(); i++){
						firstLine = lines.get(i);
						if(!firstLine.contains("Chips to send by "))
							continue;
						index = firstLine.indexOf("'");
						if(index != -1){
							chips = firstLine.substring(index);
							chipsToSend =  splitChipsString(chips);
						}
						else chipsToSend  = new Vector<Chips>();
						//get next line:
						secondLine = lines.get(i+1);
						index = secondLine.indexOf("'");
						if(index != -1){
							chips = secondLine.substring(index);
							chipsToReceive =  splitChipsString(chips);
						}
						else chipsToReceive  = new Vector<Chips>();
						
						c1 = new AgreementData();
						c2 = new AgreementData();
						if(proposerPin == player1.pin){
							player1.addOffer(chipsToSend, chipsToReceive);
							player1.numOfProposals++;
	
							c1.toSend = chipsToSend;
							c1.round = rounds;
	
							c2.toSend = chipsToReceive;
							c2.round = rounds;
						}
						else {
							player2.addOffer(chipsToSend, chipsToReceive);
							player2.numOfProposals++;
							c2.toSend = chipsToSend;
							c2.round = rounds;
	
							c1.toSend = chipsToReceive;
							c1.round = rounds;
						}
						break; //we are only interested in processing the two lines that contain that chips to send and receive by players.
					}
				}
				
				if(lines.size() > 30){
					for(int i = 31; i < lines.size(); i++){
						firstLine = lines.get(i);
						if(!firstLine.contains("accepted offer") && !firstLine.contains("rejected offer"))
							continue;
						if(firstLine.contains("accepted")){
							if(firstLine.contains(" 00 accepted"))
								player1.numOfAcceptedOffers++;
							else
								player2.numOfAcceptedOffers++;
							c1.bAccepted = true;
							c2.bAccepted = true;
						}
						else { // line contains rejected:
							if(firstLine.contains("00 rejected"))
								player1.numOfRejectedOffers++;
							else
								player2.numOfRejectedOffers++;
							c1.bAccepted = false;
							c2.bAccepted = false;
						}
					}
					
				}

				player1.addAgreementDataVec(c1);
				player2.addAgreementDataVec(c2);

			}
		}
		for(String st : lines){
			System.out.println(st);
		}


		System.out.println("End processCommPhase");
	}

	
	private Vector<Chips> splitChipsString(String chips){
		// chips 'CTRed':0 'CTPurple':0 'CTGreen':0 'grey78':0 }
		chips.trim();
		System.out.println("chips string to split: " + chips);
		Vector<Chips> chipsVec = new Vector<Chips>();
		String[] res = chips.split(" ");
		for(int i= 0; i < res.length - 1; i++){
			String[] res2 = res[i].split(":");
			Chips c = new Chips(res2[0], new Integer(res2[1]).intValue());
			if(c.number > 0)
				chipsVec.add(c);
		}
		return chipsVec;
	}

	private void processExchangePhase(Vector<String> lines){

		System.out.println("Start processExchangePhase");		
		for(String st : lines){
			System.out.println(st);
			if(st.contains("00 sent:")){
				int index = st.indexOf("'");
				if(index != -1){
					String chips = st.substring(index);
					Vector<Chips> actualSent = splitChipsString(chips);
					player1.setActualSent(rounds, actualSent);
				}
				else{ 
					Vector<Chips> actualSent = new Vector<Chips>();
					player1.setActualSent(rounds, actualSent);
				}
			}
			else if (st.contains("10 sent:")){
				int index = st.indexOf("'");
				if(index != -1){
					String chips = st.substring(index);
					Vector<Chips> actualSent = splitChipsString(chips);
					player2.setActualSent(rounds, actualSent);
				}
				else{
					Vector<Chips> actualSent = new Vector<Chips>();
					player2.setActualSent(rounds, actualSent);
				}
			}
		}
		System.out.println("End processExchangePhase");

	}

	private void processMovementPhase(Vector<String> lines){

	}

	private double calcAverageScore(Vector<PlayerData> vec){
		if(vec.isEmpty())
			return 0;
		double avg = 0;
		for(PlayerData pD : vec){
			avg += pD.score;
		}
		avg /=(double) vec.size();
		return avg;
	}
	
	private double calcAverageOfSumOfScores(Vector<PlayerData>firstPlayersVec, Vector<PlayerData>secondPlayersVec){
		if(firstPlayersVec.isEmpty() || secondPlayersVec.isEmpty())
			return 0;
		if(firstPlayersVec.size() != secondPlayersVec.size())
			return 0;
		
		double avg = 0;
		for (int i = 0; i < firstPlayersVec.size(); i++){
			avg += firstPlayersVec.get(i).score + secondPlayersVec.get(i).score;
		}
		avg /= (double)firstPlayersVec.size();
		return avg;
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length == 0){
			System.out.println("Please enter log's directory and first player");
			System.exit(0);
		}
		//DBG

		// Make a new Date object. It will be initialized to the
		// current time.
		Date now = new Date();

		// Print the result of toString()
		String dateString = now.toString();
		System.out.println(" 1. " + dateString);

		long lStartTime = now.getTime();
		System.out.println(" 2." + lStartTime);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
			System.err.println(ex.getLocalizedMessage());
		}

		Date after = new Date();
		System.out.println(" 3. " + after.toString());
		long endTime = after.getTime();
		System.out.println(" 4." + endTime);

		long diff = endTime - lStartTime;
		System.out.println(" 5. difference in ms is " + diff);
		long diffSec = diff / 1000;
		System.out.println(" 6. difference in seconds is " + diffSec);
		long diffMin = diffSec / 60;
		long diffSecOfMin = diffSec % 60;
		System.out.println(" 7. difference is : " + diffMin + " minutes and " + diffSecOfMin + " seconds");
		//DBG




		String dir = args[0];
		String firstPlayer = args[1];
		AnalyzerLebanon a = new AnalyzerLebanon();
		a.doAnalyzeDir(dir, firstPlayer);

		System.out.println("Exp. data: " + a.exp.toString());
		System.out.println("Player1 data: " + a.player1.toString());
		System.out.println("Player2 data: " + a.player2.toString());

	}
}
