import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MyDateFormat {

	Date date;
	//public enum Months {Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Set, Oct, Nov, Dec};
	
	public MyDateFormat(){
		date = new Date();
	}
	private String getMonthNumber(String m){
		String mNum = "00";
		if(m.compareTo("Jan") == 0)
			mNum = "01";
		else if(m.compareTo("Apr") == 0)
			mNum = "04";
		
		return mNum;
	}
	
	public Date parse(String s){
		String[] res1 = s.split("\t");
		String[]res2 = res1[0].split(" ");
		//String dateStr = res2[1] + "/" + res2[2] + "/" +  res2[5] + " " + res2[3] ;  
		//SimpleDateFormat df = new SimpleDateFormat("MMM/dd/yyyy HH:mm:ss");
		String month = getMonthNumber(res2[1]);
		String dateStr =  res2[3]  ;  
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		try
		{ 
		date = df.parse(dateStr);             
		System.out.println("date = " + df.format(date)); 
		} catch (ParseException e) 
		{ 
		e.printStackTrace(); 
		System.err.println("offset " + e.getErrorOffset());
		} 
		
		return date;
	}
}
