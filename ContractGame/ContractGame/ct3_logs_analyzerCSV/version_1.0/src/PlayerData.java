import java.util.Vector;

public class PlayerData {
	public int pin = -1;
	private int ID = -1;
	public double score = 0.0;
	public int numOfAcceptedOffers = 0;
	public int numOfRejectedOffers = 0;
	public int numOfFullyKeptAgreements = 0;
	public int numOfUnkeptAgreements = 0;
	public int numOfPartiallyKeptAgreements = 0;
	public int numOfPartiallyPlusKeptAgreements = 0;
	public int numOfProposals = 0;
	public boolean isProposerFirst = false;
	public boolean isTI = false;
	public int numOfStartingChips = 0;
	public String typeOfPlayer; //AGENT or HUMAN
	public boolean bReachedGoal = false;
	
	private Vector<AgreementData> agreementDataVec = null;
	//private Vector<ChipsVecData> chipsToReceiveVec = null;

	//private Vector<ChipsVecData> actualChipsSentVec = null;
	//private Vector<ChipsVecData> actualChipsReceivedVec = null;

	private Vector<OfferType> typesOfOffers = new Vector<OfferType>();
	private Vector<String> offers = new Vector<String>();

	public Vector<Integer> agreedActualSentDiff = new Vector<Integer>();
	public Vector<Integer> chipsBeyondAgreed = new Vector<Integer>();
	/*
	public void addOffer(int numToSend, int numToReceive){
		OfferType ofTy = new OfferType(numToSend, numToReceive);
		typesOfOffers.add(ofTy);
	}*/
	
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
	
	public void addOffer(Vector<Chips> chipsToSend, Vector<Chips> chipsToReceive){
		int countToSend = 0;
		for(Chips cs : chipsToSend)
			countToSend += cs.number;
		int countToReceive = 0;
		for(Chips cs: chipsToReceive)
			countToReceive += cs.number;

		OfferType ofTy = new OfferType(countToSend, countToReceive);
		typesOfOffers.add(ofTy);
		String offer = offerToString(chipsToSend, chipsToReceive);
		offers.add(offer);
	}
	
	public void addAgreementDataVec(AgreementData chipsVec){
		if(agreementDataVec == null)
			agreementDataVec = new Vector<AgreementData>();
		agreementDataVec.add(chipsVec);
	}
	
	public String typeOfOffersToString(){
		String str = "";
		for(OfferType o : typesOfOffers){
			str += o;
			str += "; ";
		}
		
		return str;
	}
	public String offerToString(Vector<Chips> chipsToSend, Vector<Chips> chipsToReceive){
		String offer = "Send: ";
		for(Chips cs: chipsToSend){
			offer += cs.toString() + "; ";
		}
		offer += "Receive: ";
		for(Chips cs: chipsToReceive){
			offer += cs.toString() + "; ";
		}
		return offer;
	}
	
	public String offersToString(Vector<String> offers){
		String offersStr = "[";
		for(String s: offers)
			offersStr += s + "]   [";
		//offersStr.substring(0, offersStr.length() - 5);
		
		return offersStr;
	}
	//agreedActualDiffToString() + chipsBeyondAgreedToString()
	public String VectorOfIntToString(Vector<Integer> intVec){
		String str = "[";
		for(Integer i : intVec)
			str += i.toString() + ";" ;
		str += "]";
		return str;
	}
	
	public String IdToString() {
		if (ID == -1) {
			return " ";
		} else {
			return String.valueOf(ID);
		}
	}
	
	public void setActualSent(int round, Vector<Chips> v){
		for(AgreementData aD : agreementDataVec){
			if((aD.round == round) && aD.bAccepted){
				aD.actualSent = v;
			}
		}
	}
	
	public void processAgreementsData(){
		for(AgreementData aD : agreementDataVec){
			if(aD.bAccepted){
				ChipsDifferenceAndBeyond cBd = checkExchange(aD.toSend, aD.actualSent);
				agreedActualSentDiff.add(cBd.agreedActualSentDiff);
				chipsBeyondAgreed.add(cBd.chipsBeyondAgreed);
			}
		}
	}
	
	public String actualSentToString(){
		String sent = new String();
		for(AgreementData aD : agreementDataVec){
			if(aD.bAccepted){
				for(Chips cs: aD.actualSent)
					sent += cs.toString()+ "; ";
			}
		}
		return sent;
	}
	
	/*
	public void addActualChipsSentVec(ChipsVecData chipsVec){
		if(actualChipsSentVec == null)
			actualChipsSentVec = new Vector<ChipsVecData>();
		actualChipsSentVec.add(chipsVec);
	}
	*/
	
	/*
	public void processAgreementsData(){
		int pI= 0;
		int pJ= 0;
		for(pI = 0; pI < chipsToSendVec.size(); ){
			for(pJ = 0; pJ < actualChipsSentVec.size(); ){
				if(chipsToSendVec.get(pI).round == actualChipsSentVec.get(pJ).round ){
					if(chipsToSendVec.get(pI).bAccepted){
						checkExchange(chipsToSendVec.get(pI).v, actualChipsSentVec.get(pJ).v);
						pI++; //Found a match, increment both "pointers".
						pJ++;
					}
					else pI++; //
				}
				else if(chipsToSendVec.get(pI).round < actualChipsSentVec.get(pJ).round ){ //This means that there was a proposal, but nothing
																						  //was actually sent in this round
					Vector<Chips> emptyVec = new Vector<Chips>(); 
					checkExchange(chipsToSendVec.get(pI).v, emptyVec);
					pI++;
				}
			}
		}
		System.out.println("After loops pJ = " + pJ);
		
		//There were more exchanges, even though there weren't any more proposals.
		if( (pI == chipsToSendVec.size() -1) && (pJ < actualChipsSentVec.size()))
			for(int j = pJ; j < actualChipsSentVec.size(); j++){
				Vector<Chips> emptyVec = new Vector<Chips>(); 
				checkExchange(chipsToSendVec.get(pI).v, emptyVec);
			}
		//There were more proposal than exchanges:
		if( (pJ == actualChipsSentVec.size() -1) && )
		
	}
		*/
	class ChipsDifferenceAndBeyond{
		public int agreedActualSentDiff = 0;
		public int chipsBeyondAgreed = 0;
	}
	public ChipsDifferenceAndBeyond checkExchange(Vector<Chips> chipsToSend, Vector<Chips> actualSent){
		System.out.println("checkExchange for: " + pin);
		System.out.println("chipsToSend: " + chipsToSend);
		System.out.println("actualSent: " + actualSent);
		ChipsDifferenceAndBeyond diffBeyond = new ChipsDifferenceAndBeyond();
		
		if(chipsToSend.equals(actualSent)){
			numOfFullyKeptAgreements++;
			diffBeyond.agreedActualSentDiff = 0;
			diffBeyond.chipsBeyondAgreed = 0;
			System.out.println(" numOfFullyKeptAgreements: " + numOfFullyKeptAgreements);
		}
		else if(strictlyContains(chipsToSend, actualSent)){
			numOfPartiallyKeptAgreements++;
			int chipsToSendCount = getTotalNumChips(chipsToSend);
			int chipsActualSentCount = getTotalNumChips(actualSent);
			diffBeyond.agreedActualSentDiff = chipsToSendCount - chipsActualSentCount;
			System.out.println("numOfPartiallyKeptAgreements: " + numOfPartiallyKeptAgreements);
		}
		else if(!isVectorOfChipsEmpty(actualSent)){
			numOfPartiallyPlusKeptAgreements++;
			diffBeyond.agreedActualSentDiff = 0;
			for(Chips cToSend : chipsToSend){
				boolean bMatchColor = false;
				for(Chips cActual : actualSent){
					if(cToSend.color.compareTo(cActual.color)== 0){
						if(cToSend.number >= cActual.number)
							diffBeyond.agreedActualSentDiff += cToSend.number - cActual.number;
						bMatchColor = true;
						break;
					}
				}
				if(!bMatchColor)
					diffBeyond.agreedActualSentDiff += cToSend.number;
			}
			for(Chips cActual : actualSent){
				boolean bMatchColor = false;
				for(Chips cToSend : chipsToSend){
					if(cActual.color.compareTo(cToSend.color)== 0){
						if(cActual.number >= cToSend.number)
							diffBeyond.chipsBeyondAgreed += cActual.number - cToSend.number;
						bMatchColor = true;
						break;
					}
				}
				if(!bMatchColor)
					diffBeyond.chipsBeyondAgreed += cActual.number;
			}
			
			System.out.println("numOfPartiallyPlusKeptAgreements: " + numOfPartiallyPlusKeptAgreements);
		}
		else {
			numOfUnkeptAgreements++;
			diffBeyond.agreedActualSentDiff = getTotalNumChips(chipsToSend);
			diffBeyond.chipsBeyondAgreed = 0;
			System.out.println("numOfUnkeptAgreements: " + numOfUnkeptAgreements);
		}
		
		return diffBeyond;
		
	}
	private boolean isVectorOfChipsEmpty(Vector<Chips> cV){
		if(cV.isEmpty())
			return true;
		int sum = 0;
		for(Chips c: cV){
			sum += c.number;
		}
		if(sum == 0)
			return true;
		return false;
	}
	/*
	private boolean areSameChipsSets(Vector<Chips> c1, Vector<Chips> c2){
		boolean areSame = true;
		List<Chips> cL1 = c1.
		for(Chips chip1: c1){
			for(Chips chip2: c2){
				if(chip1.color.compareTo(chip2.color) == 0)
					if(chip)
			}
		}
		
		
		return areSame;
	}
	*/
	//return true iff vector every element in c2 is in c1, except for the null element.
	//that is, if the second set is empty it doesn't count as if utis contained in the first. 
	public boolean strictlyContains(Vector<Chips> c1, Vector<Chips> c2){
		// check for containment
		System.out.println("check for containment");
		System.out.println("c1:" + c1);
		System.out.println("c2:" + c2);
		if(!isVectorOfChipsEmpty(c1) && isVectorOfChipsEmpty(c2)){
			System.out.println("not contained");
			return false;
		}
	
        Vector<Chips> subtract = subtract(c1, c2);
        for (Chips cs : subtract)
            if (cs.number < 0){
            	System.out.println("not contained");
            	return false;
            }
        System.out.println("contained");
        return true;
	}
	
	
	private Vector<Chips> addChipsVectors(Vector<Chips> c1, Vector<Chips> c2){
		System.out.println("chip vectors to add:");
		System.out.println("C1: " + c1);
		System.out.println("C2: " + c2);

		Vector<Chips> addition = new Vector<Chips>();
		//Get all the colors of the first vector into a new vector, all those color with number 0
		for(Chips cs: c1) {
			Chips newChips = new Chips(cs.color, 0);
			addition.add(newChips);
		}
		//If there is any other color in c2 that doesn't exist yet in addition then add it, other wise skip it.
		for(Chips cs: c2){
			Chips newChips = new Chips(cs.color, 0);
			if(!addition.contains(newChips))
				addition.add(newChips);
		}
		//Now I've got all the colors in one vector.
		for(Chips cs: addition){
			int n1 = getColor(c1, cs.color).number;
			int n2 = getColor(c2, cs.color).number;
			cs.number = n1 + n2;
		}
		System.out.println("result " + addition);
		return addition;
	}
	
	private Vector<Chips> getNegation(Vector<Chips> chipsVect){
		System.out.println("Get negation of: " + chipsVect);
		Vector<Chips> negation = new Vector<Chips>();
		for(Chips cs: chipsVect)
			negation.add(new Chips(cs.color, cs.number * -1));
		System.out.println("neg is: " + negation);
		return negation;
	}
	
	//return c1-c2
	private Vector<Chips> subtract(Vector<Chips> c1, Vector<Chips> c2){
		System.out.println("C1("+ c1+ ") - C2(" + c2 + ")");
		Vector<Chips> subtract = addChipsVectors(c1, getNegation(c2));
		System.out.println("result is: " + subtract);
		return subtract;
	}
	
	private Chips getColor(Vector<Chips> chipsVect, String color){
		for(Chips cs: chipsVect)
			if(cs.color.equals(color))
				return cs;
		Chips dummy = new Chips("", 0);
		return dummy;
	}
	private int getTotalNumChips(Vector<Chips> chipsVect){
		int total = 0;
		for(Chips cs : chipsVect){
			total += cs.number;
		}
		return total;
	}
	public PlayerData(int pin){
		this.pin = pin;
	}
	public PlayerData(){
		setPin(0);
	}
	
	public void setPin (int pin) {
		this.pin = pin;
	}
	public class AccRo{
		boolean bAccept;
		int round;
	}
	
	public String toString(){
		String retVal = typeOfPlayer + "," + isTI + "," +pin + "," + IdToString() + "," + score + "," + bReachedGoal + "," + numOfAcceptedOffers + "," + numOfRejectedOffers + "," + numOfFullyKeptAgreements + ",";
		retVal += numOfUnkeptAgreements + "," + numOfPartiallyKeptAgreements + "," + numOfPartiallyPlusKeptAgreements + ",";
		retVal += VectorOfIntToString(agreedActualSentDiff) + "," + VectorOfIntToString(chipsBeyondAgreed) + ",";
		//retVal += agreedActualSentDiff.toString() + "," + chipsBeyondAgreed.toString() + ",";
		retVal += numOfProposals + "," + isProposerFirst + "," ;
		retVal += typeOfOffersToString() + "," + offersToString(offers) + "," + actualSentToString();
		return retVal;
	}
}
