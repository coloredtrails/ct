

public class ExperimentData {

	//public enum BoardType {BOTH_DD, AGENT_TI, GUI_TI};
	//May be instead of complicating everything with adding two new classes thta inherit from expdata
	//just add more types to the enum here and that's it.
	public enum BoardType {BOTH_DD, AGENT_TI, GUI_TI, FIRST_PLAYER_TI, SECOND_PLAYER_TI, BOTH_HUMAN_TI_TD};
	
	//public enum PlayerType {BOTH_DD_FRIENDS, HUMAN_TI_FRIENDS, BOTH_DD_STRANGERS, HUMAN_TI_STRANGERS, COMPUTER_TI_FRIENDS, COMPUTER_TI_STRANGERS};
	public enum PlayerType {FRIENDS, STRANGERS, NONE};
	public PlayerType playerType = PlayerType.NONE;
	
	public BoardType boardType = BoardType.BOTH_DD;
	//public BoardTypeClass boardType;
	
	public enum ReasonEndGame {NO_CONSEC_MOVEMENTS, BOTH_HAVE_CHIPS, ONE_REACHED_GOAL, UNDEFINED};
	
	public ReasonEndGame reason = ReasonEndGame.UNDEFINED;
	
	public int numOfRoundsPlayed = 0;
	
	//length of time it took to play the game from the beginning to the end in seconds.
	public long overallGameTime = 0;  
	public boolean bIsFirstGameInExperiment = false;
	public String firstPlayer; //AGENT or HUMAN or BOTH_HUMAN
	
		
	protected String ReasonEndGameToString(ReasonEndGame r){
		String rVal;
		if(r == ReasonEndGame.BOTH_HAVE_CHIPS)
			rVal = "BOTH_HAVE_CHIPS";
		else if(r == ReasonEndGame.NO_CONSEC_MOVEMENTS)
			rVal = "NO_CONSEC_MOVEMENTS";
		else if(r == ReasonEndGame.ONE_REACHED_GOAL)
			rVal = "ONE_REACHED_GOAL";
		else rVal = "UNDEFINED";
		
		return rVal;
	}
	
	private String boardTypeToString(BoardType board){
		String bStr;
		if(board == BoardType.BOTH_DD)
			bStr = "BOTH_DD";
		else if(board == BoardType.AGENT_TI)
			bStr = "AGENT_TI";
		else if(board == BoardType.GUI_TI)
			bStr = "GUI_TI";
		else if(board == BoardType.FIRST_PLAYER_TI)
			bStr = "FIRST_PLAYER_TI";
		else if(board == BoardType.BOTH_HUMAN_TI_TD)
			bStr = "BOTH_HUMAN_TI_TD";
		else bStr = "SECOND_PLAYER_TI";
		
		return bStr;
				
	}
	
	private String playerTypeToString(){
		String str;
		if(playerType == PlayerType.FRIENDS)
			str = "Friends";
		else if (playerType == PlayerType.STRANGERS)
			str = "Strangers";
		else str = "Undefined";
	
		return str;
	}
	protected String timetoString(){
		long diffSec = overallGameTime / 1000;
		long diffMin = diffSec / 60;
		long diffSecOfMin = diffSec % 60;
		String res = diffMin + "mins " + diffSecOfMin + "secs";
		return res;
	}
	
	public String toString(){
		String retVal = boardTypeToString(boardType) + "," + playerTypeToString() + "," + numOfRoundsPlayed + "," + timetoString() + "," +ReasonEndGameToString(reason);
		return retVal;
	}
}
