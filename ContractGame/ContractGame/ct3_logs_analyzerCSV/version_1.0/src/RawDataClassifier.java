import java.io.*;
import java.util.Date;


/**
 * This program receives a directory name as a parameter.
 * Searches it for files starting with: AlternativeOfferConfig
 * For each file found it opens it and looks for the the players IDs
 * These will reveal the type of game this log belongs to according to the following key:
 * 100  (both  dependent, friends) 
 * 200  (human  independent, friends) 
 * 300 (both dependent, strangers) 
 * 400 (human independent, strangers) 
 * 500 (computer independent, friends) 
 * 600 (computer independent, strangers)
 * @author Yael Ejgenberg
 *
 */

public class RawDataClassifier {

	private final static String BOTH_DD_FRIENDS = "Board_D-D_Friends"; //100
	private final static String HUMAN_TI_FRIENDS = "Board_D-I_Human_TI_Friends"; //200
	private final static String BOTH_DD_STRANGERS = "Board_D-D_Strangers"; //300
	private final static String HUMAN_TI_STRANGERS= "Board_D-I_Human_TI_Strangers"; //400
	private final static String COMPUTER_TI_FRIENDS = "Board_D-I_Agent_TI_Friends"; //500
	private final static String COMPUTER_TI_STRANGERS = "Board_D-I_Agent_TI_Strangers"; //600
	private String mainDir;
	
	public void doClassifyFiles(String dir){
		mainDir = dir;
		//Create all possible directories for simplicity.
		createDir(mainDir + "\\" + BOTH_DD_FRIENDS);
		createDir(mainDir + "\\" + HUMAN_TI_FRIENDS);
		createDir(mainDir + "\\" + BOTH_DD_STRANGERS);
		createDir(mainDir + "\\" + HUMAN_TI_STRANGERS);
		createDir(mainDir + "\\" + COMPUTER_TI_FRIENDS);
		createDir(mainDir + "\\" + COMPUTER_TI_STRANGERS);
		
		File dirF = new File(mainDir); 
		File[] filesList = dirF.listFiles();
		int counter = 0;
		if(filesList != null ) {
			for (int i=0; i< filesList.length; i++) {
				 if(!filesList[i].isDirectory()){
					// Get name of file or directory
					String fileName = filesList[i].getName();
					if(fileName.startsWith("AlternativeOffersConfig", 0) && (!fileName.endsWith("-1.txt"))){
						doCheckFile(mainDir + "\\" + fileName, counter++);
					}
				}
			}
		}
	}
	
	private void doCheckFile(String fileName, int counter){
		String plainName = new File(fileName).getName();
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			System.out.println("Processing file: " + fileName);
			String line;
			while ((line = br.readLine()) != null) { // while loop begins here
				String[] res;
				if(line.contains("Setting chips for player:")){
					res = line.split(" ");
					int playerID = new Integer(res[res.length - 1]).intValue();
					switch(playerID){
					case 100:
					case 110:{
						String newDir =mainDir + "\\" + BOTH_DD_FRIENDS + "\\" + "Player" + counter; 
						createDir(newDir);
						copyFile(new File(fileName) , new File(newDir+ "\\" + plainName));
						break;
					}
					case 200:
					case 210:{
						String newDir =mainDir + "\\" + HUMAN_TI_FRIENDS + "\\" + "Player" + counter; 
						createDir(newDir);
						copyFile(new File(fileName) , new File(newDir + "\\" + plainName));
						break;
					}
					case 300:
					case 310:{
						String newDir =mainDir + "\\" + BOTH_DD_STRANGERS + "\\" + "Player" + counter; 
						createDir(newDir);
						copyFile(new File( fileName) , new File(newDir + "\\" + plainName));
						break;
					}
					case 400:
					case 410:{
						String newDir =mainDir + "\\" + HUMAN_TI_STRANGERS + "\\" + "Player" + counter; 
						createDir(newDir);
						copyFile(new File(fileName) , new File(newDir + "\\" + plainName));
						break;
					}
					case 500:
					case 510:{
						String newDir =mainDir + "\\" + COMPUTER_TI_FRIENDS + "\\" + "Player" + counter; 
						createDir(newDir);
						copyFile(new File(fileName) , new File(newDir + "\\" + plainName));
						break;
					}
					case 600:
					case 610:{
						String newDir =mainDir + "\\" + COMPUTER_TI_STRANGERS + "\\" + "Player" + counter; 
						createDir(newDir);
						copyFile(new File(fileName) , new File(newDir + "\\" + plainName));
						break;
					}
					
					}
				}
			}
		}// end try
		catch (IOException e) {
			System.err.println("Error: " + e);
		}

	}
	
	  private void createDir(String dirName) {
	        // check if output dir exists. if not - create it
	        File dir = new File(dirName);
	        if (!dir.exists()) {
	            dir.mkdirs();
	        }
	    }

	    void mvFiles(String srcName, String dstName) {
	        File src = new File(srcName);
	        File dst = new File(dstName);

	        copyFile(src, dst);

	        src.delete();
	    }

	    private void copyFile(File src, File dst) {
	        try {
	            InputStream in = new FileInputStream(src);
	            OutputStream out = new FileOutputStream(dst);

	            // Transfer bytes from in to out
	            byte[] buf = new byte[1024];
	            int len;
	            while ((len = in.read(buf)) > 0) {
	                out.write(buf, 0, len);
	            }
	            in.close();
	            out.close();
	        } catch (IOException e) {
	            System.err.println("Caught IOException: " + e.getMessage());
	        }
	    }

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length == 0){
			System.out.println("Please enter log's directory");
			System.exit(0);
		}
		
		String dir = args[0];
		RawDataClassifier a = new RawDataClassifier();
		a.doClassifyFiles(dir);
	}

}
