package edu.harvard.eecs.airg.coloredtrails.shared.discourse;

import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

public class RoleChangedMessage extends DiscourseMessage {
	int playerID;
	String role;

	public RoleChangedMessage(int receiverID,  String role) {
	        super(-1, receiverID, "rolechanged", -1);
	        playerID = receiverID;
	        this.role = role;
	}
}
