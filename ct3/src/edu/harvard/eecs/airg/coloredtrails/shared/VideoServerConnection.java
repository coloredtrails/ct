package edu.harvard.eecs.airg.coloredtrails.shared;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class VideoServerConnection implements Closeable {

	private Socket videoServerSocket;
	private PrintWriter videoServerOut;
	private BufferedReader videoServerIn;

	private final String VIDEO_SERVER = "lib/adminconfig/videoServer.txt";
	private final String END_OF_MSG = "444444";

	private final Logger log = Logger.getLogger(this.getClass());

	public VideoServerConnection() throws IOException {

		videoServerSocket = null;
		videoServerOut = null;
		videoServerIn = null;
	}

	public void connect() throws IOException {

		String host = null;
		int port = 0;

		try {

			// Getting the video server details from the properties file
			FileReader fr = new FileReader(VIDEO_SERVER);
			Scanner in = new Scanner(fr);

			host = in.next();
			port = in.nextInt();

			in.close();
			fr.close();

		} catch (FileNotFoundException e) {

			log.error("Can't find video server property file under: "
					+ VIDEO_SERVER);
			throw e;
		} catch (IOException e) {
			log.error("Problem reading from video server properties file: "
					+ VIDEO_SERVER);
			throw e;
		}

		try {
			videoServerSocket = new Socket(host, port);
			videoServerOut = new PrintWriter(
					videoServerSocket.getOutputStream(), true);
			videoServerIn = new BufferedReader(new InputStreamReader(
					videoServerSocket.getInputStream()));
		} catch (UnknownHostException e) {
			log.error("Unknown video server host " + host);
			throw e;
		} catch (IOException e) {
			log.error("Couldn't get I/O for " + "the connection to: " + host);
			throw e;
		}
	}

	public void sendVideoInitMessage(String player1, String player2,
			String ip1, String ip2) throws IOException {

		String message = "<Message Type=\"InitMessage\" Client1Name=\""
				+ player1 + "\" Client1IP=\"" + ip1 + "\" Client2Name=\""
				+ player2 + "\" Client2IP=\"" + ip2 + "\" RootDir=\"\" />";

		videoServerOut.println(message + END_OF_MSG);

		// Waiting for a response from the video server that all users have
		// video on
		//videoServerIn.readLine();
	}

	private void sendControlMessage(String type, String param) {

		String message = "<Message Commmand=\"" + type + "\" Argument1=\""
				+ param + "\" Type=\"ControlMessage\" />";

		videoServerOut.println(message + END_OF_MSG);
	}

	public void sendStartMessage() {
		String timeStamp = new SimpleDateFormat("HH:mm:ss.SSSSS")
				.format(Calendar.getInstance().getTime());
		sendControlMessage("Start", timeStamp);
	}

	public void sendTickMessage(String folder) {
		sendControlMessage("Tick", folder);
	}

	public void sendStopMessage() {
		sendControlMessage("Stop", null);
	}

	@Override
	public void close() throws IOException {

		videoServerIn.close();
		videoServerOut.close();
		videoServerSocket.close();
	}
}