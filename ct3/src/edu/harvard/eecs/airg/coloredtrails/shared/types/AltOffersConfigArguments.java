package edu.harvard.eecs.airg.coloredtrails.shared.types;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: kobigal
 * Date: Jun 11, 2009
 * Time: 11:20:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class AltOffersConfigArguments implements Serializable {
	 
	private static final long serialVersionUID = -8952856688390089107L;
	
	// Dependency relationships
    private  String dep ;
    // Social relationships
    private String relationship;
    // Controller identifier
    private String experimentId = "";
    
    private boolean isVideoOn;
    
	public int roundNum = -1;
	public int perRoundNum = -1;
 
    public AltOffersConfigArguments(String name, String r)
    {
        dep = new String(name);
        relationship= new String(r);
        isVideoOn=false;
    }
    
    public AltOffersConfigArguments(String name, String r, String videoInd)
    {
        dep = new String(name);
        relationship= new String(r);
        setVideoState(videoInd);        
    }
    
	public AltOffersConfigArguments(int numHumanHumanA, int numHumanHumanB, int numHumanComputer)
	{
		this.numHumanHumanA = numHumanHumanA;
		this.numHumanHumanB = numHumanHumanB;
		this.numHumanComputer = numHumanComputer;
		isVideoOn=false;
	}

	public AltOffersConfigArguments(int numHumanHumanA, int numHumanHumanB, int numHumanComputer, String videoInd)
	{
		this.numHumanHumanA = numHumanHumanA;
		this.numHumanHumanB = numHumanHumanB;
		this.numHumanComputer = numHumanComputer;
		setVideoState(videoInd);
	}
	
	private void setVideoState (String videoInd) {
        if (videoInd.equals("VIDEO")) {
        	this.isVideoOn=true;
        } else {
        	this.isVideoOn=false;
        }
	}
	
	public AltOffersConfigArguments(AltOffersConfigArguments aoca)
	{
		this.experimentId = aoca.experimentId;
		this.dep = aoca.dep;
		this.relationship = aoca.relationship;
		
		this.numHumanHumanA = aoca.numHumanHumanA;
		this.numHumanHumanB = aoca.numHumanHumanB;
		this.numHumanComputer = aoca.numHumanComputer;
		this.playerNames = aoca.playerNames;
		this.isVideoOn = aoca.isVideoOn;
	}
	
    public String getDep () {
        return dep; 
    }

    public String getRelationship () {
        return relationship;
       
    }

    public void setDep(String dep) {
		this.dep = dep;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getExperimentId() {
		return experimentId;
	}

	public void setExperimentId(String experimentId) {
		this.experimentId = experimentId;
	}
	
    public boolean isVideoOn () {
        return isVideoOn;
       
    }

    public void setVideoStatus(boolean videoOn) {
		isVideoOn = videoOn;
	}
    
	/**
	 * The total number of human players in group A on the HH board (DD, then ID board). Divide this number in half
	 * to get the number of simultaneous games within the group.
	 */
	public int numHumanHumanA;
	
	/**
	 * The total number of human players in group B on the HH board (ID, then DD board). Divide this number in half
	 * to get the number of simultaneous games within the group.
	 */
	public int numHumanHumanB;
	
	/**
	 * The total number of human players on the HC board.
	 */
	public int numHumanComputer;
	
	public String[] playerNames = new String[2];
}
