package edu.harvard.eecs.airg.coloredtrails.shared.discourse;

import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

public class RoundNotificationMessage extends DiscourseMessage {
	int round;

	public RoundNotificationMessage(int senderID, int receiverID, int messageId, int round) {
		super(senderID, receiverID, "roundmessage", messageId);

		this.round = round;
	}

	public int getRound() {
		return this.round;
	}
}
