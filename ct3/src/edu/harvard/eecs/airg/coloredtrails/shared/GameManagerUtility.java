/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sohan
 */

package edu.harvard.eecs.airg.coloredtrails.shared;

import edu.harvard.eecs.airg.coloredtrails.server.ServerGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.*;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;

import java.util.ArrayList;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Date;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class GameManagerUtility {

	// Game setup vars
	private static Scoring s = new Scoring(100, -15, 10, 0.0);
	private static int ProposerID = 0;
	private static int ResponderID = 1;

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) throws Exception {

		String gamePoolFilename = "gamepool.ser";
		String boardsFile = "";

		// Getting the parameters entered by the user
		if (args.length > 0 && !args[0].equals("-help")) {
			for (int i = 0; i < args.length; i++) {
				String arg = args[i];
				String param = (i > (args.length - 2) ? null : args[i + 1]);
				if (arg.equals("-file")) {
					gamePoolFilename = param.toString();
					i++;
				} else if (arg.equals("-inputfile")) {
					boardsFile = param.toString();
					i++;
				}
			}
		// No parameters were entered
		} else {
			System.out.println("Parameter -file <location_of_file> (default: "
					+ gamePoolFilename + ")");
			System.out
					.println("Parameter -inputfile <file containing the board names "
							+ "to generate>.");
			if (args.length > 0 && args[0].equals("-help")) {
				return;
			}
		}

		if (boardsFile == "") {
			throw new Exception(
					"no input board was enterd. can't generate games");
		}

		System.out.println("Generating games into [" + gamePoolFilename
				+ "] according to input file [" + boardsFile + "]");
		System.out.println("Scoring : " + s);

		System.out.println("Started generating games at " + new Date());
		
		ArrayList<ArrayList<GameStatus>> gamePool = makeGameList(boardsFile);

		ObjectOutputStream oos = null;

		try {
			oos = new ObjectOutputStream(new FileOutputStream(new File(
					gamePoolFilename)));
			oos.writeObject(gamePool);
		} catch (Throwable thr) {
			System.err.println("Failed to write games to file");
			thr.printStackTrace();
		} finally {
			try {
				oos.close();
			} catch (Throwable thr) {
				thr.printStackTrace();
			}
		}
	}

	public static ArrayList<ArrayList<GameStatus>> makeGameList(String inputFile) {
		ArrayList<ArrayList<GameStatus>> games = new ArrayList<ArrayList<GameStatus>>();
		BufferedReader fr;
		try {
			
			fr = new BufferedReader(new FileReader("lib/adminconfig/" + inputFile));
			String curBoardFile;
			int boardsNo = 0;
			long start;
			long interval;
			long total = 0;
				
			// Creating each board on the input file
			while ((curBoardFile = fr.readLine()) != null) {

				start = new Date().getTime();
				++ boardsNo;

				// creating the game
				ServerGameStatus gs = new ServerGameStatus();
				Set<PlayerStatus> players = new LinkedHashSet<PlayerStatus>();
				players.add(new PlayerStatus(ProposerID, -1));
				players.add(new PlayerStatus(ResponderID, -1));
				gs.setPlayers(players);
				
				for (PlayerStatus ps : gs.getPlayers()) {
					ps.setRole("");
				}

				GameBoardCreator gameBoardCreator = 
					new GameBoardCreator(gs, "gamePoolMakerLog");
				gameBoardCreator.createGameBoard(curBoardFile);
				
				ArrayList<GameStatus> curGame = new ArrayList<GameStatus>();
				curGame.add(gs);
				games.add(curGame);
				
				total += (interval = new Date().getTime() - start);
				System.out.println("Made game #" + (boardsNo) + "\t"
						+ (interval / 1000) + "s " + (interval % 1000) + "ms");
			}
			
			total /= boardsNo;
			System.out.println("Completed generating games at " + new Date()
					+ " (average " + (total / 1000) + "s " + (total % 1000)
					+ "ms per game)");
			
		} catch (FileNotFoundException e) {
			System.out
					.println("Problam opening the input file: lib/adminconfig/"
							+ inputFile);
		} catch (IOException e) {
			System.out
			.println("Problam reading from input file: lib/adminconfig/"
					+ inputFile);
		}

		return games;
	}

	public static ArrayList<ArrayList<GameStatus>> loadGamePool(
			String gamePoolFilename) {
		ObjectInputStream ois = null;
		ArrayList<ArrayList<GameStatus>> gamePool = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(new File(
					gamePoolFilename)));
			gamePool = (ArrayList) ois.readObject();
		} catch (Throwable thr) {
			System.err.println("Failed to read games from file ["
					+ gamePoolFilename + "]");
			thr.printStackTrace();
		} finally {
			try {
				ois.close();
			} catch (Throwable thr) {
				thr.printStackTrace();
			}
		}
		return gamePool;
	}
}
