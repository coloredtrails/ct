import os
import re
import sys

AGENT_ID = 337
HUMAN_ID = 200

GOAL_WEIGHT = 100
DIST_WEIGHT = 10
CHIP_WEIGHT = -5

MODE_JONATHAN_ANALYSIS = 'JonathanAnalysis'
MODE_GALIT_CONVERT = 'GalitConvert'
MODE_SUMMARY = 'Summary'
MODE_SUMMARY_FIRSTS = 'SummaryFirsts'

SELECTED_MODE = MODE_SUMMARY_FIRSTS

def get_board(content_lines):
	flag = False
	board = []
	for line in content_lines:
		if '<squaresColors>' in line:
			flag = True
			continue
		if '</squareColors>' in line:
			break
		if flag:
			s = line.split('\t')[1].strip()
			board.append(s.split(' '))
	assert flag, 'Invalid board'
	return board

def get_chips_from_line(line):
	s = line[line.find('{ chips  '):]
	chips = {}
	matches = re.findall('\'.*?\':[0-9]*', s)
	for match in matches:
		color, num = match.split(':')
		chips[color.replace('\'', '')] = int(num)
	return chips

def get_chips_str_from_line(line):
	result = []
	chips = get_chips_from_line(line)
	for k in chips.keys():
		n = chips[k]
		if n <= 0:
			continue
		result.append('%s %s chips' % (n, k))
	if len(result) == 0:
		return '(nothing)'
	result = ', '.join(result)
	return '(' + result + ')'

def get_player_chips(content_lines, player_id):
	flag = False
	to_find = 'Setting chips for player: %s' % (player_id,)
	for line in content_lines:
		if to_find in line:
			flag = True
			continue
		if flag and '{ chips  ' in line:
			return get_chips_from_line(line)
	assert False, 'Invalid player chips for player %s' % (player_id,)

def get_player_position(content_lines, player_id):
	flag = False
	to_find = 'Setting chips for player: %s' % (player_id,)
	for line in content_lines:
		if to_find in line:
			flag = True
			continue
		if flag and 'Player position: ' in line:
			match = re.findall('\(.*?,.*?\)', line)[0]
			return tuple([ int(i) for i in re.findall('\(R:([0-9]*),C:([0-9]*)\)', match)[0]])

def get_goal_position(content_lines):
	for line in content_lines:
		if 'Goal position: ' in line:
			match = re.findall('\(.*?,.*?\)', line)[0]
			return tuple([ int(i) for i in re.findall('\(R:([0-9]*),C:([0-9]*)\)', match)[0]])
	assert False, 'Invalid goal position'

def find_path_in_board(explored, board, goal_row, goal_col, player_row, player_col, player_chips):
	if goal_row == player_row and goal_col == player_col:
		return [(player_row, player_col)]
	if (player_row, player_col) in explored:
		return None
	if player_row >= len(board) or player_row < 0:
		return None
	if player_col >= len(board[player_row]) or player_col < 0:
		return None
	req_color = board[player_row][player_col]
	new_player_chips = player_chips.copy()
	if req_color not in new_player_chips or new_player_chips[req_color] <= 0:
		return None
	new_player_chips[req_color] = new_player_chips[req_color] - 1
	result = [(player_row, player_col)]
	explored.append((player_row, player_col))
	for r in xrange(player_row - 1, player_row + 2):
		for c in xrange(player_col - 1, player_col + 2):
			if r != player_row and c != player_col:
				continue
			f = find_path_in_board(explored, board, goal_row, goal_col, r, c, new_player_chips)
			if f is None:
				continue
			result.extend(f)
			return result
	return None

def get_player_path(content_lines, player_id):
	board = get_board(content_lines)
	player_chips = get_player_chips(content_lines, player_id)
	goal_row, goal_col = get_goal_position(content_lines)
	player_row, player_col = get_player_position(content_lines, player_id)
	explored = []
	first_color = board[player_row][player_col]
	if first_color not in player_chips:
		player_chips[first_color] = 0
	player_chips[first_color] = player_chips[first_color] + 1
	result = find_path_in_board(explored, board, goal_row, goal_col, player_row, player_col, player_chips)
	if result is not None:
		result.reverse()
		result = result[1:]
	return result

def is_player_depends(content_lines, player_id):
	return get_player_path(content_lines, player_id) is None

def get_game_type_string(content_lines):
	if is_player_depends(content_lines, AGENT_ID):
		if is_player_depends(content_lines, HUMAN_ID):
			return 'DD game'
		return 'Human is TI'
	return 'Agent is TI'

def get_depends_string(content_lines, player_id):
	if is_player_depends(content_lines, player_id):
		return 'dependent'
	return 'independent'

def get_round_from_line(line):
	return int(re.findall('Round: ([0-9]*)', line)[0])

def add_chips(chips_x, chips_y):
	result = chips_x.copy()
	for k in chips_y.keys():
		if k not in result.keys():
			result[k] = 0
		result[k] = result[k] + chips_y[k]
	return result

def sub_chips(chips_x, chips_y):
	result = chips_x.copy()
	for k in chips_y.keys():
		if k not in result.keys():
			result[k] = 0
		result[k] = result[k] - chips_y[k]
		assert result[k] >= 0, 'Invalid chips substraction'
	return result

def get_num_chips(chips, color):
	if color not in chips:
		return 0
	return chips[color]

def get_all_num_chips(chips):
	result = 0
	for k in chips.keys():
		result += get_num_chips(chips)
	return result

def get_resulting_score(board, goal_position, player_path, player_chips, other_chips, player_position, chips_to_receive, chips_to_send):
	new_player_chips = add_chips(sub_chips(player_chips, chips_to_send), chips_to_receive)
	new_player_position = player_position
	pos_index = player_path.index(player_position)
	not_reaching_goal = False
	for cur_position in xrange(pos_index + 1, len(player_path) - pos_index):
		req_color = board[player_path[cur_position][0], player_path[cur_position][1]]
		if get_num_chips(new_player_chips, req_color) > 0:
			new_player_chips[req_color] = new_player_chips[req_color] - 1
			new_player_position = cur_position
		else:
			not_reaching_goal = True
			break
	if not not_reaching_goal:
		new_player_position = goal_position
	return scoring_score(goal_position, new_player_position, new_player_chips)

def scoring_score(goal_position, player_position, player_chips):
	delta_row = abs(player_position[0] - goal_position[0])
	delta_col = abs(player_position[1] - goal_position[1])
	manhattan = delta_row + delta_col
	score = 0.0
	if manhattan == 0:
		score += GOAL_WEIGHT
	else:
		score += DIST_WEIGHT * manhattan
	score += get_all_num_chips(new_player_chips) * CHIP_WEIGHT
	return score

def get_round_reliability(board, goal_position, other_player_path, other_player_chips, player_chips, other_player_position, last_accepted_other_receive, last_accepted_other_send, sent_to_other):
	full_transfer_score = get_resulting_score(board, goal_position, other_player_path, other_player_chips, player_chips, other_player_position, last_accepted_other_receive, {})
	actual_score = get_resulting_score(board, goal_position, other_player_path, other_player_chips, player_chips, other_player_position, sent_to_other, {})
	current_score = get_resulting_score(board, goal_position, other_player_path, other_player_chips, player_chips, other_player_position, {}, {})
	full_benefit = full_transfer_score - current_score
	actual_benefit = actual_score - current_score
	if full_benefit == 0.0:
		return 1.0
	return actual_benefit / full_benefit

def turn_to_galit_log(content_lines, log_handle):
	is_agent_depends = is_player_depends(content_lines, AGENT_ID)
	is_human_depends = is_player_depends(content_lines, HUMAN_ID)
	assert is_human_depends or is_agent_depends, 'Invalid board dependence conditions'
	new_human_id = 0
	if is_agent_depends:
		if is_human_depends: # DD
			new_human_id = 100
		else:  # TD
			new_human_id = 200
	else: # TI
		new_human_id = 500
	new_agent_id = new_human_id + 10
	for line in content_lines:
		new_line = line[:]
		line_score = None
		if 'score is:' in new_line:
			line_score = int(re.findall('score is\W+([0-9]+).*?', new_line)[0])
			new_line = new_line.replace('score is: ' + str(line_score), 'YOUWILLNEVERGUESSME')
		new_line = re.sub(str(HUMAN_ID), str(new_human_id), new_line)
		new_line = re.sub(str(AGENT_ID), str(new_agent_id), new_line)
		new_line = re.sub('id:0', '', new_line)
		new_line = re.sub('id:1', '', new_line)
		if 'YOUWILLNEVERGUESSME' in new_line:
			new_line = new_line.replace('YOUWILLNEVERGUESSME', 'score is: ' + str(line_score))
		log_handle.write(new_line)

def jonathan_analyser(content_lines, log_handle):
	names = { AGENT_ID : 'AGENT', HUMAN_ID : 'HUMAN' }
	for player_id in names.keys():
		log_handle.write('Player %s (%s), %s\n' % (player_id, names[player_id], get_depends_string(content_lines, player_id)))
	cur_round = -1
	prot = 'Feed'
	msg_from = 0
	prop_send = ''
	msg_type = ''
	event_id = 1
	for line in content_lines:
		if 'Ending game' in line or 'All players have all needed chips to get to goal' in line:
			prot = 'End'
			continue
		if 'Communication phase, Round:' in line:
			assert prot == 'Feed', 'Invalid phase change'
			prot = 'Comm'
			new_round = get_round_from_line(line)
			assert new_round == cur_round + 1, 'Invalid round %s, prev is %s' % (new_round, cur_round)
			cur_round = new_round
			log_handle.write('\n')
			continue
		if 'Exchange phase, Round:' in line:
			assert prot == 'Comm', 'Invalid phase change'
			prot = 'Exch'
			new_round = get_round_from_line(line)
			assert new_round == cur_round, 'Invalid round %s, prev is %s' % (new_round, cur_round)
			cur_round = new_round
			continue
		if 'Movement phase, Round:' in line:
			assert prot == 'Exch', 'Invalid phase change'
			prot = 'Move'
			new_round = get_round_from_line(line)
			assert new_round == cur_round, 'Invalid round %s, prev is %s' % (new_round, cur_round)
			cur_round = new_round
			continue
		if 'Feedback phase, Round:' in line:
			assert prot == 'Move', 'Invalid phase change'
			prot = 'Feed'
			new_round = get_round_from_line(line)
			assert new_round == cur_round, 'Invalid round %s, prev is %s' % (new_round, cur_round)
			cur_round = new_round
			continue
		if prot == 'Comm':
			if 'From player:' in line:
				msg_from = int(line.split(' ')[-1])
				continue
			if 'Message type: basicproposaldiscussion' in line:
				msg_type = 'Resp'
				continue
			if 'Message type: basicproposal' in line:
				msg_type = 'Prop'
				continue
			if 'Chips to send by proposer:' in line:
				assert msg_type == 'Prop', 'Invalid line of chips to send by proposer'
				prop_send = get_chips_str_from_line(line)
				continue
			if 'Chips to send by responder:' in line:
				assert msg_type == 'Prop', 'Invalid line of chips to send by responder'
				log_handle.write('%s.\t%s offered %s for %s\n' % (event_id, names[msg_from], prop_send, get_chips_str_from_line(line)))
				event_id += 1
				continue
			if 'rejected offer' in line or 'accepted offer' in line:
				assert msg_type == 'Resp', 'Invalid response line %s' % (line,)
				if 'accepted offer' in line:
					log_handle.write('%s.\t%s accepted\n' % (event_id, names[msg_from]))
					event_id += 1
				else:
					log_handle.write('%s.\t%s rejected\n' % (event_id, names[msg_from]))
					event_id += 1
				continue
		if prot == 'Exch':
			if 'sent:' in line:
				current_id = int(re.findall('([0-9]*) sent', line)[0])
				log_handle.write('%s.\t%s sent %s\n' % (event_id, names[current_id], get_chips_str_from_line(line)))
				event_id += 1
				continue
		if prot == 'Move':
			if 'moved to' in line:
				current_id = int(re.findall('([0-9]*) moved to', line)[0])
				log_handle.write('%s.\t%s moved\n' % (event_id, names[current_id]))
				event_id += 1
				continue
		if prot == 'End':
			if 'score is' in line:
				current_id = int(re.findall('Player: ([0-9]*)', line)[0])
				current_score = int(re.findall('score is: (-*[0-9]*)', line)[0])
				log_handle.write('%s.\t%s score is %s\n' % (event_id, names[current_id], current_score))
				event_id += 1
				continue
			
def analyze(content_lines, log_handle):
	if SELECTED_MODE == MODE_GALIT_CONVERT:
		turn_to_galit_log(content_lines, log_handle)
		return
	if SELECTED_MODE == MODE_JONATHAN_ANALYSIS:
		jonathan_analyser(content_lines, log_handle)
		return
	print 'Nothing to do'

def summary_recursive(input_path, inner = False):
	results = {}
	if os.path.isfile(input_path):
		return {}
	all_files = [ os.path.join(input_path, i) for i in os.listdir(input_path) if 'alternativeoffersconfig' in i.lower() and 'analysis' not in i.lower() ]
	all_files.sort()
	first_one_success = False
	for f in all_files:
		if SELECTED_MODE == MODE_SUMMARY_FIRSTS and first_one_success:
			break
		try:
			game_scores = { AGENT_ID : None, HUMAN_ID : None }
			game_handle = open(f, 'r')
			game_data = game_handle.readlines()
			game_handle.close()
			game_type = get_game_type_string(game_data)
			for line in game_data:
				if 'score is' in line:
					current_id = int(re.findall('Player: ([0-9]*)', line)[0])
					current_score = int(re.findall('score is: (-*[0-9]*)', line)[0])
					assert game_scores[current_id] is None, 'Invalid score data for %s' % (f,)
					game_scores[current_id] = current_score
			for k in game_scores:
				assert game_scores[k] is not None, 'No score found for %d in %s' % (k, f)
			if game_type not in results:
				results[game_type] = []
			results[game_type].append((game_scores[AGENT_ID], game_scores[HUMAN_ID]))
			print '%s\n\t(%s, %s, %s)' % (f, game_type, game_scores[AGENT_ID], game_scores[HUMAN_ID])
			first_one_success = True
		except:
			continue
	first_one_success = False
	all_folders = [ os.path.join(input_path, i) for i in os.listdir(input_path) if not os.path.isfile(os.path.join(input_path, i)) ]
	for f in all_folders:
		folder_results = summary_recursive(f, True)
		for k in folder_results:
			if k not in results:
				results[k] = []
			results[k].extend(folder_results[k])
	if inner:
		return results
	print '\n\n==========================================\n'
	print SELECTED_MODE + ': Agent score'
	for k in results:
		if len(results[k]) > 0:
			print '\t\t' + k + ': average of %s (out of %s games)' % (sum([ i[0] for i in results[k] ]) / float(len(results[k])), len(results[k]))
	print SELECTED_MODE + ': Human score'
	for k in results:
		if len(results[k]) > 0:
			print '\t\t' + k + ': average of %s (out of %s games)' % (sum([ i[1] for i in results[k] ]) / float(len(results[k])), len(results[k]))

def main(input_path):
	if SELECTED_MODE == MODE_SUMMARY or SELECTED_MODE == MODE_SUMMARY_FIRSTS:
		summary_recursive(input_path)
		return
	if not os.path.isfile(input_path):
		all_files = [ os.path.join(input_path, i) for i in os.listdir(input_path) if 'alternativeoffersconfig' in i.lower() and 'analysis' not in i.lower() ]
		for f in all_files:
			main(f)
		return
	input_handle = open(input_path, 'r')
	input_lines = input_handle.readlines()
	input_handle.close()
	suffix = ''
	if SELECTED_MODE == MODE_JONATHAN_ANALYSIS:
		suffix = '.analysis.txt'
	output_handle = open(input_path + suffix, 'w')
	analyze(input_lines, output_handle)
	output_handle.close()

if __name__ == '__main__':
	main(sys.argv[1])