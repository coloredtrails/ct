:: LaunchAll.bat
:: 
:: Starts the server, human GUI, and agent
:: 

:: SERVER
START /B java -jar dist/ct3.jar -s
CALL wait 3


:: HUMAN
START /B java -jar dist/ct3.jar -c ctgui.original.GUI --pin 100 --client_hostip localhost
CALL wait 5

:: HUMAN
START /B java -jar dist/ct3.jar -c ctgui.original.GUI --pin 200 --client_hostip localhost
CALL wait 5


START /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl DD FRIEND 1 VIDEO