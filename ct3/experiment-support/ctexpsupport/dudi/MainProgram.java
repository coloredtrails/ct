/*
	Colored Trails
	
	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;

public class MainProgram {

	/**
	 * @param args
	 */
	
	public static FileWriter writeLog = new FileWriter("log"
			+ System.currentTimeMillis() + ".xls");

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		writeLog.writeln("Starting...");writeLog.writeln("");
		
		Problem problem=new Problem();
		problem.addTarget(5,5,20,1);
		problem.addTarget(10,1,30,3);
		problem.addTarget(5,9,20,1);
		problem.goals.get(1).conditionedByID=2;
		problem.addTarget(5,11,10,2);
		problem.addTarget(5,13,10,4);
		problem.addTarget(5,15,10,1);
		problem.addTarget(5,17,10,1);
		problem.addTarget(6,1,10,5);
		problem.addTarget(6,2,20,20);
		problem.addTarget(6,3,20,20);
		Position start=new Position(5,1);
		
		problem.printProblem2File(0);
		problem.printSolution(problem.bestPath(problem.goals,start,0),0);
		
		problem.printSolution(problem.bestPath(problem.goals,start,0),1);
		
		problem.printChanges(problem.generateChangeValues(start,0));
		
		writeLog.finish();
		
		System.out.println("done");

	}

}
