/*
	Colored Trails
	
	Copyright (C) 2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;

import edu.harvard.eecs.airg.coloredtrails.shared.types.Goal;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class represents a sequence of goals in the order in which 
 * the goals must be visited by the subject
 * 
 * @author Sevan G. Ficici
 *
 */
public class Plan implements Serializable
{
	/** the sequence of goals in the plan */
	private ArrayList<Goal> goals = new ArrayList<Goal>();
	/** the number of plan steps to show in the UI with arrows */
	private int showlength = 0;
	
	public Plan(int showlength)
	{
		this.showlength = showlength;
	}
	
	/**
	 * Constructor
	 */
	public Plan(ArrayList<Goal> goals)
	{
		this.goals = goals;
		showlength = goals.size();  // defaults to plan length
	}
	
	/**
	 * Copy constructor
	 * @param p
	 */
	public Plan(Plan p)
	{
		showlength = p.showlength;
		for (Goal g : p.getGoals())
			addGoal(new Goal(g));
	}
	
	/**
	 * Set the length of the sequence that is to be indicated with arrows
	 * @param showlength
	 */
	public void setShowLength(int showlength)
	{
		this.showlength = showlength;
	}
	
	/**
	 * Get the length of the sequence that is to be indicated with arrows
	 * @return
	 */
	public int getShowLength()
	{
		return showlength;
	}
	
	/**
	 * Append specified goal to the end of the plan
	 * @param g
	 */
	public void addGoal(Goal g)
	{
		goals.add(g);
	}
	
	/**
	 * Remove the first goal from the sequence
	 *
	 */
	public void removeFirstGoal()
	{
		goals.remove(0);
	}
	
	/**
	 * Get the sequence of goals
	 * @return
	 */
	public ArrayList<Goal> getGoals()
	{
		return goals;
	}
}
