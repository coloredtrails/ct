/*
	Colored Trails
	
	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;

public class Target {
	
	Position position;
	int value=10;
	int marginalValueDecrease=1;
	int conditionedByID=-1;
	
	public int goalID=0;
	
	public Target(){}
	
	public Target(Position position,int value)
	{
		this.position=position;
		this.value=value;
	}

	public Target(Position position,int value,int goalID)
	{
		this.position=position;
		this.value=value;
		this.goalID=goalID;
	}

	public Target(Position position,int value,int goalID,int marginalDecrease)
	{
		this.position=position;
		this.value=value;
		this.goalID=goalID;
		this.marginalValueDecrease=marginalDecrease;
	}

	public Target clone()
	{
		Target goal=new Target(position,value,goalID,marginalValueDecrease);
		goal.conditionedByID=this.conditionedByID;
		return goal;
	}

}
