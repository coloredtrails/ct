/*
	Colored Trails
	
	Copyright (C) 2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;

import java.io.Serializable;

import edu.harvard.eecs.airg.coloredtrails.shared.types.Goal;

/**
 * 
 * This class represents a single element of a BriefingItem
 * @author Sevan G. Ficici
 *
 */
public class BriefingElement implements Serializable
{
	/** goal's value jumps up */
	public static final int UP = 0;
	/** goal's value jumps down */
	public static final int DOWN = 1;
	/** goal's value does not change */
	public static final int NOCHANGE = 2;
	
	/** the goal we're talking about */
	public Goal goal;
	/** the change in the goal's value we're looking for */
	public int change;
	
	public BriefingElement(Goal goal, int change)
	{
		this.goal = goal;
		this.change = change;
	}
	
	public boolean changeUp()
	{
		return change == UP;
	}
	
	public boolean changeDown()
	{
		return change == DOWN;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		// goal.getValue() - does nor exists
		sb.append("Goal Type: " + goal.getType() + "  Current Value : " + /*goal.getValue() + */
				  "  Change: " + (change == UP ? "UP" : "DOWN"));
		return sb.toString();
	}
}
