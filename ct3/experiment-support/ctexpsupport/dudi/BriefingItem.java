/*
	Colored Trails
	
	Copyright (C) 2007, President and Fellows of Harvard College.  All Rights Reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ctexpsupport.dudi;

import java.io.Serializable;
import java.util.ArrayList;

import edu.harvard.eecs.airg.coloredtrails.shared.types.Goal;

/**
 * This class represents a single item in a Briefing
 * @author Sevan G. Ficici
 *
 */
public class BriefingItem implements Serializable
{
	public ArrayList<BriefingElement> elements = new ArrayList<BriefingElement>();
	
	/** representation method; i.e., how GUI will interpret the briefing */
	public int interpretation = 0;
	/** for Version 2 of the experiment, used to indicate whether this briefing item
        is being added to briefing or removed from briefing; true = new, false = removed */
	public boolean newitem = true;
	
	
	public BriefingItem(int interpretation, boolean newitem)
	{
		this.interpretation = interpretation;
		this.newitem = newitem;
	}
	
	public BriefingItem(ArrayList<BriefingElement> elements)
	{
		this.elements = elements;
	}
	
	public void addElement(BriefingElement be)
	{
		elements.add(be);
	}
	
	public void addElement(Goal goal, int change)
	{
		elements.add(new BriefingElement(goal, change));
	}
	
	public int getNumElements()
	{
		return elements.size();
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("Briefing Item:\n");
		for (BriefingElement be : elements)
			sb.append(be + "\n");
		return sb.toString();
	}
}
