/*
	Colored Trails

	Copyright (C) 2006-2007, President and Fellows of Harvard College.  All Rights Reserved.

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import edu.harvard.eecs.airg.coloredtrails.server.ServerData;
import edu.harvard.eecs.airg.coloredtrails.server.ServerPhases;
import edu.harvard.eecs.airg.coloredtrails.shared.Constants;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.RoleChangedMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.RoundNotificationMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.TransferDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.*;
import edu.harvard.eecs.airg.coloredtrails.alglib.BestUse;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.controller.AltOffersGameDetails;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.GameBoardCreator;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.ScoringMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.ForwardMessage;


import java.lang.Boolean;
import java.text.DecimalFormat;
import java.util.*;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.table.DefaultTableModel;
//import com.sun.tools.javac.tree.Tree.Case;

import ctagents.FileLogger;
import ctagents.alternateOffersAgent.palAgent.FixedPathScoreUtil;
import ctgui.original.Icons;
import ctgui.original.AllPlayersChipDisplay;
import ctgui.original.AllPlayersChipDisplay.ChipRenderer;
import edu.harvard.eecs.airg.coloredtrails.client.ClientGameStatus;
import edu.harvard.eecs.airg.coloredtrails.client.ClientGameStatus;

/**
 * This configuration implements much of the setup and functionality. Including
 * automatic exchange after accept an offer, and automatic movement.
 */
public class AlternativeOffersUltimatumConfig3Players extends GameConfigDetailsRunnable
		implements PhaseChangeHandler {

	public static final int NUMBER_OF_PLAYERS = 3;
	private static final long serialVersionUID = -3751478802171903828L;

	private static String GAME_HEADLINES = "filename,round,propID,respID,IsPropCustomer,chipsToSend,chipsToReceive,accept,"
		+"propCurrentScore,respCurrentScore,"
		+"actualSent,ActualRcvd,propResultingScore,respResultingScore,"
		+"#ofCustomerMovesInRound,customerRowPositionAfterMovePhase,customerColPositionAfterMovePhase,"
		+"Player100ChipsAfterMovePhase,Player100ScoreAfterMovePhase,Player200ChipsAfterMovePhase,"
		+"Player200ScoreAfterMovePhase,Player300ChipsAfterMovePhase,Player300ScoreAfterMovePhase,"
		+"generosityLevel,ClientReachedGoal";

	private static PrintWriter csvLog;
	private String csvLogName = "CT_DbgLog_Agent_";
	private String proposal0String4Log = null;
	private String proposal1String4Log = null;
	private String reply0String4Log = null;
	private String reply1String4Log = null;
	private String gameStatusString4Log = null;
	
	DecimalFormat df;

	
	// gs is a member variable of GameConfigDetailsRunnable,
	// defined as: protected ServerGameStatus gs;
	/**
	 * The scoring function used for players in the game. 150 for a player (instead of 100)
	 * reaching the goal, -10 per unit distance if the player does not reach the
	 * goal, 5 for each chip remaining after the player has reached the goal or
	 * cannot move any farther towards the goal.
	 */
	Scoring s = new Scoring(150, -10, 5);

	/** Local random generator for creating chipsets */
	static Random localrand = new Random();

	/** determines if there will be automatic movement */
	boolean automaticMovement = false; 
	/**
	 * determines if the chips will automatically transfer after a proposal has
	 * been accepted
	 */
	boolean automaticChipTransfer = true; 
	/** determines if the phases will loop. */
	boolean phaseLoop = true;// false;

	// The following two booleans will help us regulate the transfer of chips so
	// that the actual sending is performed
	// 1)only at the end of the time of the exchange phase
	// or 2) if both players have already "pressed" the send button
	// the idea is that we don't want each player to know before sending (or
	// not) her chips whether the
	// other player has sent (or not) his chips.
	// Each time the protocol gets a transfer request it turns the respective
	// boolean on and saves the chipset to transfer
	// until both players have requested to transfer or the exchange phase time
	// is up.
	boolean bproposer0RequestedTransfer = false;
	boolean bproposer1RequestedTransfer = false;
	boolean bResponderRequestedTransfer = false;
	ChipSet csproposer0ToTransfer = null;
	ChipSet csproposer1ToTransfer = null;
	ChipSet csResponderToTransfer = null;

	// Number of rounds to play, each round we switch roles
	int numOfRounds = 1;
	int round = 0;
	String CurrentPhaseName = null;
	boolean ended = false;
	int initialDormant = 0;
	int consecutiveNoMovementCounterPlayer1 = initialDormant;
	int numberofMoves = 0;
    String replyString0 = "UNKNOWN";
    String replyString1 = "UNKNOWN";

	
	// Keep track of the first player to propose in each round.
	// In the communication phase the first to propose alternates from round to
	// round.
	// Also within each communication phase the player that got a proposal from
	// the proposer
	// is entitled to reject and if wanted to make a counter offer. After that
	// no more offers are allowed until the next round.
	// Possible cases:
	// 1) Player 1 makes an offer, Player 2 accepts it --> next phase
	// 2) Player 1 makes an offer, Player 2 rejects, Player 2 makes counter
	// offer, Player 1 either accepts or reject --> in any case next phase
	// 3) Player 1 makes an offer, Player 2 rejects, Player 2 doesn't want to
	// counter offer--> wait until the end of the time of the phase and then
	// next phase
	// 4) In second round Player 2 swap roles with Player 1, that is he is the
	// one to make the first offer. Game continues as explained above.
	Vector<Integer> vFirstProposerInRound = new Vector<Integer>();

	// Number of counter offers allowed in each round
	static final int maxNumOfCounterOffers = 1;
	int counterOffersCount = 0;
	RowCol TDPlayerPosition = null;
	boolean TDPlayerReachedGoal = false;
	
	private int prepPhaseCounter = 0;

	boolean[] communicationAllowed = new boolean[NUMBER_OF_PLAYERS];

	//boolean[] proposalAllowed = new boolean[NUMBER_OF_PLAYERS];
	//boolean[] answerAllowed = new boolean[NUMBER_OF_PLAYERS];

	boolean[] movementAllowed = new boolean[NUMBER_OF_PLAYERS];
	
	PlayerStatus[] proposersInGame = new PlayerStatus[2];
	PlayerStatus[] respondersInGame = new PlayerStatus[2];
	PlayerStatus TDPlayer;
	int acceptReplyCounter = 0;
	int rejectReplyCounter = 0;
	int TDPlayerreplyCounter = 0;
	
	private String logName;
	private static String LOG_PREFIX = "3PlayersAlternativeOffersConfig_";
//---------------------------------------------------------------

    /* Store the Proposal Messages sent by the two proposers.
     * null if no proposal received.
     */
    BasicProposalDiscourseMessage proposer0Proposal = null;
    BasicProposalDiscourseMessage proposer1Proposal = null;
    /* Store whether or not the proposals have been accepted.
     * null means no proposal has been accepted.
     */
    Boolean acceptedProposer0;
    Boolean acceptedProposer1;

    /*will hold the output of the scores*/
    FileWriter out;

    /**
     * Filter players' views of other players' RED Chips
     * 
     * @param fromserver complete set of player statuses as known by the server
     * @param perGameId  ID of the player filtered data is being sent to
     * @return           player status filtered based on receiving player's ID
     * 
     * This function intercepts the set of player statuses as they are being 
     * sent from the server to the client, and filters the data according to
     * what the receiving player is supposed to know. This allows us to create
     * a game of partial visibility.
     *
     * In this case, we don't want players to see other players' RED Chips
     */

    @Override public Set<PlayerStatus> filterPlayerStatus
                        (Set<PlayerStatus> fromserver, int perGameId)
    {
    	Set<PlayerStatus> newStatuses = new HashSet<PlayerStatus>();
        
        // copy fromserver into a new set and make changes to the new copy
        for (PlayerStatus ps : fromserver)
        {
            PlayerStatus ps_temp = new PlayerStatus(ps);

            if (ps_temp.getPerGameId() != perGameId)
            {
                // hide opponents' chips
                ChipSet hiddenOpponentCS = ps_temp.getChips(); // new ChipSet();
                hiddenOpponentCS.set("CTRed", -1);
                ps_temp.setChips(hiddenOpponentCS);

                /* 
                 * Note: Though we have masked their true chip counts, opponents
                 * will still show up in the agent's player chip display
                 * (their chip counts will be displayed as 0). Still, this
                 * may be confusing to experimentees. If you if you want the
                 * opposing players to be completely invisible (i.e., they don't
                 * show up at all on the chip display panel), you must make
                 * changes to AllPlayersChipDisplay.java in gui/ctgui/original
                 * [lines 177-179].
                 */
                                
                // send opponents' old positions rather than the current ones
//                if (oldPositions[ps_temp.getPerGameId()] != null)
//                {
//                    RowCol oldPosition = new RowCol(oldPositions[ps_temp.getPerGameId()]);
//                    ps_temp.setPosition(oldPosition);
//                }
            }
                    
            newStatuses.add(ps_temp);
        }
        
        return newStatuses;
    }

    public void resetPermissionFlags() {
    	for (int i=0; i < NUMBER_OF_PLAYERS; i++)
    	{
    		communicationAllowed[i] = false;
//    		proposalAllowed[i] = false;
//    		answerAllowed[i] = false;
    		movementAllowed[i] = false;
    	}
	}

    private void endLog()
    {
    	PlayerStatus pStat;
		if (csvLog !=null){
			csvLog.println(csvLogName+ ",,,,,,,,,,,,,,,,,,,,,,,,"+TDPlayerReachedGoal);
		}

		csvLog.flush();
		csvLog.close();

    }
    
    public void setPermissions() {
		PlayerStatus pStat;
		
		for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
			pStat = gs.getPlayerByPerGameId(i);
				pStat.setCommunicationAllowed(communicationAllowed[i]);
//				pStat.setProposalAllowed(proposalAllowed[i]);
////				pStat.setAnswerAllowed(answerAllowed[i]);
				pStat.setMovesAllowed(movementAllowed[i]);
	    }
	}

	/* When the TI are the proposers the TD will be in the two entries of the responder;
	 * When the TI are the responders, the TD will be in the two entries of the proposer
	 */
	public void swapRoles() {
		PlayerStatus temp1, temp2;

		temp1 = proposersInGame[0];
		temp2 = proposersInGame[1];
		proposersInGame[0] = respondersInGame[0];
		proposersInGame[1] = respondersInGame[1];
		respondersInGame[0] = temp1;
		respondersInGame[1] = temp2;
		// Now, update the role in the players' status
		for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
		{
			if (gs.getPlayerByPerGameId(i).getRole() == "Proposer"){
				gs.getPlayerByPerGameId(i).setRole("Responder");
//				proposalAllowed[i] = false;
				communicationAllowed[i] = false;
			}
			else { // was the responder
				gs.getPlayerByPerGameId(i).setRole("Proposer");
//				proposalAllowed[i] = true;
				communicationAllowed[i] = true;
			}
		}
		setPermissions();
	}

	public AlternativeOffersUltimatumConfig3Players() {
		// Setting the log name according to the timestemp
		Long currTS = (new Date()).getTime();
		logName = LOG_PREFIX + currTS;
		
		// Opening the csv log file to write all the weka parameters to it
		String timeStemp = String.valueOf((new Date()).getTime());
		csvLogName += timeStemp + ".csv";
		df = new DecimalFormat("##.####");
		try {
			csvLog = new PrintWriter(csvLogName);
			csvLog.println(GAME_HEADLINES);
		} catch (FileNotFoundException e) {
			System.out.println("Problem opening csv file named: " + csvLog);
			e.printStackTrace();
			csvLog = null;
		}

	}

	/**
	 * Returns score of specified player, according to player's current state
	 */
	public int getPlayerScore(PlayerStatus ps) {
		
		RowCol gpos0 = gs.getBoard().getGoalLocations().get(0); // get first goal
																// in list
		RowCol gpos1 = gs.getBoard().getGoalLocations().get(1); // get second goal
																// in list
		int sc0 = (int) Math.floor(s.score(ps, gpos0)); // should change to double
														// at some point
		int sc1 = (int) Math.floor(s.score(ps, gpos1)); // should change to double
														// at some point
		if (!(ps.getPosition().equals(TDPlayer.getPosition())))
		{
			sc0 -= s.goalweight;
			sc1 -= s.goalweight;
		}
		else
		{
			TDPlayerReachedGoal = true;
		}
		
		if (sc0 >= sc1)												
			return sc0;
		else
			return sc1;
	}

	/**
	 * Called by GameConfigDetailsRunnable methods when calculation and
	 * assignment of player scores is desired
	 */
	protected void assignScores() {
		for (PlayerStatus ps : gs.getPlayers()) {
			ps.setScore(getPlayerScore(ps));
			getLog().writeln(
					"assignScores; Player: " + ps.getPin() + "  Score: "
							+ ps.getScore());
		}
	}

	public int getMaxNonConsecRounds() {
		return numOfRounds;
	}

	/**
	 * Called by server when a phase begins
	 */
	public void beginPhase(String phasename) {
		PlayerStatus pStat;
		boolean curLoop = gs.getPhases().getIsLoop();
//		if (curLoop == true)
//		{
//			return;
//		}
		getLog().writeln("Protocol: A New Phase Began: " + phasename+". loop is "+curLoop);
        if(phasename.equals(ServerPhases.COMM_PH)) { //REQUESTS_PH
            ++round; //only update the round for communicaton phase.
        }
        getLog()
		.writeln( "---------- Beginning " + phasename + " at round " + round + " ----------" );

		//Initialize communication, transfers and movement to false.
        //boolean communicationAllowed = false;
		
		if(phasename.equals(ServerPhases.FEEDBACK_PH)){
	    	if (TDPlayer.getPosition() == TDPlayerPosition) /* The TDPlayer remain in the same position and did not move */
	    		consecutiveNoMovementCounterPlayer1++;
	    	else
	    		consecutiveNoMovementCounterPlayer1 = initialDormant;;

			// This way we can let each player know how many consecutive no
			// movements the OTHER has
			TDPlayer.set("myConsecutiveNoMovement",
					consecutiveNoMovementCounterPlayer1);
			getLog().writeln(
					"Number of consecutive no movements for player "
							+ TDPlayer.getPin() + " is "
							+ consecutiveNoMovementCounterPlayer1);

			getLog().writeln("TDPlayerPosition is" +TDPlayer.getPosition()+"goal location 0"+
					gs.getBoard().getGoalLocations().get(0)+"goal location 1"+
					gs.getBoard().getGoalLocations().get(1));
			// update game status about dormant moves
			TDPlayer.setMyNumDormantRounds(
					consecutiveNoMovementCounterPlayer1);
			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = gs.getPlayerByPerGameId(i);
				if (pStat != TDPlayer)
					pStat.setHisNumDormantRounds(consecutiveNoMovementCounterPlayer1);
			}

			if (consecutiveNoMovementCounterPlayer1 >= numOfRounds) {
				getLog()
						.writeln(
								"Ending game, TD Player move as much as it can towards goal");
				super.doAutomaticMovement(s);
				ended = true;
			}
			
			else if ((TDPlayer.getPosition().equals(gs.getBoard().getGoalLocations().get(0)))||
					(TDPlayer.getPosition().equals(gs.getBoard().getGoalLocations().get(1))))
			{
				getLog()
				.writeln(
						"The TD player reached the goal, end the game");
				ended = true;
			}
			getLog().writeln("ended is "+ ended);
//			if (ended == false) {
//				swapRoles();
//				getLog().writeln("Swaping roles:");
//				setPermissions();
//			}
//			else {
//			((ServerPhases) gs.getPhases()).setLoop(false);
//			getLog()
//			.writeln("Loop status: " + gs.getPhases().getIsLoop());
//			gs.setEnded();
//			for (PlayerStatus p : gs.getPlayers()) {
//				getLog().writeln(
//						"Player: " + p.getPin() + " ,role: " + p.getRole()
//								+ " score is: " + p.getScore());
//				getLog().writeln("Player: " + p.getPin() + "Game Ended");
//				p.setRole("GameEnded");
//			}
//			endLog();
//			resetPermissionFlags();
//			setPermissions();
//			}
		}


	    // FYI - for the first phase it won't work from here
		else if(phasename.equals(ServerPhases.COMM_PH)) { //REQUESTS_PH
	    	
	        //set communication, transfers, and moves of the agents.
		    
		    for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = gs.getPlayerByPerGameId(i);
					pStat.setCommunicationAllowed(communicationAllowed[i]);
					//pStat.setProposalAllowed(proposalAllowed[i]);
					//pStat.setAnswerAllowed(false);
					//pStat.setMovesAllowed(false);
		    }
		    TDPlayer.setMovesAllowed(false);
        } 
	    else if (phasename.equals(ServerPhases.MOVEMENT_PH)) {
	    	gs.sendArbitraryMessage(Constants.ENDDISCUSSION);
	    	ServerPhases ph = gs.getServerPhases();
			ph.removePhase(ServerPhases.STRATEGY_PH);
			
	    	for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
				pStat = gs.getPlayerByPerGameId(i);
					pStat.setCommunicationAllowed(false);
//					pStat.setProposalAllowed(false);
//					pStat.setAnswerAllowed(false);
					//pStat.setMovesAllowed(true);
		    }
	    	TDPlayerPosition = TDPlayer.getPosition();
	    	TDPlayer.setMovesAllowed(true); // Allow to move only the initial TD player and NOT the TI players
	    }
	}
/**
     * Exchanges chips between agents.
     * @param a A player that is to exchange chips with b.
     * @param b A player that is to exchange chips with a.
     * @param a_to_b The chips a wishes to send to b.
     * @param b_to_a The chips b wishes to send to a.
     */
    private static void exchange( PlayerStatus a, PlayerStatus b, ChipSet a_to_b, ChipSet b_to_a ) {
        //determine of the chips can be transfered
        boolean aCanSend = a.getChips().contains( a_to_b );
        boolean bCanSend = b.getChips().contains( b_to_a );

        System.out.println("enter exchange");
        //If the chips can be transfered, then transfer them.
        if( aCanSend && bCanSend ) {
            ChipSet aCS = ChipSet.subChipSets( a.getChips(), a_to_b );
            aCS = ChipSet.addChipSets( aCS, b_to_a );

            ChipSet bCS = ChipSet.subChipSets( b.getChips(), b_to_a );
            bCS = ChipSet.addChipSets( bCS, a_to_b );

            a.setChips( aCS );
            b.setChips( bCS );
            
            System.out.println("made the exchange");
            
        }
    }
	
	
	/**
	 * Called by server when a phase ends
	 */
	public void endPhase(String phasename) {
		PlayerStatus pStat;
		
		getLog().writeln("A Phase Ended: " + phasename);

		// if end of feedback phase
		if (phasename.equals(ServerPhases.FEEDBACK_PH)) {
			getLog().writeln(" numberofMoves in round "+round+" is "+numberofMoves);
			
	       	 if (csvLog != null) {    
	    		 int score100 = 0 ,score200 = 0, score300 = 0;
	    		 ChipSet chipSet100 = null, chipSet200 = null, chipSet300 = null;
	    		 
	    		 if (proposer0Proposal != null) {
	    			 if (reply0String4Log == null)
	    				 reply0String4Log = "IGNORED" + "," +getPlayerScore(proposersInGame[0])+ "," 
	    				 +getPlayerScore(respondersInGame[0])+ ",,,,";
//	    			csvLog.println(proposal0String4Log+"," + "IGNORED" + "," + getPlayerScore(proposersInGame[0]) + ","
//	    					+ getPlayerScore(respondersInGame[0]) + ",");
//	    			csvLog.flush();
	    		 }
	    		 if (proposer1Proposal != null) {
	    			 if (reply1String4Log == null)
	    				 reply1String4Log = "IGNORED" + "," +getPlayerScore(proposersInGame[1])+ "," 
	    				 +getPlayerScore(respondersInGame[1])+ ",,,,";
//	     			csvLog.println(proposal1String4Log+"," + "IGNORED" + "," + getPlayerScore(proposersInGame[1]) + ","
//	     					+ getPlayerScore(respondersInGame[1]) + ",");
//	     			csvLog.flush();
	     		 }
	    		 gameStatusString4Log = numberofMoves + "," + TDPlayerPosition.toString()+ ","  
	    		 + gs.getPlayerByPerGameId(0).getChips() + "," + getPlayerScore(gs.getPlayerByPerGameId(0))+ ","  
	    		 + gs.getPlayerByPerGameId(1).getChips() + "," + getPlayerScore(gs.getPlayerByPerGameId(1))+ ","  
	    		 + gs.getPlayerByPerGameId(2).getChips() + "," + getPlayerScore(gs.getPlayerByPerGameId(2));
	    		  if (proposal0String4Log != null){
	    			csvLog.println(proposal0String4Log+"," + reply0String4Log+"," + gameStatusString4Log);
	    			csvLog.flush();
	    		 }
	    		 if (proposal1String4Log != null) {
	    			csvLog.println(proposal1String4Log+"," + reply1String4Log+"," + gameStatusString4Log);
	     			csvLog.flush();
	    		 }
	    	 }
	       	acceptReplyCounter = 0; // initialize the counter for the next negotiation phase
	    	rejectReplyCounter = 0;
	    	proposer0Proposal = null;
	   	    proposer1Proposal = null;
	   	    acceptedProposer0 = null;
	   	    acceptedProposer1 = null;
	        replyString0 = "UNKNOWN";
	        replyString1 = "UNKNOWN";

			proposal0String4Log = null;
			proposal1String4Log = null;
			reply0String4Log = null;
			reply1String4Log = null;
			gameStatusString4Log = null;

			numberofMoves = 0;
			if (ended == false) {
				swapRoles();
				getLog().writeln("Swaping roles:");
				setPermissions();
			}
			else {
			((ServerPhases) gs.getPhases()).setLoop(false);
			getLog()
			.writeln("Loop status: " + gs.getPhases().getIsLoop());
			gs.setEnded();
			for (PlayerStatus p : gs.getPlayers()) {
				getLog().writeln(
						"Player: " + p.getPin() + " ,role: " + p.getRole()
								+ " score is: " + p.getScore());
				getLog().writeln("Player: " + p.getPin() + "Game Ended");
				p.setRole("GameEnded");
			}
			endLog();
			resetPermissionFlags();
			setPermissions();
			}
			//gs.sendArbitraryMessage(Constants.NEWPHASE);
		} 
		else if (phasename.equals(ServerPhases.COMM_PH)) { // REQUESTS_PH
			for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
					pStat = gs.getPlayerByPerGameId(i);
					//pStat.setProposalAllowed(false);
					pStat.setCommunicationAllowed(false);
					//pStat.setMovesAllowed(true);
		    }
			TDPlayer.setMovesAllowed(true);
		} 
//		else if (phasename.equals(ServerPhases.MOVEMENT_PH)) {
//		}
	}

	public boolean doMove(int perGameId, RowCol newpos) {
		boolean bMoved = false;

 		bMoved = gs.doMove(perGameId, newpos);
		if (bMoved == true) {
		numberofMoves++;
		if ((TDPlayer.getPosition().equals(gs.getBoard().getGoalLocations().get(0)))||
				(TDPlayer.getPosition().equals(gs.getBoard().getGoalLocations().get(1))))
		{
			getLog()
			.writeln(
					"The TD player reached the goal, end the game");
			ended = true;
			gs.getPhases().advancePhase();
		}
		}
//		// calculate best use of chips possessed
//		BestUse bu = new BestUse(gs, TDPlayer, s, 0);
//		Path p = bu.getPaths().get(0);
//		// some checks for each step in the path
//		int numSteps = p.getNumPoints() - 1;  // number of steps (NOT points) in the path
//		//System.out.println("#steps in path: " + numSteps);
//		
//		if (numSteps == 0)
//		{
//			bu = new BestUse(gs, TDPlayer, s, 1);
//			p = bu.getPaths().get(0);
//		}
//		for (int stepPoint=0; stepPoint<numSteps; stepPoint++)
//		{
//			  RowCol pathpoint = p.getPoint(stepPoint + 1);  // point at end of this step
//			  String ppcolor = gs.getBoard().getSquare(pathpoint).getColor();       // color of the point
//			  System.out.println("Step point: " + stepPoint +
//								 "  Row: " + pathpoint.row + "  Col: " + pathpoint.col +
//								 "  Color: " + ppcolor);
//			  
//              // check if the player has the required chip
//              if (!TDPlayer.getChips().contains(new ChipSet(gs.getBoard().getSquare(pathpoint).getColor()))) {
//                 System.out.println("Status: player lacks needed chip");
//                 // return false;
//                 gs.getPhases().advancePhase();
//              }
//		}


		return bMoved;
	}
	

	// Override super method do discourse in order to make an automatic transfer
	// after accept a proposal
	public boolean doDiscourse(DiscourseMessage dm) {
		getLog().writeln("Received communication message ");
		// getLog().writeln("Class: " + dm.getClass() );
		//Send the message on.
        boolean result = gs.doDiscourse(dm);

        //If we have a response message.
        if( dm instanceof BasicProposalDiscussionDiscourseMessage) {
            BasicProposalDiscussionDiscourseMessage bpddm = (BasicProposalDiscussionDiscourseMessage) dm;

//            if(( bpddm.getFromPerGameId() != respondersInGame[0].getPerGameId())||
//            		( bpddm.getFromPerGameId() != respondersInGame[1].getPerGameId()))
//            {
//                throw new RuntimeException( "Only responder should send BasicProposalDiscussionDiscourseMessage.\n" +
//                                            "BPDDM Sent by id " + bpddm.getFromPerGameId() );
//            }
            if (bpddm.getProposerID() != TDPlayer.getPerGameId()) //i.e the proposer is the TD player 
            {
            	if (bpddm.getResponderID() != TDPlayer.getPerGameId()) // to be sure that for the TI proposing messages 
            	{
            		// only the TDPlayer can be the responder. 
            		// This is to be sure that one of the TI didn't send a proposal to the other TI Player 
            		// instead of the TD player...
            		getLog().writeln( "The service provider should send a proposal only to the client player\n");
            		gs.getPlayerByPerGameId(bpddm.getProposerID()).setCommunicationAllowed(true);
            		return result;
            	}
            }
             
            boolean reply = bpddm.accepted();
            boolean endDiscussion = false;
            
//    		if (csvLog != null) {    			
//    			PlayerStatus fromPlayer = gs.getPlayerByPerGameId(dm.getFromPerGameId());
//    			PlayerStatus toPlayer  = gs.getPlayerByPerGameId(dm.getToPerGameId());
//    			
//    			csvLog.print("," + reply + "," + getPlayerScore(fromPlayer) + ","+ getPlayerScore(toPlayer) + ","
//    			);
//    		}


            if (reply == true) {
             	acceptReplyCounter++;
            }
            else {
            	rejectReplyCounter++;
            }

            if (proposersInGame[0] == proposersInGame[1]) //i.e the proposer is the TD player 
            {
            	if (rejectReplyCounter == 1)
            		endDiscussion = true;
            	if( bpddm.getResponderID() == respondersInGame[0].getPerGameId() ) {
                    acceptedProposer0 = reply;
                    if (acceptedProposer0 == true) replyString0 = "TRUE";
                    else replyString0 = "FALSE";
                    getLog()
					.writeln( "Proposer 0 accepted? " + acceptedProposer0);
                }
                if( bpddm.getResponderID() == respondersInGame[1].getPerGameId() ) {
                    acceptedProposer1 = reply;
                    if (acceptedProposer1 == true) replyString1 = "TRUE";
                    else replyString1 = "FALSE";
                   getLog()
					.writeln( "Proposer 1 accepted? " + acceptedProposer1);
                }
//                else {
//                    throw new RuntimeException( "Response message sent to unknown proposer.\n" );
//                }	
            }
            else // Not the same proposers
            {
            if (rejectReplyCounter == 2)
            	endDiscussion = true;

            if( bpddm.getProposerID() == proposersInGame[0].getPerGameId() ) {
                acceptedProposer0 = reply;
                if (acceptedProposer0 == true) replyString0 = "TRUE";
                else replyString0 = "FALSE";
                getLog()
				.writeln( "Proposer 0 accepted? " + acceptedProposer0);
            }
            if( bpddm.getProposerID() == proposersInGame[1].getPerGameId() ) {
                acceptedProposer1 = reply;
                if (acceptedProposer1 == true) replyString1 = "TRUE";
                else replyString1 = "FALSE";
               getLog()
				.writeln( "Proposer 1 accepted? " + acceptedProposer1);
            }
//            else {
//                throw new RuntimeException( "Response message sent to unknown proposer.\n" );
//            }
            }
            
            //if there wasn't a proposal then it cannot be accepted.
            if( acceptedProposer0 == null )
            {
                acceptedProposer0 = false;
                replyString0 = "IGNORED";
            }
            if( acceptedProposer1 == null )
            {
                acceptedProposer1 = false;
                replyString1 = "IGNORED";
            }

            if ((proposer0Proposal == null) && (proposer1Proposal == null))
            	throw new RuntimeException( "Response message sent to unknown proposal.\n" );

            if ((proposer0Proposal != null) &&( acceptedProposer0 == true)) {
    			int propCS = getPlayerScore(proposersInGame[0]);
    			int respCS = getPlayerScore(respondersInGame[0]);
//            		if (csvLog != null) {   
//            			csvLog.print(proposal0String4Log+"," + replyString0 + "," + getPlayerScore(proposersInGame[0]) + ","
//            					+ getPlayerScore(respondersInGame[0]) + ",");
//            		}
                ChipSet proposer0toSend = proposer0Proposal.getChipsSentByProposer();
                ChipSet responderToSend = proposer0Proposal.getChipsSentByResponder();
               	

                exchange( proposersInGame[0], respondersInGame[0], proposer0toSend, responderToSend );
           		if (csvLog != null) {
           			reply0String4Log = replyString0 + "," +propCS+ "," +respCS + "," +
           				proposer0toSend + "," +responderToSend + "," + getPlayerScore(proposersInGame[0])+","+
    					getPlayerScore(respondersInGame[0]);
//    			csvLog.println(proposer0toSend + "," +responderToSend + "," + getPlayerScore(proposersInGame[0])+","+
//    					getPlayerScore(respondersInGame[0])+ ",,,,,,,");
//    			csvLog.flush();
           		}
                proposer0Proposal = null;
                acceptedProposer0 = null;
            
            }
            //else, if only the proposal from Proposer 1 was accepted.
            else if ((proposer1Proposal != null) && ( acceptedProposer1 == true)) {
    			int propCS = getPlayerScore(proposersInGame[1]);
    			int respCS = getPlayerScore(respondersInGame[1]);

//            		if (csvLog != null) { 
//            			csvLog.print(proposal1String4Log+"," + replyString1 + "," + getPlayerScore(proposersInGame[1]) + ","
//            					+ getPlayerScore(respondersInGame[1]) + ",");
//            		}
                    ChipSet proposer1toSend = proposer1Proposal.getChipsSentByProposer();
                    ChipSet responderToSend = proposer1Proposal.getChipsSentByResponder();

                exchange( proposersInGame[1], respondersInGame[1], proposer1toSend, responderToSend );
                
          		if (csvLog != null) {
           			reply1String4Log = replyString1 + "," +propCS+ "," +respCS + "," +
       				proposer1toSend + "," +responderToSend + "," + getPlayerScore(proposersInGame[1])+","+
					getPlayerScore(respondersInGame[1]);

//             		csvLog.println(proposer1toSend + "," +responderToSend + "," + getPlayerScore(proposersInGame[1])+","+
//            					getPlayerScore(respondersInGame[1])+",,,,,,,");
//    			csvLog.flush();
          		}
                proposer1Proposal = null;
                acceptedProposer1 = null;
            
            }
            else { //case that we had one reject, and one accept
            	getLog()
				.writeln( "case that we had one reject, and one accept" );            	
            }
             if ((acceptReplyCounter == 1) ||// ==1: to enable only ONE accept. == 2:only when there are 2 proposals and 2 answers 
					//(or when phase time expired, move on to the next phase 
                (endDiscussion == true))	
            { 
            	 if (csvLog != null) {    
            		 if (proposer0Proposal != null) {
            			 reply0String4Log = replyString0 + "," +getPlayerScore(proposersInGame[0])+ "," 
            			 +getPlayerScore(respondersInGame[0])+",,,,";
//            			csvLog.println(proposal0String4Log+"," + replyString0 + "," + getPlayerScore(proposersInGame[0]) + ","
//            					+ getPlayerScore(respondersInGame[0]) + ",");
//            			csvLog.flush();
            		 }
            		 if (proposer1Proposal != null) {
            			 reply1String4Log = replyString1 + "," +getPlayerScore(proposersInGame[1])+ "," 
            			 +getPlayerScore(respondersInGame[1])+",,,,";
//             			csvLog.println(proposal1String4Log+"," + replyString1 + "," + getPlayerScore(proposersInGame[1]) + ","
//             					+ getPlayerScore(respondersInGame[1]) + ",");
//             			csvLog.flush();
             		 }
            	 }
            	gs.sendArbitraryMessage(Constants.ENDDISCUSSION);
            	proposer0Proposal = null;
           	    proposer1Proposal = null;
           	    gs.getPhases().advancePhase();
            }   
        }
        //else, if we have a proposal message
        else if( dm instanceof BasicProposalDiscourseMessage) {
            BasicProposalDiscourseMessage bpdm = (BasicProposalDiscourseMessage) dm;
            getLog().writeln(
    				"From player: "
    						+ gs.getPlayerByPerGameId(dm.getFromPerGameId())
    								.getPin());
    		getLog()
    				.writeln(
    						"To player: "
    								+ gs.getPlayerByPerGameId(dm.getToPerGameId())
    										.getPin());
    		getLog().writeln(
					"Chips to send by proposer: "
							+ bpdm.getChipsSentByProposer());
			getLog().writeln(
					"Chips to send by responder: "
							+ bpdm.getChipsSentByResponder());
			
			if (proposersInGame[0] == proposersInGame[1]) // i.e, the same proposer in case of TD player, I'll check multi-cases using the responders
            {
            	if( bpdm.getResponderID() == respondersInGame[0].getPerGameId() ) {
                    //Make sure the responder doesn't rcv multiple proposals.
                    if( proposer0Proposal != null ) {
                       throw new RuntimeException( "Responder 0 has already rcv a proposal message this communication phase." );
                    }
                    proposer0Proposal = bpdm;
            		if (csvLog != null) {
            			proposal0String4Log = csvLogName+ "," + round + "," + gs.getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
                					+gs.getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
                					+(dm.getFromPerGameId() == TDPlayer.getPerGameId()) + "," 
                					+ bpdm.getChipsSentByProposer().removeZeros() + "," 
                					+ bpdm.getChipsSentByResponder().removeZeros();
                		}

                }
                //If the proposal is from Proposer 1
                else if( bpdm.getResponderID() == respondersInGame[1].getPerGameId() ) {
                    if( proposer1Proposal != null ) {
                    //Make sure the proposer isn't sending multiple proposals.
                        throw new RuntimeException( "Responder 0 has already rcv a proposal message this communication phase." );
                    }
                    proposer1Proposal = bpdm;
            		if (csvLog != null) {
            			proposal1String4Log = csvLogName+  "," + round + "," + gs.getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
                					+gs.getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
                					+(dm.getFromPerGameId() == TDPlayer.getPerGameId()) + "," 
                					+ bpdm.getChipsSentByProposer().removeZeros() + "," 
                					+ bpdm.getChipsSentByResponder().removeZeros();
                		}
                }
            	proposersInGame[0].setCommunicationAllowed(false); /* Enable the TD player to send only one proposal */
            	
            }
            else 
            { // the proposers are different, check according to the proposers
            //If the proposal is from Proposer 0
            if( bpdm.getProposerID() == proposersInGame[0].getPerGameId() ) {
                //Make sure the proposer isn't sending multiple proposals.
                if( proposer0Proposal != null ) {
                   //throw new RuntimeException( "Proposer 0 has already sent a proposal message this communication phase." );
                }
                else {
                proposer0Proposal = bpdm;
        		if (csvLog != null) {
        			proposal0String4Log = csvLogName+  "," + round + "," + gs.getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
            					+gs.getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
            					+(dm.getFromPerGameId() == TDPlayer.getPerGameId()) + "," 
            					
            					+ bpdm.getChipsSentByProposer().removeZeros() + "," 
            					+ bpdm.getChipsSentByResponder().removeZeros();
            		}

                proposersInGame[0].setCommunicationAllowed(false); /* Enable the TD player to send only one proposal */
                }
            }
            //If the proposal is from Proposer 1
            else if( bpdm.getProposerID() == proposersInGame[1].getPerGameId() ) {
                if( proposer1Proposal != null ) {
                //Make sure the proposer isn't sending multiple proposals.
                    //throw new RuntimeException( "Proposer 1 has already sent a proposal message this communication phase." );
                }
                else {
                proposer1Proposal = bpdm;
        		if (csvLog != null) {
        			proposal1String4Log = csvLogName+  "," + round + "," + gs.getPlayerByPerGameId(dm.getFromPerGameId()).getPin()+"," 
            					+gs.getPlayerByPerGameId(dm.getToPerGameId()).getPin() + "," 
            					+(dm.getFromPerGameId() == TDPlayer.getPerGameId()) + "," 
            					
            					+ bpdm.getChipsSentByProposer().removeZeros() + "," 
            					+ bpdm.getChipsSentByResponder().removeZeros();
            		}


                proposersInGame[1].setCommunicationAllowed(false); /* Enable the TD player to send only one proposal */
                }

            }
            }
            
            //If the proposal is from the responder then we have a problem.
            if(( bpdm.getProposerID() == respondersInGame[0].getPerGameId() ) ||
            		( bpdm.getProposerID() == respondersInGame[1].getPerGameId() ))
            {
                throw new RuntimeException( "Responder should not be sending proposal messages." );
            }
        }

		return result;
	}



	
	
	/**
	 * Start a new game and set up all of the game specifications.
	 */
	public void run() {
		System.out.println("Let the game begin...");

		System.out.println("game id= " + gs.getGameId());
		
		// generating an ID (hopefully unique) based in timestamp
		Long currTS = (new Date()).getTime();
		// keep ID number
		currTS = currTS % 10000;

		AltOffersConfigArguments par = (AltOffersConfigArguments) gs
				.getDataFromController();

		String dep = par.getDep();
		String rltp = par.getRelationship();
		String name = new String();
		int digit;
		String relationship = new String();

		System.out
		.println("inout args: dep=" + dep + "rel=" + rltp);
		if (rltp.equals("FRIEND")) {
			relationship = "friend";

			// human independent
			if (dep.equals(AltOffersGameDetails.dependencies[0])) {
				name = "boardXml_GUI_TI_3Players.txt";
				digit = 2;
			}
			// human dependent
			else if (dep.equals(AltOffersGameDetails.dependencies[1])) {
				name = "boardXml_AGENT_TI_3Players.txt";
				digit = 5;
			}
			// all interdependent
			else if (dep.equals(AltOffersGameDetails.dependencies[2])) {
				name = "boardXml_both_need_three.txt";
				digit = 1;
			}
			// defaut: all interdependent
			else {
				name = "boardXml_both_need_three.txt";
				digit = 1;
			}
		} else if (rltp.equals("STRANGER")) {
			relationship = "stranger";

			// human independent
			if (dep.equals(AltOffersGameDetails.dependencies[0])) {
				name = "boardXml_GUI_TI_3Players.txt";
				digit = 4;
			}
			// human dependent
			else if (dep.equals(AltOffersGameDetails.dependencies[1])) {
				name = "boardXml_AGENT_TI_3Players.txt";
				digit = 6;
			}
			// all interdependent
			else if (dep.equals(AltOffersGameDetails.dependencies[2])) {
				name = "boardXml_both_need_three.txt";
				digit = 3;
			}
			// defaut: all interdependent
			else {
				name = "boardXml_both_need_three.txt";
				digit = 3;
			}
			// no friends or stranger specified
		} else {
			// human independent
			if (dep.equals(AltOffersGameDetails.dependencies[0])) {
				name = "boardXml_GUI_TI_3Players.txt";
				digit = 4;
			}
			// human dependent
			else if (dep.equals(AltOffersGameDetails.dependencies[1])) {
				name = "boardXml_AGENT_TI_3Players.txt";
				digit = 6;
			}
			// all interdependent
			else if (dep.equals(AltOffersGameDetails.dependencies[2])) {
				name = "boardXml_both_need_three.txt";
				digit = 3;
			}
			// default: all interdependent
			else {
				name = "boardXml_both_need_three.txt";
				digit = 3;
			}
		}

		currTS = currTS + (digit * 10000);

		for (PlayerStatus ps : gs.getPlayers()) {
			ps.setTimeStampID(currTS.intValue() + ps.getPerGameId());
			ps.setRelationship(relationship);
		}

		getLog().writeln("ID Number: " + currTS);

		GameBoardCreator gameBoardCreator = new GameBoardCreator(gs, logName);
		gameBoardCreator.createGameBoard(name);

		PhasesLength phL = new PhasesLength();
		phL.getPhasesTimes();

		ServerPhases ph = new ServerPhases(this);
		ph.addPhase(ServerPhases.STRATEGY_PH, phL.stratPrepPhTime);
		ph.addPhase(ServerPhases.COMM_PH, phL.commPhTime);
		ph.addPhase(ServerPhases.MOVEMENT_PH, phL.movPhTime);
		ph.addPhase(ServerPhases.FEEDBACK_PH, phL.fdbckPhTime);

		
        //set the scoring function.
        gs.setScoring(this.s);

        //loop the phases.
        ph.setLoop(true);
		gs.setPhases(ph);

		gs.setInitialized();  // will generate GAME_INITIALIZED message
		
		// cause only the TI players, i.e., those players that have at the beginning grey or yellow 
		// to be the first proposers
		
		/* The TI can be two human players or one human player and one agent player. 
		 * the responders will always be the same one in the beginning. 
		 * Doesn't matter with Dependency role we play
		 */
		PlayerStatus pStat;
		int entry = 0;
		for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
			pStat = gs.getPlayerByPerGameId(i);
			//gs.sendGamePlayersChangedMessage(gs.getPlayers(), pStat.getPerGameId());
			if ((pStat.getChips().getNumChips("CTYellow")!=0)||
					(pStat.getChips().getNumChips("grey78")!=0)) 
			{
				communicationAllowed[i] = true;
//				proposalAllowed[i] = true;
//				answerAllowed[i] = false;
				movementAllowed[i] = false; // The TI players should never move
				proposersInGame[entry] = pStat;
				entry++;
				gs.getPlayerByPerGameId(i).setRole("Proposer");
				pStat.setHisNumDormantRounds(
						consecutiveNoMovementCounterPlayer1);
			}
			else {
				communicationAllowed[i] = false;
//				proposalAllowed[i] = false;
//				answerAllowed[i] = false;
				movementAllowed[i] = true; // The TD can move whenever he has enough chips to move with...
				respondersInGame[0] = pStat;
				respondersInGame[1] = pStat;
				gs.getPlayerByPerGameId(i).setRole("Responder");
				TDPlayer = pStat;
				TDPlayer.setMyNumDormantRounds(
						consecutiveNoMovementCounterPlayer1);
				//vFirstProposerInRound.add(i); 
			}
			System.out.println( "Player number " + i + " is " + gs.getPlayerByPerGameId(i).getRole());			
		}	
	}


	private boolean isThereSatisfiablePath(PlayerStatus player) {
		boolean bThereIs = false;

//		System.out.println( "getGoalLocations of type 0"+gs.getBoard().getGoalLocations()+" in size" +gs.getBoard().getGoalLocations().size());
//		
//		System.out.println( "getGoalLocations of type 10"+gs.getBoard().getGoalLocations(10)+" in size" +gs.getBoard().getGoalLocations(10).size());
//		System.out.println( "getGoalLocations of type 20"+gs.getBoard().getGoalLocations(20)+" in size" +gs.getBoard().getGoalLocations(20).size());
//		System.out.println( "getGoalLocations of type 11"+gs.getBoard().getGoalLocations(11)+" in size" +gs.getBoard().getGoalLocations(11).size());
//		System.out.println( "getGoalLocations of type 21"+gs.getBoard().getGoalLocations(21)+" in size" +gs.getBoard().getGoalLocations(21).size());
//		

//		ArrayList<Path> shortestPaths = ShortestPaths
//				.getShortestPathsForMyChips(player.getPosition(), gs.getBoard()
//						.getGoalLocations(10).get(0), gs.getBoard(), s,
//						ShortestPaths.NUM_PATHS_RELEVANT, player.getChips());
//
//		ChipSet playerChips = player.getChips();
//		for (Path path : shortestPaths) {
//			ChipSet required = path.getRequiredChips(gs.getBoard());
//			if (playerChips.contains(required)) {
//				bThereIs = true;
//				break;
//			}
//		}
		return bThereIs;
	}

	private FileLogger getLog() {
		return FileLogger.getInstance(logName);
	}
}