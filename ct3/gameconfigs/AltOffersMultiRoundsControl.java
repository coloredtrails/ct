//package edu.harvard.eecs.airg.coloredtrails.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import edu.harvard.eecs.airg.coloredtrails.controller.ControlImpl;
import edu.harvard.eecs.airg.coloredtrails.controller.GameEndWatcher;
import edu.harvard.eecs.airg.coloredtrails.controller.Round;
import edu.harvard.eecs.airg.coloredtrails.controller.MultiRoundsAllocatorAltOffers;
import edu.harvard.eecs.airg.coloredtrails.shared.types.*;

/**
 * This controller gets the number of rounds (1 or 2) for each player, and three
 * number: 2n, 2k ,2t that are used as follows: At the first round, 2n subjects
 * will play each other on the Dependent-Dependent boards,2k subjects will play
 * each other on the Independent-Dependent board, and 2t will play against the
 * agent. We assume an even number of subjects.
 * 
 * The players are divided into two groups, assuming low pins are sitting in a
 * different room then high pins so that each player always plays with a player
 * from a different room.
 * 
 * In the first round, each subject is placed randomly in D-D or I-D group, and
 * the first proposer is set randomly. In the second round, the groups switch ,
 * so that the D-D subjects play I-D and vice versa. Also, the order of
 * first-move proposers is switched, so that first-move proposers in the first
 * round become first-move responders in the second round, and vice versa. Each
 * group is shuffled so that subjects will play with a different person.
 * 
 * @author greg
 * @author KPozin, kobi
 * @author Yael blumberg
 * 
 */
public class AltOffersMultiRoundsControl {

	/**
	 * NOTE: To get this to work the various experimental conditions, change the
	 * value passed in line 62 to the name of the config file you want to use.
	 */
	public static void main(String[] args) {

		System.out.println("Code Controller starting");
		int playersPerGame = 2;
		// The number of games being played at a time
		int numSimultGames;
		// The number of rounds
		int numRounds = 2;

		int requestedRound = 0;

		AltOffersConfigArguments configArgs = null;
		if (args.length >= 4) {
			numRounds = Integer.parseInt(args[0]);
			configArgs = new AltOffersConfigArguments(
					Integer.parseInt(args[1]), Integer.parseInt(args[2]),
					Integer.parseInt(args[3]));
		} else {
			System.out
					.println("Usage:\n"
							+ "[program] roundsNo numHumanHumanA numHumanHumanB numHumanComputer [exptId requestedRound]");
			System.exit(1);
		}

		Date now = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		configArgs.setExperimentId(formatter.format(now));

		if (args.length == 6) {
			// We're going to use this to load and deserialize the appropriate
			// roster
			configArgs.setExperimentId(args[4]);
			requestedRound = Integer.parseInt(args[5]);
		}

		// Initialize controller, IP and port must be specified
		ControlImpl controlImpl = new ControlImpl("tcp://127.0.0.1:8200");

		MultiRoundsAllocatorAltOffers ra = new MultiRoundsAllocatorAltOffers(
				controlImpl, "gameconfigs", "AlternativeOffersUltimatumConfig",
				false, playersPerGame, configArgs);

		Scanner scanner = new Scanner(System.in);

		ra.generateRounds();

		// Run all the rounds, waiting for each one to fully complete
		for (int roundNum = requestedRound; roundNum < numRounds; ++roundNum) {
			System.out.println("\n\nPress Return to start Round " + roundNum
					+ ".");
			scanner.nextLine();

			Round curRound = ra.getRound(roundNum);
			System.out.println("Beginning Round " + roundNum);

			// Start the round's games
			numSimultGames = curRound.size();
			for (int g = 0; g < numSimultGames; ++g) {
				System.out.println("Trying to start game " + g);
				curRound.get(g).Start();
			}

			// Wait for this round to end before beginning the next
			GameEndWatcher gew = new GameEndWatcher(curRound);
			gew.waitForGamesToEnd();
			System.out.println("Ending Round " + roundNum + "!");
		}
	}
}