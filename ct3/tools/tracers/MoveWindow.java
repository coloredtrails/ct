
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class MoveWindow extends JWindow implements MouseListener,
        MouseMotionListener {
    Point location;
    MouseEvent pressed;

    public MoveWindow() {
        getContentPane().add(new JButton("Hello World"),
                BorderLayout.NORTH);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void mousePressed(MouseEvent me) {
        pressed = me;
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent me) {
        location = getLocation(location);
        int x = location.x - pressed.getX() + me.getX();
        int y = location.y - pressed.getY() + me.getY();
        setLocation(x, y);
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public static void main(String args[]) {
        MoveWindow window = new MoveWindow();
        window.setSize(300, 300);
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
}
