package tracers;

import org.apache.xmlrpc.AsyncCallback;
import org.apache.xmlrpc.XmlRpc;
import org.apache.xmlrpc.XmlRpcClientLite;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

public class BrokeClient implements AsyncCallback {
    public static void main(String[] args) {
        BrokeClient tc = new BrokeClient();
        try {
            XmlRpc.setDebug(true);
            XmlRpcClientLite xmlrpc = new XmlRpcClientLite(
                    "http://foo.com:8000/");
            tc = new BrokeClient();

            xmlrpc.execute("get3", new Vector());

            xmlrpc.executeAsync("get3", new Vector(), tc);
            System.out.println("waiting");
        } catch (MalformedURLException e) {
            System.out.println("that's bad");
        } catch (XmlRpcException e) {
            System.out.println("xmlrpc death\n" + e);
        } catch (IOException e) {
            System.out.println("io death\n" + e);
        }
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
        }
    }

    public void handleResult(Object result, URL url, String method) {
        System.out.println("what up");
        System.out.println(result);
    }

    public void handleError(Exception exception, URL url, String method) {
        exception.printStackTrace();
    }
}
