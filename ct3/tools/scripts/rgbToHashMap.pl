#!/usr/bin/perl

use strict;
use warnings;

while (<>) {
    my $line = $_;
    chomp($line);
    if ($line =~ /^(\d+)\s+(\d+)\s+(\d+)\s+([^\n\r]+)$/) {
        my $r = $1; my $g = $2; my $b = $3;
        my $name = $4;

        #print "$name : (R$r:G$g:B$b)\n";
#        print "colorMap.put(\"$name\", Arrays.asList(\n     new Integer($r), new Integer($g), new Integer($b)));\n"; 
        print "addColor(\"$name\", $r, $g, $b);\n";
    }
}
