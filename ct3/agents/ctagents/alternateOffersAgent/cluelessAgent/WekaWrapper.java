package ctagents.alternateOffersAgent.cluelessAgent;

import ctagents.alternateOffersAgent.cluelessAgent.GameSimulator.Culture;

import weka.classifiers.Classifier;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 * NOTE: This class contains single-threaded implementations of the Singleton
 * pattern. If a multi thread access to the classifiers is needed, you must add
 * synchronization to the methods as appropriate.
 * 
 * @author Galit Haim
 * 
 */
public class WekaWrapper {

	// Default folder name in case the agent didn't set the culture
	public static String cultureFolder = "agents/ctagents/alternateOffersAgent/palAgent/wekamodels";

	// Variables for the transfer class classifier: Reliability model
	// public static String transferModelFilename =
	// "wekamodels/PUK-TransferModel.model"; // This file is for 4
	// classifications: 0, 0.3, 0.5, 1
	// public static String transferModelFilename =
	// "wekamodels/BayesNetTransferReli2Options.model"; // This file is for 2
	// classification: 0-none, 1-all
	// public static String transferModelFilename =
	// "wekamodels/PUK-transferReli2OptionsModel.model";
	public static String transferModelFilename = "/NNTransfer.model";
	/**
	 * Actual classifier for class. Will be loaded from file.
	 */
	private static Classifier trClass = null;

	/**
	 * Set of instances. This is just used to define the relation for Transfer.
	 */
	private static Instances trInstances = null;

	/**
	 * Vector of attributes
	 */
	private static FastVector trAttributes = null;

	// Variables for the acceptance class classifier: acceptance model

	public static String acceptModelFilename = "/NNAcceptanceModel.model";
	private static Classifier acClass = null;
	private static Instances acInstances = null;
	private static FastVector acAttributes = null;

	// Variables for the human reach goal class classifier

	public static String hrgModelFilename = "agents/ctagents/alternateOffersAgent/palAgent/wekamodels/J48HumanReachedGoal.model";
	private static Classifier hrgClass = null;
	private static Instances hrgInstances = null;
	private static FastVector hrgAttributes = null;

	// Variables for the agent reach goal class classifier

	// public static String argModelFilename =
	// "wekamodels/J48AgentReachedGoal.model";
	public static String argModelFilename = "/NNAgentReachedGoal.model";
	private static Classifier argClass = null;
	private static Instances argInstances = null;
	private static FastVector argAttributes = null;

	public static Culture opponentCulture;
	/**
	 * Vector of nominal attribute values for None, Some, All
	 */
	private static FastVector fvNoneSomeAll;

	/**
	 * Vector of nominal attribute values for True, False
	 */
	private static FastVector fvTrueFalse;

	private static FastVector playerRole;

	// *** ATTRIBUTE CREATION ***
	// Note that attributes CAN NOT!! be reused in multiple classifiers

	private static Attribute a_playerCurrentScoreARG,
			a_opponentCurrentScoreARG, a_playerPreviousReliabilityARG,
			a_opponentPreviousReliabilityARG, a_playerRoleARG,
			a_opponentRoleARG, 
			a_playerDormant, a_oppDormant, 
			a_agentReachGoalClass;

	private static Attribute a_playerCurrentScoreTransfer,
			a_opponentCurrentScoreTransfer, a_playerResultingScoreTransfer,
			a_opponentResultingScoreTransfer,
			a_playerWightedPreviousReliability,
			a_opponentWightedPreviousReliability, a_playerNeededColorsTransfer,
			a_playerOtherColorsTransfer, a_opponentNeededColorsTransfer, a_opponentOtherColorsTransfer,
			a_opponentTransferClass;

//	private static Attribute a_playerCurrentScoreAccept,
//			a_opponentCurrentScoreAccept, a_playerResultingScoreAccept,
//			a_opponentResultingScoreAccept, a_OfferGenerousity,
//			a_playerNeededColorsAccept, a_playerOtherColorsAccept,
//			a_opponentNeededColorsAccept, a_opponentOtherColorsAccept,
//			a_opponentAcceptClass;

	private static Attribute a_playerCurrentScoreAccept,a_playerResultingScoreAccept,
	a_opponentCurrentScoreAccept, 
	a_opponentResultingScoreAccept, a_OfferGenerousity,
	a_playerNeededColorsAccept, a_playerOtherColorsAccept,
	a_opponentNeededColorsAccept, a_opponentOtherColorsAccept,
	a_opponentAcceptClass;

	private static Attribute a_playerCurrentScoreHRG,
			a_opponentCurrentScoreHRG, a_playerPreviousReliabilityHRG,
			a_opponentPreviousReliabilityHRG, a_playerRoleHRG,
			a_opponentRoleHRG, a_playerMissedChips, a_opponentMissedChips,
			a_humanReachGoalClass;

	static {
		// Create a vector for none, some, all

		fvNoneSomeAll = new FastVector(2);
		fvNoneSomeAll.addElement("0");
		// fvNoneSomeAll.addElement("some");
		fvNoneSomeAll.addElement("1");

		// Create a vector for True, False

		fvTrueFalse = new FastVector(2);
		fvTrueFalse.addElement("TRUE");
		fvTrueFalse.addElement("FALSE");

		playerRole = new FastVector(2);
		playerRole.addElement("TD");
		playerRole.addElement("TI");
	}

	public static void setCultureFolder(String folderName) {
		cultureFolder = folderName;
	}
	
	public static void setCulture(Culture oppCulture) {
		opponentCulture = oppCulture;
	}

	public static String getCultureFolder() {
		return (cultureFolder);
	}
	
	public static void assertNotNull(Object variable, String name) {
		if (variable == null) {
			System.out.println(name + " is null!");
		}
	}

	/**
	 * Wrapper around weka.core.SerializationHelper.read
	 * 
	 * @param filename
	 *            Filename of the serialized Weka model (generally exported from
	 *            the Weka GUI).
	 * @return Classifier that has been deserialized from the loaded model file.
	 */
	public static Classifier loadClassifier(String filename) {
		Classifier temp = null;
		try {
			temp = (Classifier) weka.core.SerializationHelper.read(filename);
			System.out.println("Successfully loaded " + filename);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}

	/*********************************************************************************/
	/**
	 * Get a FastVector of the attributes needed for the accept predictor.
	 * 
	 * @return
	 */
	public static FastVector getAcceptAttributes() {

		if (acAttributes == null) {

			// Create each of the numeric attributes, passing in their names.
			// The names should correspond to the names in the arff file /
			// model.

			a_playerCurrentScoreAccept = new Attribute("playerCurrentScore");
			a_opponentCurrentScoreAccept = new Attribute("opponentCurrentScore");
			a_playerResultingScoreAccept = new Attribute("playerResultingScore");
			a_opponentResultingScoreAccept = new Attribute("opponentResultingScore");
			a_OfferGenerousity = new Attribute("OfferGenerousity");
			a_playerNeededColorsAccept = new Attribute("playerNeededColors");
			a_playerOtherColorsAccept = new Attribute("playerOtherColors");
			a_opponentNeededColorsAccept = new Attribute("opponentNeededColors");
			a_opponentOtherColorsAccept = new Attribute("opponentOtherColors");

			a_opponentAcceptClass = new Attribute("opponentAcceptClass",
					fvTrueFalse);

			acAttributes = new FastVector();
			acAttributes.addElement(a_playerCurrentScoreAccept);
			acAttributes.addElement(a_playerResultingScoreAccept);
			acAttributes.addElement(a_opponentCurrentScoreAccept);
			acAttributes.addElement(a_opponentResultingScoreAccept);
			acAttributes.addElement(a_OfferGenerousity);
			acAttributes.addElement(a_playerNeededColorsAccept);
			acAttributes.addElement(a_playerOtherColorsAccept);
			acAttributes.addElement(a_opponentNeededColorsAccept);
			acAttributes.addElement(a_opponentOtherColorsAccept);

			acAttributes.addElement(a_opponentAcceptClass);
		}
		return acAttributes;
	}

	/**
	 * Return an Accept classifier
	 * 
	 * @return
	 */
	public static Classifier getAcceptClassifier() {
		return getAcceptClassifier(cultureFolder + acceptModelFilename);
	}

	/**
	 * Load the Accept classifier with the given filename and return it.
	 * 
	 * @param filename
	 * @return
	 */
	public static Classifier getAcceptClassifier(String filename) {
		if (acClass == null) {
			acClass = loadClassifier(filename);
		}

		return acClass;
	}

	/**
	 * Create a new Accept instance based on the passed array of values
	 * 
	 * @param values
	 * @return
	 */
	public static Instance getAcceptInstance(Object[] values) {
		return getInstance(getAcceptInstances(), getAcceptAttributes(), values);
	}

	/**
	 * Get an empty Instances object for Accept. This defines the "relation" for
	 * Accept.
	 * 
	 * @return
	 */
	public static Instances getAcceptInstances() {
		if (acInstances == null) {
			acInstances = new Instances("Acceptance", getAcceptAttributes(), 1);
			acInstances.setClassIndex(getAcceptAttributes().size() - 1);
		}
		return acInstances;
	}

	/**
	 * Get the probability distribution for Accept based on the passed Instance
	 * 
	 * @param in
	 * @return
	 * @throws Exception
	 */
	public static double getAcceptProbability(Instance in) throws Exception {
		double[] distribution = getAcceptClassifier().distributionForInstance(
				in);
		return distribution[1];
	}

	/**
	 * Get the probability distribution for Accept based on an array of values
	 * that will be converted to an Instance
	 * 
	 * @param values
	 * @return
	 * @throws Exception
	 */
	public static double getAcceptProbability(Object[] values) throws Exception {
		return getAcceptProbability(getAcceptInstance(values));
	}

	/**
	 * Generates a new Instance object from the passed array of values.
	 * 
	 * @param ins
	 *            The relation for which we're creating an instance.
	 * @param attributes
	 *            A vector of attributes describing the values
	 * @param values
	 *            The actual values
	 * @return
	 */
	public static Instance getInstance(Instances ins, FastVector attributes,
			Object[] values) {
		Instance in = new Instance(values.length);
		in.setDataset(ins);

		// Attributes and values must match up in order, data type, and number
		for (int i = 0; i < values.length; ++i) {
			Object val = values[i];
			if (val != null) {
				if (val instanceof Double || val instanceof Integer) {
					Double doubleVal = (Double) val;
					assertNotNull(doubleVal, "doubleVal");
					in.setValue((Attribute) attributes.elementAt(i), doubleVal);
				} else if (val instanceof String) {
					String stringVal = (String) val;
					assertNotNull(stringVal, "stringVal");
					in.setValue((Attribute) attributes.elementAt(i), stringVal);
				}
			}
		}

		return in;
	}

	/*************************************************************************************/
	/**
	 * Populates and returns the list of attributes for Transfer
	 * 
	 * @return
	 */
	public static FastVector getTransferAttributes() {
		if (trAttributes == null) {
			// Create each of the numeric attributes, passing in their names.
			// The names should correspond to the names in the arff file /
			// model.

			a_playerCurrentScoreTransfer = new Attribute("playerCurrentScore");
			a_opponentCurrentScoreTransfer = new Attribute(
					"opponentCurrentScore");
			a_playerResultingScoreTransfer = new Attribute(
					"playerResultingScore");
			a_opponentResultingScoreTransfer = new Attribute(
					"opponentResultingScore");
			a_playerWightedPreviousReliability = new Attribute(
					"playerWightedPreviousReliability");
			a_opponentWightedPreviousReliability = new Attribute(
					"opponentWightedPreviousReliability");
			a_playerNeededColorsTransfer = new Attribute("playerNeededColors");
			a_playerOtherColorsTransfer = new Attribute("playerOtherColors");
			a_opponentNeededColorsTransfer = new Attribute("opponentNeededColors");
			a_opponentOtherColorsTransfer = new Attribute("opponentOtherColors");

			a_opponentTransferClass = new Attribute("opponentTransfer",
					fvNoneSomeAll);

			trAttributes = new FastVector();
			trAttributes.addElement(a_playerWightedPreviousReliability);
			trAttributes.addElement(a_opponentWightedPreviousReliability);
			trAttributes.addElement(a_playerCurrentScoreTransfer);
			trAttributes.addElement(a_playerResultingScoreTransfer);
			trAttributes.addElement(a_opponentCurrentScoreTransfer);
			trAttributes.addElement(a_opponentResultingScoreTransfer);
			trAttributes.addElement(a_playerNeededColorsTransfer);
			trAttributes.addElement(a_playerOtherColorsTransfer);
			trAttributes.addElement(a_opponentNeededColorsTransfer);
			trAttributes.addElement(a_opponentOtherColorsTransfer);

			trAttributes.addElement(a_opponentTransferClass);
		}
		return trAttributes;
	}

	/**
	 * Loads and returns the Classifier for Transfer
	 * 
	 * @return
	 */
	public static Classifier getTransferClassifier() {
		return getTransferClassifier(cultureFolder + transferModelFilename);
	}

	/**
	 * Loads and returns the Classifier for Transfer
	 * 
	 * @param filename
	 * @return
	 */
	public static Classifier getTransferClassifier(String filename) {
		if (trClass == null) {
			trClass = loadClassifier(filename);
		}

		return trClass;
	}

	/**
	 * Creates a new Instance for Transfer
	 * 
	 * @param values
	 * @return
	 */
	public static Instance getTransferInstance(Object[] values) {
		return getInstance(getTransferInstances(), getTransferAttributes(),
				values);
	}

	/**
	 * Creates the Instances object (relation definition) for Transfer
	 * 
	 * @return
	 */
	public static Instances getTransferInstances() {
		if (trInstances == null) {
			trInstances = new Instances("Transfer", getTransferAttributes(), 1);
			trInstances.setClassIndex(getTransferAttributes().size() - 1);
		}
		return trInstances;
	}

	/**
	 * Uses the classifier to calculate the probability distribution for
	 * Transfer based on the passed Instance
	 * 
	 * @param in
	 * @return
	 * @throws Exception
	 */
	public static double[] getTransferProbabilities(Instance in)
			throws Exception {
		double[] distribution = getTransferClassifier()
				.distributionForInstance(in);
		return distribution;
	}

	/**
	 * Uses the classifier to calculate the probability distribution for
	 * Transfer based on the Instance created from the passed values
	 * 
	 * @param values
	 * @return
	 * @throws Exception
	 */
	public static double[] getTransferProbabilities(Object[] values)
			throws Exception {
		return getTransferProbabilities(getTransferInstance(values));
	}

	/*************************************************************************/
	/**
	 * Populates and returns the list of attributes for HumanReachGoal Model
	 * 
	 * @return
	 */
	public static FastVector getHumanReachGoalAttributes() {
		if (hrgAttributes == null) {
			// Create each of the numeric attributes, passing in their names.
			// The names should correspond to the names in the arff file /
			// model.

			a_playerCurrentScoreHRG = new Attribute("playerCurrentScore");
			a_opponentCurrentScoreHRG = new Attribute("opponentCurrentScore");
			a_playerPreviousReliabilityHRG = new Attribute(
					"playerPreviousReliability");
			a_opponentPreviousReliabilityHRG = new Attribute(
					"opponentPreviousReliability");
			a_playerRoleHRG = new Attribute("playerRole", playerRole);
			a_opponentRoleHRG = new Attribute("opponentRole", playerRole);
			a_playerMissedChips = new Attribute("playerMissedChips");
			a_opponentMissedChips = new Attribute("opponentMissedChips");

			a_humanReachGoalClass = new Attribute("humanReachGoalClass",
					fvTrueFalse);

			hrgAttributes = new FastVector();
			hrgAttributes.addElement(a_playerCurrentScoreHRG);
			hrgAttributes.addElement(a_opponentCurrentScoreHRG);
			hrgAttributes.addElement(a_playerPreviousReliabilityHRG);
			hrgAttributes.addElement(a_opponentPreviousReliabilityHRG);
			hrgAttributes.addElement(a_playerRoleHRG);
			hrgAttributes.addElement(a_opponentRoleHRG);
			hrgAttributes.addElement(a_playerMissedChips);
			hrgAttributes.addElement(a_opponentMissedChips);

			hrgAttributes.addElement(a_humanReachGoalClass);
		}
		return hrgAttributes;
	}

	/**
	 * Loads and returns the Classifier for HumanReachGoal
	 * 
	 * @return
	 */
	public static Classifier getHumanReachGoalClassifier() {
		return getHumanReachGoalClassifier(hrgModelFilename);
	}

	/**
	 * Loads and returns the Classifier for HumanReachGoal
	 * 
	 * @param filename
	 * @return
	 */
	public static Classifier getHumanReachGoalClassifier(String filename) {
		if (hrgClass == null) {
			hrgClass = loadClassifier(filename);
		}

		return hrgClass;
	}

	/**
	 * Creates a new Instance for HumanReachGoal
	 * 
	 * @param values
	 * @return
	 */
	public static Instance getHumanReachGoalInstance(Object[] values) {
		return getInstance(getHumanReachGoalInstances(),
				getHumanReachGoalAttributes(), values);
	}

	/**
	 * Creates the Instances object (relation definition) for HumanReachGoal
	 * 
	 * @return
	 */
	public static Instances getHumanReachGoalInstances() {
		if (hrgInstances == null) {
			hrgInstances = new Instances("HumanReachGoal",
					getHumanReachGoalAttributes(), 1);
			hrgInstances
					.setClassIndex(getHumanReachGoalAttributes().size() - 1);
		}
		return hrgInstances;
	}

	/**
	 * Uses the classifier to calculate the probability distribution for
	 * HumanReachGoal based on the passed Instance
	 * 
	 * @param in
	 * @return
	 * @throws Exception
	 */
	public static double[] getHumanReachGoalProbabilities(Instance in)
			throws Exception {
		double[] distribution = getHumanReachGoalClassifier()
				.distributionForInstance(in);
		return distribution;
	}

	/**
	 * Uses the classifier to calculate the probability distribution for
	 * HumanReachGoal based on the Instance created from the passed values
	 * 
	 * @param values
	 * @return
	 * @throws Exception
	 */
	public static double[] getHumanReachGoalProbabilities(Object[] values)
			throws Exception {
		return getHumanReachGoalProbabilities(getHumanReachGoalInstance(values));
	}

	/*************************************************************************/
	/**
	 * Populates and returns the list of attributes for AgentReachGoal Model
	 * 
	 * @return
	 */
	public static FastVector getAgentReachGoalAttributes() {
		if (argAttributes == null) {
			// Create each of the numeric attributes, passing in their names.
			// The names should correspond to the names in the arff file /
			// model.

			a_playerCurrentScoreARG = new Attribute("playerCurrentScore");
			a_opponentCurrentScoreARG = new Attribute("opponentCurrentScore");
			a_playerPreviousReliabilityARG = new Attribute(
					"playerPreviousReliability");
			a_opponentPreviousReliabilityARG = new Attribute(
					"opponentPreviousReliability");
			a_playerRoleARG = new Attribute("playerRole", playerRole);
			a_opponentRoleARG = new Attribute("opponentRole", playerRole);

			if (opponentCulture == Culture.LEBANON)
			{
			a_playerDormant = new Attribute("playerDormant");
			a_oppDormant = new Attribute("oppDormant");
			}
			a_agentReachGoalClass = new Attribute("agentReachGoalClass",
					fvTrueFalse);

			argAttributes = new FastVector();
			argAttributes.addElement(a_playerCurrentScoreARG);
			argAttributes.addElement(a_opponentCurrentScoreARG);
			argAttributes.addElement(a_playerRoleARG);
			argAttributes.addElement(a_opponentRoleARG);
			argAttributes.addElement(a_playerPreviousReliabilityARG);
			argAttributes.addElement(a_opponentPreviousReliabilityARG);

			if (opponentCulture == Culture.LEBANON)
			{
				argAttributes.addElement(a_playerDormant);
				argAttributes.addElement(a_oppDormant);
			}
			argAttributes.addElement(a_agentReachGoalClass);
		}
		return argAttributes;
	}

	/**
	 * Loads and returns the Classifier for AgentReachGoal
	 * 
	 * @return
	 */
	public static Classifier getAgentReachGoalClassifier() {
		return getAgentReachGoalClassifier(cultureFolder + argModelFilename);
	}

	/**
	 * Loads and returns the Classifier for AgentReachGoal
	 * 
	 * @param filename
	 * @return
	 */
	public static Classifier getAgentReachGoalClassifier(String filename) {
		if (argClass == null) {
			argClass = loadClassifier(filename);
		}

		return argClass;
	}

	/**
	 * Creates a new Instance for AgentReachGoal
	 * 
	 * @param values
	 * @return
	 */
	public static Instance getAgentReachGoalInstance(Object[] values) {
		return getInstance(getAgentReachGoalInstances(),
				getAgentReachGoalAttributes(), values);
	}

	/**
	 * Creates the Instances object (relation definition) for AgentReachGoal
	 * 
	 * @return
	 */
	public static Instances getAgentReachGoalInstances() {
		if (argInstances == null) {
			argInstances = new Instances("AgentReachGoal",
					getAgentReachGoalAttributes(), 1);
			argInstances
					.setClassIndex(getAgentReachGoalAttributes().size() - 1);
		}
		return argInstances;
	}

	/**
	 * Uses the classifier to calculate the probability distribution for
	 * AgentReachGoal based on the passed Instance
	 * 
	 * @param in
	 * @return
	 * @throws Exception
	 */
	public static double[] getAgentReachGoalProbabilities(Instance in)
			throws Exception {
		double[] distribution = getAgentReachGoalClassifier()
				.distributionForInstance(in);
		return distribution;
	}

	/**
	 * Uses the classifier to calculate the probability distribution for
	 * AgentReachGoal based on the Instance created from the passed values
	 * 
	 * @param values
	 * @return
	 * @throws Exception
	 */
	public static double[] getAgentReachGoalProbabilities(Object[] values)
			throws Exception {
		return getAgentReachGoalProbabilities(getAgentReachGoalInstance(values));
	}

}
