//////////////////////////////////////////////////////////////////////////
// File:		GameSimulator.java 					    				//
// Purpose:		Implements the game simulator class.					//
//				The game simulator class is able to simulate the game	//
//				of CT (both players).									//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.cluelessAgent;

// Imports from Java common framework
import java.util.Collections;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.List;
import java.lang.Math;
import java.util.LinkedHashSet;
import java.util.HashMap;
import java.util.Map;

// Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.FileLogger;

//////////////////////////////////////////////////////////////////////////
// Class:		GameSimulator	 					    				//
// Purpose:		Implements a game simulator.							//
//////////////////////////////////////////////////////////////////////////
public class GameSimulator
{
	// Static logging and debug members
	private static final boolean s_IsDebug = false;
	private static final FileLogger s_Logger = FileLogger.getInstance("CluelessGameSimulator");
	private static final boolean s_IsTreeLogging = true;
	private static final TreeLogger s_TreeLogger = TreeLogger.getInstance();
	
	// Weka related parameters
	private static final boolean s_UseWekaModels = true;
	private static final String s_WekaModelsBaseFolder = "agents/ctagents/alternateOffersAgent/cluelessAgent/";
	private static final Culture s_Culture = Culture.ISRAEL;

	// Board type
	private BoardType m_BoardType;
	
	// Sampling factor
	private static int s_SamplingFactor;

	// Maximum chips in a simulated transfer
	private static final int s_MaxChipsInSimulatedTransfer = 2;
	
	// Maximum meaningful chips in a transfer
	private static final int s_MaxMeaningfulChipsInExchange = 3;
	
	// Dormant steps
	private static final int s_MaxDormantSteps = 2;
	private int m_MyDormantSteps;
	private int m_OpponentDormantSteps;
	
	// Reliability and transfer count
	private double m_MyReliabilitySum;
	private double m_OpponentReliabilitySum;
	private int m_TransfersSoFar;
	
	// Saves the best actions
	private boolean m_BestResponse;
	private Proposal m_BestProposal;
	private ChipSet m_BestTransfer;
	
	// Whether the simulation has begun
	private boolean m_SimulationBegun;
	
	// Specifies whether the game is over
	private boolean m_GameOver;
	
	// Saves the scoring
	private Scoring m_Scoring;

	// Saves the board
	private Board m_Board;

	// The random generator
	private static Random s_RandomGenerator = new Random();

	// Saves whether the first proposer was me
	private boolean m_FirstProposerIsMe;

	// Saves the chips
	private ChipSet m_MyChips;
	private ChipSet m_OpponentChips;

	// Saves the positions
	private RowCol m_MyPosition;
	private RowCol m_OpponentPosition;

	// Saves the needed chips (for caching purposes)
	private ChipSet m_BothNeededChips;
	private ChipSet m_MyNeededChips;
	private ChipSet m_OpponentNeededChips;

	// Saves the goal position (for caching purposes)
	private RowCol m_GoalPosition;
	
	// My score weight against the opponent's weight for scoring
	private static double s_MyScoreWeight = 0.9;
	
	//////////////////////////////////////////////////////////////////////////
	// Enum:		Culture													//
	// Purpose:		Enumerates a countrty.									//
	//////////////////////////////////////////////////////////////////////////
	public enum Culture
	{
		ISRAEL,
		USA,
		LEBANON
	};

	//////////////////////////////////////////////////////////////////////////
	// Enum:		BoardType												//
	// Purpose:		Enumerates a board type from the agent's point of view.	//
	//////////////////////////////////////////////////////////////////////////
	public enum BoardType
	{
		DD,
		TD,
		TI
	};
		
	//////////////////////////////////////////////////////////////////////////
	// Method:		logMsg													//
	// Purpose:		Logs a new message to the log file.						//
	// Parameters:	* message - The message to log.							//
	// Remarks:		* Logs only when debug mode is activated.				//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static void logMsg(String message)
	{
		// Write a new line with the message if we're in debug mode
		if (s_IsDebug)
		{
			s_Logger.writeln(message);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		logStateToTreeLogger									//
	// Purpose:		Logs the state to the tree logger.						//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private void logStateToTreeLogger()
	{
		// Log owned chips
		s_TreeLogger.addMyChips(this.m_MyChips);
		s_TreeLogger.addOpponentChips(this.m_OpponentChips);

		// Log needed chips
		s_TreeLogger.addBothNeededChips(this.m_BothNeededChips);
		s_TreeLogger.addMyNeededChips(this.m_MyNeededChips);
		s_TreeLogger.addOpponentNeededChips(this.m_OpponentNeededChips);
		
		// Log positions
		s_TreeLogger.addMyPosition(this.m_MyPosition);
		s_TreeLogger.addOpponentPosition(this.m_OpponentPosition);

		// Log reliabilities
		s_TreeLogger.addMyReliability(this.m_MyReliabilitySum / this.m_TransfersSoFar);
		s_TreeLogger.addOpponentReliability(this.m_OpponentReliabilitySum / this.m_TransfersSoFar);

		// Log dormant steps
		s_TreeLogger.addMyDormantSteps(this.m_MyDormantSteps);
		s_TreeLogger.addOpponentDormantSteps(this.m_OpponentDormantSteps);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		intersectChipSetsAndSubstract							//
	// Purpose:		Intersects chip sets and substracts the intersection.	//
	// Parameters:	* leftChipSet - the left chip set.						//
	//				* rightChipSet - the right chip set.					//
	// Returns:		The intersection between the chip sets.					//
	// Remarks:		* The given chip sets change (chips are substracted 	//
	//				  from them).											//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static ChipSet intersectChipSetsAndSubstract(ChipSet leftChipSet,
														 ChipSet rightChipSet)
	{
		ChipSet result = new ChipSet();
		
		// Iterate the left chip set
		for (String color : leftChipSet.getColors())
		{
			// Calculate the number of chips of that color in each chip set
			int numberOfChipsInLeft = leftChipSet.getNumChips(color);
			int numberOfChipsInRight = rightChipSet.getNumChips(color);
			
			// The number of chips for that color in the intersection is the minimum
			int numberOfChipsInBoth = Math.min(numberOfChipsInLeft, numberOfChipsInRight);
			
			// Add the minimum to the result
			result.add(color, numberOfChipsInBoth);
			
			// Substract from left and right
			leftChipSet.add(color, -1 * numberOfChipsInBoth);
			rightChipSet.add(color, -1 * numberOfChipsInBoth);
		}
		
		// Return result
		return result;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		analyzeBoardType				  						//
	// Purpose:		Analyzes the board type according to the game state.	//
	// Returns:		The board type.											//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private BoardType analyzeBoardType()
	{
		BoardType result;
		logMsg("analyzeBoardType(): entering.");
		
		// Calculate the needed chips of both sides
		ChipSet myNeededChips = ChipSet.addChipSets(this.m_BothNeededChips, this.m_MyNeededChips);
		ChipSet opponentNeededChips = ChipSet.addChipSets(this.m_BothNeededChips, this.m_OpponentNeededChips);
		
		// Calculate the missing chips of both sides
		ChipSet myMissingChips = this.m_MyChips.getMissingChips(myNeededChips);
		ChipSet opponentMissingChips = this.m_OpponentChips.getMissingChips(opponentNeededChips);
		
		// Figure out the board type
		if (myMissingChips.getNumChips() > 0)
		{
			result = ((opponentMissingChips.getNumChips() > 0) ? BoardType.DD : BoardType.TD);
		}
		else
		{
			result = BoardType.TI;
		}
		
		// Return the result
		logMsg("analyzeBoardType(): board type is (\'" + result.toString() + "\').");
		logMsg("analyzeBoardType(): leaving.");
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		updateWekaModels				  						//
	// Purpose:		Analyzes the board type according to the game state.	//
	// Returns:		The board type.											//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void updateWekaModels()
	{
		// If only if we're using the Weka models
		if (s_UseWekaModels)
		{
			// Get the board type
			BoardType newBoardType = analyzeBoardType();
			
			// The model is from the opponent's point of view
			// So, we must switch sides between DI and TI
			switch (newBoardType)
			{
			case TI:
				newBoardType = BoardType.TD;
				break;
			case TD:
				newBoardType = BoardType.TI;
				break;
			}
			
			// Update the board type
			this.m_BoardType = newBoardType;
				
			// Build the culture folder and update the Weka wrapper
			String cultureFolder = s_WekaModelsBaseFolder + s_Culture.toString() + "/" + s_Culture.toString() + "_" + newBoardType.toString();
			WekaWrapper.setCultureFolder(cultureFolder);
			WekaWrapper.setCulture(s_Culture);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		GameSimulator constructor		  						//
	// Purpose:		Constracts the instance of the game simulator class.	//
	// Parameters:	* scoring - the scoring instance.						//
	//				* board - the board.									//
	//				* firstProposerIsMe - whether the first proposer is me.	//
	//				* myChips - my chips.									//
	//				* opponentChips - opponent's chips.						//
	//				* myPosition - my position.								//
	//				* opponentPosition - opponent's position.				//
	//				* myReliabilitySum - the sum of my reliabilities.		//
	//				* opponentReliabilitySum - the sum of opponent's		//
	//										   reliabilities.				//
	//				* transfersSoFar - the number of transfers so far.		//
	//				* samplingBranching - the branching factor of sampling.	//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public GameSimulator(Scoring scoring,
						 Board board,
						 boolean firstProposerIsMe,
						 ChipSet myChips,
						 ChipSet opponentChips,
						 RowCol myPosition,
						 RowCol opponentPosition,
						 double myReliabilitySum,
						 double opponentReliabilitySum,
						 int myDormantSteps,
						 int opponentDormantSteps,
						 int transfersSoFar,
						 int samplingBranching)
	{
		logMsg("GameSimulator(): entering.");
		logMsg("GameSimulator(): my chips: (" + myChips + ").");
		logMsg("GameSimulator(): opponent\'s chips: (" + opponentChips + ").");
		
		// Initialize the tree logger
		if (s_IsTreeLogging)
		{
			s_TreeLogger.activate();
		}
		
		// Initialize the game over specifier
		this.m_GameOver = false;
		
		// Simulation has not begun
		this.m_SimulationBegun = false;
		
		// Save the sampling branching factor
		s_SamplingFactor = samplingBranching;
		
		// Dormant steps
		this.m_MyDormantSteps = myDormantSteps;
		this.m_OpponentDormantSteps = opponentDormantSteps;
		
		// Initialize the best results
		this.m_BestProposal = null;
		this.m_BestResponse = false;
		this.m_BestTransfer = null;
		
		// Reliability and transfers
		this.m_MyReliabilitySum = myReliabilitySum;
		this.m_OpponentReliabilitySum = opponentReliabilitySum;
		this.m_TransfersSoFar = transfersSoFar;
		
		// Copy the scoring instance
		this.m_Scoring = scoring;
		
		// Copy the board and save the goal position
		this.m_Board = board;
		this.m_GoalPosition = board.getGoalLocations().get(0);
		
		// Save the whether I'm the first proposer
		this.m_FirstProposerIsMe = firstProposerIsMe;
		
		// Save my chips
		this.m_MyChips = new ChipSet(myChips);
		this.m_OpponentChips = new ChipSet(opponentChips);
		
		// Save positions
		this.m_MyPosition = new RowCol(myPosition);
		this.m_OpponentPosition = new RowCol(opponentPosition);

		// Calculate the needed chips according to the shortest path to the goal		
		ChipSet myNeededChips = ShortestPaths.getShortestPaths(myPosition, board.getGoalLocations().get(0), board, scoring, 10, myChips, opponentChips).get(0).getRequiredChips(board);
		ChipSet opponentNeededChips = ShortestPaths.getShortestPaths(opponentPosition, board.getGoalLocations().get(0), board, scoring, 10, myChips, opponentChips).get(0).getRequiredChips(board);
		
		// Calculate the intersection of the needed chips and substract from the other chip sets
		this.m_BothNeededChips = intersectChipSetsAndSubstract(myNeededChips, opponentNeededChips);
		this.m_MyNeededChips = myNeededChips;
		this.m_OpponentNeededChips = opponentNeededChips;
		logMsg("GameSimulator(): both needed chips: (" + this.m_BothNeededChips + ").");
		logMsg("GameSimulator(): my needed chips: (" + this.m_MyNeededChips + ").");
		logMsg("GameSimulator(): opponent needed chips: (" + this.m_OpponentNeededChips + ").");
		
		// Initialize the board type and the Weka models
		this.m_BoardType = null;
		updateWekaModels();
		
		// Finish
		logMsg("GameSimulator(): leaving.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		GameSimulator copy constructor		  					//
	// Purpose:		Constracts the instance of the game simulator class.	//
	// Parameters:	* otherGameSimulator - another instance.				//
	// Remarks:		* Just copies inner fields.								//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////		
	public GameSimulator(GameSimulator otherGameSimulator)
	{		
		// Just copy inner fields
		this.m_SimulationBegun = otherGameSimulator.m_SimulationBegun;
		this.m_Scoring = otherGameSimulator.m_Scoring;
		this.m_Board = otherGameSimulator.m_Board;
		this.m_FirstProposerIsMe = otherGameSimulator.m_FirstProposerIsMe;
		this.m_GameOver = otherGameSimulator.m_GameOver;
		this.m_GoalPosition = otherGameSimulator.m_GoalPosition;
		this.m_MyReliabilitySum = otherGameSimulator.m_MyReliabilitySum;
		this.m_OpponentReliabilitySum = otherGameSimulator.m_OpponentReliabilitySum;
		this.m_TransfersSoFar = otherGameSimulator.m_TransfersSoFar;
		this.m_MyDormantSteps = otherGameSimulator.m_MyDormantSteps;
		this.m_OpponentDormantSteps = otherGameSimulator.m_OpponentDormantSteps;
		
		// No need to copy other fields
		this.m_BestProposal = null;
		this.m_BestResponse = false;
		this.m_BestTransfer = null;
		
		// Copy chips
		this.m_MyChips = new ChipSet(otherGameSimulator.m_MyChips);
		this.m_OpponentChips = new ChipSet(otherGameSimulator.m_OpponentChips);
		this.m_BothNeededChips = new ChipSet(otherGameSimulator.m_BothNeededChips);
		this.m_OpponentNeededChips = new ChipSet(otherGameSimulator.m_OpponentNeededChips);
		this.m_MyNeededChips = new ChipSet(otherGameSimulator.m_MyNeededChips);
		
		// Copy positions
		this.m_MyPosition = new RowCol(otherGameSimulator.m_MyPosition);
		this.m_OpponentPosition = new RowCol(otherGameSimulator.m_OpponentPosition);
	}	
		
	//////////////////////////////////////////////////////////////////////////
	// Class:		DirectNodeData	 					    				//
	// Purpose:		Implements a container for a direct node in the MCMC	//
	//				tree, when the root is the proposition.					//
	//				This class saves four parameters:						//
	//				1. The sum of scores.									//
	//				2. The number of occurences for this node.				//
	//				3. The best score seen.									//
	//				4. The first transfer of the best score simulation.		//
	//////////////////////////////////////////////////////////////////////////	
	private class DirectNodeData
	{
		// This keeps the best score for the best transfer
		private double m_BestScoreSeen;
		
		// The kept exposed data
		public double m_SumOfScores;
		public int m_NumberOfOccurences;
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		DirectNodeData constructor		    					//
		// Purpose:		Constracts the instance of the equivalence class.		//
		// Parameters:	* firstScore - the first simulation score.				//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////
		public DirectNodeData(double firstScore)
		{
			// Just initialize the fields properly
			this.m_SumOfScores = firstScore;
			this.m_NumberOfOccurences = 1;
			this.m_BestScoreSeen = firstScore;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		addSimulation					    					//
		// Purpose:		Constracts the instance of the equivalence class.		//
		// Parameters:	* newScore - the simulation's score.					//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////
		public void addSimulation(double newScore)
		{
			// Sum the scores and increase the number of occurences
			this.m_SumOfScores += newScore;
			this.m_NumberOfOccurences++;
			
			// Maximize the transfer based on the current score
			this.m_BestScoreSeen = Math.max(this.m_BestScoreSeen, newScore);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Class:		EquivalenceClass 					    				//
	// Purpose:		Implements the equivalence class of chip sets.			//
	//				This class divides a chip set into four components:		//
	//				1. The number of chips needed by both opponents.		//
	//				2. The number of chips needed only by me.				//
	//				3. The number of chips needed only by the opponent.		//
	//				4. The number of un-wanted chips.						//
	//////////////////////////////////////////////////////////////////////////	
	private class EquivalenceClass
	{
		// Holds the original chip set
		public ChipSet m_OriginalChipSet;
		
		// Keeps the number of chips needed by both
		public int m_ChipsNeededByBoth;
		
		// Keeps the number of chips needed only by me
		public int m_ChipsNeededByMe;
		
		// Keeps the number of chips needed only by the opponent
		public int m_ChipsNeededByOpponent;
		
		// Keeps the number of un-wanted chips
		public int m_ChipsNotNeeded;
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		EquivalenceClass constructor		    				//
		// Purpose:		Constracts the instance of the equivalence class.		//
		// Parameters:	* originalChipSet - the original chip set.				//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////	
		public EquivalenceClass(ChipSet originalChipSet)
		{
			// Save the original chip set
			this.m_OriginalChipSet = new ChipSet(originalChipSet);
			
			// Initialize fields
			m_ChipsNeededByBoth = 0;
			m_ChipsNeededByMe = 0;
			m_ChipsNeededByOpponent = 0;
			m_ChipsNotNeeded = 0;
			
			// Iterate the entire chip set
			for (String color : originalChipSet.getColors())
			{
				// Get the number of chips of that color
				int chipsOfColor = originalChipSet.getNumChips(color);
				
				// Add the number of needed chips by both
				int neededChipsByBoth = Math.min(chipsOfColor, m_BothNeededChips.getNumChips(color));
				this.m_ChipsNeededByBoth += neededChipsByBoth;
				chipsOfColor -= neededChipsByBoth;
				if (chipsOfColor <= 0)
				{
					continue;
				}
				
				// Add the number of needed chips by me
				int neededChipsByMe = Math.min(chipsOfColor, m_MyNeededChips.getNumChips(color));
				this.m_ChipsNeededByMe += neededChipsByMe;
				chipsOfColor -= neededChipsByMe;
				if (chipsOfColor <= 0)
				{
					continue;
				}
				
				// Add the number of needed chips by the opponent
				int neededChipsByOpponent = Math.min(chipsOfColor, m_OpponentNeededChips.getNumChips(color));
				this.m_ChipsNeededByOpponent += neededChipsByOpponent;
				chipsOfColor -= neededChipsByOpponent;
				if (chipsOfColor <= 0)
				{
					continue;
				}
				
				// The rest of the chips are not needed
				this.m_ChipsNotNeeded += chipsOfColor;
			}
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		EquivalenceClass copy constructor		  				//
		// Purpose:		Constracts the instance of the equivalence class.		//
		// Parameters:	* otherEquivalenceClass - another instance.				//
		// Remarks:		* Just copies inner fields.								//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////	
		public EquivalenceClass(EquivalenceClass otherEquivalenceClass)
		{
			// Just copy inner members
			this.m_OriginalChipSet = new ChipSet(otherEquivalenceClass.m_OriginalChipSet);
			this.m_ChipsNeededByBoth = otherEquivalenceClass.m_ChipsNeededByBoth;
			this.m_ChipsNeededByMe = otherEquivalenceClass.m_ChipsNeededByMe;
			this.m_ChipsNeededByOpponent = otherEquivalenceClass.m_ChipsNeededByOpponent;
			this.m_ChipsNotNeeded = otherEquivalenceClass.m_ChipsNotNeeded;
		}

		//////////////////////////////////////////////////////////////////////////
		// Method:		equals							  						//
		// Purpose:		Overrides equals method in order to use in a set.		//
		// Parameters:	* comparedObject - another instance.					//
		// Returns:		boolean													//
		// Remarks:		* Compares just inner integers.							//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////	
		@Override
		public boolean equals(Object comparedObject)
		{
			// Self compare
			if (this == comparedObject)
			{
				return true;
			}

			// Null compare
			if (null == comparedObject)
			{
				return false;
			}

			// Other class compare
			if (getClass() != comparedObject.getClass())
			{
				return false;
			}

			// The class is our class
			EquivalenceClass comparedChips = (EquivalenceClass)comparedObject;

			// Compare inside integers without looking at the original chip set
			return ((this.m_ChipsNeededByBoth == comparedChips.m_ChipsNeededByBoth) &&
					(this.m_ChipsNeededByMe == comparedChips.m_ChipsNeededByMe) &&
					(this.m_ChipsNeededByOpponent == comparedChips.m_ChipsNeededByOpponent) &&
					(this.m_ChipsNotNeeded == comparedChips.m_ChipsNotNeeded));
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Class:		EquivalenceClassPair 					    			//
	// Purpose:		The equivalence class of a pair of chip sets.			//
	//////////////////////////////////////////////////////////////////////////
	private class EquivalenceClassPair
	{
		// Just save the pair
		private EquivalenceClass m_SendEq;
		private EquivalenceClass m_ReceiveEq;

		//////////////////////////////////////////////////////////////////////////
		// Method:		EquivalenceClassPair constructor		  				//
		// Purpose:		Constracts the instance of the equivalence class pair.	//
		// Parameters:	* sendEq - the send equivalence class.					//
		//				* receiveEq - the receive equivalence class.			//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////	
		public EquivalenceClassPair(EquivalenceClass sendEq,
									EquivalenceClass receiveEq)
		{
			// Just copy inner members
			this.m_ReceiveEq = new EquivalenceClass(receiveEq);
			this.m_SendEq = new EquivalenceClass(sendEq);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		EquivalenceClassPair copy constructor		  			//
		// Purpose:		Constracts the instance of the equivalence class pair.	//
		// Parameters:	* otherEquivalenceClassPair - another instance.			//
		// Remarks:		* Just copies inner fields.								//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////	
		public EquivalenceClassPair(EquivalenceClassPair otherEquivalenceClass)
		{
			// Just copy inner members
			this.m_ReceiveEq = new EquivalenceClass(otherEquivalenceClass.m_ReceiveEq);
			this.m_SendEq = new EquivalenceClass(otherEquivalenceClass.m_SendEq);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		equals							  						//
		// Purpose:		Overrides equals method in order to use in a set.		//
		// Parameters:	* comparedObject - another instance.					//
		// Returns:		boolean													//
		// Remarks:		* Compares just inner integers.							//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////	
		@Override
		public boolean equals(Object comparedObject)
		{
			// Self compare
			if (this == comparedObject)
			{
				return true;
			}

			// Null compare
			if (null == comparedObject)
			{
				return false;
			}

			// Other class compare
			if (getClass() != comparedObject.getClass())
			{
				return false;
			}

			// The class is our class
			EquivalenceClassPair comparedEq = (EquivalenceClassPair)comparedObject;
			
			// Compare inner members
			if ((comparedEq.m_SendEq.equals(this.m_SendEq)) &&
				(comparedEq.m_ReceiveEq.equals(this.m_ReceiveEq)))
			{
				return true;
			}
			return false;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		isNarrowed						  						//
		// Purpose:		Specifies whether the classes are narrowed, meaning 	//
		//				that there is no smaller equivalent offer.				//
		// Returns:		boolean													//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////	
		public boolean isNarrowed()
		{
			int sum = 0;

			// Check if the multipication sum is zero
			// If it is - it means that all of the features have at least one zero
			// So the sum if zero if and only if the equivalences are narrowed
			sum += (this.m_SendEq.m_ChipsNeededByBoth * this.m_ReceiveEq.m_ChipsNeededByBoth);
			sum += (this.m_SendEq.m_ChipsNeededByMe * this.m_ReceiveEq.m_ChipsNeededByMe);
			sum += (this.m_SendEq.m_ChipsNeededByOpponent * this.m_ReceiveEq.m_ChipsNeededByOpponent);
			sum += (this.m_SendEq.m_ChipsNotNeeded * this.m_ReceiveEq.m_ChipsNotNeeded);

			// Return the result
			return (sum == 0);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// Method:		toProposal						  						//
		// Purpose:		Returns the class as a proposal.						//
		// Returns:		Proposal												//
		//				* Note that the proposal's chipsToReceive member will 	//
		//				  ALWAYS be the opponent's chips and chipsToSend will	//
		//				  ALWAYS be mine.										//
		// Checked:		12/07/2012												//
		//////////////////////////////////////////////////////////////////////////
		public Proposal toProposal()
		{
			// Just return the inner field
			return new Proposal(this.m_ReceiveEq.m_OriginalChipSet, this.m_SendEq.m_OriginalChipSet);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getProposalsForSampling			  						//
	// Purpose:		Returns set of proposals for sampling.					//
	// Returns:		A set of proposals.										//
	//				* The proposals are divided to equivalence classes and  //
	//				  are fully narrowed-down.								//
	//				* We enforce a rule here: proposals will not include	//
	//				  chips that both need, and will include only needed	//
	//				  chips for both sides.									//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private Set<Proposal> getProposalsForSampling()
	{
		// Define the result and the equivalent class pairs
		Set<Proposal> result = new LinkedHashSet();
		Set<EquivalenceClassPair> equivalentProposals = new LinkedHashSet();
		
		// Get the sendable chips
		ChipSet sendableByMe = getSendableChips(true);
		ChipSet sendableByOpponent = getSendableChips(false);
		
		// Get all the relevant chip sets
		Set<ChipSet> allSends = ChipSet.getUniquePowerSet(sendableByMe);
		Set<ChipSet> allReceives = ChipSet.getUniquePowerSet(sendableByOpponent);
		
		// Build all relevant proposals
		for (ChipSet sendChips : allSends)
		{
			for (ChipSet receiveChips : allReceives)
			{	
				// Reject the empty set
				if ((0 == sendChips.getNumChips()) && (0 == receiveChips.getNumChips()))
				{
					continue;
				}
				
				// Get the equivalence class for sending and receiving
				EquivalenceClass sendEq = new EquivalenceClass(sendChips);
				EquivalenceClass receiveEq = new EquivalenceClass(receiveChips);
				EquivalenceClassPair eqPair = new EquivalenceClassPair(sendEq, receiveEq);
				
				// Reject the proposal if the classes are not narrowed
				if (!eqPair.isNarrowed())
				{
					continue;
				}
				
				// Reject the proposal if it has too many chips
				if ((s_MaxChipsInSimulatedTransfer < sendChips.getNumChips()) || (s_MaxChipsInSimulatedTransfer < receiveChips.getNumChips()))
				{
					continue;
				}
				
				// Now we can add the proposal
				equivalentProposals.add(eqPair);
			}
		}
		
		// The result is defined by the original proposals
		for (EquivalenceClassPair eqPair : equivalentProposals)
		{
			result.add(eqPair.toProposal());
		}
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRandomSubset 					    				//
	// Purpose:		Gets a random subset of the given chip set.        		//
	// Returns:		A random chip set.										//
	// Parameters:	* chipSet - the given chip set to get the subset from.  //
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static ChipSet getRandomSubset(ChipSet chipSet)
	{
		ChipSet result = new ChipSet();
		
		// Iterate upon all the colors
		for (String color : chipSet.getColors())
		{
			// For each color pick a number of chips uniformly
			result.add(color, s_RandomGenerator.nextInt(chipSet.getNumChips(color) + 1));
		}
		
		// Return result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getSendableChips				 					    //
	// Purpose:		Gets the chips that one can send without sending any of	//
	//				the needed chips.										//
	// Returns:		The sendable chip set.									//
	// Parameters:	* isMe - specifies whether the player is me.			//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getSendableChips(boolean isMe)
	{
		// Generate the relevant chip sets
		ChipSet baseChipSet = (isMe ? this.m_MyChips : this.m_OpponentChips);
		ChipSet exclusiveNeededChips = (isMe ? this.m_MyNeededChips : this.m_OpponentNeededChips);
		ChipSet neededChips = ChipSet.addChipSets(this.m_BothNeededChips, exclusiveNeededChips);
		
		// The chips that I can send are the ones that have positive values after substraction
		ChipSet sendableChips = new ChipSet();
		for (String color : baseChipSet.getColors())
		{
			int sendableItems = baseChipSet.getNumChips(color) - neededChips.getNumChips(color);
			if (sendableItems > 0)
			{
				sendableChips.add(color, sendableItems);
			}
		}
		
		// Return the result
		return sendableChips;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRandomChipsSubsetNotInNeeded 					    //
	// Purpose:		Gets a random subset from the given player's chips,		//
	//				without sending the needed chips to reach the goal.		//
	// Returns:		A random chip set.										//
	// Parameters:	* isMe - specifies whether the player is me.			//
	// Checked:		12/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getRandomChipsSubsetNotInNeeded(boolean isMe)
	{
		// Return a random subset of the sendable chips
		return getRandomSubset(getSendableChips(isMe));
	}	
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		samplePropositions 					    				//
	// Purpose:		Performs sampling upon the propositions.       			//
	// Parameters:	* isFirst - specifies whether this is the first or      //
	//                          second proposition.                         //
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private void samplePropositions(boolean isFirst)
	{
		logMsg("samplePropositions(): entering.");
		
		// Tree log
		s_TreeLogger.beginTag("PropositionsRootNode");
		logStateToTreeLogger();
		
		// The nodes directly generated from the root node in the MCMC tree.
		// The keys are the propositions, and the values are instances of DirectNodeData class.
		Map<Proposal, DirectNodeData> directNodesData = new HashMap();
		
		// This will keep the best proposal
		Proposal bestProposal = new Proposal(new ChipSet(), new ChipSet());
		double bestScore = Double.MIN_VALUE;
		boolean bestScoreCalculated = false;
		
		// Get all the proposals
		Set<Proposal> allProposals = getProposalsForSampling();
		logMsg("samplePropositions(): Got a map of size (" + allProposals.size() + ").");
		
		// Sample
		Object allProposalsArray[] = allProposals.toArray();
		logMsg("samplePropositions(): Starting to sample.");
		for (int counter = 0; counter < s_SamplingFactor; counter++)
		{
			// Get a random proposal
			int randomIndex = s_RandomGenerator.nextInt(allProposals.size());
			Proposal currentProposal = (Proposal)(allProposalsArray[randomIndex]);
						
			// Simulate the proposal
			GameSimulator currentProposalSimulation = new GameSimulator(this);

			// Propose
			s_TreeLogger.beginTag("ProposalSimulation");
			currentProposalSimulation.response(isFirst, currentProposal);
			s_TreeLogger.addTag("SimulationScore", currentProposalSimulation.getScore());
			s_TreeLogger.endTag("ProposalSimulation");
			
			// Gather information of direct nodes
			if (directNodesData.containsKey(currentProposal))
			{
				DirectNodeData gatheredData = (DirectNodeData)(directNodesData.get(currentProposal));
				gatheredData.addSimulation(currentProposalSimulation.getScore());
				directNodesData.put(currentProposal, gatheredData);
			}
			else
			{
				DirectNodeData gatheredData = new DirectNodeData(currentProposalSimulation.getScore());
				directNodesData.put(currentProposal, gatheredData);
			}
		}
		logMsg("samplePropositions(): Finished sampling.");
		
		// After the sampling is done, find the best proposal
		for (Map.Entry<Proposal, DirectNodeData> entry : directNodesData.entrySet())
		{
			// Get the gathered data
			DirectNodeData gatheredData = entry.getValue();

			// Tree logging
			s_TreeLogger.beginTag("PropositionSample");
			s_TreeLogger.addTag("Proposal", "(" + entry.getKey().chipsToSend + ", " + entry.getKey().chipsToReceive + ")");
			s_TreeLogger.addTag("Occurences", gatheredData.m_NumberOfOccurences);
			s_TreeLogger.addTag("AverageScore", (gatheredData.m_NumberOfOccurences > 0 ? gatheredData.m_SumOfScores / gatheredData.m_NumberOfOccurences : 0.0));
			s_TreeLogger.endTag("PropositionSample");
			
			// Maximize average
			logMsg("samplePropositions(): Occurences: " + gatheredData.m_NumberOfOccurences);
			double currentAverage = gatheredData.m_SumOfScores / gatheredData.m_NumberOfOccurences;
			if ((!bestScoreCalculated) || (currentAverage > bestScore))
			{
				bestScore = currentAverage;
				bestProposal = new Proposal(entry.getKey());
				bestScoreCalculated = true;
			}
		}

		// Mark the best proposal and the best transfer
		s_TreeLogger.beginTag("BestProposal");
		s_TreeLogger.addTag("MySendChips", bestProposal.chipsToSend);
		s_TreeLogger.addTag("OpponentSendChips", bestProposal.chipsToReceive);
		s_TreeLogger.addTag("AverageScore", new Double(bestScore));
		s_TreeLogger.endTag("BestProposal");
		logMsg("samplePropositions(): ** saving the best proposal (" + bestProposal.chipsToReceive + ", " + bestProposal.chipsToSend + ").");
		this.m_BestProposal = bestProposal;
		
		// Tree log
		s_TreeLogger.endTag("PropositionsRootNode");
		
		// Finish
		logMsg("samplePropositions(): leaving.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		proposition       					    				//
	// Purpose:		Simulates a proposition.	                     		//
	// Parameters:	* isFirst - specifies whether this is the first or      //
	//                          second proposition.                         //
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private void proposition(boolean isFirst)
	{
		boolean proposerIsMe;
		boolean isFirstSimulation = !this.m_SimulationBegun;
		Proposal proposal = null;
		
		logMsg("proposition(): entering.");
		
		// Finish if the game is over
		if (this.m_GameOver)
		{
			logMsg("proposition(): game is already over.");
			return;
		}
		this.m_SimulationBegun = true;

		// Tree log
		s_TreeLogger.beginTag("Proposition");
		logStateToTreeLogger();
		
		// The proposer is me if and only if one of the following conditions takes place:
		// 1. This is the first proposition and I'm the first proposer
		// 2. This is the second proposition and I'm not the first proposer
		// This is happens iff two of them are false or true.
		proposerIsMe = (isFirst == this.m_FirstProposerIsMe);
		s_TreeLogger.addTag("MeProposing", new Boolean(proposerIsMe));

		// If I'm the proposer
		if (proposerIsMe)
		{
			logMsg("proposition(): I\'m the proposer.");
			
			// Check if we need to perform sampling
			if (isFirstSimulation)
			{
				// We need to perform sampling
				samplePropositions(isFirst);
				s_TreeLogger.endTag("Proposition");
				logMsg("proposition(): leaving.");
				return;
			}
			else
			{
				// The sampling is uniformly random
				// Generate two random chip sets - one for the opponent and one for me
				ChipSet opponentProposedChips = getRandomChipsSubsetNotInNeeded(false);
				ChipSet myProposedChips = getRandomChipsSubsetNotInNeeded(true);

				// Generate the proposal
				proposal = new Proposal(opponentProposedChips, myProposedChips);
			}
		}
		else
		{
			logMsg("proposition(): opponent is the proposer.");
			
			// The proposer is the opponent, propose randomly
			// Generate two random chip sets - one for the opponent and one for me
			ChipSet opponentProposedChips = getRandomChipsSubsetNotInNeeded(false);
			ChipSet myProposedChips = getRandomChipsSubsetNotInNeeded(true);

			// Generate the proposal
			proposal = new Proposal(opponentProposedChips, myProposedChips);
		}
		
		// Respond
		s_TreeLogger.addTag("OpponentProposedChips", proposal.chipsToReceive);
		s_TreeLogger.addTag("MyProposedChips", proposal.chipsToSend);
		logMsg("proposition(): responding.");
		response(isFirst, proposal);
		
		// Quit
		s_TreeLogger.endTag("Proposition");
		logMsg("proposition(): leaving.");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		firstProposition					    				//
	// Purpose:		Simulates a proposition of the first proposer.			//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void firstProposition()
	{
		logMsg("firstProposition(): entering.");
		
		// Just call the first proposition
		proposition(true);
		logMsg("firstProposition(): leaving.");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		secondProposition					    				//
	// Purpose:		Simulates a proposition of the second proposer.			//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void secondProposition()
	{
		logMsg("secondProposition(): entering.");
		
		// Just call the second proposition
		proposition(false);
		logMsg("secondProposition(): leaving.");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		sampleResponse    				    					//
	// Purpose:		Samples the responses.									//
	// Parameters:	* isFirst - specifies whether this is the first or      //
	//                          second response.                            //
	//				* proposal - the proposal sent by the last proposer.    //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private void sampleResponse(boolean isFirst,
								Proposal proposal)
	{
		boolean acceptProposal = false;
		GameSimulator bestSimulation = null;
		double bestSimulationScore = Double.MIN_VALUE;
		boolean bestCalculated = false;

		// Tree log
		s_TreeLogger.beginTag("ResponsesRootNode");
		logStateToTreeLogger();
		
		// This will keep the scores
		double acceptScore = 0.0;
		double rejectScore = 0.0;
		int acceptOccurences = 0;
		
		logMsg("sampleResponse(): entering.");
		
		// Sample
		logMsg("sampleResponse(): starting to sample.");
		for (int counter = 0; counter < s_SamplingFactor; counter++)
		{
			// Sample a response
			boolean currentResponse = s_RandomGenerator.nextBoolean();
			GameSimulator currentSimulation = new GameSimulator(this);
			s_TreeLogger.beginTag("Response");
			s_TreeLogger.addTag("CurrentResponse", currentResponse);
			
			// Implement the state machine
			if (currentResponse)
			{
				double currentScore;
				
				// If we accept - we move to the transfer phase
				currentSimulation.transfer(proposal);
				currentScore = currentSimulation.getScore();
				s_TreeLogger.addTag("Score", currentScore);
				acceptScore += currentScore;
				
				// Maximize transfer
				if ((!bestCalculated) || (bestSimulationScore < currentScore))
				{
					bestSimulation = currentSimulation;
					bestSimulationScore = currentScore;
					bestCalculated = true;
				}
				
				// One more accept response occured
				acceptOccurences++;
			}
			else
			{
				// If we reject
				if (isFirst)
				{
					// If this was the first response, then the second proposition is now on
					currentSimulation.secondProposition();
				}
				else
				{
					// This was the second response, so transfer
					currentSimulation.transfer(null);
				}
				rejectScore += currentSimulation.getScore();
				s_TreeLogger.addTag("Score", currentSimulation.getScore());
			}
			
			// Tree log
			s_TreeLogger.endTag("Response");
		}
		logMsg("sampleResponse(): Finished sampling.");

		// Take the best action
		s_TreeLogger.addTag("NumberOfAccepts", new Integer(acceptOccurences));
		s_TreeLogger.addTag("NumberOfRejects", new Integer(s_SamplingFactor - acceptOccurences));
		if ((acceptOccurences != 0) && (acceptOccurences != s_SamplingFactor))
		{
			double acceptAverage = acceptScore / acceptOccurences;
			double rejectAverage = rejectScore / (s_SamplingFactor - acceptOccurences);
			logMsg("sampleResponse(): accept average score is (" + acceptAverage + ").");
			logMsg("sampleResponse(): reject average score is (" + rejectAverage + ").");
			s_TreeLogger.addTag("AcceptAverage", new Double(acceptAverage));
			s_TreeLogger.addTag("RejectAverage", new Double(rejectAverage));
			acceptProposal = acceptAverage >= rejectAverage;
		}
		else
		{
			logMsg("sampleResponse(): got only one type of simulations.");
			acceptProposal = (acceptOccurences != 0);
		}
		s_TreeLogger.addTag("ShouldAccept", new Boolean(acceptProposal));
		
		// Mark the best response
		logMsg("sampleResponse(): ** this is the first simulation, saving the result (" + acceptProposal + ").");
		this.m_BestResponse = acceptProposal;
		
		// Tree log
		s_TreeLogger.endTag("ResponsesRootNode");
		
		// Quit
		logMsg("sampleResponse(): leaving.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponentResponseChanceAccordingToGenerosity			//
	// Purpose:		Get the opponent's chance to accept the given proposal.	//
	//				This model is using the proposal's generosity.			//
	// Returns:		A double between 0.0 and 1.0.							//
	// Parameters:	* proposal - the given proposal.						//
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private double getOpponentResponseChanceAccordingToGenerosity(Proposal proposal)
	{
		logMsg("getOpponentResponseChanceAccordingToGenerosity(): current proposal is (" + proposal.chipsToReceive + ", " + proposal.chipsToSend + ").");
		
		// Get the opponent's current score
		double currentOpponentScore = Reliability.getPlayerScore(this.m_OpponentPosition,
																 this.m_OpponentChips,
																 this.m_GoalPosition,
																 this.m_Scoring);
		
		// Get the opponent's score after a full proposal transfer
		ChipSet opponentChipsAfterTransfer = new ChipSet(this.m_OpponentChips);
		opponentChipsAfterTransfer.addChipSet(proposal.chipsToSend);
		opponentChipsAfterTransfer.subChipSet(proposal.chipsToReceive);
		double proposalFullTransferOpponentScore = Reliability.getPlayerScore(this.m_OpponentPosition,
																			  opponentChipsAfterTransfer,
																			  this.m_GoalPosition,
																			  this.m_Scoring);
		
		// The worst opponent's score is if he has nothing
		double worstOpponentScore = Reliability.getPlayerScore(this.m_OpponentPosition,
															   new ChipSet(),
															   this.m_GoalPosition,
															   this.m_Scoring);
		
		// The best opponent's score is if he has everything
		double bestOpponentScore = Reliability.getPlayerScore(this.m_OpponentPosition,
				   											  ChipSet.addChipSets(this.m_OpponentChips, this.m_MyChips),
				   											  this.m_GoalPosition,
				   											  this.m_Scoring);
		
		// Get the minimum and maximum generosities
		double minGenerosity = worstOpponentScore - currentOpponentScore;
		double maxGenerosity = bestOpponentScore - currentOpponentScore;
		logMsg("getOpponentResponseChanceAccordingToGenerosity(): minimum generosity is (" + minGenerosity + ").");
		logMsg("getOpponentResponseChanceAccordingToGenerosity(): maximum generosity is (" + maxGenerosity + ").");	
	
		// Get the current generosity
		double currentGenerosity = proposalFullTransferOpponentScore - currentOpponentScore;
		logMsg("getOpponentResponseChanceAccordingToGenerosity(): current generosity is (" + currentGenerosity + ").");
		
		// The chance of accepting the proposal is the sigmoid function
		// normalized to the minimum and maximum generosities
		double sigmoidDistance = (Math.max(Math.abs(maxGenerosity), Math.abs(minGenerosity))) / 5.0;
		double acceptChance = (1.0 / (1.0 + Math.exp(-currentGenerosity / sigmoidDistance)));
		logMsg("getOpponentResponseChanceAccordingToGenerosity(): accept chance is (" + acceptChance + ").");
		
		// Return result
		return acceptChance;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getGenerosityLevelForWekaModel							//
	// Purpose:		Get the generosity level for a given proposal.			//
	//				This method is used by the Weka culture models.			//
	// Returns:		A double between 0.0 and 1.0.							//
	// Parameters:	* proposal - the given proposal.						//
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static double getGenerosityLevelForWekaModel(Proposal proposal)
	{
		// Calculate how many chips I send against how many chips I receive
		int diffrenceOfSend = proposal.chipsToSend.getNumChips() - proposal.chipsToReceive.getNumChips();
		if (diffrenceOfSend == 0)
		{
			return 0.5;
		}
		if (diffrenceOfSend > 0)
		{
			return 1.0;
		}
		return 0.0;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getNumberOfNeededColors									//
	// Purpose:		Get the number of needed colors in the given proposal.	//
	// Returns:		An integer.												//
	// Parameters:	* proposal - the given proposal.						//
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	//				* isMe - specifies whether it's me or the opponent.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private int getNumberOfNeededColors(Proposal proposal,
										boolean isMe)
	{
		int neededColors = 0;
		
		// Calculate the receving chips
		ChipSet receivingChips = (isMe ? proposal.chipsToReceive : proposal.chipsToSend);
		
		// Calculate the needed chips
		ChipSet neededChips = ChipSet.addChipSets(this.m_BothNeededChips,
													(isMe ? this.m_MyNeededChips : this.m_OpponentNeededChips));
				
		// Iterate the colors
		for (String color : receivingChips.getColors())
		{
			int neededFromCurrentColor = neededChips.getNumChips(color);
			int chipsOfColorInProposal = receivingChips.getNumChips(color);
			
			// Add the minimum to the result
			neededColors += Math.min(neededFromCurrentColor, chipsOfColorInProposal);
		}
		
		// Return result
		return neededColors;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponentResponseChanceFromWekaModel					//
	// Purpose:		Get the opponent's chance to accept the given proposal.	//
	//				This method is using the Weka culture models.			//
	// Returns:		A double between 0.0 and 1.0.							//
	// Parameters:	* proposal - the given proposal.						//
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private double getOpponentResponseChanceFromWekaModel(Proposal proposal)
	{
		// Get the current scores
		double myCurrentScore = getPlayerScore(true);
		double opponentCurrentScore = getPlayerScore(false);
		
		// Get the resulting chips
		ChipSet myChipsAfterTransfer = ChipSet.subChipSets(ChipSet.addChipSets(this.m_MyChips, proposal.chipsToReceive), proposal.chipsToSend);
		ChipSet opponentChipsAfterTransfer = ChipSet.subChipSets(ChipSet.addChipSets(this.m_OpponentChips, proposal.chipsToSend), proposal.chipsToReceive);
		
		// Get the resulting scores
		double myResultingScore = Reliability.getPlayerScore(this.m_MyPosition, myChipsAfterTransfer, this.m_GoalPosition, this.m_Scoring);
		double opponentResultingScore = Reliability.getPlayerScore(this.m_OpponentPosition, opponentChipsAfterTransfer, this.m_GoalPosition, this.m_Scoring);

		// Calculate the generosity level
		double generosityLevel = getGenerosityLevelForWekaModel(proposal);
		
		// Calculate the number of needed colors
		int myNeededColors = getNumberOfNeededColors(proposal, true);
		int opponentNeededColors = getNumberOfNeededColors(proposal, false);
		
		// Calculate the number of other colors
		int myOtherColors = proposal.chipsToReceive.getNumChips() - myNeededColors;
		int opponentOtherColors = proposal.chipsToSend.getNumChips() - opponentNeededColors;

		// Build the instance values
		s_TreeLogger.beginTag("WekaWrapperForAcceptModel");
		s_TreeLogger.addTag("MyCurrentScore", myCurrentScore);
		s_TreeLogger.addTag("MyResultingScore", myResultingScore);
		s_TreeLogger.addTag("OpponentCurrentScore", opponentCurrentScore);
		s_TreeLogger.addTag("OpponentResultingScore", opponentResultingScore);
		s_TreeLogger.addTag("MyNeededColors", myNeededColors);
		s_TreeLogger.addTag("MyOtherColors", myOtherColors);
		s_TreeLogger.addTag("OpponentNeededColors", opponentNeededColors);
		s_TreeLogger.addTag("OpponentOtherColors", opponentOtherColors);
		
		Object[] instanceValues = { myCurrentScore,
									myResultingScore,
									opponentCurrentScore,
									opponentResultingScore,
									generosityLevel,
									(double)myNeededColors,
									(double)myOtherColors,
									(double)opponentNeededColors,
									(double)opponentOtherColors,
									"TRUE" };
		
		// Get the accept probability from the Weka model
		double acceptChance;
		try
		{
			acceptChance = WekaWrapper.getAcceptProbability(instanceValues);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			acceptChance = 0.5;
		}
		s_TreeLogger.addTag("AcceptChance", acceptChance);
		s_TreeLogger.endTag("WekaWrapperForAcceptModel");
		
		// Return the result
		return acceptChance;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		response    					    					//
	// Purpose:		Simulates a response.									//
	// Parameters:	* isFirst - specifies whether this is the first or      //
	//                          second response.                            //
	//				* proposal - the proposal sent by the last proposer.    //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private void response(boolean isFirst,
						  Proposal proposal)
	{
		boolean responderIsMe;
		boolean acceptProposal;
		boolean isFirstSimulation = !this.m_SimulationBegun;
		
		logMsg("response(): entering.");
		
		// Finish if the game is over
		if (this.m_GameOver)
		{
			logMsg("response(): game is already over.");
			return;
		}
		this.m_SimulationBegun = true;
		
		// Tree log
		s_TreeLogger.beginTag("Response");
		logStateToTreeLogger();
		s_TreeLogger.addTag("ProposalMySendChips", proposal.chipsToSend);
		s_TreeLogger.addTag("ProposalOpponentSendChips", proposal.chipsToReceive);

		logMsg("response(): the proposal is (" + proposal.chipsToReceive + ", " + proposal.chipsToSend + ").");
		
		// The responder is me if and only if one of the following conditions takes place:
		// 1. This is the first response and I'm the not the first proposer
		// 2. This is the second response and I'm the first proposer
		// This happens iff the conditions differ.
		responderIsMe = (isFirst != this.m_FirstProposerIsMe);
		s_TreeLogger.addTag("MeResponding", new Boolean(responderIsMe));
		
		// If the proposal is feasible
		if ((this.m_OpponentChips.contains(proposal.chipsToReceive)) &&
			(this.m_MyChips.contains(proposal.chipsToSend)))
		{
			s_TreeLogger.addTag("Feasibility", new Boolean(true));
			
			// If the responder is not me
			if (!responderIsMe)
			{
				logMsg("response(): the opponent is the responder.");
				
				// The opponent responds
				double acceptChance = 0.0;
				if (s_UseWekaModels)
				{
					acceptChance = getOpponentResponseChanceFromWekaModel(proposal);
				}
				else
				{
					acceptChance = getOpponentResponseChanceAccordingToGenerosity(proposal);
				}
				acceptProposal = s_RandomGenerator.nextDouble() <= acceptChance;
			}
			else
			{
				logMsg("response(): I\'m the responder.");

				// Checks whether we need to perform sampling
				if (isFirstSimulation)
				{
					// We need to perform sampling
					sampleResponse(isFirst, proposal);
					s_TreeLogger.endTag("Response");
					logMsg("response(): leaving.");
					return;
				}
				else
				{
					// Respond randomly
					acceptProposal = s_RandomGenerator.nextBoolean();
				}
			}
		}
		else
		{
			// Proposal is not feasible
			s_TreeLogger.addTag("Feasibility", new Boolean(false));
			logMsg("response(): proposal is not feasible, rejecting.");
			acceptProposal = false;
		}
		
		// Tree log
		s_TreeLogger.addTag("AcceptProposal", new Boolean(acceptProposal));
		
		// Advance to the next phase
		if (acceptProposal)
		{
			logMsg("response(): proposal was accepted.");
			transfer(proposal);
		}
		else
		{
			logMsg("response(): proposal was rejected.");
			if (isFirst)
			{
				secondProposition();
			}
			else
			{
				transfer(null);
			}
		}

		// Quit
		s_TreeLogger.endTag("Response");
		logMsg("response(): leaving.");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		firstResponse    					    				//
	// Purpose:		Simulates the response of the first responder.			//
	// Parameters:	* proposal - the proposal sent by the first proposer.   //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void firstResponse(Proposal proposal)
	{
		// Just call the first response
		logMsg("firstResponse(): entering.");
		response(true, proposal);
		logMsg("firstResponse(): leaving.");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		secondResponse    					    				//
	// Purpose:		Simulates the response of the second responder.			//
	// Parameters:	* proposal - the proposal sent by the second proposer.  //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void secondResponse(Proposal proposal)
	{
		// Just call the second response
		logMsg("secondResponse(): entering.");
		response(false, proposal);
		logMsg("secondResponse(): leaving.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		intersectChipSets					    				//
	// Purpose:		Intersect two chip sets.								//
	// Returns:		The intersection of the given chip sets.				//
	// Parameters:	* setX - the first chip set.							//
	//				* setY - the second chip set.							//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static ChipSet intersectChipSets(ChipSet setX,
											 ChipSet setY)
	{
		ChipSet result = new ChipSet();
		
		// Iterate the colors and get the minimum of both
		for (String color : setX.getColors())
		{
			int toAdd = Math.min(setX.getNumChips(color), setY.getNumChips(color));
			result.add(color, toAdd);
		}
		
		// Return the result
		return result;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		subChipSetsNoNegatives				    				//
	// Purpose:		Substracts chip sets.									//
	// Parameters:	* givenChipSet - the chip set to substract from.		//
	//				* toSub - the chip set to substract from the first		//
	//						  given chip set.							    //
	// Returns:		The substracted chip set.								//
	// Remarks:		* This function exists because the ChipSet class's		//
	//				  substraction method can reach negative values.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private static ChipSet subChipSetsNoNegatives(ChipSet givenChipSet,
 											      ChipSet toSub)
	{
		// Will keep the result
		ChipSet result = new ChipSet(givenChipSet);
		
		// Iterate colors and substract as much as possible
		for (String color : toSub.getColors())
		{
			int numColor = Math.max(0, givenChipSet.getNumChips(color) - toSub.getNumChips(color));
			result.set(color, numColor);
		}
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getMeaningfulChipsInProposal		    				//
	// Purpose:		Get the meaningful chips in the given proposals.		//
	// Parameters:	* proposal - the proposal sent by the second proposer.  //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Remarks:		* The meaningful chips are those that the receiver		//
	//				  needs and the proposer doesn't need.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getMeaningfulChipsToSendInProposal(Proposal proposal)
	{
		// Will keep the result
		ChipSet result = null;
		
		// Calculate the chips needed by me
		ChipSet chipsNeededByMe = ChipSet.addChipSets(this.m_MyNeededChips, this.m_BothNeededChips);
		
		// Calculate the meaningful chips in the proposal, which relate to the needed chips
		ChipSet relevantChips = subChipSetsNoNegatives(proposal.chipsToSend, chipsNeededByMe);
		result = intersectChipSets(relevantChips, this.m_OpponentNeededChips);
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOptionsToSendChips				    				//
	// Purpose:		Get the relevant options to send chips from a given.	//
	//				proposal.												//
	// Parameters:	* proposal - the proposal sent by the second proposer.  //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	//				* maxMeaningfulChips - the maximum amount of meaningful	//
	//									   chips to send. Note that the		//
	//									   empty chip set and the full		//
	//									   exchange are included anyway.	//
	// Remarks:		* The partial exchanges contain only meaningful chips.	//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private Set<ChipSet> getOptionsToSendChips(Proposal proposal,
											   int maxMeaningfulChips)
	{
		// Saves the result
		Set<ChipSet> result = new LinkedHashSet();
		
		// Get all the meaningful chips to send
		ChipSet meaningfulChipsToSend = getMeaningfulChipsToSendInProposal(proposal);
		
		// Take representatives for each option
		for (int toTake = 0; toTake <= maxMeaningfulChips; toTake++)
		{
			// Break if you cannot take more chips
			if (meaningfulChipsToSend.getNumChips() < toTake)
			{
				break;
			}
			
			// Get that amount
			ChipSet currentOption = meaningfulChipsToSend.getSubset(toTake);
			result.add(currentOption);
		}
		
		// Add the empty chip set and the full exchange as well
		result.add(new ChipSet());
		result.add(new ChipSet(proposal.chipsToSend));
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponentTransferFromReliability	    				//
	// Purpose:		Get the opponent's transfer from the reliability model. //
	// Parameters:	* proposal - the last accepted proposal. NULL proposal	//
	//				             indicates that no proposal was accepted.   //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getOpponentTransferFromReliability(Proposal proposal)
	{
		ChipSet opponentSendChips = null;
		
		// Calculate the opponent send chance and randomize accordingly
		double opponentSendChance = this.m_OpponentReliabilitySum / this.m_TransfersSoFar;
		opponentSendChips = Reliability.getChipSetFromReliability(proposal.chipsToReceive,
																  opponentSendChance,
																  s_RandomGenerator);
		
		// Return the result
		return opponentSendChips;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponentTransferFromWekaModel	    				//
	// Purpose:		Get the opponent's transfer from the Weka model.		//
	// Parameters:	* proposal - the last accepted proposal. NULL proposal	//
	//				             indicates that no proposal was accepted.   //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Remarks:		* Assumes the opponent sends everything or nothing.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getOpponentTransferFromWekaModel(Proposal proposal)
	{
		// Will hold the result
		ChipSet opponentSendChips = new ChipSet();
		
		// Get the current scores
		double myCurrentScore = getPlayerScore(true);
		double opponentCurrentScore = getPlayerScore(false);
		
		// Get the resulting chips
		ChipSet myChipsAfterTransfer = ChipSet.subChipSets(ChipSet.addChipSets(this.m_MyChips, proposal.chipsToReceive), proposal.chipsToSend);
		ChipSet opponentChipsAfterTransfer = ChipSet.subChipSets(ChipSet.addChipSets(this.m_OpponentChips, proposal.chipsToSend), proposal.chipsToReceive);
		
		// Get the resulting scores
		double myResultingScore = Reliability.getPlayerScore(this.m_MyPosition, myChipsAfterTransfer, this.m_GoalPosition, this.m_Scoring);
		double opponentResultingScore = Reliability.getPlayerScore(this.m_OpponentPosition, opponentChipsAfterTransfer, this.m_GoalPosition, this.m_Scoring);
		
		// Calculate the number of needed colors
		int myNeededColors = getNumberOfNeededColors(proposal, true);
		int opponentNeededColors = getNumberOfNeededColors(proposal, false);
		
		// Calculate the number of other colors
		int myOtherColors = proposal.chipsToReceive.getNumChips() - myNeededColors;
		int opponentOtherColors = proposal.chipsToSend.getNumChips() - opponentNeededColors;
		
		// Calculate the chance of fulfilling the proposal
		s_TreeLogger.beginTag("WekaWrapperForTransferProbabilitiesModel");
		s_TreeLogger.addTag("ConsideredProposal", "(" + proposal.chipsToSend + ", " + proposal.chipsToReceive + ")");
		s_TreeLogger.addTag("MyReliability", this.m_MyReliabilitySum / this.m_TransfersSoFar);
		s_TreeLogger.addTag("OpponentReliability",  this.m_OpponentReliabilitySum / this.m_TransfersSoFar);
		s_TreeLogger.addTag("MyCurrentScore", myCurrentScore);
		s_TreeLogger.addTag("MyResultingScore", myResultingScore);
		s_TreeLogger.addTag("OpponentCurrentScore", opponentCurrentScore);
		s_TreeLogger.addTag("OpponentResultingScore", opponentResultingScore);
		s_TreeLogger.addTag("MyNeededColors", myNeededColors);
		s_TreeLogger.addTag("MyOtherColors", myOtherColors);
		s_TreeLogger.addTag("OpponentNeededColors", opponentNeededColors);
		s_TreeLogger.addTag("OpponentOtherColors", opponentOtherColors);
		double[] probabilitiesFulfill = { 0 };
		Object[] instanceValuesFulfill = { this.m_MyReliabilitySum / this.m_TransfersSoFar,
										   this.m_OpponentReliabilitySum / this.m_TransfersSoFar,
										   myCurrentScore,
										   myResultingScore,
										   opponentCurrentScore,
										   opponentResultingScore,
										   (double)myNeededColors,
										   (double)myOtherColors,
										   (double)opponentNeededColors,
										   (double)opponentOtherColors,
										   "1" };
		try
		{
			probabilitiesFulfill = WekaWrapper.getTransferProbabilities(instanceValuesFulfill);
			s_TreeLogger.addTag("ProbabilityForFulfillingProposal", probabilitiesFulfill[1]);
			if (s_RandomGenerator.nextDouble() <= probabilitiesFulfill[1])
			{
				opponentSendChips = new ChipSet(proposal.chipsToReceive);
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		s_TreeLogger.endTag("WekaWrapperForTransferProbabilitiesModel");
		
		// Return the result
		return opponentSendChips;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		sampleTransfers 					    				//
	// Purpose:		Performs sampling upon the transfers.       			//
	// Parameters:	* proposal - the last accepted proposal. NULL proposal	//
	//				             indicates that no proposal was accepted.   //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Checked:		23/07/2012												//
	//////////////////////////////////////////////////////////////////////////	
	private void sampleTransfers(Proposal proposal)
	{
		logMsg("sampleTransfers(): entering.");
		
		// Tree log
		s_TreeLogger.beginTag("TransfersRootNode");
		logStateToTreeLogger();
		
		// The nodes directly generated from the root node in the MCMC tree.
		// The keys are the transfers, and the values are instances of DirectNodeData class.
		Map<ChipSet, DirectNodeData> directNodesData = new HashMap();
		
		// This will keep the best proposal
		ChipSet bestTransfer = new ChipSet();
		double bestScore = Double.MIN_VALUE;
		boolean bestScoreCalculated = false;
		
		// Finish on NULL proposal
		if (null == proposal)
		{
			logMsg("sampleTransfers(): Got an empty last proposal, quitting.");
			this.m_BestTransfer = bestTransfer;
			return;
		}
		
		// Get all the transfers
		Set<ChipSet> allTransfers = getOptionsToSendChips(proposal, s_MaxMeaningfulChipsInExchange);
		logMsg("sampleTransfers(): Got a map of size (" + allTransfers.size() + ").");
		
		// Sample
		Object allTransfersArray[] = allTransfers.toArray();
		logMsg("sampleTransfers(): Starting to sample.");
		for (int counter = 0; counter < s_SamplingFactor; counter++)
		{
			// Get a random transfer
			int randomIndex = s_RandomGenerator.nextInt(allTransfers.size());
			ChipSet currentTransfer = (ChipSet)(allTransfersArray[randomIndex]);

			// Simulate the opponent's transfer
			ChipSet opponentSendChips = null;
			if (s_UseWekaModels)
			{
				opponentSendChips = getOpponentTransferFromWekaModel(proposal);
			}
			else
			{
				opponentSendChips = getOpponentTransferFromReliability(proposal);
			}		
			logMsg("sampleTransfer(): opponent\'s chips to send is (" + opponentSendChips + ").");
			
			// Simulate the transfer
			s_TreeLogger.beginTag("TransferSimulation");
			GameSimulator currentTransferSimulation = new GameSimulator(this);
			currentTransferSimulation.doTransfer(proposal,  currentTransfer, opponentSendChips);
			currentTransferSimulation.move();
			s_TreeLogger.addTag("SimulationScore", currentTransferSimulation.getScore());
			s_TreeLogger.endTag("TransferSimulation");
			
			// Gather information of direct nodes
			if (directNodesData.containsKey(currentTransfer))
			{
				DirectNodeData gatheredData = (DirectNodeData)(directNodesData.get(currentTransfer));
				gatheredData.addSimulation(currentTransferSimulation.getScore());
				directNodesData.put(currentTransfer, gatheredData);
			}
			else
			{
				DirectNodeData gatheredData = new DirectNodeData(currentTransferSimulation.getScore());
				directNodesData.put(currentTransfer, gatheredData);
			}
		}
		logMsg("sampleTransfers(): Finished sampling.");
		
		// After the sampling is done, find the best transfer
		for (Map.Entry<ChipSet, DirectNodeData> entry : directNodesData.entrySet())
		{
			// Get the gathered data
			DirectNodeData gatheredData = entry.getValue();

			// Maximize average
			logMsg("sampleTransfer(): Occurences: " + gatheredData.m_NumberOfOccurences);
			double currentAverage = gatheredData.m_SumOfScores / gatheredData.m_NumberOfOccurences;
			if ((!bestScoreCalculated) || (currentAverage > bestScore))
			{
				bestScore = currentAverage;
				bestTransfer = new ChipSet(entry.getKey());
				bestScoreCalculated = true;
			}
		}

		// Mark the best transfer
		s_TreeLogger.beginTag("BestTransfer");
		s_TreeLogger.addTag("BestTransfer", bestTransfer);
		s_TreeLogger.addTag("AverageScore", new Double(bestScore));
		s_TreeLogger.endTag("BestTransfer");
		logMsg("sampleTransfers(): ** saving the best transfer (" + bestTransfer + ").");
		this.m_BestTransfer = bestTransfer;
		
		// Tree log
		s_TreeLogger.endTag("TransfersRootNode");
		
		// Finis
		logMsg("sampleTransfers(): leaving.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		doTransfer         					    				//
	// Purpose:		Does an actual transfer.								//
	// Parameters:	* proposal - the last accepted proposal. NULL proposal	//
	//				             indicates that no proposal was accepted.   //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	//				* mySendChips - the chips sent by Clueless.				//
	//				* opponentSendChips - the chips sent by the opponent.	//
	// Checked:		23/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	private void doTransfer(Proposal proposal,
							ChipSet mySendChips,
							ChipSet opponentSendChips)
	{
		s_TreeLogger.beginTag("actualTransfer");
		logStateToTreeLogger();
		
		// Get the current scores
		double myCurrentScore = getPlayerScore(true);
		double opponentCurrentScore = getPlayerScore(false);
		
		// Update reliabilities
		double myRoundReliability = Reliability.getRoundReliability(this.m_OpponentPosition,
																	this.m_GoalPosition,
																	this.m_Scoring,
																	this.m_OpponentChips,
																	proposal.chipsToSend,
																	mySendChips,
																	opponentCurrentScore);
		double opponentRoundReliability = Reliability.getRoundReliability(this.m_MyPosition,
																		  this.m_GoalPosition,
																		  this.m_Scoring,
																		  this.m_MyChips,
																		  proposal.chipsToReceive,
																		  opponentSendChips,
																		  myCurrentScore);
		s_TreeLogger.addTag("MyRoundReliability", myRoundReliability);
		s_TreeLogger.addTag("OpponentRoundReliability", opponentRoundReliability);
		logMsg("doTransfer(): my round reliability (" + myRoundReliability + ").");
		logMsg("doTransfer(): opponent round reliability (" + opponentRoundReliability + ").");
		this.m_MyReliabilitySum = Reliability.getAccumulatedReliability(this.m_MyReliabilitySum, myRoundReliability, this.m_TransfersSoFar);
		this.m_OpponentReliabilitySum = Reliability.getAccumulatedReliability(this.m_OpponentReliabilitySum, opponentRoundReliability, this.m_TransfersSoFar);
		s_TreeLogger.addTag("MyOverallReliabilitySum", this.m_MyReliabilitySum);
		s_TreeLogger.addTag("OpponentOverallReliabilitySum", this.m_OpponentReliabilitySum);
		logMsg("doTransfer(): my overall reliability (" + this.m_MyReliabilitySum + ").");
		logMsg("doTransfer(): opponent overall reliability (" + this.m_OpponentReliabilitySum + ").");
		
		// Do the real transfer
		this.m_MyChips.addChipSet(opponentSendChips);
		this.m_OpponentChips.addChipSet(mySendChips);
		this.m_MyChips.subChipSet(mySendChips);
		this.m_OpponentChips.subChipSet(opponentSendChips);
		
		// Update the number of transfers
		this.m_TransfersSoFar++;
		
		s_TreeLogger.endTag("actualTransfer");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		transfer         					    				//
	// Purpose:		Simulates the transfer.									//
	// Parameters:	* proposal - the last accepted proposal. NULL proposal	//
	//				             indicates that no proposal was accepted.   //
	//							 note that the proposal's chipsToReceive    //
	//							 member will ALWAYS be the opponent's chips //
	//							 and chipsToSend will ALWAYS be mine.		//
	// Remarks:		* Assumes the opponent sends only subsets of his part   //
	//				  of the last accepted proposal.						//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void transfer(Proposal proposal)
	{
		boolean isFirstSimulation = !this.m_SimulationBegun;
		ChipSet bestOfMyChipsToSend;
		ChipSet mySendChips;
		
		logMsg("transfer(): entering.");
		
		// Finish if the game is over
		if (this.m_GameOver)
		{
			logMsg("transfer(): game is already over.");
			return;
		}
		this.m_SimulationBegun = true;
		
		// Tree log
		s_TreeLogger.beginTag("Transfer");
		logStateToTreeLogger();
		
		// If no proposal was accepted - don't simulate transfers at all
		if (null == proposal)
		{
			// Update the number of transfers
			this.m_TransfersSoFar++;
			
			// Update reliabilities
			this.m_MyReliabilitySum = (this.m_MyReliabilitySum / this.m_TransfersSoFar) * (this.m_TransfersSoFar + 1);
			this.m_OpponentReliabilitySum = (this.m_OpponentReliabilitySum / this.m_TransfersSoFar) * (this.m_TransfersSoFar + 1);
			
			// Go to the move stage and return
			s_TreeLogger.addTag("LastProposalMyChips", "null");
			s_TreeLogger.addTag("LastProposalOpponentChips", "null");
			if (isFirstSimulation)
			{
				this.m_BestTransfer = new ChipSet();
				logMsg("transfer(): empty proposal, skipping.");
			}
			else
			{
				logMsg("transfer(): empty proposal, skipping and moving.");
				logMsg("transfer(): moving.");
				move();
			}
			logMsg("transfer(): leaving.");
			s_TreeLogger.endTag("Transfer");
			return;
		}
		
		// Tree logging
		s_TreeLogger.addTag("LastProposalMyChips", proposal.chipsToSend);
		s_TreeLogger.addTag("LastProposalOpponentChips", proposal.chipsToReceive);
		
		// Checks whether we need to perform sampling
		if (isFirstSimulation)
		{
			// We need to perform sampling
			sampleTransfers(proposal);
			s_TreeLogger.endTag("Transfer");
			logMsg("transfer(): leaving.");
			return;
		}
		
		// Simulate the opponent's transfer
		ChipSet opponentSendChips = null;
		if (s_UseWekaModels)
		{
			opponentSendChips = getOpponentTransferFromWekaModel(proposal);
		}
		else
		{
			opponentSendChips = getOpponentTransferFromReliability(proposal);
		}		
		logMsg("transfer(): opponent\'s chips to send is (" + opponentSendChips + ").");
		
		// Simulate my transfer uniformly
		Set<ChipSet> sendOptions = getOptionsToSendChips(proposal, s_MaxMeaningfulChipsInExchange);
		logMsg("transfer(): size of send options is (" + sendOptions.size() + ").");
		Object allSendOptionsArray[] = sendOptions.toArray();
		int randomIndex = s_RandomGenerator.nextInt(sendOptions.size());
		mySendChips = (ChipSet)(allSendOptionsArray[randomIndex]);
		
		// Tree log
		s_TreeLogger.addTag("MySendChips", mySendChips);
		s_TreeLogger.addTag("OpponentSendChips", opponentSendChips);
		
		// Do the actual transfer
		doTransfer(proposal, mySendChips, opponentSendChips);
		
		// Move to the next phase
		logMsg("transfer(): moving.");
		move();
		s_TreeLogger.endTag("Transfer");
		logMsg("transfer(): leaving.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		checkForGameEnding      					    		//
	// Purpose:		Checks if the game has finished and marks it in the		//
	//				member which specifies that the game is over.			//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////	
	private void checkForGameEnding()
	{
		logMsg("checkForGameEnding(): entering.");
		
		// Check for dormant steps
		if (Math.min(this.m_MyDormantSteps, this.m_OpponentDormantSteps) >= s_MaxDormantSteps)
		{
			logMsg("checkForGameEnding(): game is over (dormant steps reached).");
			this.m_GameOver = true;
		}
		
		// Check if someone reached the goal
		if ((this.m_MyPosition.equals(this.m_GoalPosition)) ||
			(this.m_OpponentPosition.equals(this.m_GoalPosition)))
		{
			logMsg("checkForGameEnding(): game is over (someone reached the goal).");
			this.m_GameOver = true;
		}
		
		logMsg("checkForGameEnding(): leaving.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		increaseDormantSteps      					    		//
	// Purpose:		Increases the number of dormant steps.					//
	// Parameters:	* isMe - specifies whether it's me or the opponent.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////	
	private void increaseDormantSteps(boolean isMe)
	{
		// Just increase the dormant steps
		if (isMe)
		{
			this.m_MyDormantSteps++;
		}
		else
		{
			this.m_OpponentDormantSteps++;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		tryToMoveToGoal		      					    		//
	// Purpose:		Tries the move towards the goal.						//
	// Parameters:	* isMe - specifies whether it's me or the opponent.		//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////	
	private void tryToMoveToGoal(boolean isMe)
	{
		RowCol currentPosition = (isMe ? this.m_MyPosition : this.m_OpponentPosition);
		ChipSet currentChips = (isMe ? this.m_MyChips : this.m_OpponentChips);
		
		// Tree log
		s_TreeLogger.addTag("CurrentPosition", currentPosition);
		
		// Get the relevant path
		ArrayList<Path> paths = ShortestPaths.getShortestPaths(currentPosition,
															   this.m_GoalPosition,
															   this.m_Board,
															   this.m_Scoring,
															   1,
															   this.m_MyChips,
															   this.m_OpponentChips);
		if (paths.size() == 0)
		{
			s_TreeLogger.addTag("NewPosition", currentPosition);
			increaseDormantSteps(isMe);
			return;
		}
	
		// Get the next point and the required color
		RowCol pointToMove = paths.get(0).getPoint(1);
		String neededColor = this.m_Board.getSquare(pointToMove).getColor();
		
		// Check if the color is in the player's posession
		if (currentChips.getNumChips(neededColor) == 0)
		{
			s_TreeLogger.addTag("NewPosition", currentPosition);
			increaseDormantSteps(isMe);
			return;
		}
		
		// It's possible to move
		s_TreeLogger.addTag("NewPosition", pointToMove);
		if (isMe)
		{
			this.m_MyPosition = pointToMove;
			this.m_MyChips.add(neededColor, -1);
			this.m_MyNeededChips.add(neededColor, -1);
			this.m_MyDormantSteps = 0;
		}
		else
		{
			this.m_OpponentPosition = pointToMove;
			this.m_OpponentChips.add(neededColor, -1);
			this.m_OpponentNeededChips.add(neededColor, -1);
			this.m_OpponentDormantSteps = 0;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		move         					    					//
	// Purpose:		Simulates the move stage.								//
	// Remarks:		* Always moves both sides.								//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////	
	private void move()
	{
		logMsg("move(): entering.");
		
		// Finish if the game is over
		if (this.m_GameOver)
		{
			logMsg("move(): game is already over.");
			return;
		}
		s_TreeLogger.beginTag("Move");
	
		// Moves both sides if possible and if the dormant steps cause game ending
		if (this.m_MyDormantSteps >= s_MaxDormantSteps)
		{
			s_TreeLogger.beginTag("MyMove");
			tryToMoveToGoal(true);
			s_TreeLogger.endTag("MyMove");
		}
		else
		{
			increaseDormantSteps(true);
		}
		if (this.m_OpponentDormantSteps >= s_MaxDormantSteps)
		{
			s_TreeLogger.beginTag("OpponentMove");
			tryToMoveToGoal(false);
			s_TreeLogger.endTag("OpponentMove");
		}
		else
		{
			increaseDormantSteps(false);
		}
		
		// Switch sides of proposers
		this.m_FirstProposerIsMe = !(this.m_FirstProposerIsMe);	
		
		// Check for game ending
		checkForGameEnding();
		
		// Propose
		logMsg("move(): going to first proposition phase.");
		firstProposition();
		s_TreeLogger.endTag("Move");
		logMsg("move(): leaving.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPlayerScore     					    				//
	// Purpose:		Gets the player score.								 	//
	// Parameters:	* isMyPlayer - whether it's me or the opponent.			//
	// Returns:		double													//
	// Remarks:		* Simulates walking to the goal.						//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////	
	private double getPlayerScore(boolean isMyPlayer)
	{		
		// Set the relevant chips
		RowCol playerPosition = (isMyPlayer ? this.m_MyPosition : this.m_OpponentPosition);
		ChipSet playerChips = (isMyPlayer ? this.m_MyChips : this.m_OpponentChips);
		
		// Return the score
		return Reliability.getPlayerScore(playerPosition, playerChips, this.m_GoalPosition, this.m_Scoring);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		setMyScoreWeight										//
	// Purpose:		Sets the weight of my score against the opponent's.		//
	// Parameters:	* myWeight - the weight of my score.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public static void setMyScoreWeight(double myWeight)
	{
		// Check for errors
		if ((0.0 > myWeight) ||
			(1.0 < myWeight))
		{
			throw new RuntimeException("The given weight should be a real number between 0 and 1.");
		}
		
		// Set and quit
		s_MyScoreWeight = myWeight;
	}
		
	//////////////////////////////////////////////////////////////////////////
	// Method:		getScore         					    				//
	// Purpose:		Gets the score - positive is good for me, negative is   //
	//				good for the opponent.									//
	// Returns:		double													//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////	
	public double getScore()
	{
		double opponentScoreWeight = 1.0 - s_MyScoreWeight;
		return s_MyScoreWeight * getPlayerScore(true) + opponentScoreWeight * getPlayerScore(false);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getBestResponse         					    		//
	// Purpose:		Gets the best response.									//
	// Returns:		boolean													//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public boolean getBestResponse()
	{
		// Just return the best response
		return this.m_BestResponse;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getBestProposal         					    		//
	// Purpose:		Gets the best proposal.									//
	// Returns:		Proposal												//
	// Remakrs:		* note that the proposal's chipsToReceive member will 	//
	//				  ALWAYS be the opponent's chips and chipsToSend will 	//
	//				  ALWAYS be mine.										//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public Proposal getBestProposal()
	{
		// Just return the best proposal
		return this.m_BestProposal;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getBestTransfer         					    		//
	// Purpose:		Gets the best transfer.									//
	// Returns:		ChipSet													//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet getBestTransfer()
	{
		// Just return the best transfer
		return this.m_BestTransfer;
	}
}
