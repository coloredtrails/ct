//////////////////////////////////////////////////////////////////////////
// File:		MclAgentFrontEnd.java 					   				//
// Purpose:		Implements the front end of MCL.						//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.mclAgent;

//Imports from Java common framework
import java.util.*;

//////////////////////////////////////////////////////////////////////////
// Class:		MclAgentFrontEnd				  		  				//
// Purpose:		Implements the front end of MCL.						//
//////////////////////////////////////////////////////////////////////////
public final class MclAgentFrontEnd 
{
	//////////////////////////////////////////////////////////////////////////
	// Method:		main							    					//
	// Purpose:		Implements the main function, which starts the player.	//
	//////////////////////////////////////////////////////////////////////////
	public static void main(String[] args)
	{
		// Parse arguments
		int samplingBranchingForResponses = Integer.parseInt(args[1]);
		int samplingBranchingForProposals = Integer.parseInt(args[2]);
		int samplingBranchingForTransfers = Integer.parseInt(args[3]);
		double myInitialReliability = Double.parseDouble(args[4]);
		double opponentInitialReliability = Double.parseDouble(args[5]);
		double lastRoundReliabilityWeight = Double.parseDouble(args[6]);
		double myScoreWeight = Double.parseDouble(args[7]);
		
		// Create the agent
		MclPlayer agent = new MclPlayer(samplingBranchingForResponses,
										samplingBranchingForProposals,
										samplingBranchingForTransfers,
										myInitialReliability,
										opponentInitialReliability,
										lastRoundReliabilityWeight,
										myScoreWeight);
		
		// Give the agent a proper PIN number and start it
		agent.setClientName(args[0]);
		agent.start();
	}
}
