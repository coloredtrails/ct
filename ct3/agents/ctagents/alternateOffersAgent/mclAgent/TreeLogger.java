package ctagents.alternateOffersAgent.mclAgent;

import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.io.FileWriter;

import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import ctagents.alternateOffersAgent.Proposal;

public class TreeLogger
{
	private FileWriter m_Logger;
	private int m_IndentionLevel;
	private boolean m_IsActivated;
	private static Map<String, TreeLogger> s_Instances = new HashMap();
	private int m_Counter;
	private String m_InstanceName;
	
	public static TreeLogger getInstance(String instanceName)
	{
		if (!(s_Instances.containsKey(instanceName)))
		{
			s_Instances.put(new String(instanceName), new TreeLogger(instanceName));
		}
		return s_Instances.get(instanceName);
	}
	
	public void activate()
	{
		this.m_IsActivated = true;
	}
	
	private TreeLogger(String instanceName)
	{
		// Counter is zero
		this.m_Counter = 0;
		
		// Save the instance name
		this.m_InstanceName = new String(instanceName);
		
		// Not activated by default
		this.m_IsActivated = false;
		
		// Initialize the file writer
		try
		{
			this.m_Logger = new FileWriter(Integer.toString(this.m_Counter) + "_" + this.m_InstanceName);
		}
		catch (IOException e)
		{
			throw new RuntimeException("Problem creating tree logger.");
		}
		
		// No indention
		this.m_IndentionLevel = 0;
	}
	
	public void startNew()
	{
		// Do nothing if not activated
		if (!this.m_IsActivated)
		{
			return;
		}
		
		// Flush
		flush();
		
		// Increase counter
		this.m_Counter++;
		
		// Initialize the file writer
		try
		{
			this.m_Logger = new FileWriter(Integer.toString(this.m_Counter) + "_" + this.m_InstanceName);
		}
		catch (IOException e)
		{
			throw new RuntimeException("Problem creating tree logger.");
		}
		
		// No indention
		this.m_IndentionLevel = 0;
		
	}
	
	private void writeData(String data)
	{
		// Write the data
		try
		{
			this.m_Logger.write(data);
		}
		catch (IOException e)
		{
			throw new RuntimeException("Problem writing to the tree logger.");
		}
	}
	
	public void beginTag(String tagName)
	{
		// Do nothing if not activated
		if (!this.m_IsActivated)
		{
			return;
		}
		
		// Start the tag
		startTagNoLinebreaks(tagName);
		writeData("\r\n");
	}
	
	private void startTagNoLinebreaks(String tagName)
	{
		// Do nothing if not activated
		if (!this.m_IsActivated)
		{
			return;
		}
		
		// Indent
		for (int counter = 0; counter < this.m_IndentionLevel; counter++)
		{
			writeData("\t");
		}
		
		// Write the tag beginning
		writeData("<" + tagName + ">");
		
		// Increase intention
		this.m_IndentionLevel++;		
	}
	
	private void endTagNoIndent(String tagName)
	{
		// Do nothing if not activated
		if (!this.m_IsActivated)
		{
			return;
		}
		
		// Write the tag beginning
		writeData("</" + tagName + ">\r\n");
		
		// Decrease intention
		if (this.m_IndentionLevel > 0)
		{
			this.m_IndentionLevel--;
		}
	}

	public void endTag(String tagName)
	{
		// Do nothing if not activated
		if (!this.m_IsActivated)
		{
			return;
		}
		
		// Indent
		if (this.m_IndentionLevel > 0)
		{
			for (int counter = 0; counter < this.m_IndentionLevel - 1; counter++)
			{
				writeData("\t");
			}
		}
		
		// End the tag
		endTagNoIndent(tagName);
	}
	
	public void addTag(String tagName,
					   Object tagData)
	{
		// Do nothing if not activated
		if (!this.m_IsActivated)
		{
			return;
		}
		
		// Write the tag to the log
		startTagNoLinebreaks(tagName);
		writeData(tagData.toString());
		endTagNoIndent(tagName);
	}
	
	public void addMyChips(ChipSet myChips)
	{
		// Write the chips
		addTag("MyChips", myChips);
	}
	
	public void addOpponentChips(ChipSet opponentChips)
	{
		// Write the chips
		addTag("OpponentChips", opponentChips);
	}
	
	public void addMyNeededChips(ChipSet myNeededChips)
	{
		// Write the chips
		addTag("MyNeededChips", myNeededChips);
	}
	
	public void addOpponentNeededChips(ChipSet opponentNeededChips)
	{
		// Write the chips
		addTag("OpponentNeededChips", opponentNeededChips);		
	}
	
	public void addBothNeededChips(ChipSet bothNeededChips)
	{
		// Write the chips
		addTag("BothNeededChips", bothNeededChips);	
	}
	
	public void addMyPosition(RowCol myPosition)
	{
		// Write the position
		addTag("MyPosition", myPosition);	
	}

	public void addOpponentPosition(RowCol opponentPosition)
	{
		// Write the position
		addTag("OpponentPosition", opponentPosition);	
	}
	
	public void addMyReliability(double myReliability)
	{
		// Write the reliability
		addTag("MyReliability", new Double(myReliability));	
	}
	
	public void addOpponentReliability(double opponentReliability)
	{
		// Write the reliability
		addTag("OpponentReliability", new Double(opponentReliability));	
	}

	public void addMyDormantSteps(int myDormantSteps)
	{
		// Write the dormant steps
		addTag("MyDormantSteps", new Integer(myDormantSteps));	
	}
	
	public void addOpponentDormantSteps(int opponentDormantSteps)
	{
		// Write the dormant steps
		addTag("OpponentDormantSteps", new Integer(opponentDormantSteps));	
	}
	
	public void addProposalGroup(Set<Proposal> proposals,
								 double generosity)
	{
		// Do nothing if not activated
		if (!this.m_IsActivated)
		{
			return;
		}
		
		// Add the header and the generosity
		beginTag("ProposalGroup");
		addTag("Generosity", new Double(generosity));
		beginTag("Proposals");
		
		// Iterate the proposals
		for (Proposal proposal : proposals)
		{
			beginTag("Proposal");
			addTag("MySendChips", proposal.chipsToSend);
			addTag("OpponentSendChips", proposal.chipsToReceive);
			endTag("Proposal");
		}
		
		// Add the footer
		endTag("Proposals");
		endTag("ProposalGroup");
	}
	
	public void flush()
	{
		// Do nothing if not activated
		if (!this.m_IsActivated)
		{
			return;
		}
		
		// Flush
		try
		{
			this.m_Logger.flush();
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}
}
