//////////////////////////////////////////////////////////////////////////
// File:		MclPlayer.java 					   						//
// Purpose:		Implements the core of the MCL agent.					//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.mclAgent;

//Imports from Java common framework
import java.util.ArrayList;
import java.util.Set;
import java.lang.Math;

//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Phases;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.DiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.discourse.BasicProposalDiscussionDiscourseMessage;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.agent.events.RoleChangedEventListener;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.alternateOffersAgent.SimplePlayer;
import ctagents.FileLogger;

//////////////////////////////////////////////////////////////////////////
// Class:		MclPlayer							    				//
// Purpose:		Implements the MCL player.								//
//////////////////////////////////////////////////////////////////////////
public class MclPlayer extends SimplePlayer implements RoleChangedEventListener
{
	// Saves whether Clueless is the first proposer
	private boolean m_IsFirstProposer;
	
	// Saves the last transfer
	private ChipSet m_LastTransfer;
	
	// Saves whether this is the first communications round
	private boolean m_FirstCommunicationsRound;
	
	// Saves the last accepted proposal
	private Proposal m_LastAcceptedProposal;
	
	// Saves the number of rounds so far
	private int m_RoundsSoFar;
	
	// Reliability related members
	private double m_MyReliabilitySum;
	private double m_OpponentReliabilitySum;
	
	// Saves the opponents' last chips
	private ChipSet m_MyLastChips;
	private ChipSet m_OpponentLastChips;
	
	// Saves the last round reliability weight
	private double m_LastRoundReliabilityWeight;
	
	// Saves the scoring
	private Scoring m_Scoring;
	
	// Maximum number of dormant rounds
	private static final int s_MaxDormantRounds = 2;
	
	// Upper limit for proposals
	private static final int s_ProposalsUpperLimitSeconds = 60;
	
	// Upper limit for responses
	private static final int s_ReponsesUpperLimitSeconds = 60;
	
	// Upper limit for transfers
	private static final int s_TransfersUpperLimitSeconds = 60;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		debugPrint												//
	// Purpose:		Prints a debug message.									//
	// Parameters:	* message - The message to print.						//
	//////////////////////////////////////////////////////////////////////////
	static private void debugPrint(String message)
	{
		// Just print
		System.out.println("\t(MCL): " + message);
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		MclPlayer constructor									//
	// Purpose:		Initializes the MCL agent.								//
	// Parameters:	* samplingBranchingForResponses - Branching factor for  //
	//												  response sampling.	//
	// Parameters:	* samplingBranchingForProposals - Branching factor for  //
	//												  proposal sampling.	//
	// Parameters:	* samplingBranchingForTransfers - Branching factor for  //
	//												  transfer sampling.	//
	//				* myInitialReliability - My initial reliability.		//
	//				* opponentInitialReliability - The opponent's initial	//
	//											   reliability.				//
	//				* lastRoundReliabilityWeight - The last round weight of	//
	//											   the reliability compared	//
	//											   to the history.			//
	//				* myScoreWeight - My weight for scoring (against the    //
	//								  opponent's weight).					//
	//////////////////////////////////////////////////////////////////////////
	public MclPlayer(int samplingBranchingForResponses,
					 int samplingBranchingForProposals,
					 int samplingBranchingForTransfers,
					 double myInitialReliability,
					 double opponentInitialReliability,
					 double lastRoundReliabilityWeight,
					 double myScoreWeight)
	{
		// Initialize parent
		super();
		
		// We are a listener for the role changing event
		client.addRoleChangedEventListener(this);
		
		// Set the global GameSimulator parameters
		GameSimulator.setMyScoreWeight(myScoreWeight);
		GameSimulator.setLastRoundReliabilityWeight(lastRoundReliabilityWeight);
		GameSimulator.setBranchingFactor(samplingBranchingForResponses, GameSimulator.ActionType.RESPONSE);
		GameSimulator.setBranchingFactor(samplingBranchingForProposals, GameSimulator.ActionType.PROPOSE);
		GameSimulator.setBranchingFactor(samplingBranchingForTransfers, GameSimulator.ActionType.TRANSFER);
		
		// Save the last round reliability weight
		this.m_LastRoundReliabilityWeight = lastRoundReliabilityWeight;
		
		// I'm not the first proposer unless deduced otherwise
		this.m_IsFirstProposer = false;
		
		// The last accepted proposal
		this.m_LastAcceptedProposal = null;
		
		// The first round of communications
		this.m_FirstCommunicationsRound = true;
		
		// Although no transfers were made till now, we initialize the
		// reliability to a positive value, so in order to take an average
		// we will "simulate" one trasfer of the given reliability to both sides.
		this.m_RoundsSoFar = 1;
		
		// Initialize the reliability measures
		this.m_MyReliabilitySum = myInitialReliability;
		this.m_OpponentReliabilitySum = opponentInitialReliability;
		
		// Initialize the scoring
		this.m_Scoring = null;
		
		// Initialize the last transfer
		this.m_LastTransfer = null;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getMe													//
	// Purpose:		Returns my player status.								//
	// Returns:		My player status.										//
	//////////////////////////////////////////////////////////////////////////
	private PlayerStatus getMe()
	{
		// Done easily using the client game status
		return client.getGameStatus().getMyPlayer();
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOpponent												//
	// Purpose:		Returns the opponent's player status.					//
	// Returns:		The opponent's player status.							//
	//////////////////////////////////////////////////////////////////////////
	private PlayerStatus getOpponent()
	{
		// Iterate the players
		for (PlayerStatus playerStatus : client.getGameStatus().getPlayers())
		{
			// If the PIN numbers disagree then it's the opponent
			if (playerStatus.getPin() != getMe().getPin())
			{
				return (PlayerStatus)(playerStatus.clone());
			}
		}
		
		// Not found
		throw new RuntimeException("Opponent ID not found.");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getChipsToTransfer										//
	// Purpose:		Gets the chips to send for a transfer.					//
	// Parameters:	* proposal - the last accepted proposal.				//
	// Returns:		The chips to send.										//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getChipsToTransfer(Proposal proposal)
	{
		// Initialize
		this.m_LastTransfer = null;
		
		// Look for an empty proposal
		if (null == proposal)
		{
			this.m_LastTransfer = new ChipSet();
			return new ChipSet();
		}
		
		// Get all the colors
		Set<String> allColors = client.getGameStatus().getBoard().getColors();

		// Create a simulation of the current game status
		GameSimulator simulation = new GameSimulator(this.m_Scoring,
													 client.getGameStatus().getBoard(),
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getOpponent().getChips(),
													 getMe().getPosition(),
													 getOpponent().getPosition(),
													 this.m_MyReliabilitySum / this.m_RoundsSoFar,
													 this.m_OpponentReliabilitySum / this.m_RoundsSoFar,
													 this.m_RoundsSoFar,
													 getMe().getMyDormantRounds(),
													 getMe().getHisDormantRounds());
		
		// Simulate the transfer
		ChipSet bestTransfer = simulation.SampleTransfers(proposal, Math.min((((getPhaseTimeoutSeconds() - 1) * 3) / 4), s_TransfersUpperLimitSeconds));
		
		// Fill with zeros
		for (String color : allColors)
		{
			if (bestTransfer.getNumChips(color) == 0)
			{
				bestTransfer.setNumChips(color, 0);
			}
		}
		
		// Save the last transfer
		this.m_LastTransfer = new ChipSet(bestTransfer);
		
		// Return the simulation's best transfer
		return bestTransfer;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getPhaseTimeoutSeconds									//
	// Purpose:		Gets the phase timeout in seconds.						//
	// Returns:		The current phase timeout in seconds.					//
	//////////////////////////////////////////////////////////////////////////
	private long getPhaseTimeoutSeconds()
	{
		// Return the current phase timeout
		long timeout = client.getGameStatus().getPhases().getPhaseDuration() - client.getGameStatus().getPhases().getCurrentSecsElapsed();
		debugPrint("Current phase timeout is: " + timeout);
		return timeout;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		createProposal											//
	// Purpose:		Generates a proposal.									//
	// Returns:		The best simulated proposal.							//
	//////////////////////////////////////////////////////////////////////////
	private Proposal createProposal()
	{
		// Initialize
		this.m_LastAcceptedProposal = null;
		
		// Get all the colors
		Set<String> allColors = client.getGameStatus().getBoard().getColors();
		
		// Create a simulation of the current game status
		GameSimulator simulation = new GameSimulator(this.m_Scoring,
													 client.getGameStatus().getBoard(),
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getOpponent().getChips(),
													 getMe().getPosition(),
													 getOpponent().getPosition(),
													 this.m_MyReliabilitySum / this.m_RoundsSoFar,
													 this.m_OpponentReliabilitySum / this.m_RoundsSoFar,
													 this.m_RoundsSoFar,
													 getMe().getMyDormantRounds(),
													 getMe().getHisDormantRounds());
		
		// Simulate the proposition
		Proposal bestProposal = simulation.sampleProposals(this.m_FirstCommunicationsRound, Math.min((((getPhaseTimeoutSeconds() - 1) * 3) / 4), s_ProposalsUpperLimitSeconds));

		// Fill with zeros
		for (String color : allColors)
		{
			if (bestProposal.chipsToReceive.getNumChips(color) == 0)
			{
				bestProposal.chipsToReceive.setNumChips(color, 0);
			}
			if (bestProposal.chipsToSend.getNumChips(color) == 0)
			{
				bestProposal.chipsToSend.setNumChips(color, 0);
			}
		}
		
		// Return the simulation's best proposal
		return bestProposal;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		respondToProposal										//
	// Purpose:		Responds to a proposal.									//
	// Parameters:	* proposal - the proposal to respond to.				//
	//////////////////////////////////////////////////////////////////////////
	private boolean respondToProposal(Proposal proposal)
	{
		// Initialize
		this.m_LastAcceptedProposal = null;
		
		// Get all the colors
		Set<String> allColors = client.getGameStatus().getBoard().getColors();
		
		// Create a simulation of the current game status
		GameSimulator simulation = new GameSimulator(this.m_Scoring,
													 client.getGameStatus().getBoard(),
													 this.m_IsFirstProposer,
													 getMe().getChips(),
													 getOpponent().getChips(),
													 getMe().getPosition(),
													 getOpponent().getPosition(),
													 this.m_MyReliabilitySum / this.m_RoundsSoFar,
													 this.m_OpponentReliabilitySum / this.m_RoundsSoFar,
													 this.m_RoundsSoFar,
													 getMe().getMyDormantRounds(),
													 getMe().getHisDormantRounds());
		
		// Simulate the response
		boolean bestResponse = simulation.sampleResponses(this.m_FirstCommunicationsRound, proposal, Math.min((((getPhaseTimeoutSeconds() - 1) * 3) / 4), s_ReponsesUpperLimitSeconds));
		
		// Return the simulation's best response
		return bestResponse;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onReceipt												//
	// Purpose:		Receives a discourse message.							//
	// Parameters:	* discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	// Checked:		13/07/2012												//
	//////////////////////////////////////////////////////////////////////////
	public void onReceipt(DiscourseMessage discourseMessage)
	{		
		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// If the message is a response to my proposal
		if (discourseMessage instanceof BasicProposalDiscussionDiscourseMessage)
		{
			debugPrint("Received a response to my proposal");

			// The opponent responded -- if he accepts my proposal then save it
			BasicProposalDiscussionDiscourseMessage response = (BasicProposalDiscussionDiscourseMessage)discourseMessage;
			
			// Save information regarding to the response
			if (response.accepted())
			{
				this.m_FirstCommunicationsRound = true;
				this.m_LastAcceptedProposal = new Proposal(response.getChipsSentByResponder(), response.getChipsSentByProposer());
				debugPrint("My proposal was accepted: " + this.m_LastAcceptedProposal);
			}
			else
			{
				debugPrint("My proposal was rejected");
				this.m_FirstCommunicationsRound = !this.m_FirstCommunicationsRound;
				this.m_LastAcceptedProposal = null;
			}
			return;
		}
		
		// If the message is a proposal
		if (discourseMessage instanceof BasicProposalDiscourseMessage)
		{
			// The message is a proposal
			BasicProposalDiscourseMessage proposal = (BasicProposalDiscourseMessage)discourseMessage;
			Proposal proposalToConsider = new Proposal(proposal.getChipsSentByProposer(), proposal.getChipsSentByResponder());
			debugPrint("Received a proposal " + proposalToConsider);
			
			// Build a response to the proposal
			BasicProposalDiscussionDiscourseMessage response = new BasicProposalDiscussionDiscourseMessage(proposal);
			
			// If the proposal should be accepted by us
			if (respondToProposal(proposalToConsider))
			{
				debugPrint("Accepting proposal");
				this.m_LastAcceptedProposal = proposalToConsider;
				this.m_FirstCommunicationsRound = true;
				response.acceptOffer();
			}
			else
			{
				debugPrint("Rejecting proposal");
				this.m_LastAcceptedProposal = null;
				this.m_FirstCommunicationsRound = !this.m_FirstCommunicationsRound;
				response.rejectOffer();
			}
			
			// Send the response
			client.communication.sendDiscourseRequest(response);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		roleChanged												//
	// Purpose:		Called whenever a role has changed.						//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	//////////////////////////////////////////////////////////////////////////
	public void roleChanged()
	{		
		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		
		// Communication phase 
		if (phaseName.equals("Communication Phase"))
		{
			String roleStr = getMe().getRole();
			
			// If we are a proposer we should make a proposal
			if (roleStr.equals("Proposer"))
			{
				// Get my proposal
				Proposal myProposal = createProposal();
				
				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = getOpponent().getPerGameId();
				
				// Create the proposal message
				BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myProposal.chipsToSend, myProposal.chipsToReceive);
				client.communication.sendDiscourseRequest(proposal);
			}
		}
	}	
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getChipsThatWereSentToMe								//
	// Purpose:		Conclude the chips that were sent to me.				//
	// Remarks:		* Must be called during the movement phase.				//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getChipsThatWereSentToMe()
	{
		// My chips before the transfer
		ChipSet chipsBeforeTransfer = this.m_MyLastChips;
		
		// The chips that I sent
		ChipSet chipsSentByMe = new ChipSet();
		if (null != this.m_LastAcceptedProposal)
		{
			chipsSentByMe = this.m_LastTransfer;
		}

		// My current chips
		ChipSet myCurrentChips = getMe().getChips();
		
		// The chips after I sent them but without receiving any
		ChipSet chipsAfterSendingWithoutReceiving = new ChipSet();
		if (chipsBeforeTransfer.contains(chipsSentByMe))
		{
			chipsAfterSendingWithoutReceiving = ChipSet.subChipSets(chipsBeforeTransfer, chipsSentByMe);
		}
		
		// The chips that were sent to me
		ChipSet chipsSentToMe = ChipSet.subChipSets(myCurrentChips, chipsAfterSendingWithoutReceiving);
		for (String color : chipsSentToMe.getColors())
		{
			if (chipsSentToMe.getNumChips(color) < 0)
			{
				chipsSentToMe.set(color, 0);
			}
		}
		
		// Return the result
		return chipsSentToMe;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getResultingScore										//
	// Purpose:		Calculate the resulting score.							//
	// Parmeters:	* playerChips - The player's chips.						//
	//				* playerPosition - The player's position.				//
	//////////////////////////////////////////////////////////////////////////
	private double getResultingScore(ChipSet playerChips,
									 RowCol playerPosition)
	{
		PlayerStatus playerStatus = new PlayerStatus();
		int pathPositionIndex = 0;
		
		// Copy the player chips and the position
		ChipSet newPlayerChips = new ChipSet(playerChips);
		RowCol newPlayerPosition = new RowCol(playerPosition);
		
		// Get the entire game chips
		ChipSet myChips = getMe().getChips();
		ChipSet opponentChips = getOpponent().getChips();
		
		// Get as close as possible towards the goal
		Path pathTowardsGoal = ShortestPaths.getShortestPaths(newPlayerPosition, client.getGameStatus().getBoard().getGoalLocations().get(0), client.getGameStatus().getBoard(), client.getGameStatus().getScoring(), 1, myChips, opponentChips).get(0);
		
		// Look for the position on the goal
		for (pathPositionIndex = 0; pathPositionIndex < pathTowardsGoal.getNumPoints(); pathPositionIndex++)
		{
			if (pathTowardsGoal.getPoint(pathPositionIndex).equals(newPlayerPosition))
			{
				break;
			}
		}
		
		// Try to walk as much as we can towards the goal
		boolean notReachingGoal = false;
		for (int currentPosition = pathPositionIndex + 1; currentPosition < pathTowardsGoal.getNumPoints(); currentPosition++)
		{
			String currentColor = client.getGameStatus().getBoard().getSquare(pathTowardsGoal.getPoint(currentPosition)).getColor();
			if (newPlayerChips.getNumChips(currentColor) > 0)
			{
				newPlayerChips.add(currentColor, -1);
			}
			else
			{
				newPlayerPosition = pathTowardsGoal.getPoint(currentPosition - 1);
				notReachingGoal = true;
				break;
			}
		}
		if (!notReachingGoal)
		{
			newPlayerPosition = client.getGameStatus().getBoard().getGoalLocations().get(0);
		}
		
		// Set the player status
		playerStatus.setPosition(newPlayerPosition);
		playerStatus.setChips(newPlayerChips);
		
		// Return the score
		return this.m_Scoring.score(playerStatus, client.getGameStatus().getBoard().getGoalLocations().get(0));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRoundReliability										//
	// Purpose:		Gets the round reliability of the given player.			//
	// Parmeters:	* otherPlayerPosition - The other player's position.	//
	//				* otherPlayerOriginalChips - The other player's chips	//
	//											 before the transfer.		//
	//				* chipsMentToBeSentToOther - The full exchange chips	//
	//											 that were ment to be sent	//
	//											 to the other player.		//
	//				* chipsSentToOther - The chips that were actuallt sent. //
	//////////////////////////////////////////////////////////////////////////
	private double getRoundReliability(RowCol otherPlayerPosition,
									   ChipSet otherPlayerOriginalChips,
									   ChipSet chipsMentToBeSentToOther,
									   ChipSet chipsSentToOther)
	{
		// Calculate the chips for a full transfer
		ChipSet chipsForFullTransfer = ChipSet.addChipSets(otherPlayerOriginalChips, chipsMentToBeSentToOther);
		ChipSet chipsActuallySent = ChipSet.addChipSets(otherPlayerOriginalChips, chipsSentToOther);
		
		// Calculate scores
		double fullTransferScore = getResultingScore(chipsForFullTransfer, otherPlayerPosition);
		double actualScore = getResultingScore(chipsActuallySent, otherPlayerPosition);
		double currentScore = getResultingScore(otherPlayerOriginalChips, otherPlayerPosition);
		
		// Calculates benifits
		double fullBenifit = fullTransferScore - currentScore;
		double actualBenifit = actualScore - currentScore;
		
		// Calculate the results
		if (0.0 == fullBenifit)
		{
			return 1.0;
		}
		return actualBenifit / fullBenifit;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getWeightedReliability									//
	// Purpose:		Gets the weighted reliability.							//
	// Parameters:	* sumSoFar - The sum of reliabilities so far.			//
	//				* roundReliability - The new round's reliability.		//
	//////////////////////////////////////////////////////////////////////////
	private double getWeightedReliability(double sumSoFar,
										  double roundReliability)
	{
		// Calculate the weights
		double historyWeight = (1.0 - this.m_LastRoundReliabilityWeight);
		
		// Calculate the reliability history
		double reliabilityHistory = sumSoFar / this.m_RoundsSoFar;
		
		// Calculate the weighted reliability
		return (historyWeight * reliabilityHistory + m_LastRoundReliabilityWeight * roundReliability);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		updateReliabilitiesAfterTransfer						//
	// Purpose:		Update reliabilities after a transfer.					//
	// Remarks:		* Should be called from the movement phase.				//
	//////////////////////////////////////////////////////////////////////////
	private void updateReliabilitiesAfterTransfer()
	{
		// If a proposal was not accepted
		if (null == this.m_LastAcceptedProposal)
		{
			debugPrint("Did not accept any proposal");
			return;
		}
		debugPrint("Last accepted proposal is " + this.m_LastAcceptedProposal);
		
		// Get the chips that were sent by the players
		ChipSet chipsSentByMe = new ChipSet();
		if (null != this.m_LastTransfer)
		{
			chipsSentByMe = this.m_LastTransfer;
		}
		ChipSet chipsSentByOpponent = getChipsThatWereSentToMe();
		debugPrint("Chips sent by me " + chipsSentByMe);
		debugPrint("Chips sent by opponent " + chipsSentByOpponent);

		// Calculate round reliabilities
		double myRoundReliability = getRoundReliability(getOpponent().getPosition(),
														this.m_OpponentLastChips,
														this.m_LastAcceptedProposal.chipsToSend,
														chipsSentByMe);
		double opponentRoundReliability = getRoundReliability(getMe().getPosition(),
															  this.m_MyLastChips,
															  this.m_LastAcceptedProposal.chipsToReceive,
															  chipsSentByOpponent);
		debugPrint("My round reliability is " + myRoundReliability);
		debugPrint("Opponent round reliability is " + opponentRoundReliability);
		debugPrint("My old overall reliability is " + this.m_MyReliabilitySum / this.m_RoundsSoFar);
		debugPrint("Opponent old overall reliability is " + this.m_OpponentReliabilitySum / this.m_RoundsSoFar);
		debugPrint("Rounds so far " + this.m_RoundsSoFar);
		
		// Update the weighted reliabilities
		int roundsSoFar = this.m_RoundsSoFar + 1;
		this.m_MyReliabilitySum = getWeightedReliability(this.m_MyReliabilitySum, myRoundReliability) * roundsSoFar;
		this.m_OpponentReliabilitySum = getWeightedReliability(this.m_OpponentReliabilitySum, opponentRoundReliability) * roundsSoFar;
		this.m_RoundsSoFar = roundsSoFar;
		debugPrint("My new reliability is " + (this.m_MyReliabilitySum / this.m_RoundsSoFar));
		debugPrint("Opponent new reliability is " + (this.m_OpponentReliabilitySum / this.m_RoundsSoFar));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		phaseAdvanced											//
	// Purpose:		A callback for phase advances.							//
	// Parameters:	* discourseMessage - the discourse message.				//
	// Remarks:		* This method is a part of SimplePlayer implementation. //
	//				* The discourse message can either be:					//
	//				  1. A response to my previous proposal.				//
	//				  2. A new proposal by the opponent.					//
	//////////////////////////////////////////////////////////////////////////
	public void phaseAdvanced(Phases phases)
	{
		// Update scoring
		if (null == this.m_Scoring)
		{
			this.m_Scoring = client.getGameStatus().getScoring();
		}
		
		// Get the current phase
		String phaseName = client.getGameStatus().getPhases().getCurrentPhaseName();
		
		// Communication phase 
		if (phaseName.equals("Communication Phase"))
		{
			debugPrint("Now in communications phase");
			
			// Get the role
			String roleStr = getMe().getRole();
			
			// Deduce whether I'm the first proposer
			this.m_IsFirstProposer = roleStr.equals("Proposer");
			debugPrint("My role is " + roleStr);
			
			// Make an offer if I'm the proposer
			if (this.m_IsFirstProposer)
			{
				debugPrint("Creating proposal");
				
				// Get my proposal
				Proposal myProposal = createProposal();
				debugPrint("Proposal is " + myProposal);
				
				// Get the IDs
				int proposerId = getMe().getPerGameId();
				int responderId = getOpponent().getPerGameId();
				
				// Create the proposal message
				BasicProposalDiscourseMessage proposal = new BasicProposalDiscourseMessage(proposerId, responderId, -1, myProposal.chipsToSend.removeZeros(), myProposal.chipsToReceive.removeZeros());
				client.communication.sendDiscourseRequest(proposal);
			}
		}
	
		// Move phase
		if (phaseName.equals("Movement Phase"))
		{
			debugPrint("Now in movement phase");
			
			// Update reliabilities
			debugPrint("Updating reliabilities");
			updateReliabilitiesAfterTransfer();
			
			// Check if the number of dormant rounds requires moving
			if (getMe().getMyDormantRounds() >= s_MaxDormantRounds)
			{
				debugPrint("Next round will cause dormant steps to end the game");
				debugPrint("Trying to move");
				
				// Try to move if it is possible
				RowCol goalPosition = client.getGameStatus().getBoard().getGoalLocations().get(0);

				// Looking for the paths towards the goal
				ArrayList<Path> paths = ShortestPaths.getShortestPaths(getMe().getPosition(), goalPosition, client.getGameStatus().getBoard(), this.m_Scoring, 10, getMe().getChips(), getOpponent().getChips());

				// Iterate the paths
				for (Path path : paths)
				{
					RowCol pointToMove = path.getPoint(1);
					
					// Get the needed color for the path
					String neededColor = client.getGameStatus().getBoard().getSquare(pointToMove).getColor();
					debugPrint("In order to move the needed color is " + neededColor);
					
					// Check if the color is in the current chips
					if (getMe().getChips().getNumChips(neededColor) > 0)
					{
						debugPrint("Moving to a new point " + pointToMove);
						client.communication.sendMoveRequest(pointToMove);
						break;
					}
				}
			}
		}
		
		// Exchange phase
		if (phaseName.equals("Exchange Phase"))
		{
			debugPrint("Now in exchange phase");
			
			// Save the last chips
			this.m_MyLastChips = new ChipSet(getMe().getChips());
			this.m_OpponentLastChips = new ChipSet(getOpponent().getChips());
						
			// Send only if we accepted a proposal
			ChipSet chipsToTransfer = new ChipSet();
			if (null != this.m_LastAcceptedProposal)
			{
				chipsToTransfer = getChipsToTransfer(this.m_LastAcceptedProposal);
				if (!(this.m_LastAcceptedProposal.chipsToSend.contains(chipsToTransfer)))
				{
					debugPrint("Something went wrong, the chips to transfer are not contained in the last accepted proposal");
					debugPrint("Last accepted proposal is " + this.m_LastAcceptedProposal);
					debugPrint("My chips to transfer are " + chipsToTransfer);
					chipsToTransfer = new ChipSet(getMe().getChips());
					this.m_LastTransfer = new ChipSet(chipsToTransfer);
				}
			}
			client.communication.sendTransferRequest(getOpponent().getPerGameId(), chipsToTransfer);
		}
		
		// Feedback phase
		if (phaseName.equals("Feedback Phase"))
		{
			debugPrint("Now in feedback phase");
			
			// Initialize everything
			this.m_LastTransfer = null;
			this.m_LastAcceptedProposal = null;
		}
		
		// Strategy prepreration phase
		if (phaseName.equals("Strategy Prep Phase"))
		{
			debugPrint("Now in strategy prep phase");
		}		
	}	
}
