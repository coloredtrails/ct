package ctagents.alternateOffersAgent;

import java.util.ArrayList;

import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.client.ClientGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;

/**
 * This class finds all the players paths to the goal and matches them up with
 * every possible path of the opponent that doesn't use the same chips. It is
 * used as a base for creating a feasible proposal since the pairs of paths
 * avoids conflicts between the players.
 * 
 * @author Yael Blumberg
 */
public class NegotiationPathsPairs {
	ArrayList<ArrayList<Path>> pathsPairs;

	public NegotiationPathsPairs(ClientGameStatus cgs, int oppId) {

		ChipSet myChips = cgs.getMyPlayer().getChips();
		ChipSet oppChips = cgs.getPlayerByPerGameId(oppId).getChips();
		RowCol goal = cgs.getBoard().getGoalLocations().get(0);

		createNegoPairs(cgs, oppId, myChips, oppChips, goal);
	}

	/*
	 * This constractor is used when needed to simulate a negotiation with chips
	 * different from the cureent players chips
	 */
	public NegotiationPathsPairs(ClientGameStatus cgs, int oppId,
			ChipSet myChips, ChipSet oppChips, RowCol goalPos) {
		createNegoPairs(cgs, oppId, myChips, oppChips, goalPos);
	}

	private void createNegoPairs(ClientGameStatus cgs, int oppId,
			ChipSet myChips, ChipSet oppChips, RowCol goal) {

		pathsPairs = new ArrayList<ArrayList<Path>>();

		// Getting all the possible paths to the goal of the current player
		ArrayList<Path> myShortestPaths = ShortestPaths.getShortestPaths(cgs
				.getMyPlayer().getPosition(), goal, cgs.getBoard(), cgs
				.getScoring(), ShortestPaths.NUM_PATHS_RELEVANT, myChips,
				oppChips);

		// Getting all the possible paths to the goal of the opponent
		ArrayList<Path> oppShortestPaths = ShortestPaths.getShortestPaths(cgs
				.getPlayerByPerGameId(oppId).getPosition(), goal, cgs
				.getBoard(), cgs.getScoring(),
				ShortestPaths.NUM_PATHS_RELEVANT, oppChips, myChips);

		// Going over my shortest path and the opponent's, and trying to pair
		// them up.
		for (Path myPossiblePath : myShortestPaths) {

			ChipSet requiredChipsForPath = myPossiblePath.getRequiredChips(cgs
					.getBoard());
			ChipSet myMissing = myChips.getMissingChips(requiredChipsForPath);
			ChipSet myExtra = myChips.getExtraChips(requiredChipsForPath);

			Path myPath = new Path(myPossiblePath);

			// go over opponent's shortest paths.
			// if opponent's path we are checking can spare my missing chips,
			// and I can spare its missing chip
			// then add that opponent's path to the list of feasible negotiation
			// paths for my current path
			for (Path opPossiblePath : oppShortestPaths) {

				ChipSet oppRequiredChipsForPath = opPossiblePath
						.getRequiredChips(cgs.getBoard());
				ChipSet opMissing = oppChips
						.getMissingChips(oppRequiredChipsForPath);
				ChipSet opExtra = oppChips
						.getExtraChips(oppRequiredChipsForPath);

				if (opExtra.contains(myMissing) && myExtra.contains(opMissing)) {
					Path opPath = new Path(opPossiblePath);
					ArrayList<Path> pair = new ArrayList<Path>();
					pair.add(myPath);
					pair.add(opPath);
					pathsPairs.add(pair);
				}
			}
		}
	}

	public Path getMyPath(int index) {
		return pathsPairs.get(index).get(0);
	}

	public Path getOpPath(int index) {
		return pathsPairs.get(index).get(1);
	}

	public int getPairsNo() {
		return pathsPairs.size();
	}
}