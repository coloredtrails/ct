package ctagents.alternateOffersAgent;

import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

public class Proposal {
		public ChipSet chipsToReceive = null;//chipsSentByResponder;
	    public ChipSet chipsToSend = null; //chipsSentByProposer;
	    
	    public Proposal(){
	    	chipsToReceive = new ChipSet();
	    	chipsToSend = new ChipSet();
	    }
	    
	    public Proposal(Proposal other){
	    	chipsToReceive = new ChipSet(other.chipsToReceive);
	    	chipsToSend = new ChipSet(other.chipsToSend);
	    }
	    
	    public Proposal(ChipSet recieve, ChipSet send){
	    	chipsToReceive = recieve;
	    	chipsToSend = send;
	    }
	    
		public String toString() {
			String str = "(";
			if (this.chipsToReceive != null)
			{
				str += (this.chipsToReceive.toString());
			}
			str += ", ";
			if (this.chipsToSend != null)
			{
				str += (this.chipsToSend.toString());
			}
			str += ")";
			return str;
		}
	    
		@Override
	    public boolean equals (Object o)
	    {
	    	Proposal otherProposal = (Proposal) o;
	    	if(otherProposal.chipsToReceive.equals(this.chipsToReceive) && otherProposal.chipsToSend.equals(this.chipsToSend))
	    		return true;
	    	return false;
	    }
	    
	    @Override
	    public int hashCode()
	    {
	    	return ((Double)(Math.pow(2, chipsToReceive.hashCode()) * Math.pow(3, chipsToSend.hashCode()))).intValue();
	    }
}