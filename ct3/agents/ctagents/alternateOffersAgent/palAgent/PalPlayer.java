package ctagents.alternateOffersAgent.palAgent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import weka.core.SerializationHelper;
import ctagents.FileLogger;
import ctagents.alternateOffersAgent.LearningHandler;
import ctagents.alternateOffersAgent.NegotiationPathsPairs;
import ctagents.alternateOffersAgent.Proposal;
import ctagents.alternateOffersAgent.ProposerResponderPlayer;
import ctagents.alternateOffersAgent.ProposerResponderPlayer.EndingReasons;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.client.ClientGameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;

import java.lang.Math;

public class PalPlayer extends ProposerResponderPlayer {

	public enum Culture {
		ISRAEL, USA, LEBANON
	};

	public enum BoardType {
		DD, TD, TI
	};
	

	private static final int MAX_EXCHANGE_CHIPS = 7;
	private static final int CHOOSE_OFFER_EPSILON = 10; // 10;
	private static final int ACCEPTANCE_EPSILON = 10;
	
	private static final double HIGH_GENEROUSITY = 1;
	private static final double MEDIUM_GENEROUSITY = 0.5;
	private static final double LOW_GENEROUSITY = 0;

	private static final String DEPENDENT = "TD";
	private static final String INDEPENDENT = "TI";

	private static final int ES_CALC_DEPTH = 2;

	private PalLearningHandler learningEngine;
	private ChipSet bestChipsToSend;

	private Culture oppCulture;
	private BoardType oppBoardType = BoardType.DD;;
	private String cultureFolder = "agents/ctagents/alternateOffersAgent/palAgent/";
	private double[] heuristicExchangeConsts;
	private boolean palLog = false;

	private Random rand;

	// TI or TD
	private String myRole;
	private String opRole;

	private RowCol goalLoc;

	// All existing score quartets (myCur, myResulting, oppCur, opResulting) in
	// the training data
	private ArrayList<double[]> scoreQuartets;

	private static String TRANSFER_HEADLINES = "TRANSFER,chipsToSend,chipsToReceive,,,,myCurrentScore,myResultingScore,opCurrentScore,"
			+ "opResultingScore,myWPR,opWPR,probFulfill,probNotFulfill";

	private static String ARG_HEADLINES = "ARG,proposed:toSend,proposed:toReceive,accept,planed:toSend,planed:toReceive,myCurrentScore,"
			+ "opCurrentScore,myRole,opRole,myPR,opPR,probReachedGoal,probNotReachedGoal,ES";

	private static String ACCEPT_HEADLINES = "ACCEPT,chipsToSend,chipsToReceive,,,,myCurrentScore,myResultingScore,opCurrentScore"
			+ ",opResultingScore,generosityLevel,probabilityAccept";

	private static PrintWriter csvLog;
	private String csvLogName = "CT_DbgLog_Agent_";
	private String logNamePrefix;
	DecimalFormat df;

	/*
	 * This class is used to hold the ected score and the proposal and
	 * transfer details that will result in that score
	 */
	private class ectedScoreData implements Comparable<ectedScoreData> {
		public double es;
		public ChipSet csToSend;
		public Proposal offer;

		public int compareTo(ectedScoreData other) {
			if (es == other.es) {
				return 0;
			} else if (es > other.es) {
				return 1;
			} else {
				return -1;
			}
		}
	}

	/*
	 * This class is used to hold a hush table of the ected scores that were
	 * already computed for a given planned transfer and the PR computed for the
	 * agent as a result of this transfer.
	 */
	private class State {
		private ChipSet toSend;
		private ChipSet toRecieve;
		private double resultPR;

		public State(ChipSet send, ChipSet receive, double PR) {
			toSend = send;
			toRecieve = receive;
			resultPR = PR;
		}

		public State(State copy) {
			toSend = new ChipSet(copy.toSend);
			toRecieve = new ChipSet(copy.toRecieve);
			resultPR = copy.resultPR;
		}

		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(resultPR);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result
					+ ((toRecieve == null) ? 0 : toRecieve.hashCode());
			result = prime * result
					+ ((toSend == null) ? 0 : toSend.hashCode());
			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			State other = (State) obj;
			if (Double.doubleToLongBits(resultPR) != Double
					.doubleToLongBits(other.resultPR))
				return false;
			if (toRecieve == null) {
				if (other.toRecieve != null)
					return false;
			} else if (!toRecieve.equals(other.toRecieve))
				return false;
			if (toSend == null) {
				if (other.toSend != null)
					return false;
			} else if (!toSend.equals(other.toSend))
				return false;
			return true;
		}
	}

	/*
	 * This class is used to save results of computations for specific states.
	 * The ES are saved separately since the computation that units them uses
	 * the fulfill probability that may change from one proposal to another
	 */
	private class BaseES {
		double esFulfill;
		double esNoFulfill;
	}

	/*
	 * This class was created for convenience reasons, so we won't have to pass
	 * four variables between all the methods that participate in the recursive
	 * calculation of the ected score.
	 */
	private class ReliabilityData {
		public double myPR;
		public double opPR;
		public double myWPR;
		public double opWPR;

		/*
		 * Calculates the Reliabilities as they are right now
		 */
		public ReliabilityData() {
			myWPR = learningEngine.getWeightedPreviousReliability(getMyID());
			opWPR = learningEngine.getWeightedPreviousReliability(getOpponent()
					.getPerGameId());
			myPR = learningEngine.getPreviousReliability(getMyID());
			opPR = learningEngine.getPreviousReliability(getOpponent()
					.getPerGameId());
		}

		public ReliabilityData(Proposal offered, Proposal toBeDone,
				ChipSet myChips, ChipSet opChips) {
			updateReliability(offered, toBeDone, myChips, opChips);
		}

		/*
		 * Updates the reliabilities according to a deal that was set to happen
		 */
		public void updateReliability(Proposal offered, Proposal toBeDone,
				ChipSet myChips, ChipSet opChips) {
			myPR = learningEngine.getHypotheticPR(getMyID(),
					offered.chipsToSend, toBeDone.chipsToSend, opChips);
			opPR = learningEngine.getHypotheticPR(getOpponent().getPerGameId(),
					offered.chipsToReceive, toBeDone.chipsToReceive, myChips);
			/* myWPR = learningEngine.calcWPR(learningEngine.getWeightedPreviousReliability(getMyID()), myPR);
			opWPR = learningEngine.calcWPR(learningEngine.getWeightedPreviousReliability(getOpponent()
					.getPerGameId()), opPR);*/
			myWPR = learningEngine.calcWPR(myWPR, myPR);
			opWPR = learningEngine.calcWPR(opWPR, opPR);
			
		}
	}

	public Culture getOppCulture() {
		return oppCulture;
	}
	
	@SuppressWarnings("unchecked")
	public PalPlayer(int boardType, int culture) {
		super();

		// Saving the basic log name
		logNamePrefix = logName;

		// Getting the opponents culture
		if (culture == Culture.ISRAEL.ordinal()) {
			oppCulture = Culture.ISRAEL;
		} else if (culture == Culture.USA.ordinal()) {
			oppCulture = Culture.USA;
		} else if (culture == Culture.LEBANON.ordinal()) {
			oppCulture = Culture.LEBANON;
		}

//		// Getting the board type 
//		if (boardType == BoardType.DD.ordinal()) {
//			oppBoardType = BoardType.DD;
//		} else if (boardType == BoardType.TD.ordinal()) {
//			oppBoardType = BoardType.TD;
//		} else if (boardType == BoardType.TI.ordinal()) {
//			oppBoardType = BoardType.TI;
//		}
		
		// Getting the board type: each condition has 2 numbers for friends and stranger
		if ((boardType == 110) || (boardType == 310)){
			oppBoardType = BoardType.DD;
		} else if ((boardType == 510)  || (boardType == 610)){
			oppBoardType = BoardType.TD;
		} else if ((boardType == 210) || (boardType == 410)) {
			oppBoardType = BoardType.TI;
		}

		// Updating the culture folder name
		cultureFolder += oppCulture.toString();
		// Updating the boardType folder name
		cultureFolder += "/"+oppCulture.toString()+"_"+oppBoardType.toString();
		
		WekaWrapper.setCultureFolder(cultureFolder);
		WekaWrapper.setCulture(oppCulture);

		// Getting the score quartets
		try {
			scoreQuartets = (ArrayList<double[]>) SerializationHelper
					.read(cultureFolder + "/scoreQuartets.db");
		} catch (Exception e1) {
			scoreQuartets = null;
			System.out.println("Problem reading the score quartets file");
			e1.printStackTrace();
		}

		// Opening the csv log file to write all the weka parameters to it
		String timeStemp = String.valueOf((new Date()).getTime());
		csvLogName += timeStemp + ".csv";
		df = new DecimalFormat("##.####");

		try {
			csvLog = new PrintWriter(csvLogName);
			csvLog.println("model type");
			csvLog.println(TRANSFER_HEADLINES);
			csvLog.println(ARG_HEADLINES);
			csvLog.println(ACCEPT_HEADLINES);
		} catch (FileNotFoundException e) {
			System.out.println("Problem opening csv file named: " + csvLog);
			e.printStackTrace();
			csvLog = null;
		}

		heuristicExchangeConsts = getCultureConstants();

		// Creating the random seed for randomization needs
		rand = new Random();
	}


	
	// These get methods exists because we can't calculate it in gameStarted
	// cause not all the board data is initialized till then. After the first
	// init it will update every movement phase
	public String getMyRole() {
		if (myRole == null) {
			updateRoles();
		}

		return myRole;
	}

	public String getOpRole() {
		if (opRole == null) {
			updateRoles();
		}
		return opRole;
	}

	/*
	 * Saving the goal location once for future use (for performance reasons)
	 */
	private RowCol getGoalLocation() {
		if (goalLoc == null) {
			goalLoc = client.getGameStatus().getBoard().getGoalLocations()
					.get(0);
		}

		return goalLoc;
	}

	/*
	 * According to the culture of the opponent, constant relevant to the
	 * heuristic score function are extracted from a file
	 */
	private double[] getCultureConstants() {

		double[] constants = { 1, 1, 1 };

		String fileName = cultureFolder + "/Constants.txt";
		try {
			BufferedReader bf = new BufferedReader(new FileReader(fileName));
			String curLine;
			String[] tokens;

			while ((curLine = bf.readLine()) != null) {

				if (curLine.startsWith("reachGoal_AgentTI")) {
					tokens = curLine.split("=");
					constants[0] = Double.parseDouble(tokens[1].trim());
				} else if (curLine.startsWith("reachGoal_AgentTD")) {
					tokens = curLine.split("=");
					constants[1] = Double.parseDouble(tokens[1].trim());

				} else if (curLine.startsWith("cantReachGoal_HumanTI")) {
					tokens = curLine.split("=");
					constants[2] = Double.parseDouble(tokens[1].trim());
				}
			}

//			FileLogger.getInstance(logName).writelnNoFlush(
//					"Using the following constants: [" + constants[0] + "],["
//							+ constants[1] + "],[" + constants[2] + "]");

			bf.close();

		} catch (FileNotFoundException e) {
			FileLogger.getInstance(logName).writelnNoFlush(
					"Problem opening file: " + fileName
							+ " Using value 1 for all constants");
		} catch (NumberFormatException e) {
			FileLogger.getInstance(logName).writelnNoFlush(
					"Wrong format of file: " + fileName
							+ " Using value 1 for all constants");
		} catch (IOException e) {
			FileLogger.getInstance(logName).writelnNoFlush(
					"Problem reading file: " + fileName
							+ " Using value 1 for all constants");
		}

		return constants;
	}

	public void gameStarted() {

		super.gameStarted();

		// This is done here because only now we have an initialized gameStatus
		// object
		learningEngine = new PalLearningHandler(client.getGameStatus(),
				client.getPerGameID(), cultureFolder);
		bestChipsToSend = null;
	}

	private void updateRoles() {

		// Getting the players roles with their current chips
		myRole = calcRole(getMyID(), client.getChips());
		opRole = calcRole(getOpponent().getPerGameId(), getOpponent()
				.getChips());
	}

	private String calcRole(int playerId, ChipSet playerChips) {
		String role = DEPENDENT;

		// Calculating the players score with the given chipSet
		// double score = FixedPathScoreUtil.getInstance(client.getGameStatus())
		// .getPlayerScore(playerChips, playerId);

		// Checking if the players can reach the goal at the current state
		// if (score > scoring.goalweight) {
		// {
		// role = INDEPENDENT;
		// }
		role = FixedPathScoreUtil.getInstance(client.getGameStatus())
				.getPlayerRole(playerChips, playerId);

		return role;
	}

	private double getHeuristicScore(boolean reachedGoal, ChipSet myChips,
			ChipSet opChips, String myRole, String opRole, boolean isMe) {

		// Jonathan Bar Or
		// Added the boolean which specifies whether it's my score or the opponent's
		// If it's false it should be from the opponent's point of view, so "myChips"
		// is the opponent's chips, etc..
		RowCol myPos = null;
		RowCol opPos = null;
		if (isMe)
		{
			myPos = client.getGameStatus().getMyPlayer().getPosition();
			opPos = getOpponent().getPosition();
		}
		else
		{
			opPos = client.getGameStatus().getMyPlayer().getPosition();
			myPos = getOpponent().getPosition();
		}

		// Computing my distance from the goal
		int myGoalDist = Math.abs(myPos.row - getGoalLocation().row)
				+ Math.abs(myPos.col - getGoalLocation().col);

		double score;

		if (reachedGoal) {

			// If I will reach the goal, I will definitely have goal score
			// plus number of chips left after using the amount needed
			score = scoring.goalweight + (myChips.getNumChips() - myGoalDist)
					* scoring.chipweight;

			if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
					"ReachedGoal: score = goalweight+(NumChips-GoalDist)*chipweight = "
							+ score + "=" + scoring.goalweight + " +"
							+ (myChips.getNumChips() - myGoalDist) + "*"
							+ scoring.chipweight);

			// If I'm TI and the opponent is TD, I will probably take more chips
			// from him in order to let him get to the goal
			if (myRole.equals("TI") && opRole.equals("TD")) {

				// Calculating how many chips the opponent is missing to reach
				// the goal (assuming only one available path)
				Path opPath = ShortestPaths.getShortestPaths(opPos,
						getGoalLocation(), client.getGameStatus().getBoard(),
						scoring, ShortestPaths.NUM_PATHS_RELEVANT, opChips,
						myChips).get(0);

				int opMissingChips = opChips.getMissingChips(
						opPath.getRequiredChips(client.getGameStatus()
								.getBoard())).getNumChips();

				score += opMissingChips * heuristicExchangeConsts[0]
						* scoring.chipweight;

				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"ReachedGoal: MyRole: TI, opRole: TD. NewScore(plus) ="
								+ score + "(opMissingChips*h[0]*chipweight = "
								+ opMissingChips + "*"
								+ heuristicExchangeConsts[0] + "*"
								+ scoring.chipweight + ")");

				// If I'm TD and the opponent is TI, I will probably give him
				// more chips in order to get to the goal
			} else if (myRole.equals("TD") && opRole.equals("TI")) {

				// Calculating how many chips I'm missing to reach
				// the goal (assuming only one available path)
				Path myPath = ShortestPaths.getShortestPaths(myPos,
						getGoalLocation(), client.getGameStatus().getBoard(),
						scoring, ShortestPaths.NUM_PATHS_RELEVANT, myChips,
						opChips).get(0);

				int myMissingChips = myChips.getMissingChips(
						myPath.getRequiredChips(client.getGameStatus()
								.getBoard())).getNumChips();

				score -= myMissingChips * heuristicExchangeConsts[1]
						* scoring.chipweight;

				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"ReachedGoal: MyRole: TD, opRole: TI. NewScore(minus) ="
								+ score + "(myMissingChips*h[1]*chipweight = "
								+ myMissingChips + "*"
								+ heuristicExchangeConsts[1] + "*"
								+ scoring.chipweight + ")");

			}

		} else {

			// Basically, if I'm not reaching the goal, and both of us are TD,
			// the score will probably won't change significantly
			score = FixedPathScoreUtil
					.getInstance(client.getGameStatus())
					.getPlayerScore(myChips,
							(isMe ? client.getGameStatus().getMyPlayer().getPerGameId() : getOpponent().getPerGameId()));

			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
					"NotReachedGoal: score = " + score);

			// If I'm TD and the opponent is TI, I will probably give him
			// chips in order to get the chips I need from him (which he didn't
			// give). We are not checking the opposite case (I'm TI) because it
			// can't be that I'm TI and I'm not reaching the goal
			if (myRole.equals("TD") && opRole.equals("TI")) {

				// Calculating how many chips I'm missing to reach
				// the goal (assuming only one available path)
				Path myPath = ShortestPaths.getShortestPaths(myPos,
						getGoalLocation(), client.getGameStatus().getBoard(),
						scoring, ShortestPaths.NUM_PATHS_RELEVANT, myChips,
						opChips).get(0);

				int myMissingChips = myChips.getMissingChips(
						myPath.getRequiredChips(client.getGameStatus()
								.getBoard())).getNumChips();
				score -= myMissingChips * heuristicExchangeConsts[2]
						* scoring.chipweight;

				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"NotReachedGoal: MyRole: TD, opRole: TI. NewScore(minus) ="
								+ score + "(myMissingChips*h[2]*chipweight = "
								+ myMissingChips + "*"
								+ heuristicExchangeConsts[2] + "*"
								+ scoring.chipweight + ")");

			}
		}

		return score;
	}

	/*
	 * Calculate the ected score with a given chipset and the probabilities
	 * to reach the goal
	 */
	private double calcectedScore(Boolean accepted, Proposal offered,
			Proposal toBeDone) {

		ChipSet playerChips = client.getChips();
		ChipSet opponentChips = getOpponent().getChips();
		ReliabilityData relData = new ReliabilityData();
		
		if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush
				("calcectedScore:"+"myWPR="+relData.myWPR+",opWPR="+relData.opWPR);
		return calcNextRoundectedScore(accepted, offered, toBeDone,
				playerChips, opponentChips, relData, ES_CALC_DEPTH);
	}

	private double calcNextRoundectedScore(Boolean accepted,
			Proposal offered, Proposal toBeDone, ChipSet playerChips,
			ChipSet opponentChips, ReliabilityData curRelData, int roundsNo) {

		if (palLog)
		FileLogger.getInstance(logName).writelnNoFlush(
				"Calculating ES for n+" + roundsNo);

		// If the rounds number is 1 it means we simply want the next round
		if (roundsNo == 1) {

			return calcectedScore(accepted, offered, toBeDone, playerChips,
					opponentChips, curRelData.myPR, curRelData.opPR);
		} else {

			// Coping the reliabilityData by reference, in case we change the
			// data, we have to create a new object so we won't change the
			// original object.
			ReliabilityData newRelData = curRelData;

			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush
				("calcNextRoundectedScore:"+"myWPR="+newRelData.myWPR+",opWPR="+newRelData.opWPR);
		
			// In case the offer is accepted the chips amount will change
			if (accepted) {
				playerChips = ChipSet.addChipSets(playerChips,
						toBeDone.chipsToReceive);
				playerChips.subChipSet(toBeDone.chipsToSend);

				opponentChips = ChipSet.addChipSets(opponentChips,
						toBeDone.chipsToSend);
				opponentChips.subChipSet(toBeDone.chipsToReceive);

				// The reliability data has also changed
				newRelData = new ReliabilityData(offered, toBeDone,
						playerChips, opponentChips);
							if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush
				("accepted: calcNextRoundectedScore:"+"myWPR="+newRelData.myWPR+",opWPR="+newRelData.opWPR);

			}

			// Getting the next offer that the agent will offer at the next
			// round
			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
					"Generating hypothtical offer for round n+"
							+ (roundsNo - 1));
			ectedScoreData nextOffer = generateBestProposal(playerChips,
					opponentChips, newRelData, roundsNo - 1);

			return nextOffer.es;
		}
	}

	
	
	
	
	
	
	// Jonathan Bar Or
	// Adding a wrapper method for getting the reversed proposal
	// This is a hack done in order to have the goal reach probability for the opponent
	private static Proposal getReversedProposal(Proposal proposal)
	{
		if (null == proposal)
			return null;
		return new Proposal(proposal.chipsToSend, proposal.chipsToReceive);
	}
	
	/*
	 * This is the actual formula for the ected score. It is based on the
	 * probability to reach the goal and a heuristic function that calculates
	 * the final score for each state
	 */
	private double calcectedScore(Boolean accepted, Proposal offered,
			Proposal toBeDone, ChipSet playerChips, ChipSet opponentChips,
			double myPR, double opPR) {

		// Jonathan Bar Or
		// Adding the opponent's probability of reaching the goal
		// It will use the same model as the agent's
		// There is a hack here that reversed the proposals in order to use the
		// same method, but from the opponent's point of view.
		double[] myProbReachGoal = getReachGoalProbs(accepted, offered, toBeDone,
				playerChips, opponentChips, myPR, opPR);
		double[] opProbReachGoal = getReachGoalProbs(accepted, getReversedProposal(offered), getReversedProposal(toBeDone),
				opponentChips, playerChips, opPR, myPR);		
		
		// If this is an accepted offer we need to update the chips as if the
		// exchange was made
		if (accepted) {
			playerChips = ChipSet.addChipSets(playerChips,
					toBeDone.chipsToReceive);
			playerChips.subChipSet(toBeDone.chipsToSend);
			opponentChips = ChipSet.addChipSets(opponentChips,
					toBeDone.chipsToSend);
			opponentChips.subChipSet(toBeDone.chipsToReceive);
		}

		// Calculating the roles according to the new chips status
		String myRole = calcRole(getMyID(), playerChips);
		String opRole = calcRole(getOpponent().getPerGameId(), opponentChips);

		// Jonathan Bar Or
		// Adding my ected score and op ected score and return their weighted sum
		double myEctedScore = myProbReachGoal[1] * getHeuristicScore(true, playerChips, opponentChips, myRole, opRole, true)
				+ myProbReachGoal[0] * getHeuristicScore(false, playerChips, opponentChips, myRole, opRole, true);
		double opEctedScore = opProbReachGoal[1] * getHeuristicScore(true, opponentChips, playerChips, opRole, myRole, true)
				+ opProbReachGoal[0] * getHeuristicScore(false, opponentChips, playerChips, opRole, myRole, false);
		
		// Jonathan Bar Or
		// The weighted sum
		double myWeight = 0.9;
		double ectedScore = myWeight * myEctedScore + (1.0 - myWeight) * opEctedScore;
		

		// Adding the ected score to the and of the line
		if (csvLog != null) {
			csvLog.println("," + ectedScore);
		}

		return ectedScore;
	}

	/*
	 * This function checks for a specific player and a specific exchange if the
	 * exchange is the one that will allow the movement. Therefore this function
	 * returns: 1) true, if the movement was allowed anyway without the exchange
	 * 2) false, if the movement is not allowed before the exchange and also not
	 * allowed after the exchange. 3) true, if the movement is allowed as a
	 * result of this exchange.
	 */
	private boolean willExchangeAllowToMove(ChipSet playerChips , RowCol ppos,ChipSet chipsAfterExchange ) {

		boolean bWillAllow = false;
		boolean bCanMoveToANeigb = false;
		Board gameBoard = client.getGameStatus().getBoard();

//		ChipSet playerChips = player.getChips();
//		RowCol ppos = player.getPosition();
		LinkedHashSet<RowCol> neighbors = (LinkedHashSet<RowCol>) ppos
				.getNeighbors(gameBoard);

		// Checking if the player was able to move without the exchange
		for (RowCol rc : neighbors) {
			String color = gameBoard.getSquare(rc).getColor();

			if (playerChips.getNumChips(color) > 0) {
				bCanMoveToANeigb = true;
				break;
			}
		}

		// THIS IS DIFFERENT FROM THE AGENTS FUNCTION.
		// In the agent the function returns false in this condition
		if (bCanMoveToANeigb) {
			bWillAllow = true;
		} else {

			// Now check if we can move if the exchange was accepted:
			for (RowCol rc : neighbors) {
				String color = gameBoard.getSquare(rc).getColor();
				if (chipsAfterExchange.getNumChips(color) > 0) {
					bWillAllow = true;
					break;
				}
			}
		}
		return bWillAllow;
	}
	
	/*
	 * Calculates the probability to get to the goal after an hypothetical move
	 * (acceptance, offer, transfer)
	 */
	private double[] getReachGoalProbs(boolean accepted, Proposal proposed,
			Proposal chosenExchange, ChipSet myChips, ChipSet opChips,
			double myPrevReliability, double opPrevReliability) {
		double[] probabilitiesReachedGoal = { 0, 0 };

		ChipSet myNewChips;
		ChipSet opNewChips;

		if (accepted) {
			// Getting the new chips of the agent and the human according to the
			// chosen move
			myNewChips = ChipSet.addChipSets(myChips,
					chosenExchange.chipsToReceive);
			myNewChips.subChipSet(chosenExchange.chipsToSend);
			opNewChips = ChipSet.addChipSets(opChips,
					chosenExchange.chipsToSend);
			opNewChips.subChipSet(chosenExchange.chipsToReceive);

			// Calculating the reliability that will result from this move
			myPrevReliability = learningEngine.getHypotheticPR(getMyID(),
					proposed.chipsToSend, chosenExchange.chipsToSend, opChips);
			opPrevReliability = learningEngine.getHypotheticPR(getOpponent()
					.getPerGameId(), proposed.chipsToReceive,
					chosenExchange.chipsToReceive, opChips);
		} else {
			// If the offer was rejected the chips won't change (and the PR as
			// well so it doesn't change)
			myNewChips = myChips;
			opNewChips = opChips;
		}
		boolean willExchangeAllowMovementAgent = willExchangeAllowToMove(
				myChips, client.getGameStatus().getMyPlayer().getPosition(),
				myNewChips);
		boolean willExchangeAllowMovementOther = willExchangeAllowToMove(
				opChips, getOpponent().getPosition(),opNewChips);

		double myWPR = learningEngine.getWeightedPreviousReliability(getMyID());
		double myHypotheticalWPR = learningEngine.calcWPR(myWPR, myPrevReliability);
		double oppWPR = learningEngine.getWeightedPreviousReliability(getOpponent().getPerGameId());
		double oppHypotheticalWPR = learningEngine.calcWPR(oppWPR, opPrevReliability);

		// Getting the players current score if the game was ending after this
		// move.
		double myResultinScore = FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getPlayerScore(myNewChips, getMyID());
		double opResultinScore = FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getPlayerScore(opNewChips,
				getOpponent().getPerGameId());

		// Calculating the roles according to these scores
		String myResultingRole = DEPENDENT;
		String opResultingRole = DEPENDENT;

		// if (myResultinScore > scoring.goalweight) {
		myResultingRole = FixedPathScoreUtil
				.getInstance(client.getGameStatus()).getPlayerRole(myNewChips,
						getMyID());
		// myResultingRole = INDEPENDENT;
		// }

		// if (opResultinScore > scoring.goalweight) {
		// opResultingRole = INDEPENDENT;
		// }
		opResultingRole = FixedPathScoreUtil
				.getInstance(client.getGameStatus()).getPlayerRole(opNewChips,
						getOpponent().getPerGameId());

		
		// if the game is about to end (reaching max dormant rounds), the ARG will be zero-the agent won't reach the goal
		EndingReasons isGameAboutToEnd = EndingReasons.NOT_ENDING;
		isGameAboutToEnd = isGameAboutToEnd();
		double opDormantRounds = (Integer)getOpponent().get("opConsecutiveNoMovement")+1;
		double myDormantRounds = (double)consecutiveNoMovements+1;
		
		if (palLog)
		FileLogger.getInstance(logName).writelnNoFlush(
		"isGameAboutToEnd: " + isGameAboutToEnd + "AgentMove" + willExchangeAllowMovementAgent + 
		"OtherMove" + willExchangeAllowMovementOther + "myNewChips" + myNewChips);
		if (myResultingRole == INDEPENDENT) {
			// if myResultingRole is TI, there is no need to calculate the reach
			// goal, since the ARG probability should be 1
			probabilitiesReachedGoal[1] = 1;
			probabilitiesReachedGoal[0] = 0;

//			double h_RG = 1-(Math.exp((-1)*myHypotheticalWPR));
//				FileLogger.getInstance(logName).writelnNoFlush(
//				"instanceValuesARG: oppHypotheticalWPR(myWPR,PR) = "+myHypotheticalWPR+"("+myWPR+","+myPrevReliability+"). Update P(RG=T). Orig = "+probabilitiesReachedGoal[1]+
//				", h_RG = "+h_RG);
//				// Update the reach goal probability according to the prev reliability of the agent
//				probabilitiesReachedGoal[1] = h_RG;
//				probabilitiesReachedGoal[0] = 1 - probabilitiesReachedGoal[1];
		} 
		// Checking how the exchange will effect the ability of the players to
		// move
		else if ((isGameAboutToEnd == EndingReasons.OPP_ONE_STEP_FROM_GOAL)||
				(isGameAboutToEnd == EndingReasons.I_HAVE_TO_MOVE
					&& !willExchangeAllowMovementAgent)||
				(isGameAboutToEnd == EndingReasons.OPP_HAS_TO_MOVE
					&& !willExchangeAllowMovementOther)||
				(isGameAboutToEnd == EndingReasons.BOTH_HAVE_TO_MOVE
				&& (!willExchangeAllowMovementAgent || !willExchangeAllowMovementOther))){
			
				probabilitiesReachedGoal[0] = 1;
				probabilitiesReachedGoal[1] = 0;
		}
		else {
			// Calculating the probability to reach the goal
			if (getOppCulture() == Culture.LEBANON)
			{
				if ((isGameAboutToEnd == EndingReasons.OPP_HAS_TO_MOVE)||
					(isGameAboutToEnd == EndingReasons.BOTH_HAVE_TO_MOVE))
				{
					probabilitiesReachedGoal[0] = 1;
					probabilitiesReachedGoal[1] = 0;
				}
				else {
				Object[] instanceValuesAgentReachGoalLebanon = { myResultinScore,
						opResultinScore, myResultingRole, opResultingRole,
						myPrevReliability, opPrevReliability, myDormantRounds, opDormantRounds, "TRUE" };
				try {
					probabilitiesReachedGoal = WekaWrapper
							.getAgentReachGoalProbabilities(instanceValuesAgentReachGoalLebanon);

				} catch (Exception e) {
					e.printStackTrace();
				}
				}
			}
			else
			{
			Object[] instanceValuesAgentReachGoal = { myResultinScore,
					opResultinScore, myResultingRole, opResultingRole,
					myPrevReliability, opPrevReliability, "TRUE" };
			try {
				probabilitiesReachedGoal = WekaWrapper
						.getAgentReachGoalProbabilities(instanceValuesAgentReachGoal);

			} catch (Exception e) {
				e.printStackTrace();
			}
			}
			if (probabilitiesReachedGoal[1] != 0)
			{
			double h_RG = 1-(Math.exp((-1)*myHypotheticalWPR));
			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
			"instanceValuesARG: oppHypotheticalWPR(myWPR,PR) = "+myHypotheticalWPR+"("+myWPR+","+myPrevReliability+"). Update P(RG=T). Orig = "+probabilitiesReachedGoal[1]+				", h_RG = "+h_RG);
			// Update the reach goal probability according to the prev reliability of the agent
			probabilitiesReachedGoal[1] = probabilitiesReachedGoal[1]*h_RG;
			probabilitiesReachedGoal[0] = 1 - probabilitiesReachedGoal[1];
			}
		}
		if (palLog)
		FileLogger.getInstance(logName).writelnNoFlush(
				"instanceValuesARG: myResultinScore" + myResultinScore
						+ " opResultinScore: " + opResultinScore + " myRole: "
						+ myResultingRole + " opRole: " + opResultingRole
						+ " myPR: " + df.format(myPrevReliability) + " opPR: "
						+ df.format(opPrevReliability)
						+ " myDormant: " + myDormantRounds + " opDormant:" + opDormantRounds
						+ " probabilitiesAgentReachedGoal {"
						+ df.format(probabilitiesReachedGoal[0]) + "},{"
						+ df.format(probabilitiesReachedGoal[1]) + "}");

		if (csvLog != null) {
			// Leaving the line open in order to add the ES at the end of it
			csvLog.print("ARG,"
					+ (accepted ? proposed.chipsToSend.removeZeros() : "")
					+ ","
					+ (accepted ? proposed.chipsToReceive.removeZeros() : "")
					+ ","
					+ accepted
					+ ","
					+ (accepted ? chosenExchange.chipsToSend.removeZeros() : "")
					+ ","
					+ (accepted ? chosenExchange.chipsToReceive.removeZeros()
							: "") + "," + myResultinScore + ","
					+ opResultinScore + "," + myResultingRole + ","
					+ opResultingRole + "," + df.format(myPrevReliability)
					+ "," + df.format(opPrevReliability) + ","
					+ df.format(probabilitiesReachedGoal[1]) + ","
					+ df.format(probabilitiesReachedGoal[0]));
		}

		return probabilitiesReachedGoal;
	}

	/*
	 * This method gets a proposal and finds the best subset of chips to send in
	 * order to get the best ected result
	 */
	private ectedScoreData findBestectedScore(Proposal offer) {

		// Getting the reliability data
		ReliabilityData curRelData = new ReliabilityData();
		Hashtable<State, BaseES> checkedStates = new Hashtable<State, BaseES>();
			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush
				("findBestectedScore:"+"myWPR="+curRelData.myWPR+",opWPR="+curRelData.opWPR);

		return findBestectedScore(offer, client.getChips(), getOpponent()
				.getChips(), curRelData, ES_CALC_DEPTH, checkedStates);
	}

	private ectedScoreData findBestectedScore(Proposal offer,
			ChipSet myChips, ChipSet opChips, ReliabilityData relData,
			int roundOfEs, Hashtable<State, BaseES> checkedStates) {

		if (palLog)
		FileLogger.getInstance(logName).writelnNoFlush(
				"findBestectedScore: Sent by op: " + offer.chipsToReceive
						+ "sent by me: " + offer.chipsToSend);

		// Getting the players current score if the game was ending now.
		double myCurrentScore = FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getPlayerScore(myChips, getMyID());
		double opCurrentScore = FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getPlayerScore(opChips,
				getOpponent().getPerGameId());

		// Calculating the resulting score of both players if the agreement was
		// fulfilled by both sides
		ChipSet myCSFulfill = ChipSet
				.addChipSets(myChips, offer.chipsToReceive);
		myCSFulfill.subChipSet(offer.chipsToSend);
		ChipSet opCSFulfill = ChipSet.addChipSets(opChips, offer.chipsToSend);
		opCSFulfill.subChipSet(offer.chipsToReceive);

		double myResultingScoreFulfill = FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getPlayerScore(myCSFulfill, getMyID());
		double opResultingScoreFulfill = FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getPlayerScore(opCSFulfill,
				getOpponent().getPerGameId());

		// Getting the number of needed and other chips
		double playerNeededColors = 0;
		for (String color : FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getNeededColors(getMyID())) {
			if (color.equals("CTYellow") || color.equals("grey78") )
				playerNeededColors += offer.chipsToReceive.getNumChips(color);
		}
		double playerOtherColors = offer.chipsToReceive.getNumChips()
				- playerNeededColors;

		double oppNeededColors = 0;
		for (String color : FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getNeededColors(
				getOpponent().getPerGameId())) {
			// TODO meanwhile ask only of yellow and gray chips. 
			//This is not a general solution. The optimal solution is to have 3 sets for each sides: 
			//	needed chips (from the proposal), missing chips (from the board) and others 
			if (color.equals("CTYellow") || color.equals("grey78") )
				oppNeededColors += offer.chipsToSend.getNumChips(color);
		}
		double oppOtherColors = offer.chipsToSend.getNumChips() - oppNeededColors;


		// Calculating the probabilities for the opponent to fulfill the
		// agreement
		double[] probabilitiesFulfill = { 0 };
		Object[] instanceValuesFulfill = { relData.myWPR, relData.opWPR,
				myCurrentScore, myResultingScoreFulfill, opCurrentScore,
				opResultingScoreFulfill, playerNeededColors, playerOtherColors,
				oppNeededColors, oppOtherColors, "1" };
		try {

			probabilitiesFulfill = WekaWrapper
					.getTransferProbabilities(instanceValuesFulfill);

			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
					"transferInstanceValues: myCurrentScore: " + myCurrentScore
							+ " myResultingScore: " + myResultingScoreFulfill
							+ " opCurrentScore: " + opCurrentScore
							+ " opResultingScore: " + opResultingScoreFulfill
							+ " myNeededColors: " + playerNeededColors
							+ " myOtherColors: " + playerOtherColors
							+ " oppNeededColors: " + oppNeededColors
							+ " oppOtherColors: " + oppOtherColors + " myWPR: "
							+ df.format(relData.myWPR) + " (PR: "
							+ df.format(relData.myPR) + ") opWPR: "
							+ df.format(relData.opWPR) + " (PR: "
							+ df.format(relData.opPR)
							+ ") probabilitiesFulfill {"
							+ df.format(probabilitiesFulfill[0]) + "},{"
							+ df.format(probabilitiesFulfill[1]) + "}");

			if (csvLog != null) {
				csvLog.println("TRANSFER," + offer.chipsToSend.removeZeros()
						+ "," + offer.chipsToReceive.removeZeros() + ",,,,"
						+ myCurrentScore + "," + myResultingScoreFulfill + ","
						+ opCurrentScore + "," + opResultingScoreFulfill + ","
						+ df.format(relData.myWPR) + ","
						+ df.format(relData.opWPR) + ","
						+ df.format(probabilitiesFulfill[1]) + ","
						+ df.format(probabilitiesFulfill[0]));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Get all subsets of the chips the were promised to be sent by the
		// agent
		Set<ChipSet> myPlayerSubsets = ChipSet
				.getUniquePowerSet(offer.chipsToSend);
		if (palLog) {
		FileLogger.getInstance(logName).writelnNoFlush(
				"Total number of myPlayerSubsets: " + myPlayerSubsets.size());
		FileLogger.getInstance(logName).writelnNoFlush("");
		}
		ectedScoreData bestectedScore = new ectedScoreData();
		bestectedScore.offer = offer;
		double curectedScore = 0;
		int subsetNo = 1;

		// Checking which subset will maximize the ected score
		for (ChipSet cs : myPlayerSubsets) {

			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
					"Calculating subset no: " + subsetNo);
			++subsetNo;

			// Checking if this combination of chips sent by me ,chips ected
			// from the opponent and my resulting PR was calculated
			// before. If so, skipping the calculation. Otherwise, adding it to
			// the list and calculating
			double myResultingPR = learningEngine.getHypotheticPR(getMyID(),
					offer.chipsToSend, cs, opChips);
			State curState = new State(cs, offer.chipsToReceive, myResultingPR);

			BaseES ectedScores;

			if (checkedStates.containsKey(curState)) {
				// Getting the saved result and skipping the calculation
				ectedScores = checkedStates.get(curState);

				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"Calculation for sending: " + cs + "and receiving: "
								+ offer.chipsToReceive
								+ " was done in the past.");
			} else {

				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"Calculating ES for sending: " + cs + "and receiving: "
								+ offer.chipsToReceive);

				ectedScores = new BaseES();
				ectedScores.esFulfill = calcNextRoundectedScore(true,
						offer, new Proposal(offer.chipsToReceive, cs), myChips,
						opChips, relData, roundOfEs);

				if (palLog) {
				FileLogger.getInstance(logName).writelnNoFlush(
						"esFulfill: " + ectedScores.esFulfill);
				FileLogger.getInstance(logName).writelnNoFlush(
						"Calculating ES for sending: " + cs
								+ "and receiving nothing");
				}
				ectedScores.esNoFulfill = calcNextRoundectedScore(true,
						offer, new Proposal(new ChipSet(), cs), myChips,
						opChips, relData, roundOfEs);
				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"esNoFulfill: " + ectedScores.esNoFulfill);

				// Saving the result in the checked states (creating a copy so
				// the key will not be changed from another place)
				checkedStates.put(new State(curState), ectedScores);
			}

			// Calculate ES according to the Transfer (Reliability) model
			curectedScore = probabilitiesFulfill[1]
					* ectedScores.esFulfill + probabilitiesFulfill[0]
					* ectedScores.esNoFulfill;

			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
					"ES for sending: " + cs + " is " + curectedScore);

			if (roundOfEs == ES_CALC_DEPTH) {
				if (palLog)
				FileLogger
						.getInstance(logName)
						.writelnNoFlush(
								"============================================================");
			} else {
				if (palLog)
				FileLogger
						.getInstance(logName)
						.writelnNoFlush(
								"------------------------------------------------------------");
			}

			// check if there is a new max ected score
			if (curectedScore > bestectedScore.es) {
				bestectedScore.es = curectedScore;
				bestectedScore.csToSend = cs;
			}
		}

		return bestectedScore;
	}

	/*
	 * @see ctagents.alternateOffersAgent.ProposerResponderPlayer#doExchange()
	 */
	protected void doExchange() {

		// Flushing the data written so far to the log files
		csvLog.flush();
		if (palLog)
		FileLogger.getInstance(logName).flush();

		if (!offerAccepted) {
			if (palLog)
			FileLogger
					.getInstance(logName)
					.writeln(
//	Galit						"In exchange phase, offer not accepted --> sending empty chipSet");
					"In exchange phase, offer not accepted --> not sending anything");
								sending = new ChipSet();

		} else {

			// Checking if the agent accepted the offer and saved the best
			// transfer at the communication phase. If so, there is no need to
			// find the best ES again
			if (bestChipsToSend != null) {
				sending = bestChipsToSend;

				// Initializing the member for the next round
				bestChipsToSend = null;
			} else {
				ectedScoreData result = findBestectedScore(new Proposal(
						opChipSetCommitted, sending));
				sending = result.csToSend;

				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"ERROR!!!!!!!!!!!!! not supposed to get here!!!");
			}
//Galit		}

			if (palLog)
		FileLogger.getInstance(logName).writelnNoFlush(
				"chipsToTransfer is  " + sending);

		// In case something is sent, adding all the missing colors as zeros
		// so the agent message and human message will be the same
		if (sending.getNumChips() != 0) {
			for (String c : client.getGameStatus().getGamePalette().getColors()) {
				sending.add(c, 0);
			}
		}

		// Send the chips that cause the max ected score
		client.communication.sendTransferRequest(getOpponent().getPerGameId(),
				sending);
		} // Galit
		
		// Changing the log file to a new one before the next round
		try {
			FileLogger.getInstance(logName).close();
		} catch (IOException e) {
			System.out.println("Problem closing log file: " + logName);
			e.printStackTrace();
		}
		logName = logNamePrefix + numOfIterations;
	}

	/*
	 * This method generate all possible proposals and chooses the one that is
	 * most likely to give the greatest benefit. Actually, we don't create all
	 * possible proposals, only the ones that include at least one needed chip
	 * for each side (if he is not TI) and in addition all combination of extra
	 * chips for a specific side (the colors dosn't matter if they are not used
	 * for the path to the goal and only the difference of extra chips for each
	 * side is relevant so only one side should get extra chips)
	 * 
	 * @see
	 * ctagents.alternateOffersAgent.ProposerResponderPlayer#generateBestProposal
	 * ()
	 */
	private ectedScoreData generateBestProposal(ChipSet myChips,
			ChipSet opChips, ReliabilityData reliabData, int roundOfEs) {

		// Galit:majorProposalNo = 0;
		if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush
				("generateBestProposal:"+"myWPR="+reliabData.myWPR+",opWPR="+reliabData.opWPR);

		// Getting all the feasible negotiation paths between me and my opponent
		NegotiationPathsPairs feasibleNegotiationPaths = new NegotiationPathsPairs(
				client.getGameStatus(), getOpponent().getPerGameId(), myChips,
				opChips, getGoalLocation());

		// Creating all the possible proposals based on the path pairs
		ArrayList<Proposal> allProps = new ArrayList<Proposal>();

		for (int pairNo = 0; pairNo < feasibleNegotiationPaths.getPairsNo(); ++pairNo) {

			Path myPath = feasibleNegotiationPaths.getMyPath(pairNo);
			Path opPath = feasibleNegotiationPaths.getOpPath(pairNo);

			// Getting each players needed chips to get to the goal and their
			// extra chips
			ChipSet myRequierdChips = myPath.getRequiredChips(client
					.getGameStatus().getBoard());
			ChipSet myMissingChips = myChips.getMissingChips(myRequierdChips);
			ChipSet myExtraChips = myChips.getExtraChips(myRequierdChips);

			ChipSet opRequierdChips = opPath.getRequiredChips(client
					.getGameStatus().getBoard());
			ChipSet opMissingChips = opChips.getMissingChips(opRequierdChips);
			ChipSet opExtraChips = opChips.getExtraChips(opRequierdChips);

			// Removing all the chips needed to the opponent from the players
			// extra chips so it will be only the chips no one needs
			for (String c : opMissingChips.getColors()) {

				if (opMissingChips.getNumChips(c) > 0) {
					myExtraChips.setNumChips(c, 0);
				}
			}
			for (String c : myMissingChips.getColors()) {

				if (myMissingChips.getNumChips(c) > 0) {
					opExtraChips.setNumChips(c, 0);
				}
			}

			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
					" My missing chips: " + myMissingChips
							+ " my extra chips: " + myExtraChips
							+ " op missing chips: " + opMissingChips
							+ " op extra chips: " + opExtraChips);

			//Galit: Both players no missing chips needed (both TI). Therefore, there is no need to create proposals... 
			if (!myMissingChips.isEmpty() || !opMissingChips.isEmpty()) 
			{
			// Creating all the proposals with extra chips promised only for my
			// player (as if I don't have extra chips to give)
			allProps.addAll(getProposals(myMissingChips, opMissingChips,
					new ChipSet(), opExtraChips));

			// Creating all the proposals with extra chips promised only for the
			// opponent (as if the opponent don't have extra chips to give)
			allProps.addAll(getProposals(myMissingChips, opMissingChips,
					myExtraChips, new ChipSet()));
			}
			else
			{
				ectedScoreData chosenES = new ectedScoreData();
				//chosenES.csToSend = new ChipSet();
				chosenES.es = FixedPathScoreUtil.getInstance(
						client.getGameStatus()).getPlayerScore(myChips, getMyID());
				//chosenES.offer = all.get(0);
				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
				" Both Players TI. No need to generate proposals. ES =" +chosenES.es);
				return chosenES;
			}
		}

		if (palLog){
		FileLogger.getInstance(logName).writelnNoFlush(
				"Number of proposals generated: " + allProps.size());
		FileLogger.getInstance(logName).writelnNoFlush(
				"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		}
		// Choosing the best proposal among all
		return chooseBestProposal(allProps, myChips, opChips, reliabData,
				roundOfEs);
	}

	/*
	 * This version is using the current chips state of the players. The second
	 * one can be used for simulating a proposal in an advanced stage of the
	 * game, with different chipsSet.
	 * 
	 * @see
	 * ctagents.alternateOffersAgent.ProposerResponderPlayer#generateBestProposal
	 * ()
	 */
	protected Proposal generateBestProposal() {
		majorProposalNo = 0;
		
		// Flushing the data written so far to the log files
		csvLog.flush();
		FileLogger.getInstance(logName).flush();

		// Getting the reliability data
		ReliabilityData curRelData = new ReliabilityData();
		
				if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush
				("generateBestProposal void:"+"myWPR="+curRelData.myWPR+",opWPR="+curRelData.opWPR);


		Proposal bestProp = generateBestProposal(client.getChips(),
				getOpponent().getChips(), curRelData, ES_CALC_DEPTH).offer;

		// Flushing the data written while choosing an offer to the logs
		csvLog.flush();
		FileLogger.getInstance(logName).flush();

		return bestProp;
	}

	private int majorProposalNo;

	/*
	 * Choosing the best proposal from the list taking into consideration the
	 * given chips state
	 */
	private ectedScoreData chooseBestProposal(ArrayList<Proposal> all,
			ChipSet myChips, ChipSet opChips, ReliabilityData reliabData,
			int roundOfEs) {
		
				if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush
				("chooseBestProposal "+"myWPR="+reliabData.myWPR+",opWPR="+reliabData.opWPR);

			
		List<ectedScoreData> ectedScores = new ArrayList<ectedScoreData>();

		// Getting the players current score if the game was ending now.
		double myCurrentScore = FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getPlayerScore(myChips, getMyID());
		double opCurrentScore = FixedPathScoreUtil.getInstance(
				client.getGameStatus()).getPlayerScore(opChips,
				getOpponent().getPerGameId());

		// A list of all the codes representing states that their ES was already
		// calculated and there is no need to calculate them once more
		Hashtable<State, BaseES> checkedStates = new Hashtable<State, BaseES>();

		// In case of rejecting the offer the proposal's details arn't
		// important for the ected score
		if (palLog)
		FileLogger
				.getInstance(logName)
				.writelnNoFlush(
						"Calculating the ES in case the chosen proposal will be rejected");

		double esRejected = calcNextRoundectedScore(false, null, null,
				myChips, opChips, reliabData, roundOfEs);

		if (palLog)
		FileLogger.getInstance(logName).writelnNoFlush(
				"***************** ES if offer rejected is: " + df.format(esRejected));

		int minorProposalNo = 1;
		int skippedOffersNo = 0;
		int skippedOffersAgentTINo = 0;
		int skippedOffersHumanTINo = 0;
		int skippedOffersBothTINo = 0; 
		

		for (Proposal p : all) {

			ectedScoreData curScoreData = new ectedScoreData();

			if (roundOfEs == ES_CALC_DEPTH) {
				++majorProposalNo;
				if (palLog)
				FileLogger.getInstance(logName)
						.writelnNoFlush(
								"Starting to check proposal number: "
										+ majorProposalNo);
			} else if (roundOfEs == ES_CALC_DEPTH - 1) {
				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"Checking proposal number: " + majorProposalNo + "("
								+ minorProposalNo + ")");
				++minorProposalNo;
			}

			// Calculating both players score if this proposal was fully
			// fulfilled by both
			ChipSet myResultingChipSet = ChipSet.addChipSets(myChips,
					p.chipsToReceive);
			myResultingChipSet.subChipSet(p.chipsToSend);
			ChipSet opResultingChipSet = ChipSet.addChipSets(opChips,
					p.chipsToSend);
			opResultingChipSet.subChipSet(p.chipsToReceive);
			double myResultingScore = FixedPathScoreUtil.getInstance(
					client.getGameStatus()).getPlayerScore(myResultingChipSet,
					getMyID());
			double opResultingScore = FixedPathScoreUtil.getInstance(
					client.getGameStatus()).getPlayerScore(opResultingChipSet,
					getOpponent().getPerGameId());

			// Calculating the roles according to these scores
			String myResultingRole = DEPENDENT;
			String opResultingRole = DEPENDENT;

			myResultingRole = FixedPathScoreUtil
					.getInstance(client.getGameStatus()).getPlayerRole(myChips,
							getMyID());
			
			opResultingRole = FixedPathScoreUtil
			.getInstance(client.getGameStatus()).getPlayerRole(opChips,
					getOpponent().getPerGameId());

			if ((myResultingRole == INDEPENDENT) && (opResultingRole == INDEPENDENT)) // both players' role are TI, the game is finished..
			{
				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"Both players are TI:"
								+ myCurrentScore + "," + myResultingScore + ","
								+ opCurrentScore + "," + opResultingScore
								+ "] skipping offer: send"
								+ p.chipsToSend.removeZeros() + " receive "
								+ p.chipsToReceive.removeZeros());
				++skippedOffersBothTINo;
				// Getting the ected score of this offer considering the
				// probability of the other player to accept the offer
				curScoreData.es = myCurrentScore;
				curScoreData.csToSend = p.chipsToSend;
				curScoreData.offer = p;

				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"1.Current ES is: " + curScoreData.es + " when sending "
								+ curScoreData.csToSend);

				// Adding this result to the list of results
				ectedScores.add(curScoreData);
				continue;
		}

			// Making sure that this quartet of scores exists in the training
			// data set
			if ((scoreQuartets != null)
					&& (!quartetExists(myCurrentScore, myResultingScore,
							opCurrentScore, opResultingScore))) {
				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"The following score quartets doesn't exist: ["
								+ myCurrentScore + "," + myResultingScore + ","
								+ opCurrentScore + "," + opResultingScore
								+ "] skipping offer: send"
								+ p.chipsToSend.removeZeros() + " receive "
								+ p.chipsToReceive.removeZeros());
				++skippedOffersNo;
				continue;
			}
			if ((myResultingScore == myCurrentScore) && // Eliminate nothing for nothing offers
					(opResultingScore == opCurrentScore)){
				if (palLog)
					FileLogger.getInstance(logName).writelnNoFlush(
							"nothing for nothing offer: ["
									+ myCurrentScore + "," + myResultingScore + ","
									+ opCurrentScore + "," + opResultingScore
									+ "] skipping offer: send"
									+ p.chipsToSend.removeZeros() + " receive "
									+ p.chipsToReceive.removeZeros());
					++skippedOffersNo;
					continue;
					
				}
			

			if (myResultingRole == INDEPENDENT)
			{
				if (myResultingScore < myCurrentScore) {
					if (palLog)
					FileLogger.getInstance(logName).writelnNoFlush(
							"myResultingScore < myCurrentScore: ["
									+ myCurrentScore + "," + myResultingScore + ","
									+ opCurrentScore + "," + opResultingScore
									+ "] skipping offer: send"
									+ p.chipsToSend.removeZeros() + " receive "
									+ p.chipsToReceive.removeZeros());
					++skippedOffersAgentTINo;
					continue;
				}
			}
			if (opResultingRole == INDEPENDENT)
			{
				if (opResultingScore < opCurrentScore) {
					if (palLog)
					FileLogger.getInstance(logName).writelnNoFlush(
							"opResultingScore < opCurrentScore: ["
									+ myCurrentScore + "," + myResultingScore + ","
									+ opCurrentScore + "," + opResultingScore
									+ "] skipping offer: send"
									+ p.chipsToSend.removeZeros() + " receive "
									+ p.chipsToReceive.removeZeros());
					++skippedOffersHumanTINo;
					continue;					
				}
			}
			
			// Checking the level of generosity
			double generosityLevel = HIGH_GENEROUSITY;

			if (p.chipsToSend.getNumChips() < p.chipsToReceive.getNumChips()) {
				generosityLevel = LOW_GENEROUSITY;
			} else if (p.chipsToSend.getNumChips() == p.chipsToReceive
					.getNumChips()) {
				generosityLevel = MEDIUM_GENEROUSITY;
			}

			// Getting the number of needed and other chips
			double playerNeededColors = 0;
			for (String color : FixedPathScoreUtil.getInstance(
					client.getGameStatus()).getNeededColors(getMyID())) {
				// TODO meanwhile ask only of yellow and gray chips. 
				//This is not a general solution. The optimal solution is to have 3 sets for each sides: 
				//	needed chips (from the proposal), missing chips (from the board) and others 
				if (color.equals("CTYellow") || color.equals("grey78") )
					playerNeededColors += p.chipsToReceive.getNumChips(color);
			}
			double playerOtherColors = p.chipsToReceive.getNumChips()
					- playerNeededColors;

			double oppNeededColors = 0;
			for (String color : FixedPathScoreUtil.getInstance(
					client.getGameStatus()).getNeededColors(
					getOpponent().getPerGameId())) {
				if (color.equals("CTYellow") || color.equals("grey78") )
					oppNeededColors += p.chipsToSend.getNumChips(color);
			}
			double oppOtherColors = p.chipsToSend.getNumChips() - oppNeededColors;

			// Calculate the probabilities for the opponent to accept the
			// offer
			double probabilityAccept = 0;
//			Object[] instanceValues = { myCurrentScore, opCurrentScore,
//					myResultingScore, opResultingScore, generosityLevel,
//					playerNeededColors, playerOtherColors, oppNeededColors,
//					oppOtherColors, "TRUE" };
			
			Object[] instanceValues = { myCurrentScore, myResultingScore,
					opCurrentScore, opResultingScore, generosityLevel,
					playerNeededColors, playerOtherColors, oppNeededColors,
					oppOtherColors, "TRUE" };


			try {
				probabilityAccept = WekaWrapper
						.getAcceptProbability(instanceValues);

				if (palLog) {
				FileLogger.getInstance(logName).writelnNoFlush(
						"When offering to send: " + p.chipsToSend
								+ " and receive: " + p.chipsToReceive);
				FileLogger.getInstance(logName).writelnNoFlush(
						"instanceValues: myCurrentScore" + myCurrentScore
								+ " myResultingScore: " + myResultingScore
								+ " opCurrentScore: " + opCurrentScore
								+ " opResultingScore: " + opResultingScore
								+ " generosityLevel: " + generosityLevel
								+ " myNeededColors: " + playerNeededColors
								+ " myOtherColors: " + playerOtherColors
								+ " oppNeededColors: " + oppNeededColors
								+ " oppOtherColors: " + oppOtherColors
								+ " probabilityAccept: "
								+ df.format(probabilityAccept));

				if (csvLog != null) {
					csvLog.println("ACCEPT," + p.chipsToSend.removeZeros()
							+ "," + p.chipsToReceive.removeZeros() + ",,,,"
							+ myCurrentScore + "," + myResultingScore + ","
							+ opCurrentScore + "," + opResultingScore + ","
							+ generosityLevel + ","
							+ df.format(probabilityAccept));
				}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// Calculate the ected result for the current offer
							if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush
				("chooseBestProposal:"+"myWPR="+reliabData.myWPR+",opWPR="+reliabData.opWPR);

			ectedScoreData esAccepted = findBestectedScore(p, myChips,
					opChips, reliabData, roundOfEs, checkedStates);


			// Getting the ected score of this offer considering the
			// probability of the other player to accept the offer
			curScoreData.es = probabilityAccept * esAccepted.es
					+ (1 - probabilityAccept) * esRejected;
			curScoreData.csToSend = esAccepted.csToSend;
			curScoreData.offer = p;

			if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
					"2. Current ES is: " + curScoreData.es + " when sending "
							+ curScoreData.csToSend);

			// Adding this result to the list of results
			ectedScores.add(curScoreData);
		}

		ectedScoreData bestOffer;
		ectedScoreData chosenES;

		// TODO: temporary solution! Find the best way to handle it!!
		if (ectedScores.size() == 0) {
				FileLogger.getInstance(logName).flush();
			System.out.println("ERROR!!! num proposals: " + all.size()
					+ " num skipped: " + skippedOffersNo+skippedOffersAgentTINo+skippedOffersHumanTINo+skippedOffersBothTINo);

			chosenES = new ectedScoreData();
			chosenES.csToSend = new ChipSet();
			chosenES.es = myCurrentScore; // 0;
			chosenES.offer = all.get(0);
			bestOffer = chosenES;
		} else {

			// Sorting the proposals according to their ected score
			Collections.sort(ectedScores);
			// Searching the worst proposal location that it's ected score
			// is epsilon points less then the best proposal
			int bestESindex = ectedScores.size() - 1;
			bestOffer = ectedScores.get(bestESindex);
			int leastBest = bestESindex;
			if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
			"Collection ES is: BestEsIndex = " + bestESindex + ", ES = " + bestOffer.es);
			
			for (int curIndex = bestESindex - 1; curIndex >= 0; --curIndex) {
				if (palLog)
				FileLogger.getInstance(logName).writelnNoFlush(
						"curIndex = " + curIndex + ", ES = " + ectedScores.get(curIndex).es);
				if (ectedScores.get(curIndex).es + CHOOSE_OFFER_EPSILON < bestOffer.es) { 
					leastBest = curIndex + 1;
					break;
				}
			}

			// Choosing randomly from the group of offers that are less than the
			// best ES at maximum epsilon points
			chosenES = ectedScores.get(rand.nextInt(bestESindex - leastBest
					+ 1)
					+ leastBest);
			
		}

		// Saving the chips that were chosen to be sent for use in the
		// exchange phase if the offer will be accepted
		bestChipsToSend = chosenES.csToSend;

		if (palLog)
		{		FileLogger.getInstance(logName).writelnNoFlush(
				"Best proposal is: promise:" + bestOffer.offer.chipsToSend
						+ " ask:" + bestOffer.offer.chipsToReceive
						+ " its ES is: " + bestOffer.es + " if I'll send: "
						+ bestOffer.csToSend);

		FileLogger.getInstance(logName).writelnNoFlush(
				"Chosen proposal is: promise:" + chosenES.offer.chipsToSend
						+ " ask:" + chosenES.offer.chipsToReceive
						+ " its ES is: " + chosenES.es + " if I'll send: "
						+ bestChipsToSend);
		}
		return chosenES;
	}

	/*
	 * Checks if a specific quartet of scores appears in the training data
	 */
	private boolean quartetExists(double myCurrent, double myResulting,
			double opCurrent, double opResulting) {

		int counter = 0;

		double myBenefit = myResulting - myCurrent;
		double opBenefit = opResulting - opCurrent;

		for (int index = 0; index < scoreQuartets.get(0).length; ++index) {
			if (myBenefit - opBenefit == (scoreQuartets.get(1)[index] - scoreQuartets
					.get(0)[index])
					- (scoreQuartets.get(3)[index] - scoreQuartets.get(2)[index])) {
				++counter;
				if (counter == 2) {
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * This method created all possible proposal within the limit of maximum
	 * chips allowed per transfer, according to each sides needed chips and the
	 * extra chips he can give. All offers include at least one needed chips for
	 * each side otherwise the offer dosn't make any sense..
	 */
	private ArrayList<Proposal> getProposals(ChipSet sideANeeded,
			ChipSet sideBNeeded, ChipSet sideAExtra, ChipSet sideBExtra) {

		ArrayList<Proposal> proposals = new ArrayList<Proposal>();
		int minNeededChipsRequested = 1;

		// Check if the player is TI, that is it's needed chips number is zero.
		// In this case we want to create proposals where all the requested
		// chips are from the opponents extra chips. In other cases we want to
		// ask for at least one needed chip
		if (sideANeeded.getNumChips() == 0) {
			minNeededChipsRequested = 0;
		}

		for (int neededChipsNo = minNeededChipsRequested; neededChipsNo <= sideANeeded
				.getNumChips(); ++neededChipsNo) {
			for (int extraChipsNo = 0; (extraChipsNo <= MAX_EXCHANGE_CHIPS
					- neededChipsNo)
					&& (extraChipsNo <= sideBExtra.getNumChips()); ++extraChipsNo) {

				// Create the chip set requested with the current amount of
				// needed chips and the current amount of extra chips
				ChipSet requestedCS = new ChipSet(
						sideANeeded.getSubset(neededChipsNo));
				requestedCS.addChipSet(sideBExtra.getSubset(extraChipsNo));

				int minNeededChipsPromised = 1;

				// Check if the opponent is TI. If so, we create promises of
				// only extra chips
				if (sideBNeeded.getNumChips() == 0) {
					minNeededChipsPromised = 0;
				}

				// Create the promised chip set with the current amount of
				// needed chips and the current amount of extra chips
				for (int opNeededChipsNo = minNeededChipsPromised; opNeededChipsNo <= sideBNeeded
						.getNumChips(); ++opNeededChipsNo) {
					for (int opExtraChipsNo = 0; (opExtraChipsNo <= MAX_EXCHANGE_CHIPS
							- opNeededChipsNo)
							&& (opExtraChipsNo <= sideAExtra.getNumChips()); ++opExtraChipsNo) {

						ChipSet promisedCS = new ChipSet(
								sideBNeeded.getSubset(opNeededChipsNo));
						promisedCS.addChipSet(sideAExtra
								.getSubset(opExtraChipsNo));

						// Adding it all up to a new proposal
						Proposal curProposal = new Proposal();
						curProposal.chipsToReceive = requestedCS;
						curProposal.chipsToSend = promisedCS;
						proposals.add(curProposal);
					}
				}
			}
		}

		return proposals;
	}

	/*
	 * Updates the reliability data of the agent and the opponent after an
	 * exchange was made
	 * 
	 * @see
	 * ctagents.alternateOffersAgent.ProposerResponderPlayer#updatePersonality()
	 */
	protected void updatePersonality() {

		learningEngine.calcReliabilityParams(getMyID(), oppCsBeforeExchange);
		learningEngine.calcReliabilityParams(getOpponent().getPerGameId(),
				myCsBeforeExchange);

		if (palLog){
		FileLogger.getInstance(logName).writelnNoFlush(
				"my PR: " + learningEngine.getPreviousReliability(getMyID()));
		FileLogger.getInstance(logName).writelnNoFlush(
				"my WPR: "
						+ learningEngine
								.getWeightedPreviousReliability(getMyID()));
		FileLogger.getInstance(logName).writelnNoFlush(
				"opp PR: "
						+ learningEngine.getPreviousReliability(getOpponent()
								.getPerGameId()));
		FileLogger.getInstance(logName).writelnNoFlush(
				"opp WPR: "
						+ learningEngine
								.getWeightedPreviousReliability(getOpponent()
										.getPerGameId()));
	}
		// At this point, because the exchange was made, we need to update the
		// TD-TI state again
		updateRoles();
	}

	/*
	 * Decides whether or not it is beneficial to accept the offer, according to
	 * the comparison between the ected result in both cases
	 * 
	 * @see
	 * ctagents.alternateOffersAgent.ProposerResponderPlayer#shouldAcceptOffer
	 * (ctagents.alternateOffersAgent.Proposal)
	 */
	protected Boolean shouldAcceptOffer(Proposal proposal) {

		if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
				"***************Calculating reject ES**************"+"\n");
		// In case of rejecting the offer the proposal's details arn't
		// important for the ected score
		double esReject = calcectedScore(false, proposal, null);

		if (palLog)
			FileLogger.getInstance(logName).writelnNoFlush(
				"***************Calculating accept ES**************"+"\n");
		ectedScoreData esAccept = findBestectedScore(proposal);

		if (palLog) {
			FileLogger.getInstance(logName).writelnNoFlush(
				"ES if accept: " + esAccept.es + " when sending "
						+ esAccept.csToSend+"\n");
		FileLogger.getInstance(logName).writelnNoFlush(
				"ES if reject: " + esReject+"\n");
		}
		boolean shouldAccept = false;

		if (esAccept.es >= esReject) {
			shouldAccept = true;
		} else {
			// Checking if the distance between the options is less then
			// epsilon. If
			// so, choosing randomly between the two
			if (Math.abs(esAccept.es - esReject) <= ACCEPTANCE_EPSILON) {
				double n = rand.nextDouble();

				if (n > 0.5) {
					shouldAccept = true;
				}

				if (palLog)
					FileLogger.getInstance(logName).writelnNoFlush(
						"Randomly decided to "
								+ (shouldAccept ? "accept" : "reject"));
			}
		}
		// } else if (esAccept.es > esReject) {

		// shouldAccept = true;
		// }

		if (shouldAccept) {
			// Saving the chips that were chosen to be sent for use in the
			// exchange phase
			bestChipsToSend = esAccept.csToSend;
		}

		csvLog.flush();
		FileLogger.getInstance(logName).flush();

		return shouldAccept;
	}

	public void gameEnded() {
		csvLog.close();
		super.gameEnded();
	}

	protected LearningHandler getLearningHandler() {
		return learningEngine;
	}
}