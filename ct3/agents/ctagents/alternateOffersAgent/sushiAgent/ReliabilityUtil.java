//////////////////////////////////////////////////////////////////////////
// File:		ReliabilityUtil.java 				    				//
// Purpose:		Implements reliability utilities.						//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;

//////////////////////////////////////////////////////////////////////////
// Class:		ReliabilityUtil											//
// Purpose:		Reliability utilities.									//
//////////////////////////////////////////////////////////////////////////
public class ReliabilityUtil
{
	//////////////////////////////////////////////////////////////////////////
	// Method:		getResultingScore										//
	// Purpose:		Gets the resulting score of the specified player.		//
	// Parameters:	* board - The board.									//
	//				* scoring - The scoring.								//
	//				* currentPlayerChips - The current player's chips.		//
	//				* otherPlayerChips - The other player's chips.			//
	//				* currentPlayerPosition - The current player's			//
	//										  position.						//
	//				* chipsToReceive - The received chips.					//
	//				* chipsToSend - The sent chips.							//
	//////////////////////////////////////////////////////////////////////////
	public static double getResultingScore(Board board,
										   Scoring scoring,
										   ChipSet currentPlayerChips,
										   ChipSet otherPlayerChips,
										   RowCol currentPlayerPosition,
										   ChipSet chipsToReceive,
										   ChipSet chipsToSend)
	{
		PlayerStatus playerStatus = new PlayerStatus();
		ChipSet playerChips = new ChipSet(currentPlayerChips);
		RowCol playerPosition = new RowCol(currentPlayerPosition);
		int pathPositionIndex = 0;
		
		// Add and substract the chips
		playerChips = ChipSet.subChipSets(ChipSet.addChipSets(playerChips, chipsToReceive), chipsToSend);
		
		// Get as close as possible towards the goal
		Path pathTowardsGoal = ShortestPaths.getShortestPaths(playerPosition, board.getGoalLocations().get(0), board, scoring, 1, currentPlayerChips, otherPlayerChips).get(0);
		
		// Look for the position on the goal
		for (pathPositionIndex = 0; pathPositionIndex < pathTowardsGoal.getNumPoints(); pathPositionIndex++)
		{
			if (pathTowardsGoal.getPoint(pathPositionIndex).equals(playerPosition))
			{
				break;
			}
		}
		
		// Try to walk as much as we can towards the goal
		boolean notReachingGoal = false;
		for (int currentPosition = pathPositionIndex + 1; currentPosition < pathTowardsGoal.getNumPoints(); currentPosition++)
		{
			String currentColor = board.getSquare(pathTowardsGoal.getPoint(currentPosition)).getColor();
			if (playerChips.getNumChips(currentColor) > 0)
			{
				playerChips.add(currentColor, -1);
			}
			else
			{
				playerPosition = pathTowardsGoal.getPoint(currentPosition - 1);
				notReachingGoal = true;
				break;
			}
		}
		if (!notReachingGoal)
		{
			playerPosition = board.getGoalLocations().get(0);
		}
		
		// Set the player status
		playerStatus.setPosition(playerPosition);
		playerStatus.setChips(playerChips);
		
		// Return the score
		return scoring.score(playerStatus, board.getGoalLocations().get(0));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getCurrentScore											//
	// Purpose:		Gets the current score of the specified player.			//
	// Parameters:	* board - The board.									//
	//				* scoring - The scoring.								//
	//				* currentPlayerChips - The current player's chips.		//
	//				* otherPlayerChips - The other player's chips.			//
	//				* currentPlayerPosition - The current player's			//
	//										  position.						//
	//////////////////////////////////////////////////////////////////////////
	public static double getCurrentScore(Board board,
										 Scoring scoring,
										 ChipSet currentPlayerChips,
										 ChipSet otherPlayerChips,
			   							 RowCol currentPlayerPosition)
	{
		// Return the resulting score without chips to send or receive
		return getResultingScore(board, scoring, currentPlayerChips, otherPlayerChips, currentPlayerPosition, new ChipSet(), new ChipSet());
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getRoundReliability										//
	// Purpose:		Return the round reliability of the given player.		//
	// Parameters:	* board - The board.									//
	//				* scoring - The scoring.								//
	//				* currentPlayerChips - The current player's chips.		//
	//				* otherPlayerChips - The other player's chips.			//
	//				* otherPlayerPosition - The other player's position.	//
	//				* isMe - Whether it's me or the opponent.				//
	//				* lastAcceptedOffer - The last accepted offer.			//
	//				* chipsSentToOther - The actual chips sent to the		//
	//									 other player.						//
	//////////////////////////////////////////////////////////////////////////
	public static double getRoundReliability(Board board,
											 Scoring scoring,
											 ChipSet currentPlayerChips,
											 ChipSet otherPlayerChips,
											 RowCol otherPlayerPosition,
											 boolean isMe,
											 Offer lastAcceptedOffer,
											 ChipSet chipsSentToOther)
	{
		// Get the chips that were ment to be sent to the other player
		ChipSet chipsMentToBeSentToOther = (isMe ? lastAcceptedOffer.m_ChipsSentByMe : lastAcceptedOffer.m_ChipsSentByOpponent);
		
		// Calculate scores
		double fullTransferScore = getResultingScore(board, scoring, otherPlayerChips, currentPlayerChips, otherPlayerPosition, chipsMentToBeSentToOther, new ChipSet());
		double actualScore = getResultingScore(board, scoring, otherPlayerChips, currentPlayerChips, otherPlayerPosition, chipsSentToOther, new ChipSet());
		double currentScore = getCurrentScore(board, scoring, otherPlayerChips, currentPlayerChips, otherPlayerPosition);

		// Calculates benifits
		double fullBenifit = fullTransferScore - currentScore;
		double actualBenifit = actualScore - currentScore;
		
		// Calculate the results
		if (0.0 == fullBenifit)
		{
			return 1.0;
		}
		return actualBenifit / fullBenifit;
	}
}