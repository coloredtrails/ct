package ctagents.alternateOffersAgent.sushiAgent;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
//import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.MultilayerPerceptron;
//import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ConverterUtils.DataSource;

public class MakeWekaModels {

	static String homeFolder = "";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		final int PLAYER_I_PR = 4;
		final int PLAYER_J_PR = 5;

		if (args.length < 5) {
			System.out
					.println("Please enter the arff filenames in the order of - TRANSFER, ACCEPTANCE, ARG "
							+ "and the culture (ISRAEL / LEBANON / USA)"
							+ "and the boardType (DD / TD / TI");
			System.exit(0);
		}

		// The folder where all the output files will be written to
		homeFolder = "agents/ctagents/alternateOffersAgent/palAgent/" + args[3] + "/" + args[3] + "_"+ args[4];
		System.out
		.println(homeFolder);

		try {
			// Getting the data sources and instances
			DataSource transferTrainingSource = new DataSource(homeFolder + "/"
					+ args[0]);
			Instances transferTrainingData = transferTrainingSource
					.getDataSet();

			DataSource acceptanceTrainingSource = new DataSource(homeFolder
					+ "/" + args[1]);
			Instances acceptanceTrainingData = acceptanceTrainingSource
					.getDataSet();

			DataSource argTrainingSource = new DataSource(homeFolder + "/"
					+ args[2]);
			Instances argTrainingData = argTrainingSource.getDataSet();

			// setting class attribute if the data format does not provide this
			// information. Must be done according to Weka Help
			if (transferTrainingData.classIndex() == -1) {
				transferTrainingData.setClassIndex(transferTrainingData
						.numAttributes() - 1);
			}
			if (acceptanceTrainingData.classIndex() == -1) {
				acceptanceTrainingData.setClassIndex(acceptanceTrainingData
						.numAttributes() - 1);
			}
			if (argTrainingData.classIndex() == -1) {
				argTrainingData
						.setClassIndex(argTrainingData.numAttributes() - 1);
			}
			
			// Create quartets data base
			createScoreQuartets(transferTrainingSource);
			
			// Building Naive Bayes classifiers
//			NaiveBayes nbTransfer = new NaiveBayes();
//			nbTransfer.buildClassifier(transferTrainingData);
//
//			NaiveBayes nbAcceptance = new NaiveBayes();
//			nbAcceptance.buildClassifier(acceptanceTrainingData);
//
//			NaiveBayes nbARG = new NaiveBayes();
//			
//			nbARG.buildClassifier(argTrainingData);
//
//			// Writing the model files to the folder
//			SerializationHelper.write(homeFolder + "/NaiveBayesTransfer.model",
//					nbTransfer);
//			SerializationHelper.write(homeFolder
//					+ "/NaiveBayesAcceptanceModel.model", nbAcceptance);
//			SerializationHelper.write(homeFolder
//					+ "/NaiveBayesAgentReachedGoal.model", nbARG);

			MultilayerPerceptron nnAcceptance = new MultilayerPerceptron();
			nnAcceptance.buildClassifier(acceptanceTrainingData);
			
			SerializationHelper.write(homeFolder
					+ "/NNAcceptanceModel.model", nnAcceptance);
			
			MultilayerPerceptron nnTransfer = new MultilayerPerceptron();
			nnTransfer.buildClassifier(transferTrainingData);
			
			SerializationHelper.write(homeFolder
					+ "/NNTransfer.model", nnTransfer);
			
			MultilayerPerceptron nnARG = new MultilayerPerceptron();
			nnARG.buildClassifier(argTrainingData);
			
			SerializationHelper.write(homeFolder
					+ "/NNAgentReachedGoal.model", nnARG);
			
			
			

//			// Cross validating the classifiers and saving the results
//			crossValidate(nbTransfer, transferTrainingData,
//					"NaiveBayesTransfer.buffer");
//			crossValidate(nbAcceptance, acceptanceTrainingData,
//					"NaiveBayesAcceptanceModel.buffer");
//			crossValidate(nbARG, argTrainingData,
//					"NaiveBayesAgentReachedGoal.buffer");
//
//			// Building decision tree classifiers and validating with 10cv.
//			J48 j48Transfer = new J48();
//			j48Transfer.buildClassifier(transferTrainingData);
//
//			J48 j48Acceptance = new J48();
//			j48Acceptance.buildClassifier(acceptanceTrainingData);
//
//			J48 j48ARG = new J48();
//			j48ARG.buildClassifier(argTrainingData);
//
//			// Cross validating the classifiers and saving the results
//			crossValidate(j48Transfer, transferTrainingData,
//					"J48Transfer.buffer");
//			crossValidate(j48Acceptance, acceptanceTrainingData,
//					"J48AcceptanceModel.buffer");
//			crossValidate(j48ARG, argTrainingData, "J48AgentReachedGoal.buffer");

			// Saving the PR averages to a file
			FileWriter prFile = new FileWriter(homeFolder + "/PRavg.txt");
			prFile.write(Double.toString(calcColumnAvg(argTrainingSource,
					PLAYER_I_PR))
					+ "\n"
					+ Double.toString(calcColumnAvg(argTrainingSource,
							PLAYER_J_PR)));
			prFile.close();
			
		} catch (Exception e) {
			System.out.println("Problem reading data files");
			e.printStackTrace();
		}
	}

	/*
	 * Creating a matrix holding all possible score quartets
	 */
	private static void createScoreQuartets(DataSource transferData) {

		final int PROP_CURRENT_SCORE_INDEX = 2;
		final int PROP_RESULT_SCORE_INDEX = 3;
		final int RESP_CURRENT_SCORE_INDEX = 4;
		final int RESP_RESULT_SCORE_INDEX = 5;

		ArrayList<double[]> scoreQuartets = new ArrayList<double[]>();

		try {
			scoreQuartets.add(transferData.getDataSet().attributeToDoubleArray(
					PROP_CURRENT_SCORE_INDEX));

			scoreQuartets.add(transferData.getDataSet().attributeToDoubleArray(
					PROP_RESULT_SCORE_INDEX));

			scoreQuartets.add(transferData.getDataSet().attributeToDoubleArray(
					RESP_CURRENT_SCORE_INDEX));

			scoreQuartets.add(transferData.getDataSet().attributeToDoubleArray(
					RESP_RESULT_SCORE_INDEX));

			// Writing this object to a file
			SerializationHelper.write(homeFolder + "/scoreQuartets.db",
					scoreQuartets);
		} catch (Exception e) {
			System.out.println("Problem creating quartets db");
			e.printStackTrace();
		}
	}

	private static void crossValidate(Classifier toValidate, Instances data,
			String outputFile) {
		Evaluation validator;
		try {
			FileWriter stringBuffer = new FileWriter(homeFolder + "/"
					+ outputFile);
			validator = new Evaluation(data);
			validator.crossValidateModel(toValidate, data, 10, new Random());
			stringBuffer.write(toValidate.toString()
					+ validator.toSummaryString() + "\n"
					+ validator.toClassDetailsString() + "\n"
					+ validator.toMatrixString());
			stringBuffer.close();
		} catch (Exception e) {
			System.out.println("Problem cross validating the classifier "
					+ toValidate.debugTipText());
			e.printStackTrace();
		}
	}

	/*
	 * Calculating the average of a given column
	 */
	private static double calcColumnAvg(DataSource data, int column) {
		double average = 0;
		try {
			double[] columnData = data.getDataSet().attributeToDoubleArray(
					column);

			for (int i = 0; i < columnData.length; ++i) {
				average += columnData[i];
			}

			if (columnData.length != 0) {
				average /= columnData.length;
			}
		} catch (Exception e) {
			System.out.println("Problem calculating average");
			e.printStackTrace();
		}

		return average;
	}
}
