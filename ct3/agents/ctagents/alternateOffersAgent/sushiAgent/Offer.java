//////////////////////////////////////////////////////////////////////////
// File:		Offer.java 							    				//
// Purpose:		Saves offers in a convenient form.						//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

//Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;

//////////////////////////////////////////////////////////////////////////
// Class:		Offer													//
// Purpose:		Saves offers in a convenient form.						//
// Checked:		31/01/2013												//
//////////////////////////////////////////////////////////////////////////
public class Offer
{
	// The offer's chips
	public ChipSet m_ChipsSentByMe;
	public ChipSet m_ChipsSentByOpponent;
	
	// Synonyms
	public ChipSet m_ChipsSentByAgent;
	public ChipSet m_ChipsSentByHuman;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Constructor						    					//
	// Purpose:		Creates a new offer.									//
	// Parameters:	* chipsSentByMe - The chips sent by me.					//
	// 				* chipsSentByOpponent - The chips sent by the opponent.	//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public Offer(ChipSet chipsSentByMe,
				 ChipSet chipsSentByOpponent)
	{
		// Copy members
		this.m_ChipsSentByMe = new ChipSet(chipsSentByMe);
		this.m_ChipsSentByOpponent = new ChipSet(chipsSentByOpponent);
		
		// Shallow copy
		this.m_ChipsSentByAgent = this.m_ChipsSentByMe;
		this.m_ChipsSentByHuman = this.m_ChipsSentByOpponent;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		Copy constructor				    					//
	// Purpose:		Creates a new offer from an existing one.				//
	// Parameters:	* otherOffer - The other offer.							//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public Offer(Offer otherOffer)
	{
		// Copy members
		this.m_ChipsSentByMe = new ChipSet(otherOffer.m_ChipsSentByMe);
		this.m_ChipsSentByOpponent = new ChipSet(otherOffer.m_ChipsSentByOpponent);
		
		// Shallow copy
		this.m_ChipsSentByAgent = this.m_ChipsSentByMe;
		this.m_ChipsSentByHuman = this.m_ChipsSentByOpponent;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		switchSides												//
	// Purpose:		Returns a new offer with switched sides.				//
	// Checked:		31/01/2013												//
	//////////////////////////////////////////////////////////////////////////
	public Offer switchSides()
	{
		// Just switch sides
		return new Offer(this.m_ChipsSentByOpponent, this.m_ChipsSentByMe);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		toString												//
	// Purpose:		Represents an offer as a string.						//
	// Checked:		02/02/2013												//
	//////////////////////////////////////////////////////////////////////////
	public String toString()
	{
		// Represent as a string
		return new String("(AgentSend: " + this.m_ChipsSentByAgent.toString() + ", OpponentSend: " + this.m_ChipsSentByHuman.toString() + ")");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getNumChips												//
	// Purpose:		Gets the number of chips.								//
	// Checked:		02/02/2013												//
	//////////////////////////////////////////////////////////////////////////
	public int getNumChips()
	{
		// Simply calculate both sides
		return (this.m_ChipsSentByAgent.getNumChips() + this.m_ChipsSentByHuman.getNumChips());
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		equals													//
	// Purpose:		Indicates whether two offers are equal.					//
	// Parameters:	* obj - The compared object.							//
	//////////////////////////////////////////////////////////////////////////
	@Override
	public boolean equals(Object obj)
	{		
		// Comparing other classes
		if (getClass() != obj.getClass())
		{
			return false;
		}
		
		// Comparing inner chips
		Offer other = (Offer)obj;
		return ((this.m_ChipsSentByAgent.equals(other.m_ChipsSentByAgent)) && (this.m_ChipsSentByHuman.equals(other.m_ChipsSentByHuman)));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		hashCode												//
	// Purpose:		Returns a hash code for an offer.						//
	//////////////////////////////////////////////////////////////////////////
	@Override
	public int hashCode()
	{
		// Return a unique hash code
    	return ((Double)(Math.pow(2, this.m_ChipsSentByAgent.hashCode()) * Math.pow(3, this.m_ChipsSentByHuman.hashCode()))).intValue();
	}
}