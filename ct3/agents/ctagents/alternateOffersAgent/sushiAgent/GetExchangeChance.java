//////////////////////////////////////////////////////////////////////////
// File:		GetExchangeChance.java					   				//
// Purpose:		Gets exchange chance of opponent.						//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

//Imports from Java common framework
import java.util.*;

//////////////////////////////////////////////////////////////////////////
// Class:		GetExchangeChance					  		  			//
// Purpose:		Gets exchange chance of opponent.						//
//////////////////////////////////////////////////////////////////////////
public final class GetExchangeChance
{
	//////////////////////////////////////////////////////////////////////////
	// Method:		main							    					//
	// Purpose:		Implements the main function, which gets the exchange	//
	//				chance of the opponent.									//
	// Parameters:	* args - The command line arguments. Their order:		//
	//						 1. Culture.									//
	//						 2. Board type.									//
	//						 3. My reliability.								//
	//						 4. Opponent's reliability.						//
	//						 5. My current score.							//
	//						 6. Opponent's current score.					//
	//						 7. My resulting score.							//
	//						 8. Opponent's resulting score.					//
	//						 9. My needed colors.							//
	//						 10. My other colors.							//
	//						 11. Opponent needed colors.					//
	//						 12. Opponent other colors.						//
	//////////////////////////////////////////////////////////////////////////
	public static void main(String[] args)
	{
		// Get culture and board type
		String culture = args[0];
		String boardType = args[1];
		
		// Get reliabilities
		double myReliability = Double.parseDouble(args[2]);
		double opponentReliability = Double.parseDouble(args[3]);
		
		// Get current and resulting scores
		double myCurrentScore = Double.parseDouble(args[4]);
		double myResultingScore = Double.parseDouble(args[5]);
		double opponentCurrentScore = Double.parseDouble(args[6]);
		double opponentResultingScore = Double.parseDouble(args[7]);
				
		// Get needed colors and other colors
		int myNeededColors = Integer.parseInt(args[8]);
		int myOtherColors = Integer.parseInt(args[9]);
		int opponentNeededColors = Integer.parseInt(args[10]);
		int opponentOtherColors = Integer.parseInt(args[11]);
		
		// Initialize the Weka model
		String cultureFolder = "agents/ctagents/alternateOffersAgent/sushiAgent/" + culture + "/" + culture + "_" + boardType;
		WekaWrapper.setCultureFolder(cultureFolder);
		WekaWrapper.setCulture(WekaWrapper.Culture.valueOf(culture));
		
		// Build the instance values
		double[] probabilitiesFulfill = { 0 };
		Object[] instanceValuesFulfill = { myReliability,
										   opponentReliability,
										   myCurrentScore,
										   myResultingScore,
										   opponentCurrentScore,
										   opponentResultingScore,
										   (double)myNeededColors,
										   (double)myOtherColors,
										   (double)opponentNeededColors,
										   (double)opponentOtherColors,
										   "1" };
		
		// Get the exchange probability from the Weka model
		try
		{
			probabilitiesFulfill = WekaWrapper.getTransferProbabilities(instanceValuesFulfill);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		
		// Print the outcome
		System.out.println("Exchange chance is: " + Double.toString(probabilitiesFulfill[1]));
	}
}
