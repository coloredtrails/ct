//////////////////////////////////////////////////////////////////////////
// File:		WekaBasedModel.java 	 		  						//
// Purpose:		Implements a Weka based rollout model.					//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

// Imports from Java common framework
import java.util.Random;
import java.util.List;
import java.lang.Math;
import java.util.ArrayList;

// Imports from CT framework
import edu.harvard.eecs.airg.coloredtrails.shared.Scoring;
import edu.harvard.eecs.airg.coloredtrails.alglib.ShortestPaths;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Board;
import edu.harvard.eecs.airg.coloredtrails.shared.types.ChipSet;
import edu.harvard.eecs.airg.coloredtrails.shared.types.GameStatus;
import edu.harvard.eecs.airg.coloredtrails.shared.types.Path;
import edu.harvard.eecs.airg.coloredtrails.shared.types.RowCol;
import edu.harvard.eecs.airg.coloredtrails.shared.types.PlayerStatus;

// Imports from Weka
import weka.core.SerializationHelper;

//////////////////////////////////////////////////////////////////////////
// Class:		WekaModel												//
// Purpose:		A Weka based rollout model.								//
//////////////////////////////////////////////////////////////////////////
public class WekaBasedModel implements RolloutModel
{
	// A logger
	private XMLLogger m_Logger;
	
	// Saves reliabilities
	private double m_AgentReliabilitySum;
	private double m_HumanReliabilitySum;
	
	// Saves the last accepted offer and the last offer
	private Offer m_LastAcceptedOffer;
	private Offer m_LastOffer;
	
	// The number of transfers so far
	private int m_Transfers;
	
	// The culture
	private String m_Culture;
	
	// The random generator
	private static Random s_RandomGenerator = null;
	
	// The random seed
	private static final long s_RandomSeed = 1337;
	
	// The chips needed by the human and the agent
	private static ChipSet s_ChipsNeededByAgent = null;
	private static ChipSet s_ChipsNeededByHuman = null;
	
	// All existing score quartets (agentCurrentScore, agentResultingScore, humanCurrentScore, humanResultingScore) in the training data
	private static ArrayList<double[]> s_ScoreQuartets = null;
	
	// Saves whether the Weka models are initialized
	private static boolean s_IsWekaInitialized = false;
	
	// Saves the reliability history weight
	private static final double s_ReliabilityHistoryWeight = 0.3;
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		Constructor												//
	// Purpose:		Constructs a new Weka based model instance.				//
	// Parameters:	* initialGameState - The initial game state.			//
	//				* myInitialReliability - My initial reliability.		//
	//				* opponentInitialReliability - The opponent's initial	//
	//											   reliability.				//
	//				* culture - The culture of the human.					//
	//////////////////////////////////////////////////////////////////////////
	public WekaBasedModel(XMLLogger logger,
						  GameState initialGameState,
						  double myInitialReliability,
						  double opponentInitialReliability,
						  String culture)
	{
		// Save the logger
		this.m_Logger = logger;
		
		// Save the culture
		this.m_Culture = culture;
		
		// Save reliabilities
		this.m_AgentReliabilitySum = myInitialReliability;
		this.m_HumanReliabilitySum = opponentInitialReliability;
		
		// Simulate one transfer
		this.m_Transfers = 1;
		
		// Nullify offers
		this.m_LastAcceptedOffer = null;
		this.m_LastOffer = null;
		
		// Initialize the random generator
		if (s_RandomGenerator == null)
		{
			s_RandomGenerator = new Random(s_RandomSeed);
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getBoardType											//
	// Purpose:		Gets the board type (DD, TI or TD) from the human's		//
	//				point of view.											//
	// Parameters:	* gameState - The initial game state.					//
	//////////////////////////////////////////////////////////////////////////
	private String getBoardType(GameState gameState)
	{
		String result = "DD";
		
		// Analyze whether the players reach the goal
		boolean agentReachesGoal = gameState.canReachGoal(true);
		boolean humanReachesGoal = gameState.canReachGoal(false);

		// DD board
		if ((!agentReachesGoal) && (!humanReachesGoal))
		{
			return "DD";
		}
		
		// TD board
		if (agentReachesGoal)
		{
			return "TD";
		}
		
		// TI board
		return "TI";		
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		quartetExists											//
	// Purpose:		Specifies whether the given features are in the			//
	//				quartets.												//
	// Parameters:	* myCurrentScore - My current score.					//
	//				* myResultingScore - My resulting score.				//
	//				* opponentCurrentScore - The opponent's current score.	//
	//				* opponentResultingScore - The opponent's resulting		//
	//										   score.						//
	//////////////////////////////////////////////////////////////////////////
	private static boolean quartetExists(double myCurrentScore,
										 double myResultingScore,
										 double opponentCurrentScore,
										 double opponentResultingScore)
	{
		int counter = 0;

		// Calculate benefits
		double myBenefit = myResultingScore - myCurrentScore;
		double opponentBenefit = opponentResultingScore - opponentCurrentScore;

		// Search through the quartets
		for (int index = 0; index < s_ScoreQuartets.get(0).length; index++)
		{
			if (myBenefit - opponentBenefit == (s_ScoreQuartets.get(1)[index] - s_ScoreQuartets.get(0)[index]) - (s_ScoreQuartets.get(3)[index] - s_ScoreQuartets.get(2)[index]))
			{
				counter++;
				if (counter == 2)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSimulationStart				    					//
	//////////////////////////////////////////////////////////////////////////
	public void onSimulationStart(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("OnSimulationStart");
		
		// Update the last offer and last accepted offer
		this.m_LastOffer = gameState.getLastOffer();
		this.m_LastAcceptedOffer = gameState.getLastAcceptedOffer();
		
		// Save the needed chips
		if ((s_ChipsNeededByAgent == null) || (s_ChipsNeededByHuman == null))
		{
			s_ChipsNeededByAgent = gameState.getPlayerNeededUnownedChips(true);
			s_ChipsNeededByHuman = gameState.getPlayerNeededUnownedChips(false);
		}
		
		// Initialize the Weka model
		if (!s_IsWekaInitialized)
		{
			// Build the culture folder and update the Weka wrapper
			String cultureFolder = "agents/ctagents/alternateOffersAgent/sushiAgent/" + this.m_Culture + "/" + this.m_Culture + "_" + getBoardType(gameState);
			WekaWrapper.setCultureFolder(cultureFolder);
			WekaWrapper.setCulture(WekaWrapper.Culture.valueOf(this.m_Culture));
			
			// Get the score quartets
			if (null == s_ScoreQuartets)
			{
				try
				{
					s_ScoreQuartets = (ArrayList<double[]>)SerializationHelper.read(cultureFolder + "/scoreQuartets.db");
				}
				catch (Exception e)
				{
					s_ScoreQuartets = null;
					System.out.println("Problem reading the score quartets file");
					e.printStackTrace();
				}
			}
			
			// Mark as initialized
			s_IsWekaInitialized = true;
		}
		
		// Logging
		this.m_Logger.endTag("OnSimulationStart");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getWeightedReliability									//
	// Purpose:		Gets a new weighted reliability							//
	// Parameters:	* oldReliability - The old reliability.					//
	// 				* newRoundReliability - The current round reliability.	//
	//////////////////////////////////////////////////////////////////////////
	public static double getWeightedReliability(double oldReliability,
												double newRoundReliability)
	{
		// Calculate the new round reliability weight
		double newWeight = 1.0 - s_ReliabilityHistoryWeight;
		
		// Return the weighted sum
		return ((s_ReliabilityHistoryWeight * oldReliability) + (newWeight * newRoundReliability));
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSendingChips					    					//
	//////////////////////////////////////////////////////////////////////////
	public void onSendingChips(GameState gameState,
							   ChipSet chipsSentByAgent,
							   ChipSet chipsSentByHuman)
	{
		// Logging
		this.m_Logger.beginTag("OnSendingChips");
		this.m_Logger.addTag("LastAcceptedOffer", new String(this.m_LastAcceptedOffer == null ? "null" : this.m_LastAcceptedOffer.toString()));
		this.m_Logger.addTag("ChipsSentByAgent", chipsSentByAgent);
		this.m_Logger.addTag("ChipsSentByHuman", chipsSentByHuman);
		
		// Update reliabilities of both players if it's relevant
		if (this.m_LastAcceptedOffer != null)
		{
			// Whether to update or not
			boolean updateAgent = (this.m_LastAcceptedOffer.m_ChipsSentByAgent.getNumChips() > 0);
			boolean updateHuman = (this.m_LastAcceptedOffer.m_ChipsSentByHuman.getNumChips() > 0);
			
			// Get the round reliabilities
			double agentRoundReliability = ReliabilityUtil.getRoundReliability(gameState.getBoard(),
																			   gameState.getScoring(),
																			   gameState.getPlayerChips(true),
																			   gameState.getPlayerChips(false),
																			   gameState.getPlayerPosition(false),
																			   true,
																			   this.m_LastAcceptedOffer,
																			   chipsSentByAgent);
			double humanRoundReliability = ReliabilityUtil.getRoundReliability(gameState.getBoard(),
																			   gameState.getScoring(),
																			   gameState.getPlayerChips(false),
																			   gameState.getPlayerChips(true),
																			   gameState.getPlayerPosition(true),
																			   false,
																			   this.m_LastAcceptedOffer,
																			   chipsSentByHuman);
			this.m_Logger.addTag("AgentRoundReliability", agentRoundReliability);
			this.m_Logger.addTag("HumanRoundReliability", humanRoundReliability);
			
			// Update reliability sums and number of transfers
			double agentNewReliability = (updateAgent ? getWeightedReliability(this.m_AgentReliabilitySum / this.m_Transfers, agentRoundReliability) : this.m_AgentReliabilitySum / this.m_Transfers);
			double humanNewReliability = (updateHuman ? getWeightedReliability(this.m_HumanReliabilitySum / this.m_Transfers, humanRoundReliability) : this.m_HumanReliabilitySum / this.m_Transfers);
			this.m_Transfers++;
			this.m_AgentReliabilitySum = agentNewReliability * m_Transfers;
			this.m_HumanReliabilitySum = humanNewReliability * m_Transfers;
			this.m_Logger.addTag("AgentNewReliability", new Double(this.m_AgentReliabilitySum / this.m_Transfers));
			this.m_Logger.addTag("HumanNewReliability", new Double(this.m_HumanReliabilitySum / this.m_Transfers));
		}
		
		// Forget the last accepted offer and the last offer
		this.m_LastAcceptedOffer = null;
		this.m_LastOffer = null;
		this.m_Logger.endTag("OnSendingChips");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onSentChips						    					//
	//////////////////////////////////////////////////////////////////////////
	public void onSentChips(GameState gameState)
	{
		// TODO
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onProposal						    					//
	//////////////////////////////////////////////////////////////////////////
	public void onProposal(GameState gameState,
						   boolean isAgentTurn,
						   Offer offer)
	{
		// Logging
		this.m_Logger.beginTag("OnProposal");
		
		// Save the offer
		this.m_LastOffer = (offer == null ? null : new Offer(offer));
		this.m_Logger.addTag("Offer", offer);
		this.m_Logger.endTag("OnProposal");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onResponse												//
	//////////////////////////////////////////////////////////////////////////
	public void onResponse(GameState gameState,
						   boolean isAgentTurn,
						   boolean response)
	{
		// Logging
		this.m_Logger.beginTag("OnResponse");
		this.m_Logger.addTag("Response", new Boolean(response));
		
		// If we accepted - save the last accepted offer, otherwise - forget the offers
		if (response)
		{
			this.m_LastAcceptedOffer = new Offer(this.m_LastOffer);
			this.m_Logger.addTag("LastAcceptedOffer", this.m_LastAcceptedOffer);
		}
		else
		{
			this.m_LastAcceptedOffer = null;
			this.m_LastOffer = null;
		}
		this.m_Logger.endTag("OnResponse");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onGameOver												//
	//////////////////////////////////////////////////////////////////////////
	public void onGameOver(GameState gameState,
						   double agentScore,
						   double humanScore)
	{
		// Logging
		this.m_Logger.beginTag("OnGameOver");
		this.m_Logger.addTag("AgentScore", new Double(agentScore));
		this.m_Logger.addTag("HumanScore", new Double(humanScore));
		this.m_Logger.endTag("OnGameOver");
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		onMove													//
	//////////////////////////////////////////////////////////////////////////
	public void onMove(GameState gameState,
					   boolean isAgentMove)
	{
		// Logging
		this.m_Logger.beginTag("OnMove");
		this.m_Logger.addTag("IsAgentMove", new Boolean(isAgentMove));
		this.m_Logger.endTag("OnMove");
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getAgentOffer											//
	// Purpose:		Gets an agent's new offer.								//
	// Parameters:	* gameState - The current game state.					//
	//////////////////////////////////////////////////////////////////////////
	private Offer getAgentOffer(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("GetAgentOffer");

		// Get all possible offers
		List <Object> allPossibleOffers = gameState.getAllPossibleActions();
		
		// Randomly choose one
		int chosenIndex = s_RandomGenerator.nextInt(allPossibleOffers.size());
		Offer result = (Offer)(allPossibleOffers.get(chosenIndex));
		
		// Return the result
		this.m_Logger.endTag("GetAgentOffer");
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanOffer											//
	// Purpose:		Gets a human's new offer.								//
	// Parameters:	* gameState - The current game state.					//
	//////////////////////////////////////////////////////////////////////////
	private Offer getHumanOffer(GameState gameState)
	{
		// Logging
		this.m_Logger.beginTag("GetHumanOffer");

		// Get all possible offers
		List <Object> allPossibleOffers = gameState.getAllPossibleActions();
		
		// Randomly choose one
		int chosenIndex = s_RandomGenerator.nextInt(allPossibleOffers.size());
		Offer result = (Offer)(allPossibleOffers.get(chosenIndex));
		
		// Return the result
		this.m_Logger.endTag("GetHumanOffer");
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getOffer						    					//
	//////////////////////////////////////////////////////////////////////////
	public Offer getOffer(GameState gameState,
						  boolean isAgentTurn)
	{
		Offer offer = new Offer(new ChipSet(), new ChipSet());
		
		// Logging
		this.m_Logger.beginTag("GetOffer");
		this.m_Logger.addTag("IsAgentTurn", new Boolean(isAgentTurn));
		
		// Select the action according to the turn
		if (isAgentTurn)
		{
			offer = getAgentOffer(gameState);
		}
		else
		{
			offer = getHumanOffer(gameState);
		}
		
		// Return the offer
		this.m_Logger.addTag("Offer", offer);
		this.m_Logger.endTag("GetOffer");
		return offer;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getAgentResponse				    					//
	// Purpose:		Gets the agent's response.								//
	// Parameters:	* gameState - The game state.							//
	//				* offer - The offer to consider.						//
	//////////////////////////////////////////////////////////////////////////
	private boolean getAgentResponse(GameState gameState,
									 Offer offer)
	{
		this.m_Logger.beginTag("GetAgentResponse");
		
		// Calculate the current scores
		double myCurrentScore = gameState.getScore(true);
		double opponentCurrentScore = gameState.getScore(false);		
		
		// Calculate resulting scores
		double myResultingScore = ReliabilityUtil.getResultingScore(gameState.getBoard(), gameState.getScoring(), gameState.getPlayerChips(true), gameState.getPlayerChips(false), gameState.getPlayerPosition(true), offer.m_ChipsSentByHuman, offer.m_ChipsSentByAgent);
		double opponentResultingScore = ReliabilityUtil.getResultingScore(gameState.getBoard(), gameState.getScoring(), gameState.getPlayerChips(false), gameState.getPlayerChips(true), gameState.getPlayerPosition(false), offer.m_ChipsSentByAgent, offer.m_ChipsSentByHuman);
		
		// If the offer shouldn't be considered
		if (!shouldConsiderOffer(gameState, offer, myCurrentScore, myResultingScore, opponentCurrentScore, opponentResultingScore))
		{
			this.m_Logger.addTag("AutoRejectReason", "ShouldNotConsiderOffer");
			this.m_Logger.endTag("GetAgentResponse");
			return false;
		}
		
		// Calculate generosity level
		double generosityLevel = 1.0 - getGenerosityLevelForWekaModel(offer);
		
		// Get needed colors
		int myNeededColors = getNumberOfNeededChips(gameState, true, offer.m_ChipsSentByOpponent);
		int opponentNeededColors = getNumberOfNeededChips(gameState, false, offer.m_ChipsSentByMe);

		// Get other colors
		int myOtherColors = offer.m_ChipsSentByOpponent.getNumChips() - myNeededColors;
		int opponentOtherColors = offer.m_ChipsSentByMe.getNumChips() - opponentNeededColors;
		
		// Tree logging
		this.m_Logger.addTag("Offer", offer);
		this.m_Logger.addTag("MyCurrentScore", myCurrentScore);
		this.m_Logger.addTag("MyResultingScore", myResultingScore);
		this.m_Logger.addTag("OpponentCurrentScore", opponentCurrentScore);
		this.m_Logger.addTag("OpponentResultingScore", opponentResultingScore);
		this.m_Logger.addTag("GenerosityLevel", generosityLevel);
		this.m_Logger.addTag("MyNeededColors", myNeededColors);
		this.m_Logger.addTag("MyOtherColors", myOtherColors);
		this.m_Logger.addTag("OpponentNeededColors", opponentNeededColors);
		this.m_Logger.addTag("OpponentOtherColors", opponentOtherColors);
		
		// Build the instance values
		Object[] instanceValues = { opponentCurrentScore,
									opponentResultingScore,
									myCurrentScore,
									myResultingScore,
									generosityLevel,
									(double)opponentNeededColors,
									(double)opponentOtherColors,
									(double)myNeededColors,
									(double)myOtherColors,
									"TRUE" };
		
		// Get the accept probability from the Weka model
		double acceptChance;
		try
		{
			acceptChance = WekaWrapper.getAcceptProbability(instanceValues);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			acceptChance = 0.5;
		}
		
		// Act according to the accept chance
		this.m_Logger.addTag("AcceptProbability", acceptChance);
		this.m_Logger.endTag("GetAgentResponse");
		return (this.s_RandomGenerator.nextDouble() <= acceptChance);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getGenerosityLevelForWekaModels							//	
	// Purpose:		Get the generosity level for a given offer.				//
	//				This method is used by the Weka culture models.			//
	// Parameters:	* offer - The given offer.								//
	//////////////////////////////////////////////////////////////////////////
	private double getGenerosityLevelForWekaModel(Offer offer)
	{
		// Calculate how many chips I send against how many chips I receive
		int diffrenceOfSend = offer.m_ChipsSentByAgent.getNumChips() - offer.m_ChipsSentByHuman.getNumChips();
		if (diffrenceOfSend == 0)
		{
			return 0.5;
		}
		if (diffrenceOfSend > 0)
		{
			return 1.0;
		}
		return 0.0;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getNumberOfNeededChips									//
	// Purpose:		Gets the number of needed chips.						//
	// Parameters:	* gameState - The current game state.					//
	//				* isAgent - Whether it's the agent or the human.		//
	//				* chipsToReceive - The chips to receive.				//
	//////////////////////////////////////////////////////////////////////////
	private int getNumberOfNeededChips(GameState gameState,
									   boolean isAgent,
									   ChipSet chipsToReceive)
	{
		ChipSet meaningfulChips = (isAgent ? s_ChipsNeededByAgent : s_ChipsNeededByHuman);
		int result = 0;
		
		// If the player can reach the goal
		if (gameState.canReachGoal(isAgent))
		{
			return 0;
		}
		
		// Iterate colors and accumulate
		for (String color : meaningfulChips.getColors())
		{
			if (meaningfulChips.getNumChips(color) > 0)
			{
				result += Math.max(0, chipsToReceive.getNumChips(color));
			}
		}
		
		// Return the result
		return result;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		shouldConsiderOffer					    				//
	// Purpose:		Identifies whether a given offer should be considered.	//
	// Parameters:	* gameState - The game state.							//
	//				* offer - The offer to consider.						//
	//				* myCurrentScore - My current score.					//
	//				* myResultingScore - My resulting score.				//
	//				* opponentCurrentScore - Opponent's current score.		//
	//				* opponentResultingScore - Opponent's resulting score.	//
	//////////////////////////////////////////////////////////////////////////
	public static boolean shouldConsiderOffer(GameState gameState,
											  Offer offer,
											  double myCurrentScore,
											  double myResultingScore,
											  double opponentCurrentScore,
											  double opponentResultingScore)
	{
		double relevantCurrentScore;
		double relevantResultingScore;
		
		// Do not consider nothing for nothing
		if ((myCurrentScore == myResultingScore) && (opponentCurrentScore == opponentResultingScore))
		{
			return false;
		}
		
		// Identify the player's reaching goal capabilities
		boolean isMeReachingToGoal = gameState.willPlayerBecomeIndependent(true, offer.m_ChipsSentByMe, offer.m_ChipsSentByOpponent);
		boolean isOpponentReachingToGoal = gameState.willPlayerBecomeIndependent(false, offer.m_ChipsSentByOpponent, offer.m_ChipsSentByMe);
		
		// If I'm going to reach the goal - my score cannot decrease
		if ((isMeReachingToGoal) && (myResultingScore < myCurrentScore))
		{
			return false;
		}
		
		// If the opponent is going to reach the goal - the opponent's score cannot decrease
		if ((isOpponentReachingToGoal) && (opponentResultingScore < opponentCurrentScore))
		{
			return false;
		}
		
		// If both can reach the goal - consider the offer
		if ((isMeReachingToGoal) && (isOpponentReachingToGoal))
		{
			return true;
		}
		
		// Checking whether the offer is in the quartets
		if ((s_ScoreQuartets != null) && (!quartetExists(myCurrentScore, myResultingScore, opponentCurrentScore, opponentResultingScore)))
		{
			return false;
		}
		
		// Default
		return true;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanResponse					    				//
	// Purpose:		Gets the human's response.								//
	// Parameters:	* gameState - The game state.							//
	//				* offer - The offer to consider.						//
	//////////////////////////////////////////////////////////////////////////
	private boolean getHumanResponse(GameState gameState,
									 Offer offer)
	{
		this.m_Logger.beginTag("GetHumanResponse");
		
		// Calculate the current scores
		double myCurrentScore = gameState.getScore(true);
		double opponentCurrentScore = gameState.getScore(false);		
		
		// Calculate resulting scores
		double myResultingScore = ReliabilityUtil.getResultingScore(gameState.getBoard(), gameState.getScoring(), gameState.getPlayerChips(true), gameState.getPlayerChips(false), gameState.getPlayerPosition(true), offer.m_ChipsSentByHuman, offer.m_ChipsSentByAgent);
		double opponentResultingScore = ReliabilityUtil.getResultingScore(gameState.getBoard(), gameState.getScoring(), gameState.getPlayerChips(false), gameState.getPlayerChips(true), gameState.getPlayerPosition(false), offer.m_ChipsSentByAgent, offer.m_ChipsSentByHuman);
		
		// If the offer shouldn't be considered
		if (!shouldConsiderOffer(gameState, offer, myCurrentScore, myResultingScore, opponentCurrentScore, opponentResultingScore))
		{
			this.m_Logger.addTag("AutoRejectReason", "ShouldNotConsiderOffer");
			this.m_Logger.endTag("GetHumanResponse");
			return false;
		}
		
		// Calculate generosity level
		double generosityLevel = getGenerosityLevelForWekaModel(offer);
		
		// Get needed colors
		int myNeededColors = getNumberOfNeededChips(gameState, true, offer.m_ChipsSentByOpponent);
		int opponentNeededColors = getNumberOfNeededChips(gameState, false, offer.m_ChipsSentByMe);

		// Get other colors
		int myOtherColors = offer.m_ChipsSentByOpponent.getNumChips() - myNeededColors;
		int opponentOtherColors = offer.m_ChipsSentByMe.getNumChips() - opponentNeededColors;
		
		// Tree logging
		this.m_Logger.addTag("Offer", offer);
		this.m_Logger.addTag("MyCurrentScore", myCurrentScore);
		this.m_Logger.addTag("MyResultingScore", myResultingScore);
		this.m_Logger.addTag("OpponentCurrentScore", opponentCurrentScore);
		this.m_Logger.addTag("OpponentResultingScore", opponentResultingScore);
		this.m_Logger.addTag("GenerosityLevel", generosityLevel);
		this.m_Logger.addTag("MyNeededColors", myNeededColors);
		this.m_Logger.addTag("MyOtherColors", myOtherColors);
		this.m_Logger.addTag("OpponentNeededColors", opponentNeededColors);
		this.m_Logger.addTag("OpponentOtherColors", opponentOtherColors);
		
		// Build the instance values
		Object[] instanceValues = { myCurrentScore,
									myResultingScore,
									opponentCurrentScore,
									opponentResultingScore,
									generosityLevel,
									(double)myNeededColors,
									(double)myOtherColors,
									(double)opponentNeededColors,
									(double)opponentOtherColors,
									"TRUE" };
		
		// Get the accept probability from the Weka model
		double acceptChance;
		try
		{
			acceptChance = WekaWrapper.getAcceptProbability(instanceValues);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			acceptChance = 0.5;
		}
		
		// Act according to the accept chance
		this.m_Logger.addTag("AcceptProbability", acceptChance);
		this.m_Logger.endTag("GetHumanResponse");
		return (this.s_RandomGenerator.nextDouble() <= acceptChance);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getResponse						    					//
	//////////////////////////////////////////////////////////////////////////
	public boolean getResponse(GameState gameState,
							   boolean isAgentTurn,
							   Offer offer)
	{
		boolean response = false;
		
		// Logging
		this.m_Logger.beginTag("GetResponse");
		this.m_Logger.addTag("IsAgentTurn", new Boolean(isAgentTurn));
		this.m_Logger.addTag("Offer", offer);
		
		// Get the response according to the turn
		if (isAgentTurn)
		{
			response = getAgentResponse(gameState, offer);
		}
		else
		{
			response = getHumanResponse(gameState, offer);
		}

		// Logging
		this.m_Logger.addTag("Response", new Boolean(response));
		this.m_Logger.endTag("GetResponse");
		return response;
	}

	//////////////////////////////////////////////////////////////////////////
	// Method:		getAgentExchange					    				//
	// Purpose:		Gets the agent's exchange.								//
	// Parameters:	* gameState - The game state.							//
	//				* lastAcceptedOffer - The last accepted offer.			//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getAgentExchange(GameState gameState,
									 Offer lastAcceptedOffer)
	{
		ChipSet mySendChips = new ChipSet();
		
		// Logging
		this.m_Logger.beginTag("GetAgentExchange");
		
		// Get reliabilities
		double myReliability = this.m_AgentReliabilitySum / this.m_Transfers;
		double opponentReliability = this.m_HumanReliabilitySum / this.m_Transfers;
		
		// Calculate the current scores
		double myCurrentScore = gameState.getScore(true);
		double opponentCurrentScore = gameState.getScore(false);		
		
		// Calculate resulting scores
		double myResultingScore = ReliabilityUtil.getResultingScore(gameState.getBoard(), gameState.getScoring(), gameState.getPlayerChips(true), gameState.getPlayerChips(false), gameState.getPlayerPosition(true), lastAcceptedOffer.m_ChipsSentByHuman, lastAcceptedOffer.m_ChipsSentByAgent);
		double opponentResultingScore = ReliabilityUtil.getResultingScore(gameState.getBoard(), gameState.getScoring(), gameState.getPlayerChips(false), gameState.getPlayerChips(true), gameState.getPlayerPosition(false), lastAcceptedOffer.m_ChipsSentByAgent, lastAcceptedOffer.m_ChipsSentByHuman);

		// If the offer shouldn't be considered
		if (!shouldConsiderOffer(gameState, lastAcceptedOffer, myCurrentScore, myResultingScore, opponentCurrentScore, opponentResultingScore))
		{
			this.m_Logger.addTag("AutoCheatReason", "ShouldNotConsiderOffer");
			this.m_Logger.endTag("GetAgentExchange");
			return mySendChips;
		}
		
		// Get needed colors
		int myNeededColors = getNumberOfNeededChips(gameState, true, lastAcceptedOffer.m_ChipsSentByOpponent);
		int opponentNeededColors = getNumberOfNeededChips(gameState, false, lastAcceptedOffer.m_ChipsSentByMe);

		// Get other colors
		int myOtherColors = lastAcceptedOffer.m_ChipsSentByOpponent.getNumChips() - myNeededColors;
		int opponentOtherColors = lastAcceptedOffer.m_ChipsSentByMe.getNumChips() - opponentNeededColors;
		
		// Tree logging
		this.m_Logger.addTag("LastAcceptedOffer", lastAcceptedOffer);
		this.m_Logger.addTag("MyReliability", myReliability);
		this.m_Logger.addTag("OpponentReliability", opponentReliability);
		this.m_Logger.addTag("MyCurrentScore", myCurrentScore);
		this.m_Logger.addTag("MyResultingScore", myResultingScore);
		this.m_Logger.addTag("OpponentCurrentScore", opponentCurrentScore);
		this.m_Logger.addTag("OpponentResultingScore", opponentResultingScore);
		this.m_Logger.addTag("MyNeededColors", myNeededColors);
		this.m_Logger.addTag("MyOtherColors", myOtherColors);
		this.m_Logger.addTag("OpponentNeededColors", opponentNeededColors);
		this.m_Logger.addTag("OpponentOtherColors", opponentOtherColors);
		
		// Get from the Weka wrapper
		double[] probabilitiesFulfill = { 0 };
		Object[] instanceValuesFulfill = { opponentReliability,
										   myReliability,
										   opponentCurrentScore,
										   opponentResultingScore,										   
										   myCurrentScore,
										   myResultingScore,
										   (double)opponentNeededColors,
										   (double)opponentOtherColors,
										   (double)myNeededColors,
										   (double)myOtherColors,
										   "1" };
		try
		{
			probabilitiesFulfill = WekaWrapper.getTransferProbabilities(instanceValuesFulfill);
			this.m_Logger.addTag("ProbabilityForFulfillingOffer", probabilitiesFulfill[1]);
			if (s_RandomGenerator.nextDouble() <= probabilitiesFulfill[1])
			{
				mySendChips = new ChipSet(lastAcceptedOffer.m_ChipsSentByAgent);
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		
		// Return the result
		this.m_Logger.endTag("GetAgentExchange");
		return mySendChips;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getHumanExchange					    				//
	// Purpose:		Gets the human's exchange.								//
	// Parameters:	* gameState - The game state.							//
	//				* lastAcceptedOffer - The last accepted offer.			//
	//////////////////////////////////////////////////////////////////////////
	private ChipSet getHumanExchange(GameState gameState,
									 Offer lastAcceptedOffer)
	{
		ChipSet opponentSendChips = new ChipSet();
		
		// Logging
		this.m_Logger.beginTag("GetHumanExchange");
		
		// Get reliabilities
		double myReliability = this.m_AgentReliabilitySum / this.m_Transfers;
		double opponentReliability = this.m_HumanReliabilitySum / this.m_Transfers;
		
		// Calculate the current scores
		double myCurrentScore = gameState.getScore(true);
		double opponentCurrentScore = gameState.getScore(false);		
		
		// Calculate resulting scores
		double myResultingScore = ReliabilityUtil.getResultingScore(gameState.getBoard(), gameState.getScoring(), gameState.getPlayerChips(true), gameState.getPlayerChips(false), gameState.getPlayerPosition(true), lastAcceptedOffer.m_ChipsSentByHuman, lastAcceptedOffer.m_ChipsSentByAgent);
		double opponentResultingScore = ReliabilityUtil.getResultingScore(gameState.getBoard(), gameState.getScoring(), gameState.getPlayerChips(false), gameState.getPlayerChips(true), gameState.getPlayerPosition(false), lastAcceptedOffer.m_ChipsSentByAgent, lastAcceptedOffer.m_ChipsSentByHuman);

		// If the offer shouldn't be considered
		if (!shouldConsiderOffer(gameState, lastAcceptedOffer, myCurrentScore, myResultingScore, opponentCurrentScore, opponentResultingScore))
		{
			this.m_Logger.addTag("AutoCheatReason", "ShouldNotConsiderOffer");
			this.m_Logger.endTag("GetHumanExchange");
			return opponentSendChips;
		}
		
		// Get needed colors
		int myNeededColors = getNumberOfNeededChips(gameState, true, lastAcceptedOffer.m_ChipsSentByOpponent);
		int opponentNeededColors = getNumberOfNeededChips(gameState, false, lastAcceptedOffer.m_ChipsSentByMe);

		// Get other colors
		int myOtherColors = lastAcceptedOffer.m_ChipsSentByOpponent.getNumChips() - myNeededColors;
		int opponentOtherColors = lastAcceptedOffer.m_ChipsSentByMe.getNumChips() - opponentNeededColors;
		
		// Tree logging
		this.m_Logger.addTag("LastAcceptedOffer", lastAcceptedOffer);
		this.m_Logger.addTag("MyReliability", myReliability);
		this.m_Logger.addTag("OpponentReliability", opponentReliability);
		this.m_Logger.addTag("MyCurrentScore", myCurrentScore);
		this.m_Logger.addTag("MyResultingScore", myResultingScore);
		this.m_Logger.addTag("OpponentCurrentScore", opponentCurrentScore);
		this.m_Logger.addTag("OpponentResultingScore", opponentResultingScore);
		this.m_Logger.addTag("MyNeededColors", myNeededColors);
		this.m_Logger.addTag("MyOtherColors", myOtherColors);
		this.m_Logger.addTag("OpponentNeededColors", opponentNeededColors);
		this.m_Logger.addTag("OpponentOtherColors", opponentOtherColors);
		
		// Get from the Weka wrapper
		double[] probabilitiesFulfill = { 0 };
		Object[] instanceValuesFulfill = { myReliability,
										   opponentReliability,
										   myCurrentScore,
										   myResultingScore,
										   opponentCurrentScore,
										   opponentResultingScore,
										   (double)myNeededColors,
										   (double)myOtherColors,
										   (double)opponentNeededColors,
										   (double)opponentOtherColors,
										   "1" };
		try
		{
			probabilitiesFulfill = WekaWrapper.getTransferProbabilities(instanceValuesFulfill);
			this.m_Logger.addTag("ProbabilityForFulfillingOffer", probabilitiesFulfill[1]);
			if (s_RandomGenerator.nextDouble() <= probabilitiesFulfill[1])
			{
				opponentSendChips = new ChipSet(lastAcceptedOffer.m_ChipsSentByHuman);
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		
		// Return the result
		this.m_Logger.endTag("GetHumanExchange");
		return opponentSendChips;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Method:		getExchange												//
	//////////////////////////////////////////////////////////////////////////
	public ChipSet getExchange(GameState gameState,
							   boolean isAgentTurn,
							   Offer lastAcceptedOffer)
	{
		ChipSet exchange = new ChipSet();
		
		// Logger
		this.m_Logger.beginTag("GetExchange");
		this.m_Logger.addTag("IsAgentTurn", new Boolean(isAgentTurn));
		this.m_Logger.addTag("LastAcceptedOffer", new String(null == lastAcceptedOffer ? "null" : lastAcceptedOffer.toString()));
		
		// Verify that something was accepted
		if (null != lastAcceptedOffer)
		{
			// Get the exchange according to the turn
			if (isAgentTurn)
			{
				exchange = getAgentExchange(gameState, lastAcceptedOffer);
			}
			else
			{
				exchange = getHumanExchange(gameState, lastAcceptedOffer);
			}
		}
		
		// Return the exchange
		this.m_Logger.addTag("Exchange", exchange);
		this.m_Logger.endTag("GetExchange");
		return exchange;
	}
}
