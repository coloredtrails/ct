//////////////////////////////////////////////////////////////////////////
// File:		GamePhase.java 					   						//
// Purpose:		Defines an enum of a game phase of CT.					//
//////////////////////////////////////////////////////////////////////////
package ctagents.alternateOffersAgent.sushiAgent;

//////////////////////////////////////////////////////////////////////////
// Enum:		GamePhase												//
// Purpose:		Saves the game phase.									//
// Remarks:		Movements are done automatically after the second		//
//				exchange phase, so there's no need to save them.		//
//////////////////////////////////////////////////////////////////////////
public enum GamePhase
{
	PROP1,		// First proposition
	RESP1,		// First response
	PROP2,		// Second proposition
	RESP2,		// Second response
	EXCH1,		// First exchange (in transaction) - agent's
	EXCH2,		// Second exchange (in transaction) - opponent's
	OVER		// Game over
}