:: LaunchAll.bat
:: 
:: Starts the server, human GUI, and agent
:: 

@echo off
SET /a agentpin=110
echo %agentpin%

:: SERVER
START /B java -jar dist/ct3.jar -s
ping 127.0.0.1 -n 7 -w 1000 > nul


:: HUMAN
START /B java -jar dist/ct3.jar -c ctgui.original.GUI --pin 100 --client_hostip localhost
ping 127.0.0.1 -n 7 -w 1000 > nul


:: AGENT
START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.palAgent.PalAgentFrontEnd %agentpin% 0 0
ping 127.0.0.1 -n 7 -w 1000 > nul


:: CONTROLLER OPTIONS
:: Friends, Balanced
:100
   SET dep=DS
   SET rel=FRIEND
   goto :run

:run
START /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl %dep% %rel%
goto :eof



:: Friends, Human Ind
:200
   SET dep=ID
   SET rel=FRIEND
   goto :run

:: Strangers, Balanced
:300
   SET dep=DD
   SET rel=STRANGER
   goto :run

:: Strangers, Human Ind
:400
   SET dep=ID
   SET rel=STRANGER
      goto :run


:: Friends, Comp Ind
:500
   SET dep=DI
   SET rel=FRIEND
   goto :run

:: Strangers, Comp Ind
:600
   SET dep=DI
   SET rel=STRANGER
   goto :run

