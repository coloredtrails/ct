t 
Last updated 7/21/08 by Kobi Gal

For important release notes, see RELEASE_NOTES.txt

For licensing information, see LICENSES.txt

For information on building and running, see README_BUILD.txt

For information on building and running computer agents, see agents/README_AGENTS.txt

For further documentation, see doc/README_DOC.txt

To generate an API for most of the classes in CT in doc/javadoc, run the following command

$ ant javadoc


General Organization of CT workspace:

activemq-data: 	contains files created and used by our networking infrastructure (JMS)
agents:		contains sample code for writing CT agents
build:		where the system gets compiled
dist:		contains the final distribution of the system after compilation
doc:		contains documentation and javadoc
experiment-support:	contains any support code you need (see documentation)
gameconfigs:	contains your game configuration files, and samples
gui:		where you put your custom GUI code
lib/adminconfig:	sample administrator scripts to startup games
lib/conf:	contains files for third-party libraries
lib/images:	contains all GUI icons
lib/jars:	contains third-party libraries
src:		contains source files for core CT system and some third-party code
tools:		vestigial files, will be removed
webctrl:	contains controller functionality (API) for running an 			administrator client over a webserver.


Notes:

The package hierarchy is now: edu.harvard.eecs.airg.coloredtrails.* for all core CT packages

The agent API classes are under: edu.harvard.eecs.airg.coloredtrails.agent

Example agents are in the "agent" directory. A suggested starting point for agent development is: 
ctagents.* 

### BUT BE AWARE ###: If you were to upgrade the CT3 environment, then this could easily override your own changes. The same situation applies to the game configuration files under gameconfig. So, please have copies of your own code before upgrading CT to a newer version.
