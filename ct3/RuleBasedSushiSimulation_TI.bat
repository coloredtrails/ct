echo off
cls
echo starting FW...
start /B java -jar dist/ct3.jar -s
ping 127.0.0.1 -n 7 -w 1000 > nul

echo starting human player...
start /B java -jar dist/ct3.jar -c ctgui.original.GUI --pin 200 --client_hostip localhost
ping 127.0.0.1 -n 7 -w 1000 > nul

echo starting Sushi...
rem Sushi parameters
set MY_INITIAL_RELIABILITY=0.5
set OPPONENT_INITIAL_RELIABILITY=0.5
set LOGGING=true

start /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.sushiAgent.SushiRolloutSimulatorFrontEnd 337 %MY_INITIAL_RELIABILITY% %OPPONENT_INITIAL_RELIABILITY% %LOGGING%
ping 127.0.0.1 -n 7 -w 1000 > nul

echo starting game config...
rem Board parameters
rem BoardType Could be DD, ID or DI
set BoardType=ID
rem BoardRelation could be STRANGER or FRIEND
set BoardRelation=FRIEND
echo Board params are (%BoardType%, %BoardRelation%)
start /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl %BoardType% %BoardRelation%
