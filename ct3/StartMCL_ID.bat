echo off
cls
echo starting FW...
start /B java -jar dist/ct3.jar -s
ping 127.0.0.1 -n 7 -w 1000 > nul

echo starting human player...
start /B java -jar dist/ct3.jar -c ctgui.original.GUI --pin 200 --client_hostip localhost
ping 127.0.0.1 -n 7 -w 1000 > nul

echo starting MCL...
rem MCL parameters
set BRANCHING_FOR_RESPONSES=10000
set BRANCHING_FOR_PROPOSALS=30000
set BRANCHING_FOR_TRANSFERS=10000
set MY_INITIAL_RELIABILITY=0.3
set OPPONENT_INITIAL_RELIABILITY=0.3
set LAST_ROUND_RELIABILITY_WEIGHT=0.7
set MY_SCORE_WEIGHT=1.0

echo ===============================================
echo branching params are (%BRANCHING_FOR_RESPONSES%, %BRANCHING_FOR_PROPOSALS%, %BRANCHING_FOR_TRANSFERS%)
echo UTC params are (%EXPLORATION_FACTOR_FOR_RESPONSES%, %EXPLORATION_FACTOR_FOR_PROPOSALS%, %EXPLORATION_FACTOR_FOR_TRANSFERS%)
echo MCL fine tunable params are (%MY_INITIAL_RELIABILITY%, %OPPONENT_INITIAL_RELIABILITY%, %LAST_ROUND_RELIABILITY_WEIGHT%, %MY_SCORE_WEIGHT%)
echo ===============================================
start /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.mclAgent.MclAgentFrontEnd 337 %BRANCHING_FOR_RESPONSES% %BRANCHING_FOR_PROPOSALS% %BRANCHING_FOR_TRANSFERS% %MY_INITIAL_RELIABILITY% %OPPONENT_INITIAL_RELIABILITY% %LAST_ROUND_RELIABILITY_WEIGHT% %MY_SCORE_WEIGHT% %EXPLORATION_FACTOR_FOR_RESPONSES% %EXPLORATION_FACTOR_FOR_PROPOSALS% %EXPLORATION_FACTOR_FOR_TRANSFERS%
ping 127.0.0.1 -n 7 -w 1000 > nul

echo starting game config...
rem Board parameters
rem BoardType Could be DD, ID or DI
set BoardType=ID
rem BoardRelation could be STRANGER or FRIEND
set BoardRelation=STRANGER
echo Board params are (%BoardType%, %BoardRelation%)
start /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl %BoardType% %BoardRelation%
