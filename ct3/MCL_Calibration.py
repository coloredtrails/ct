import os
import subprocess
import time
import win32api

SLEEP_TIME = 4.0
IS_PURB = False
IS_PAL = True
ROUNDS = 1

def get_result_files():
    # Find all the result files
    all_files = [ i for i in os.listdir(r'.') if i.lower().endswith('.txt') and i.lower().startswith('alternativeoffersconfig_') ]
    return all_files

def delete_result_files():

    # Delete while there are still result files
    while len(get_result_files()) > 0:
        _ = os.system('del AlternativeOffersConfig_*.txt > NUL')
        time.sleep(1)

def add_result(my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight, board_type, purb_score, other_score):

    # Write down the results
    results_handle = open('./TournamentResults.jbo', 'a')
    results_handle.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight, board_type, purb_score, other_score))
    results_handle.close()

def tournament(my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight, board_type):

    # Kill all previous Java instances
    print 'Killing all previous Java instances...'
    _ = os.system(r'taskkill /im java.exe /f')
    time.sleep(SLEEP_TIME)

    # Delete all previous result files
    print 'Deleting previous result files...'
    delete_result_files()

    # Start the framework
    print 'Starting framework...'
    _ = os.system(r'start /B java -jar dist/ct3.jar -s > NUL')
    time.sleep(SLEEP_TIME)
    
    # Start MCL
    print 'Starting MCL (%s, %s, %s, %s)...' % (my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight)
    mcl_cmd_line = r'java -classpath dist/ct3.jar ctagents.alternateOffersAgent.mclAgent.MclAgentFrontEnd 337 10000 30000 10000 %s %s %s %s > NUL' % (my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight)
    mcl_handle = subprocess.Popen(mcl_cmd_line, shell=True)
    time.sleep(SLEEP_TIME)

    # Start PURB
    print 'Starting PURB...'
    if IS_PURB:
        _ = os.system(r'START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.ProposerResponderFrontEnd 999 0.5 0.5 > NUL')
    else:
        if IS_PAL:
            _ = os.system(r'START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.palAgent.PalAgentFrontEnd 510 1')
        else:
            _ = os.system(r'START /B java -classpath dist/ct3.jar ctagents.alternateOffersAgent.nastyAgent.NastyAgentFrontEnd 666')
    time.sleep(SLEEP_TIME)
    
    # Start game config
    print 'Starting game config (%s)...' % (board_type,)
    _ = os.system(r'start /B java -classpath "dist/ct3.jar;gameconfigs" AltOffersControl %s STRANGER > NUL' % (board_type,))

    # Wait for the game to finish
    print 'Waiting for game to finish...'
    _ = mcl_handle.wait()
    print 'Game finished!'

    # Kill all Java instances
    _ = os.system(r'taskkill /im java.exe /f')
    time.sleep(SLEEP_TIME)

    # Analyze the game result
    all_result_files = get_result_files()
    assert 1 == len(all_result_files), 'Too many result files!'
    result_file_path = all_result_files[0]
    result_file_handle = open(result_file_path, 'r')
    all_lines = result_file_handle.readlines()
    result_file_handle.close()
    saved_filename = 'SavedGame_%s_%s_%s_%s_%s_%s.txt' % (my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight, board_type, win32api.GetTickCount())
    saved_path = os.path.join(os.path.dirname(result_file_path), 'SavedLogs\\' + saved_filename)
    saved_data_handle = open(saved_path, 'w')
    saved_data_handle.writelines(all_lines)
    saved_data_handle.close()
    ending_line = [ line for line in all_lines if 'end the game' in line or 'Ending game, all players move as much as they can towards goal' in line or 'got to goal, advance the other and end game' in line ]
    assert 1 == len(ending_line), 'Too many ending lines!'
    information_lines = all_lines[all_lines.index(ending_line[0]):]
    player_scores = {}
    player_scores[int(information_lines[1].split('\t')[1].split(':')[1][1:].split(' ')[0])] = int(information_lines[1].split('\t')[1].split(':')[-1][1:].strip())
    player_scores[int(information_lines[2].split('\t')[1].split(':')[1][1:].split(' ')[0])] = int(information_lines[2].split('\t')[1].split(':')[-1][1:].strip())
    other_id = [ x for x in player_scores if x != 337 ][0]

    # Write down the scores
    add_result(my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight, board_type, player_scores[other_id], player_scores[337])


def try_weights(my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight, board_type):

    # Run five tournaments
    for _ in xrange(ROUNDS):
        tournament(my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight, board_type)
        time.sleep(SLEEP_TIME)


def calibrate():

    # Fixed values
    #opponent_initial_reliability = 0.6196358987363171
    #my_initial_reliability = 0.6207383327969102
    
    # Iterate all board types
    for board_type in [ 'DI' ]:

        # Iterate all fields
        for opponent_initial_reliability in [ 0.2, 0.3, 0.4, 0.5 ]:
            for my_initial_reliability in [ 0.2, 0.3, 0.4, 0.5 ]:
                for last_round_reliability_weight in [0.2, 0.3, 0.7]:
                    for my_score_weight in [0.8, 0.9, 1.0]:
                        okay = False
                        while not okay:
                            try:
                                try_weights(my_initial_reliability, opponent_initial_reliability, last_round_reliability_weight, my_score_weight, board_type)
                                okay = True
                            except:
                                print 'EXCEPTION!!!!!!!!!!!!!!!!!!!!!!!!!!'
                                open('C:\\users\\user\\exception.txt', 'wb').write('No!')

if __name__== '__main__':
    calibrate()