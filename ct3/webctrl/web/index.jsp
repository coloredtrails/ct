
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<html:html>
  <head><title>CT Controller</title></head>
  <body>
  <html:form action="/Connect" focus="address" method="post" enctype="multipart/form-data">
      <h1>Game Configuration Parameters</h1>
      Address: <html:text property="address"/><br>
      User IDs: <html:text property="userIDs"/><br>
      Configuration Class: <html:file property="configFile"/><br>
      <br>
      <html:submit>Connect to Server and start game</html:submit>

      </html:form>
  </body>
</html:html>