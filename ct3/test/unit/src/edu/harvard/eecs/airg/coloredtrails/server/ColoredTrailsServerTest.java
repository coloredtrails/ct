/*
 * JUnit tests for ColoredTrailsServer
 */

package edu.harvard.eecs.airg.coloredtrails.server;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Bart Kamphorst
 * @since 3 Jan 2011
 */
public class ColoredTrailsServerTest {

    ColoredTrailsServer s;

    public ColoredTrailsServerTest() {
        
    }

    @Before
    public void setUp() {
        s = ColoredTrailsServer.getInstance();
    }

    @After
    public void tearDown() {
        s = null;
    }

    @Test
    public void serverHostnameTest() {
        assertTrue(s.getServerHostname() != null);
    }

    @Test
    public void serverAcceptsConnectionsTest() {
        assertTrue(true);
    }

    @Test
    public void serverPortTest() {
        assertTrue(s.getServerPort() == ColoredTrailsServer.DEFAULT_PORT);
    }
}